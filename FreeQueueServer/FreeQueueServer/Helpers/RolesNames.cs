﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreeQueueServer.Helpers
{
    public class RolesNames
    {
        public const string SYSTEM = "1";
        public const string DEVELOPER = "2";
        public const string MANAGER = "3";
        public const string STORE = "4";
        public const string CUSTOMER = "5";
        public const string GUEST = "6";
    }
}