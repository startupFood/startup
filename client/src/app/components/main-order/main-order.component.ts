import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/services/store.service';
import { CartService } from 'src/app/services/cart.service';
import { Order } from 'src/app/classes/order';
import { OrderService } from 'src/app/services/order.service';


@Component({
  selector: 'app-main-order',
  templateUrl: './main-order.component.html',
  styleUrls: ['./main-order.component.css']
})
export class MainOrderComponent implements OnInit {

  constructor(private storeServ: StoreService, private cartServ: CartService, private orderServ: OrderService) {

  }

  public personal: boolean = true;
  public nextGroup: boolean = false;
  public nextOrder: boolean = false;
  public buy: boolean = false;
  public next: boolean = true;
  public pre: boolean = false;

  ngOnInit() { }

  nextComponent(componentNumber: number) {

    switch (componentNumber) {
      //go to order deatils.
      case 0:
        {
          //this.nextGroup = true;
          this.nextOrder = true;
          this.personal = false;
          this.buy = false;
          document.getElementById('orderStep').setAttribute('class', 'order-step order-step-active');
          document.getElementById('personalStep').setAttribute('class', 'order-step');
          document.getElementById('summaryStep').setAttribute('class', 'order-step');
          this.next = true;
          this.pre = true;
          //is still hidden by ngIf.
          document.getElementById('nextBTN').setAttribute('class', 'btn-pill next_btn');
          break;
        }
      case 1:
        {
          this.nextOrder = true;
          // this.nextGroup = false;
          document.getElementById('orderStep').setAttribute('class', 'order-step order-step-active');
          document.getElementById('personalStep').setAttribute('class', 'order-step');
          document.getElementById('summaryStep').setAttribute('class', 'order-step');
          this.next = true;
          this.pre = true;
          document.getElementById('nextBTN').setAttribute('class', 'btn-pill next_btn');
          break;
        }
      //summary.

      case 2:
        {
          this.buy = true;
          this.nextOrder = false;
          document.getElementById('summaryStep').setAttribute('class', 'order-step order-step-active');
          document.getElementById('orderStep').setAttribute('class', 'order-step');
          document.getElementById('personalStep').setAttribute('class', 'order-step');
          this.next = false;
          this.pre = true;
        }
        break;
      //personal details.
      case 3:
        {
          this.personal = true;
          this.nextOrder = false;
          document.getElementById('personalStep').setAttribute('class', 'order-step order-step-active');
          document.getElementById('summaryStep').setAttribute('class', 'order-step');
          document.getElementById('orderStep').setAttribute('class', 'order-step');
          this.next = true;
          this.pre = false;
          document.getElementById('nextBTN').setAttribute('class', 'btn-pill only_next');
        }
    }
  }

  expandDiv(event: boolean) {
    if (event) {
      document.getElementById("wizard").setAttribute('class', 'orderWizard bigWizard')
    }
    else {
      document.getElementById("wizard").setAttribute('class', 'orderWizard')
    }
  }

  getSum() {
    return this.cartServ.getSum();
  }

  // between the wizard steps.
  nextCo() {
    //person - the next is order details.
    if (this.personal && this.step1) {
      this.orderServ.personal.next(true);
      this.nextComponent(0);
    }
    //order - the next is summary.
    else if (this.nextOrder && this.step1 &&this.step2) {
      this.orderServ.order.next(true);
      this.nextComponent(2);
    }


  }

  preComponent() {
    //order - the pre step is personal.
    if (this.nextOrder) {
      this.orderServ.order.next(true);
      this.nextComponent(3);
    }
    //summary - the pre step is order details.
    else if (this.buy) {
      this.nextComponent(0);
    }
  }

  buyFnc() {
    this.orderServ.buy.next(true);
  }

  step1:boolean=false;
  step2:boolean=false;
  step3:boolean=false;

  validStep1(event)
  {
    this.step1=event;
  }
  validStep2(event)
  {
    this.step2=event;
  }
  validStep3()
  {
    this.step3=true;
  }

}
