import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginWorkerPopUpComponent } from './login-worker-pop-up.component';

describe('LoginWorkerPopUpComponent', () => {
  let component: LoginWorkerPopUpComponent;
  let fixture: ComponentFixture<LoginWorkerPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginWorkerPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginWorkerPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
