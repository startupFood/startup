import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServerService {
 
  constructor() {
    if(this.currentUserToken == undefined)
    this.currentUserToken="";
  }

  URL="http://localhost:4588/api/";

  public currentUserToken:string;
//subscribe when the component change to display the matches icons on the header.
  // 0 -login
  //1 - storesList , products
  //2 - store , menu , product , cart
  public Component:Subject<number> = new Subject<number>();

  setCurrentUserToken(value)
  {
    this.currentUserToken=value;
  }

/*   get CurrentUserToken()
  {
    return this.currentUserToken;
  }

  set CurrentUserToken(value)
  {
    
    this.currentUserToken=value;
  } */

  getHeader():HttpHeaders{
    var headers_object = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));
    return headers_object;
  }
}
