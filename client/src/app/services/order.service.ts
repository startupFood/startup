import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { PersonDetails } from '../classes/person-details';
import { OrderDetails } from '../classes/order-details';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor() {
  
   }
   public personal:Subject<boolean> =  new Subject();
   public order:Subject<boolean> = new Subject();
   public buy:Subject<boolean> = new Subject();

}
