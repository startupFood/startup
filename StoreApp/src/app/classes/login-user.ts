import { User } from "./user";

export class LoginUser {
    constructor(
        public user:User,
        public token:string,
        public role:string
    ) {}
}
