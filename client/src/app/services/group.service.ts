import { Injectable } from '@angular/core';
import { Group } from '../classes/group';
import { Observable } from 'rxjs';
import { StoreService } from './store.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  private groups:Array<Group> = new Array<Group>();
  private group:number = 0;
  private groupManager:boolean = false;
  constructor(private storeService:StoreService ,private httpClient:HttpClient) { }
  URL="http://localhost:4588/";
  

  get Group()
  {
    return this.group;
  }

  set Group(value:number)
  {
    this.group = value;
  }
  //return the local member groups.
  //returns Array<Group>
  get Groups()
  {
    return this.groups;
  }
  
  get GroupManager()
  {
    return this.groupManager;
  }
דק
  set GroupManager(value:boolean)
  {
    this.groupManager = value;
  }
  //sets the local member groups.
  //parameter: groups: Array<Group>
  set Groups(groups:Array<Group>)
  {
    this.groups = groups;
  }

  getStoresGroups():Observable<Group[]>
  {
    return this.httpClient.get<Group[]>(this.URL + "api/Group/GetGroupes/" + this.storeService.getStoreId());
  }

  addGroup( groupToAdd:Group ):Observable<Group[]>
  {
    //in the DTO we need to add property saveGroup: boolean (here I add it in comment in the group class)
    return this.httpClient.post<Group[]>(this.URL + "api/Group/AddGroup/" , groupToAdd );
  }



}
