use DB_freeQueue

go
create table tbl_groups(
Id int primary key identity,
StoreId int foreign key references tbl_stores(Id),
GroupName nvarchar(30),
OpeningDate datetime,
)

--year in four digits, month , day , hour , minutes , seconds
-- 'yyyy-mm-dd 00:00:00'
insert into tbl_group values(1 , 'the group' , '2019-08-19 00:00:00' )
insert into tbl_group values(1 , 'my group' , '2019-08-19 00:00:00' )


--������� ����� �� ����� ����
go
drop table tbl_purchases


drop table tbl_stores

--����!
drop table tbl_storesActivityTime


drop table tbl_productsCategories

drop table tbl_storesMenu

go
delete from tbl_stores

