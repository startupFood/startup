﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using FreeQueueServer.Models;

namespace FreeQueueServer.Helpers
{
    public class PasswordsManagement
    {

        static DB_freeQueueEntities1 DB = new DB_freeQueueEntities1();
        /// <summary>
        /// הצפנת סיסמא באמצעות SHA512
        /// </summary>
        /// <param name="password"></param>
        /// <returns>סיסמא מוצפנת</returns>
        public static string GetSha512Hash(string password)
        {
            var bytes = Encoding.UTF8.GetBytes(password);
            using (var hash = SHA512.Create())
            {
                var hashedInputBytes = hash.ComputeHash(bytes);
                var hashedInputStringBuilder = new StringBuilder(128);
                foreach (var b in hashedInputBytes)
                    hashedInputStringBuilder.Append(b.ToString("x2"));
                return hashedInputStringBuilder.ToString();
            }
        }

        /// <summary>
        /// class to random new password
        /// </summary>
        public static class RandomPassword
        {
            // Generate a random number between two numbers    
            private static int RandomNumber()
            {
                Random random = new Random();
                return random.Next(1000, 9999);
            }

            // Generate a random string with a given size and case.   
            // If second parameter is true, the return string is lowercase  
            private static string RandomString(bool lowerCase)
            {
                StringBuilder builder = new StringBuilder();
                Random random = new Random();
                char ch;
                for (int i = 0; i < 2; i++)
                {
                    ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                    builder.Append(ch);
                }
                if (lowerCase)
                    builder.ToString().ToLower();
                return builder.ToString();
            }

            // Generate a random password of a given length (optional)  
            public static string CreateRandomPassword()
            {
                StringBuilder builder = new StringBuilder();
                builder.Append(RandomString(true));
                builder.Append(RandomNumber());
                builder.Append(RandomString(false));
                return builder.ToString();
            }
        }

        /// <summary>
        /// equals the password the user enter with the password in the DB and return the user.
        /// </summary>
        /// <param name="user"></param>
        /// <returns >UserDTO</returns>
        public static UsersDTO VerifyPassword(UsersDTO user)
        {
            try
            {
                var encryptedPass = GetSha512Hash(user.userPassword);
                foreach (var u in DB.tbl_users)
                {
                    if (u.UserEmail == user.userEmail && u.UserPassword == encryptedPass)
                        return UsersDTO.ConvertToDTO(u);
                }
                return null;
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }

        public static string CreateNewPassword(UsersDTO user)
        {
            try
            {
                var encryptPass = "";
                string passRandom = "";
                do
                {
                    passRandom = RandomPassword.CreateRandomPassword();
                    encryptPass = GetSha512Hash(passRandom.ToString());
                } while (DB.tbl_users.FirstOrDefault(u => u.UserPassword == encryptPass) != null);

                DB.tbl_users.FirstOrDefault(u => u.UserEmail == user.userEmail).UserPassword = encryptPass;
                DB.SaveChanges();

                string[] email = new string[1];
                email[0] = user.userEmail;

                Message.SendEmail(email, "Init Password", "your new password is " + passRandom);
                return encryptPass;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}