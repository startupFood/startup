import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Store } from '../classes/store';
import { HttpClient } from '@angular/common/http';
import { StoresMenu } from '../classes/stores-menu';
import { MenuAdditions } from '../classes/menu-additions';
import { MenuTastes } from '../classes/menu-tastes';
import { ProductCategory } from '../classes/product-category';
import { Purchase } from '../classes/purchase';
import { User } from '../classes/user';
import { Response } from '../classes/response';
import { StoresActivityTime } from '../classes/stores-activity-time';
import { ServerService } from './server.service';
import { CartService } from './cart.service';
import { StorePosition } from '../classes/store-position';
import { StoresCategory } from '../classes/stores-category';
import { Hechshers } from '../classes/hechshers';

@Injectable({
  providedIn: 'root'
})

export class StoreService {

  constructor(private httpClient: HttpClient, private serverServ: ServerService) {
  }

  //all this parameters are filled when the user chooses a store.
  //!!!!!!!!!!WE HAVE TO CHANGE IT!!!!!!!!!!!!!!!
  private store: Store;
  private menu: Array<StoresMenu> = new Array<StoresMenu>();
  private additions: Array<MenuAdditions> = new Array<MenuAdditions>();
  private tastes: Array<MenuTastes> = new Array<MenuTastes>();
  private favoriteProducts: Array<StoresMenu> = null;
  private lastPurchaseId: number = 0;
  private storesActivityTimes: Array<StoresActivityTime>;

  public serverSubject: Subject<boolean> = new Subject();

  private today: StoresActivityTime;


  storeId: number = 1;
  userId: number = 1;
  user: User = null;
  private hours: Array<number> = new Array<number>();
  public storeSubject = new Subject<Store>();


  private stores:Array<Store> = new Array<Store>();
  private hechshers:Array<Hechshers> = new Array<Hechshers>();
  public storesCategories:Array<StoresCategory> = new Array<StoresCategory>();
  url = "http://localhost:4588/";

  //getters and setters

  set Menu(value: Array<StoresMenu>) {
    this.menu = value;
  }

  get Menu() {
    return this.menu;
  }

  set Additions(value: Array<MenuAdditions>) {
    this.additions = value;
  }
  get Additions() {
    return this.additions;
  }

  set Tastes(value: Array<MenuTastes>) {
    this.tastes = value;
  }
  get Tastes() {
    return this.tastes;
  }

  set Store(value: Store) {
    this.store = value;
  }
  get Store() {
    return this.store;
  }
  set StoresActivityTimes(value: Array<StoresActivityTime>) {
    this.storesActivityTimes = value;
  }
  get StoresActivityTimes() {
    return this.storesActivityTimes;
  }

  get URL() {
    return this.url;
  }

  get Hours() {
    return this.hours;
  }

  set Hours(value: Array<number>) {
    this.hours = value;
  }
  get Today() {
    return this.today;
  }


  get Stores()
  {
    return this.stores;
  }

  set Stores(value:Array<Store>)
  {
    this.stores = value;
  }

  get Hechshers()
  {
    return this.hechshers;
  }

  set Hechshers(value:Array<Hechshers>)
  {
    this.hechshers = value;
  }

  get StoresCategories()
  {
    return this.storesCategories;
  }

  set StoresCategories(value:Array<StoresCategory>)
  {
    this.storesCategories = value;
  }

  
  initUser(id: number) {
    this.userId = id;

    //user will init in the function that get the user from the DB
  }

  //function to init the chosen store
  initStore(id: number): void {
    this.storeId = id;

    this.getStoreMenu().subscribe(
      data => this.menu = data
    )

    this.getStoreAdditions().subscribe(
      data => this.additions = data
    )

    this.getStoreTastes().subscribe(
      data => this.tastes = data
    )

    this.getStoreDetails().subscribe(
      data => {
        this.store = new Store(data.id, data.storeName, data.storeAddress, data.phone, data.about, data.kashrutCertifiction, data.img, data.storeCategory, data.reservedSeats, data.club, data.tip, data.storeLoad);
        this.storeSubject.next(data);

      })

  }

  //init the array of the activity time.
  initStoresActivityTime(): void {

    this.getStoresActivityTime().subscribe(
      data => {
        this.storesActivityTimes = data;
        //select the today object.
        this.today = this.storesActivityTimes.find(sat => sat.activityDay == new Date().getDay());
        this.serverSubject.next(true);
      }
    );
  }

  //Returns the store ID
  getStoreId(): number {
    return this.storeId;
  }

  //functions  that get data from the server

  //Gets all the stores from DB
  getStoresList(): Observable<Store[]> {
    return this.httpClient.get<Store[]>(this.serverServ.URL + "Store/GetStores", { headers: this.serverServ.getHeader() });
  }

  //Gets the quick products of the specific store
  getStoreQuickProducts(): Observable<StoresMenu[]> {
    return this.httpClient.get<StoresMenu[]>(this.URL + "api/Menu/GetStoreQuickProducts/" + this.storeId, { headers: this.serverServ.getHeader() });
  }

  //Gets the store products
  getStoreMenu(): Observable<StoresMenu[]> {
    return this.httpClient.get<StoresMenu[]>(this.URL + "api/Menu/GetByStore/" + this.storeId, { headers: this.serverServ.getHeader() });
  }

  //Gets the store products additions
  getStoreAdditions(): Observable<MenuAdditions[]> {
    return this.httpClient.get<MenuAdditions[]>(this.URL + "api/Menu/GetAdditionsByStore/" + this.storeId, { headers: this.serverServ.getHeader() });
  }

  //Gets the store products tastes
  getStoreTastes(): Observable<MenuTastes[]> {
    return this.httpClient.get<MenuTastes[]>(this.URL + "api/Menu/GetTastesByStore/" + this.storeId, { headers: this.serverServ.getHeader() });
  }


  getProductAdditions(prodId:number):Observable<MenuAdditions[]>
  {
    return this.httpClient.get<MenuAdditions[]>(this.url+"api/Menu/GetAdditionsByProduct/"+prodId, { headers: this.serverServ.getHeader()} );
  }

  
  getProductTastes(prodId:number):Observable<MenuTastes[]>
  {
    return this.httpClient.get<MenuTastes[]>(this.url+"api/Menu/GetTastesByProduct/"+prodId, { headers: this.serverServ.getHeader()} );
  }


  //Gets the store details
  getStoreDetails(): Observable<Store> {
    return this.httpClient.get<Store>(this.URL + "api/Store/GetStoreDetails/" + this.storeId, { headers: this.serverServ.getHeader() });
  }

  //Gets the store menu categories
  getStoreMenuCategories(): Observable<ProductCategory[]> {
    
    return this.httpClient.get<ProductCategory[]>(this.serverServ.URL + "Menu/GetMenuCategoriesByStore/" + this.storeId, { headers: this.serverServ.getHeader() });
  }


    //Gets the store menu categories
    getAllStoreMenuCategories(): Observable<ProductCategory[]> {
    
      return this.httpClient.get<ProductCategory[]>(this.serverServ.URL + "Menu/GetMenuCategories" , { headers: this.serverServ.getHeader() });
    }
  //Gets the user favorite purchases to this store
  getFavoritePurchases(): Observable<Purchase[]> {
    return this.httpClient.get<Purchase[]>(this.URL + "api/Order/GetFavoritePurchases/" + this.storeId + "/" + this.userId, { headers: this.serverServ.getHeader() })
  }

  //Gets the favorite products for specific purchase from server
  private getFavoriteProductsByPurchase(purchaseId: number): Observable<StoresMenu[]> {
    return this.httpClient.get<StoresMenu[]>(this.URL + "api/Order/GetFavoriteProductsByPurchase/" + purchaseId, { headers: this.serverServ.getHeader() })
  }

  //
  getFavoriteProducts(purchaseId: number): Array<StoresMenu> {
    //checks if the last purchase is the favorite purchase return its products
    if (this.lastPurchaseId == purchaseId && this.favoriteProducts != null)
      return this.favoriteProducts;
    //if not-
    //changes the last purchase to this purchase
    this.lastPurchaseId = purchaseId;
    //sends a request to the server to get the favorite products.
    this.getFavoriteProductsByPurchase(purchaseId).subscribe(
      data => { this.favoriteProducts = data; return this.favoriteProducts; }
    );

  }

  getStoreResponses(): Observable<Response[]> {
    return this.httpClient.get<Response[]>(this.URL + "api/Store/GetStoreResponses/" + this.storeId, { headers: this.serverServ.getHeader() })
  }

  addNewResponse(response: Response): Observable<Response[]> {
    return this.httpClient.post<Response[]>(this.URL + "api/Store/AddNewResponse/" + this.storeId, response, { headers: this.serverServ.getHeader() });
  }

  getStoresActivityTime(): Observable<StoresActivityTime[]> {
    return this.httpClient.get<StoresActivityTime[]>(this.URL + "api/Store/GetStoresActivityTime/" + this.storeId, { headers: this.serverServ.getHeader() });
  }



  // getStoreslist(): Observable<Store[]> {
  //   return this.httpClient.get<Store[]>(this.URL + "api/Store/GetStores", { headers: this.serverServ.getHeader() });
  // }

  getStoresCategories():Observable<StoresCategory[]>
  {
    return this.httpClient.get<StoresCategory[]>(this.URL + "api/Store/GetStoresCategories", { headers: this.serverServ.getHeader() });
  }
  
  getStoresHechshers():Observable<Hechshers[]>
  {
    return this.httpClient.get<Hechshers[]>(this.URL + "api/Store/GetStoresHechshers", { headers: this.serverServ.getHeader() });
  }

  getUserId() {
    return this.userId;
  }

  getUser() {
    return this.user;
  }


  convertDateToNumber(date: string) {
    let dateNumber: number;
    for (let i = 0; i < date.length; i++) {
      dateNumber += date.charCodeAt(0) - 48;
      dateNumber *= 10;
    }
    return dateNumber;
  }

  getStorePosition(storeId:number):Observable<StorePosition>
  {
    return this.httpClient.get<StorePosition>(this.URL + "api/Store/GetStorePosition/"+storeId, { headers: this.serverServ.getHeader() });
  }



}
// openDialog(component: ComponentType<any>): void {
//   const dialogRef = this.dialog.open(component, {
//     width: '500px'
//   });
// }
