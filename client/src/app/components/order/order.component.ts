import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { CartService } from 'src/app/services/cart.service';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  orderForm: FormGroup;
  purchaseSumFromCart: number;
  tipType: number;
  //need to be type of user class.
  user: string = "";
  group: boolean = false;
  delivery: boolean = false;
  groupId: number;
  //need to be type of user class.
  userPhone = "";
  minTime: number;
  maxTime: number;

  constructor(private cartServ: CartService,
    private router: Router) {

  }

  ngOnInit() {

  }

  convertDateToNumber(date: string) {
    let dateNumber: number;
    for (let i = 0; i < date.length; i++) {
      dateNumber += date.charCodeAt(i) - 48;
      dateNumber *= 10;
    }
    return dateNumber;
  }

  createOrderForm(userName, userPhone): FormGroup {
    return new FormGroup({
      customerName: new FormControl(userName, [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
      customerPhone: new FormControl(userPhone, [Validators.required, Validators.minLength(9), Validators.maxLength(10), creditDigitsValidator]),
      //creditCardValidator
      creditCard: new FormControl("", [Validators.required, Validators.minLength(8), Validators.maxLength(18), creditCardValidator]),
      creditDate: new FormControl("", [Validators.required, creditDateValidator, Validators.maxLength(5)]),
      creditDigits: new FormControl("", [Validators.required, Validators.maxLength(3)]),
      deliveryAddress: new FormControl("", [Validators.maxLength(200)]),
      reservedSeats: new FormControl(0, []),
      receiptTime: new FormControl("", [Validators.required, Validators.min(this.minTime), Validators.max(this.maxTime)])
    })
    // })

  }

  get customerName(): AbstractControl {
    return this.orderForm.get("customerName");
  }
  get customerPhone(): AbstractControl {
    return this.orderForm.get("customerPhone");
  }
  get creditCard(): AbstractControl {
    return this.orderForm.get("creditCard");
  }
  get creditDate(): AbstractControl {
    return this.orderForm.get("creditDate");
  }
  get creditDigits(): AbstractControl {
    return this.orderForm.get("creditDigits");
  }
  get deliveryAddress(): AbstractControl {
    return this.orderForm.get("deliveryAddress");
  }
  // get groupName():AbstractControl{
  //   return this.orderForm.get("groupName");
  // }
  get reservedSeats(): AbstractControl {
    return this.orderForm.get("reservedSeats");
  }
  get receiptTime(): AbstractControl {
    return this.orderForm.get("receiptTime");
  }

  //gets the group id that the user joined to her from the group component.
  //group not must to be.
  getGroupId(groupId: number) {
    this.groupId = groupId;
  }



  save(): void {
    //and if group is not choosen????????????????? 
    if (this.groupId == undefined) {
      this.groupId = 0;
    }
    //if there is load inthe store the orders in hour are 20, so we ne to check if this order can be,
    //we need to check if the hour that he choose is in the range of th eworking hour of this store.
    // this.cartServ.addOrder(this.orderForm.value  , this.groupId);
    this.orderForm.reset();
    //this.purchaseSumFromCart = this.httpServ.getSum();
    this.router.navigate(["/storePage/"]);

  }
}

export function creditDateValidator(control: AbstractControl): { [key: string]: any } | null {
  //(0[1-9])|      
  let regular = /^(1[0-2]|0[1-9])\/[0-9]{2}$/;
  //let regular = /^(0?[0-9]|1[0-2])\/\d{2}$/;
  let valid = regular.test(control.value);
  return valid ? null : { date: { valid: valid, value: control.value } };
}

export function creditDigitsValidator(control: AbstractControl): { [key: string]: any } | null {
  // let regular = /^[0-9]{9,10}$/;
  let regular = /^05[0-9]{8}|0[0-9]{8}$/
  let valid = regular.test(control.value);
  return valid ? null : { digitsOnly: { valid: valid, value: control.value } };
}


export function creditCardValidator(control: AbstractControl): { [key: string]: any } | null {
  let regular = /^[0-9]+$/
  let valid = regular.test(control.value);
  return valid ? null : { digitsOnly: { valid: valid, value: control.value } };
}