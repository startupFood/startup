﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FreeQueueServer.Helpers;
using FreeQueueServer.Models;

namespace FreeQueueServer.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/Group")]
    public class GroupController : ApiController
    {
        DB_freeQueueEntities1 DB = new DB_freeQueueEntities1();

        public static tbl_groups ConvertGroupFromDTO(GroupDTO group)
        {
            return new tbl_groups()
            {
                Id = group.id,
                StoreId = group.storeId,
                GroupName = group.groupName,
                GroupPassword = group.groupPassword,
                OpeningDate = group.openingDate
            };
        }



        /// <summary>
        /// get store id and return its groups.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [Route("GetGroupes/{id}")]
        [AllowAnonymous]
        public IHttpActionResult Get(int id)
         {
            var groups = GroupDTO.ConvertToDTO(DB.tbl_groups.Where(g => g.StoreId == id).ToList());

            return Ok(GroupDTO.ConvertToDTO(DB.tbl_groups.Where(g => g.StoreId == id).ToList()));
        }

        [Route("AddGroup")]
        [Authorize(Roles = RolesNames.CUSTOMER)]
        public IHttpActionResult Post([FromBody] GroupDTO group)
        {
            DB.tbl_groups.Add(ConvertGroupFromDTO(group));
            DB.SaveChanges();
            return Ok(GroupDTO.ConvertToDTO(DB.tbl_groups.Where(g => g.StoreId == group.storeId).ToList()));
        }

        /// <summary>
        /// functions that select the groups of the accepted purchases. 
        /// </summary>
        /// <param name="purchases"></param>
        /// <returns>list of groups</returns>
        [AllowAnonymous]
        [Route("GetGroupsByPurchases")]
        public IHttpActionResult Post([FromBody]List<PurchaseDTO> purchases)
        {
            List<GroupDTO> groups = new List<GroupDTO>();
            //select the groups of the purchases.
            purchases.ForEach(p =>
            {
                if (p.groupId != 0)
                {
                    groups.Add(GroupDTO.ConvertToDTO(DB.tbl_groups.FirstOrDefault(g => g.Id == p.groupId)));
                }
            });

            //save the groups in the local DB.
            LocalDB.Groups = groups;

            return Ok(groups);
        }



    }
}