﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreeQueueServer.Models
{
    public class StorePositionDTO
    {
        public int Id { get; set; }
        public int? StoreId { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }



        public static StorePositionDTO ConvertToDTO(tbl_storesPositions storePosition)
        {
            return new StorePositionDTO()
            {
                Id = storePosition.Id,
                StoreId = storePosition.StoreId,
                Latitude = storePosition.Latitude,
                Longitude = storePosition.Longitude
            };
        }
        public static List<StorePositionDTO> ConvertToDTO(List<tbl_storesPositions> storesPositions)
        {
            return storesPositions.Select(sp => ConvertToDTO(sp)).ToList();
        }
    }
}