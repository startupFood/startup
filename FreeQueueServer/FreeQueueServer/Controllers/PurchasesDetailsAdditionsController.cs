﻿using FreeQueueServer.Helpers;
using FreeQueueServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FreeQueueServer.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/PurchasesDetailsAdditions")]
    public class PurchasesDetailsAdditionsController : ApiController
    {

         DB_freeQueueEntities1 DB = new DB_freeQueueEntities1();

        /// <summary>
        /// convert PurchasesDetailsAddition object to tbl_purchasesAdditions object.
        /// </summary>
        /// <param name="purchasesDetailsAddition"></param>
        /// <returns>tbl_purchasesAdditions</returns>
        public  tbl_purchasesAdditions ConvertPurchasesDetailsAdditionFromDTO(PurchasesDetailsAdditionDTO purchasesDetailsAddition)
        {
            return new tbl_purchasesAdditions()
            {
                Id = purchasesDetailsAddition.id,
                Addition = (purchasesDetailsAddition.addition != null) ? DB.tbl_menuAddittions.FirstOrDefault(ma => ma.Id == purchasesDetailsAddition.addition).Id : 0,
                Product = purchasesDetailsAddition.product
            };
        }



        // GET: api/PurchasesDetailsAdditions
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        
        [Route("GetPurchaseAdditions/{purchaseId}")]
        public IHttpActionResult GetPurchaseAdditions(int purchaseId)
        {
            return Ok(PurchasesDetailsAdditionDTO.ConvertToDTO(DB.tbl_purchasesAdditions.Where(pa => pa.tbl_purchasesProducts.PurchaseId == purchaseId).ToList()));
        }
        
        [AllowAnonymous]
        [Route("GetPurchasesProductsAdditions")]
        public IHttpActionResult Post([FromBody]List<PurchasesProductDTO> purchasesProducts)
        {
            //lock (DB)
            //{

            PurchasesProductController ppc = new PurchasesProductController();
                List<tbl_purchasesProducts> PurchasesProductsList = new List<tbl_purchasesProducts>();
                //convert the purchases products from DTO.
                purchasesProducts.ForEach(pp => PurchasesProductsList.Add( ppc.ConvertPurchasesProductFromDTO(pp)));
                List<tbl_purchasesAdditions> purchasesAdditionsList = new List<tbl_purchasesAdditions>();

                try
                {
                    //select there additions.
                    PurchasesProductsList.ForEach(ppl => DB.tbl_purchasesAdditions.Where(pa => pa.Product == ppl.Id).ToList().ForEach(tpa => purchasesAdditionsList.Add(tpa)));
                }

                catch (NullReferenceException ex)
                {
                    return null;
                }
                purchasesAdditionsList.RemoveAll(pal => pal == null);
                return Ok(PurchasesDetailsAdditionDTO.ConvertToDTO(purchasesAdditionsList));
            //}
        }

        [AllowAnonymous]
        [Route("GetProductsAdditionsToProduct/{purchaseProductId}")]
        [HttpGet]
        public IHttpActionResult GetProductsAdditionsToProduct(int purchaseProductId)
        {
            List<PurchasesDetailsAdditionDTO> additions = PurchasesDetailsAdditionDTO.ConvertToDTO(DB.tbl_purchasesAdditions.Where(a => a.Product == purchaseProductId).ToList());

            return Ok(additions);  
        }


        [AllowAnonymous]
        [Route("AddPurchasesDetailsAdditions")]
        public IHttpActionResult Post([FromBody]List<PurchasesDetailsAdditionDTO> purchasesDetailsAdditions)
        {
            purchasesDetailsAdditions.ForEach(pda => DB.tbl_purchasesAdditions.Add(ConvertPurchasesDetailsAdditionFromDTO(pda)));
            DB.SaveChanges();
            return Ok(true);
        }
        // PUT: api/PurchasesDetailsAdditions/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/PurchasesDetailsAdditions/5
        public void Delete(int id)
        {
        }
    }
}
