import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-made-group-pop-up',
  templateUrl: './made-group-pop-up.component.html',
  styleUrls: ['./made-group-pop-up.component.css']
})
export class MadeGroupPopUpComponent implements OnInit {

  groupId:number;
  constructor( public storeServ:StoreService,
    public dialogRef: MatDialogRef<MadeGroupPopUpComponent>,
    private router:Router,
    //:DialogData
    @Inject(MAT_DIALOG_DATA) public data) { 

      this.groupId = data.groupId;
    }

  ngOnInit() {
  }

getGroupName()
{
  return this.storeServ.Groups.find(g => g.id == this.groupId).groupName;
}
  delete()
  {
    //update the statuses of all the purchase in this group.
    this.storeServ.StoresOrders.forEach(s => {
      if(s.groupId == this.groupId )
      {
        this.storeServ.updatePurchasesStatus(s.id).subscribe(data =>{
          this.storeServ.StoresOrders.find(p => p.id == s.id).status = true;
        });
      }
    });
    //send true - delete this group.
this.onNoClick(true);

  }

  notDelete()
  {
    //send false  - not to delete this group.
this.onNoClick(false);
  }


   
  closePopup() {
    this.router.navigate(["/openPage"]);
  }

  
  onNoClick(result:boolean)
  {
    try{
          this.dialogRef.close(result);
    }
    catch
    {
      this.closePopup();
    }

  }
}
