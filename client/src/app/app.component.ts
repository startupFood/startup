import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from './services/users.service';
import { StoreService } from './services/store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

 
  constructor(private router:Router ,
     private userServ:UsersService,
     private storeServ:StoreService){
 this.checkuser();

 
// this.storeServ.getStoresList().subscribe(
//   data => {
//     this.storeServ.Stores = data;
//   }
// )
    this.userServ.showLoginSubject.subscribe(data => {
      
      this.login = data;
      this.afterLogin = true;
    })


  }
  title = 'no-queues';
public afterLogin:boolean = false;
  public login:Number = 1;

checkuser()
{
  if(localStorage.getItem("token") == undefined)
  {
    this.router.navigate(["/logIn"])
  }
  else
  {
    this.afterLogin = true;
    this.router.navigate(["/storesList"])
  }
}

account()
{
  this.router.navigate(["/account"]);
}

}