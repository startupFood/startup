export class StoreBankDetails {
    constructor(
        public id:number,
        public storeId:number,
        public bank:number,
        public brunch:number,
        public account:string
    ){}
}
