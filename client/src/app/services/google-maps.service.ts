import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GoogleMapsService {


  //geocoding
  geocoder: google.maps.Geocoder;
  constructor() { }

  
  // //calculate the distance between the user position and the store position
  // calculateDistance(lat, lng): number {
  //   //variable for the user position
  //   const p1 = new google.maps.LatLng(
  //     this.userAddressPoint.lat,
  //     this.userAddressPoint.lng
  //   );
  //   //variable for the store position
  //   const p2 = new google.maps.LatLng(
  //     lat,
  //     lng
  //   );
  //   //calculate the distance and return it.
  //   return (
  //     Number.parseFloat((google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000
  //     ).toString()));
  // }


  
  //Gets lat and lng point and converts it to an literal address.
  geocode(latLng: google.maps.LatLng): Observable<google.maps.GeocoderResult[]> {
    return Observable.create((observer: Observer<google.maps.GeocoderResult[]>) => {
      // Invokes geocode method of Google Maps API geocoding.
      this.geocoder.geocode({ location: latLng }, (
        (results: google.maps.GeocoderResult[], status: google.maps.GeocoderStatus) => {
          if (status === google.maps.GeocoderStatus.OK) {
            observer.next(results);
            observer.complete();
          } else {
            console.log('Geocoding service: geocoder failed due to: ' + status);
            observer.error(status);
          }
        })
      );
    });
  }
}
