import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerEditAdditionComponent } from './manager-edit-addition.component';

describe('ManagerEditAdditionComponent', () => {
  let component: ManagerEditAdditionComponent;
  let fixture: ComponentFixture<ManagerEditAdditionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerEditAdditionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerEditAdditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
