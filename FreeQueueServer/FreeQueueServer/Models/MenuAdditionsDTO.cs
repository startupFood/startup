﻿using FreeQueueServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreeQueueServer.Models
{
    public class MenuAdditionsDTO
    {
        public int id { get; set; }
        public int product { get; set; }
        public string additionName { get; set; }
        public double additionPrice { get; set; }
        public bool additionStatus { get; set; }
        public string additionImage { get; set; }
        /// <summary>
        /// get sql entity and convert it to simple object without references
        /// </summary>
        /// <param name="addition"></param>
        /// <returns></returns>
        public static MenuAdditionsDTO ConvertToDTO(tbl_menuAddittions addition)
        {
            return new MenuAdditionsDTO()
            {
                id = addition.Id,
                product = addition.Product,
                additionName = addition.Addition,
                additionPrice = addition.AddtionPrice,
                additionStatus = addition.AdditionStatus,
                additionImage = addition.AdditionImage,
            };
        }

        public static List<MenuAdditionsDTO> ConvertToDTO(List<tbl_menuAddittions> addition)
        {
            return addition.Select(a => ConvertToDTO(a)).ToList();
        }
    }
}