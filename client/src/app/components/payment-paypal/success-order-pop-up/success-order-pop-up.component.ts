import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-success-order-pop-up',
  templateUrl: './success-order-pop-up.component.html',
  styleUrls: ['./success-order-pop-up.component.css']
})
export class SuccessOrderPopUpComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<SuccessOrderPopUpComponent>,
    //:DialogData
    @Inject(MAT_DIALOG_DATA) public data,
    public router:Router) {
     }

  ngOnInit() {
  }


  
  closePopup() {
    this.router.navigate(["/summary"]);
  }

  
  onNoClick()
  {
    try{
          this.dialogRef.close();
    }
    catch
    {
      this.closePopup();
    }

  }
}
