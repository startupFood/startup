import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { User } from '../classes/user';
import { HttpClient } from '@angular/common/http';
import { LoginUser } from '../classes/login-user';
import { Router } from '@angular/router';
import { ServerService } from './server.service';
import { WorkerService } from './worker.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(public httpClient:HttpClient,
     public router:Router,
      private serverServ:ServerService,
      private workerServ:WorkerService) { }

  private userId:number=0;
  private user:User=null;
  loginFlag:Subject<boolean> = new Subject<boolean>();

  get User():User
  {
    return this.user;
  }

  set User(user:User)
  {
    this.user=user;
  }

  get UserId():number
  {
    return this.userId;
  }

  set UserId(id:number)
  {
    this.userId=id;
  }

  getUsers():Observable<User[]>
  {
    return this.httpClient.get<User[]>(this.serverServ.URL+"auth/getUsers");
  }

  loginToServer(user:User):Observable<LoginUser>
  {
    return this.httpClient.post<LoginUser>(this.serverServ.URL+"auth/login", user);
  }

  login(user:User):void
  {
    var complete:boolean=null;
    this.loginToServer(user).subscribe(
      data=>{
        localStorage.setItem('token', data.token);
        let localWorkers:Array<User> = new Array<User>();
        // JSON.parse( localStorage.getItem('workers') );
        // if(localWorkers)
        // {
          //add the worker to the saved workers.
          localWorkers.push(data.user);
          localStorage.setItem('workers' , JSON.stringify(localWorkers));
        // }
        // else{
        //   localStorage.setItem('workers' , JSON.stringify(new Array<Worker>()));

        // }
         this.user=data.user; this.userId=data.user.id;
         //add the login worker to the workers in the duty.
         this.workerServ.WorkersOnDuty.push(data.user);
         this.loginFlag.next(false);
         this.router.navigate(["/openPage"]);
    },
      err=>{alert("your login details are incorrect. please try again")},
      // ()=>{this.router.navigate(["/openPage"])}
    );
  }

  addUserToDb(user:User):Observable<User>
  {
     return this.httpClient.post<User>(this.serverServ.URL+"Users/CreateUser", user);
  }

  addUser(user:User, changeUser:boolean):boolean
  {
    this.addUserToDb(user).subscribe(
      data=>{if(changeUser==true)
              {this.User=data;
                this.UserId=data.id;}
            return true;},
      err=>{return false}
    )
    return null;
  }

  initPassword(emailAddress:string):Observable<void>
  {
    return this.httpClient.get<void>(this.serverServ.URL+"auth/initPassword/"+emailAddress);
  }
}
