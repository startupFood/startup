import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerEditTasteComponent } from './manager-edit-taste.component';

describe('ManagerEditTasteComponent', () => {
  let component: ManagerEditTasteComponent;
  let fixture: ComponentFixture<ManagerEditTasteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerEditTasteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerEditTasteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
