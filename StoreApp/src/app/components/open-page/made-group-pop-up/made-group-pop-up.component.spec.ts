import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MadeGroupPopUpComponent } from './made-group-pop-up.component';

describe('MadeGroupPopUpComponent', () => {
  let component: MadeGroupPopUpComponent;
  let fixture: ComponentFixture<MadeGroupPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MadeGroupPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MadeGroupPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
