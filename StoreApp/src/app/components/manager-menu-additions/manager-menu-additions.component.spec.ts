import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerMenuAdditionsComponent } from './manager-menu-additions.component';

describe('ManagerMenuAdditionsComponent', () => {
  let component: ManagerMenuAdditionsComponent;
  let fixture: ComponentFixture<ManagerMenuAdditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerMenuAdditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerMenuAdditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
