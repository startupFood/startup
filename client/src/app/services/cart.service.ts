import { Injectable, EventEmitter } from '@angular/core';
import { PurchasesProduct } from '../classes/purchases-product';
import { PurchasesDetailsAddition } from '../classes/purchases-details-addition';
import { PurchaseDetailsTaste } from '../classes/purchase-details-taste';
import { FormGroup } from '@angular/forms';
import { StoreService } from './store.service';
import { MenuAdditions } from '../classes/menu-additions';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Purchase } from '../classes/purchase';
import { Order } from '../classes/order';
import { ServerService } from './server.service';
import { GroupService } from './group.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  ////////////////////////////////////////////////////////////////////////////////
  constructor(private storeServ: StoreService, public httpClient: HttpClient, private serverServ: ServerService, private groupServ: GroupService) {
/*     sessionStorage.setItem('productsCart', JSON.stringify(new Array<PurchasesProduct>()));
    sessionStorage.setItem('additionsCart',  JSON.stringify(new Array<PurchasesDetailsAddition>()));
    sessionStorage.setItem('tastesCart',  JSON.stringify(new Array<PurchaseDetailsTaste>())); */
  }

  URL = "http://localhost:4588/";
  //need to be type of user class.
  club: boolean = true;
  sum: number = 0;
  orderForm: FormGroup;
  private order: Order = new Order("", "", "00000000", "00/00", "000", "", 0, "");

  public additionsCahnged: EventEmitter<PurchasesDetailsAddition> = new EventEmitter<PurchasesDetailsAddition>();

  public additionsChangedSubject = new Subject<PurchasesDetailsAddition>();

  public tasteCahnged: EventEmitter<PurchaseDetailsTaste> = new EventEmitter<PurchaseDetailsTaste>();

  public tasteChangedSubject = new Subject<PurchaseDetailsTaste>();

  public buySubject = new Subject<boolean>();

  get Order() {
    return this.order;
  }

  //getCart
  //return Array<PurchasesProduct>.
  getCart(): Array<PurchasesProduct> {
    var cartProducts: Array<PurchasesProduct> = JSON.parse(sessionStorage.getItem('productsCart'));
    return cartProducts? cartProducts:new Array<PurchasesProduct>();
  }

  setAllCart(products: PurchasesProduct[]): void {
    var cartProducts: Array<PurchasesProduct> = products;
    sessionStorage.setItem('productsCart', JSON.stringify(cartProducts));
  }

  getCartDetailsAdditions(): Array<PurchasesDetailsAddition> {
    var cartAdditions: Array<PurchasesDetailsAddition> = JSON.parse(sessionStorage.getItem('additionsCart'));
    return cartAdditions?cartAdditions:new Array<PurchasesDetailsAddition>();
  }

  setAllCartDetailsAditions(additions: PurchasesDetailsAddition[]) {
    var cartAdditions: Array<PurchasesDetailsAddition> = additions;
    sessionStorage.setItem('additionsCart', JSON.stringify(cartAdditions));
  }

  //returns array of PurchaseDetailsTastes that contains all the tastes in this cart.
  getCartDetailsTastes(): Array<PurchaseDetailsTaste> {
    var cartTates: Array<PurchaseDetailsTaste> = JSON.parse(sessionStorage.getItem('tastesCart'));
    return cartTates?cartTates:new Array<PurchaseDetailsTaste>();
  }

  //sets the array of the cartDetailsTastes. gets tastes:PurchaseDetailsTaste[].
  setAllCartDetailsTastes(tastes: PurchaseDetailsTaste[]) {
    var cartTates: Array<PurchaseDetailsTaste> = tastes;
    sessionStorage.setItem('tastesCart', JSON.stringify(cartTates));
  }

  //get product:PurchasesProduct , and returns array of additions:PurchasesDetailsAddition of him that need to pay of them.
  getAdditionstoPay(product: PurchasesProduct) {
    //array of MenuAdditions that conect to this product.
    var thisProductAdditoins = new Array<MenuAdditions>();
    var additionsToPay = new Array<PurchasesDetailsAddition>();
    let numAdditionsToPay = 0;

    //adding the Menu additions for the specific product thats gets.
    this.getCartDetailsAdditions().forEach(cda => {
      if (cda.product == product.id) {
        thisProductAdditoins.push(this.storeServ.Additions.find(a => a.id == cda.addition));
      }
    });

    numAdditionsToPay = this.storeServ.Menu.find(m => m.id == product.product).additionQuantity - thisProductAdditoins.length;
    //if there is more additions than the quantity that gets free.
    if (numAdditionsToPay < 0) {
      //find the expensive additions.
      for (let i = 0, max = 0, maxAddition; i > numAdditionsToPay; i-- , max = 0) {
        for (let j = 0; j < thisProductAdditoins.length; j++) {
          if (thisProductAdditoins[j].additionPrice > max) {
            max = thisProductAdditoins[j].additionPrice;
            maxAddition = thisProductAdditoins[j];
          }
        }
        additionsToPay.push(this.getCartDetailsAdditions().find(cda => cda.addition == MenuAdditions.apply(maxAddition).id));
        thisProductAdditoins.splice(thisProductAdditoins.indexOf(maxAddition), 1);
      }
    }
    //returns array of purchasesDetailsAddition that need to pay on them.
    return additionsToPay;
  }

  //return the sum of the purchase.
  getSum() {
    var sum = 0;
    //calculate the products price
    this.getCart().forEach(p => sum += p.price * p.productCount);
    //add the additions price
    var additionsToPay = new Array<PurchasesDetailsAddition>();

    //pass on all the cart and for each product take his additions
    this.getCart().forEach(p => {

      //get the additions that need to pay on them.
      this.getAdditionstoPay(p).forEach(atp => additionsToPay.push(atp));
      //add the sum of the additions to the total sum.
      additionsToPay.forEach(atp => sum += this.storeServ.Additions.find(a => a.id == atp.addition).additionPrice);
      }
    );

    return sum;
  }

  //set the sum of the purchase , get sum:number. 
  setSum(sum: number): void {
    this.sum = sum;
  }


  getUser() {
    return localStorage.getItem('user');
  }


  SetPersonalDetails(name: string, phone: string) {
    this.order.customerName = name;
    this.order.customerPhone = phone;
  }

  SetOrderDetails(reservedSeats: number, reciepTime: string, deliveryAddress: string) {
    this.order.reservedSeats = reservedSeats;
    this.order.receiptTime = reciepTime;
    this.order.deliveryAddress = deliveryAddress;
  }

  // getOrderForm(){
  //   return this.orderForm;
  // } 

  // setOrderForm(orderForm:FormGroup)
  // {
  //   this.orderForm = orderForm;
  // }


  //add the order to the server.
  addOrder() {
    let groupId: number = JSON.parse(sessionStorage.getItem('groupId'));
    if (groupId == null) {
      groupId = 0;
    }
    //favorite = true! - need to check from where to take this data!
    //convert the date to string in the desirable pattern.
    let date: string = "";
    if (new Date().getDate() < 10) {
      date += "0";
    }
    date += new Date().getDate() + "/";
    if (new Date().getMonth() < 9) {
      date += "0";
    }
    date += (new Date().getMonth() + 1) + "/" + new Date().getFullYear();
    //order.tip - if we include it in the purchase else - now I put 0 and we will change it to tip table that refer to purchase table.
    let purchase: Purchase = new Purchase(0, this.storeServ.getStoreId(), date, this.order.customerName, this.order.customerPhone, this.order.creditCard, this.order.creditDate, this.order.creditDigits, this.order.deliveryAddress, groupId, this.order.reservedSeats, this.club, 0, this.getSum(), this.order.receiptTime, true, this.storeServ.getUserId(), false);

    this.addPurchase(purchase).subscribe(
      data => {
let newCart:Array<PurchasesProduct> = new Array<PurchasesProduct>();
        //insert the purchaseId to each product in the cart.
        this.getCart().forEach(c => {
          c.purchase = data;
        newCart.push(c);
        });
        //add the cart.
        this.addPurchasesProducts(newCart).subscribe(data => {
          let newCartDetailsAdditions:Array<PurchasesDetailsAddition> = new Array<PurchasesDetailsAddition>();

          this.getCartDetailsAdditions().forEach(cda => {
            cda.product =
            //find the new purchase product Id.
            data.find(d => d.product ==
            //store menu.
            this.storeServ.Additions.find(a => a.id == cda.addition).product).id;
            newCartDetailsAdditions.push(cda);
          });
          //add the additions.
          this.addPurchasesDetailsAdditions(newCartDetailsAdditions).subscribe(additionsData => {

            let newTastes:Array<PurchaseDetailsTaste> = new Array<PurchaseDetailsTaste>();
            this.getCartDetailsTastes().forEach(
              cdt => {
                cdt.product = 
                data.find(d => d.product = 
                  this.storeServ.Tastes.find(t => t.id == cdt.taste).product).id;
                  newTastes.push(cdt);
              }
            )
            ///add the taste.
            this.addPurchasesDetailsTaste(newTastes).subscribe(tastesData => {
              this.sum = 0;
              // alert("your order executed succesfully! thank you!");
              this.setAllCart(new Array<PurchasesProduct>());
              this.setAllCartDetailsAditions(new Array<PurchasesDetailsAddition>());
              this.setAllCartDetailsTastes(new Array<PurchaseDetailsTaste>());
              this.buySubject.next(true);
              this.SetOrderDetails(0, "", "");
              this.SetPersonalDetails("", "");
              sessionStorage.setItem('groupId', JSON.stringify(0));
              this.groupServ.GroupManager = false;
            });

          });

        });
      });
  }

  //add purchase to the server , gets purchase:Purchase and return the auto id of this purchase. 
  addPurchase(purchase: Purchase): Observable<number> {
    return this.httpClient.post<number>(this.URL + 'api/purchase/AddOrder', purchase, { headers: this.serverServ.getHeader() });
  }
  //
  addPurchasesProducts(purchasesProducts: Array<PurchasesProduct>): Observable<PurchasesProduct[]> {

    return this.httpClient.post<PurchasesProduct[]>(this.URL + 'api/purchasesProduct/AddPurchasesProduct', purchasesProducts, { headers: this.serverServ.getHeader() });
  }

  //
  addPurchasesDetailsAdditions(purchasesDetailsAdditions: Array<PurchasesDetailsAddition>): Observable<boolean> {
    return this.httpClient.post<boolean>(this.URL + 'api/purchasesDetailsAdditions/AddPurchasesDetailsAdditions', purchasesDetailsAdditions, { headers: this.serverServ.getHeader() })
  }

  //
  addPurchasesDetailsTaste(purchasesDetailsTastes: Array<PurchaseDetailsTaste>): Observable<boolean> {
    return this.httpClient.post<boolean>(this.URL + 'api/purchasesDetailsTastes/AddPurchasesDetailsTastes', purchasesDetailsTastes, { headers: this.serverServ.getHeader() });
  }


  //addToCart
  //add PurchaseProduct to the cart.
  //parameters:
  //purchaseProduct:PurchasesProduct
  //additions:Array<PurchasesDetailsAddition>
  //taste:PurchaseDetailsTaste
  addToCart(purchaseProduct: PurchasesProduct, additions: Array<PurchasesDetailsAddition>, taste: PurchaseDetailsTaste) {
    //if the cart doesn't contain this product already
    //the function creates new object with id and push it to the cart
    var cart= this.getCart();
      var cartAdditions=this.getCartDetailsAdditions();
    if (cart.find(c => c.product == purchaseProduct.product) == null) {
      cart.length > 0 ? purchaseProduct.id = cart[cart.length - 1].id + 1 : purchaseProduct.id = 1;
      cart.push(purchaseProduct);
      this.setAllCart(cart);
    }
    //else the function uses with the old object and just modify it.
    //the function delete the additions and the tastes
    else {

      cart.find(c => c.product == purchaseProduct.product).productCount = purchaseProduct.productCount;
      //change the price too????????????
      purchaseProduct.id = cart.find(c => c.product == purchaseProduct.product).id;

      cartAdditions.forEach(function (a) {
        if (a.product == purchaseProduct.product)
          this.cartDetailsAdditions.splice(this.cartDetailsAdditions.indexOf(a));
      })
      cartAdditions.splice(cartAdditions.findIndex(t => t.product == purchaseProduct.id));
      this.setAllCartDetailsAditions(cartAdditions);
    }

    //anyway the function creates/ re-creates the additions and tasted
    if (additions[0] != undefined && additions[0]!=null) {
      additions.forEach(a => a.product = purchaseProduct.id);
      additions.forEach(a => cartAdditions.push(a));
      this.setAllCartDetailsAditions(cartAdditions);
    }
    if (taste != undefined && taste!=null) {
      taste.product = purchaseProduct.id;
      var cartTastes=this.getCartDetailsTastes();
      cartTastes.push(taste);
      this.setAllCartDetailsTastes(cartTastes);
    }
  }

  //removeFromCart
  //remove PurchaseProduct from the cart.
  //parameters:
  //purchaseProduct:PurchasesProduct
  removeFromCart(purchaseProduct: PurchasesProduct) {
    //remove the purchaseProduct from the cart.
    var cart= this.getCart();
    cart.splice(cart.indexOf(purchaseProduct));
    this.setAllCart(cart);
    //remove the purchaseDetailsAdditions from the cartDetailsAdditions by purchaseProduct.product(:numebr).
    var cartAdditions=this.getCartDetailsAdditions();
    cartAdditions.forEach(a => {
      if (a.product == purchaseProduct.product) {
       cartAdditions.splice(cartAdditions.indexOf(a));
      }
    });
    this.setAllCartDetailsAditions(cartAdditions);
    //remove the pirchaseDetailsTaste from the cartDetailsTastes by purchseProduct.product(:numebr).
    var cartTastes=this.getCartDetailsTastes();
    cartTastes.splice(cartTastes.findIndex(t => t.product == purchaseProduct.product));
  }

  //setCartDetailsAdditions
  //set the PurchaseDetailsAdditions of specific PurchaseProduct.
  //parameters:
  //purchaseProduct:PurchasesProduct 
  //purchaseDetailsAdditions:Array<PurchasesDetailsAddition
  setCartDetailsAdditions(purchaseProduct: PurchasesProduct, purchaseDetailsAdditions: Array<PurchasesDetailsAddition>) {
    //remove the old additions.
    var cartAdditions=this.getCartDetailsAdditions();
    cartAdditions.forEach(a => {
      if (a.product == purchaseProduct.product) {
        cartAdditions.splice(cartAdditions.indexOf(a));
      }
    });
    //add the new additions.
    purchaseDetailsAdditions.forEach(a => cartAdditions.push(a));
    this.setAllCartDetailsAditions(cartAdditions);
  }

  //setCartDetailsTastes
  //set the PurchaseDetailsTaste of specific PurchaseProduct.
  //parameters:
  //purchaseProduct:PurchasesProduct
  //purchaseDetailsTaste:PurchaseDetailsTaste
  setCartDetailsTastes(purchaseProduct: PurchasesProduct, purchaseDetailsTaste: PurchaseDetailsTaste) {
    var cartTastes=this.getCartDetailsTastes();
    //remove the old taste.
    cartTastes.splice(cartTastes.findIndex(t => t.product == purchaseProduct.product));
    //add the new taste.
    cartTastes.push(purchaseDetailsTaste);
    this.setAllCartDetailsTastes(cartTastes);
  }

  getPurchaseProducts(purchaseId: number): Observable<PurchasesProduct[]> {
    return this.httpClient.get<PurchasesProduct[]>(this.storeServ.URL + "api/purchasesProduct/GetPurchaseProducts/" + purchaseId);
  }

  getPurchaseAdditions(purchaseId: number): Observable<PurchasesDetailsAddition[]> {
    return this.httpClient.get<PurchasesDetailsAddition[]>(this.storeServ.URL + "api/purchasesDetailsAdditions/GetPurchaseAdditions/" + purchaseId);
  }

  getPurchaseTastes(purchaseId: number): Observable<PurchaseDetailsTaste[]> {
    return this.httpClient.get<PurchaseDetailsTaste[]>(this.storeServ.URL + "api/purchasesDetailsTastes/GetPurchaseTastes/" + purchaseId);
  }

}

