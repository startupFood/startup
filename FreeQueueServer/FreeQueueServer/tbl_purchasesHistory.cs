//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FreeQueueServer
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_purchasesHistory
    {
        public int Id { get; set; }
        public int Tbl_PurchaseId { get; set; }
        public int StoreId { get; set; }
        public string PurchaseDate { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CreditCard { get; set; }
        public string CreditDate { get; set; }
        public string CreditDigits { get; set; }
        public string DeliveryAddress { get; set; }
        public Nullable<int> GroupDetails { get; set; }
        public Nullable<int> ReservedSeats { get; set; }
        public Nullable<bool> Club { get; set; }
        public Nullable<double> Tip { get; set; }
        public double PurchaseSum { get; set; }
        public string ReceiptTime { get; set; }
        public Nullable<bool> favorite { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<int> ModifyUser { get; set; }
        public Nullable<int> modifyType { get; set; }
    
        public virtual tbl_modifyTypes tbl_modifyTypes { get; set; }
        public virtual tbl_modifyTypes tbl_modifyTypes1 { get; set; }
    }
}
