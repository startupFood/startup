import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GooglemapsAutocompleteComponent } from './googlemaps-autocomplete.component';

describe('GooglemapsAutocompleteComponent', () => {
  let component: GooglemapsAutocompleteComponent;
  let fixture: ComponentFixture<GooglemapsAutocompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GooglemapsAutocompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GooglemapsAutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
