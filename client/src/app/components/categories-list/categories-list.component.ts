import { Component, OnInit, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { StoreService } from 'src/app/services/store.service';
import { ProductCategory } from 'src/app/classes/product-category';
import { ServerService } from 'src/app/services/server.service';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.css']
})
export class CategoriesListComponent implements OnInit {

  @ViewChild('svgContainer', null) container: ElementRef;

  categories:Array<ProductCategory>=new Array<ProductCategory>();
  categoryPresentation:Array<boolean> = new Array<boolean>();

  constructor(private storeServ:StoreService,
              private renderer:Renderer2,
              private serverServ:ServerService) {
    
    storeServ.getStoreMenuCategories().subscribe(
      data=> 
      {
        this.categories = data.filter(c => c.store==storeServ.storeId);
      this.categories.forEach(c => this.categoryPresentation.splice(c.id , 0 , false ) );
      }
    );

         //to dispaly the match icons in the header.
this.serverServ.Component.next(2);
  }

  //expand the category details.
  expandCategory(category:ProductCategory)
  {
    //open - to close.
    //arrow_right
     if(this.categoryPresentation[category.id])
     {
        document.getElementById('arrow'+category.id).innerHTML = "arrow_right";
     }
     //close - to be open.
     //arrow_drop_down
     else{
      document.getElementById('arrow'+category.id).innerHTML = "arrow_drop_down";

     }
    this.categoryPresentation[category.id] = ! this.categoryPresentation[category.id]
  }



  ngOnInit() {
  }

  
}