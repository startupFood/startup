import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/classes/user';
import { ManagerService } from 'src/app/services/manager.service';
import { CellCustomComponent } from 'src/app/helpers/cell-custom/cell-custom.component';
import { MatDialog } from '@angular/material/dialog';
import { ManagerEditWorkerComponent } from '../manager-edit-worker/manager-edit-worker.component';

@Component({
  selector: 'app-manager-workers',
  templateUrl: './manager-workers.component.html',
  styleUrls: ['./manager-workers.component.css']
})
export class ManagerWorkersComponent implements OnInit {

  errorMessage:string=null;
  constructor(private managerServ:ManagerService, public dialog: MatDialog) {
    managerServ.getStoreWorkers().subscribe(
      data=>{this.rowData=data; this.errorMessage=null},
      err=>{this.errorMessage=err.error}
    );
  }

    
  openDialog():void {
    const dialogRef = this.dialog.open(ManagerEditWorkerComponent, { disableClose: true,
      width: '500px',
      data: {userId:0}
    });

    dialogRef.afterClosed().subscribe();
  }
  
  columnDefs = [
    {headerName: 'id', field: 'id', sortable: true},
    {headerName: 'name', field: 'userName', sortable: true, filter: true},
    {headerName: 'phone', field: 'userPhone', sortable: true, filter: true},
    {headerName: 'email', field: 'userEmail', sortable: true, filter: true},
    {headerName: 'Actions', field: 'action', cellRendererFramework: CellCustomComponent}
  ];

  rowData: Array<User>;

  ngOnInit() {
  }

}
