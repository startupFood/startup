﻿using FreeQueueServer.Helpers;
using FreeQueueServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FreeQueueServer.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/Users")]
    public class UsersController : ApiController
    {
        DB_freeQueueEntities1 DB = new DB_freeQueueEntities1();
        // GET: api/Users
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        /// <summary>
        /// returns workwers list to every store
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns>workers list</returns>

        [AllowAnonymous]
        [Route("GetWorkersList/{storeId}")]
        public IHttpActionResult Get(int storeId)
        {
            try
            {
                var q = DB.tbl_workerToStore.Where(s => s.StoreId == storeId).Select(s1 => s1.tbl_users).ToList();
                if (q != null || q.Count==0 && q[0] != null)
                    return Ok(UsersDTO.ConvertToDTO(q));
                return Ok(new List<UsersDTO>());
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, "Oops! something went wrong..."));
            }
            //return Ok(UsersDTO.ConvertToDTO(DB.tbl_users.Where(u => u.RoleId == RolesNames.STORE && u.tbl_workerToStore != null).ToList()));
        }
        //[Authorize(Roles = "MANAGER")]
        [Authorize(Roles = RolesNames.SYSTEM + ", " + RolesNames.DEVELOPER + ", " + RolesNames.MANAGER + ", " + RolesNames.STORE)]
        [Route("getWorkerPurchases/{storeId}")]
        public IHttpActionResult GetWorkerPurchases(int workerId)
        {
            return Ok(PurchaseDTO.ConvertToDTO(DB.tbl_purchaseToWorker.Where(w => w.UserId == workerId).Select(p => p.tbl_purchases).ToList()));
        }

        // POST: api/Users
        public void Post([FromBody]string value)
        {
        }



        [Authorize(Roles = RolesNames.SYSTEM + ", " + RolesNames.DEVELOPER + ", " + RolesNames.MANAGER + ", " + RolesNames.STORE)]
        [Route("UpdateUser/{userId}")]
        [HttpPut]
        // PUT: api/Users/5
        public IHttpActionResult Put(int userId, [FromBody]UsersDTO newUser)
        {
            try
            {
                var user = DB.tbl_users.FirstOrDefault(u => u.Id == userId);
                if (user != null)
                {
                    DB.Entry(user).CurrentValues.SetValues(UsersDTO.ConvertFromDTO(newUser));
                    //user = UsersDTO.ConvertFromDTO(newUser);
                    DB.SaveChanges();
                }
                else
                    throw new Exception("no match record in DB");

            }
            catch(Exception ex)
            {
                return Ok(ex.Message);
            }
            return Ok();
        }

        // PUT: api/Auth/5
        //Everyone who has token can use with this function:
        [Authorize]
        [HttpPost, Route("CreateUser")]
        public IHttpActionResult Create([FromBody]UsersDTO user)
        {
            //we have to check that just manager and up can add worker
            user.userPassword = PasswordsManagement.GetSha512Hash(user.userPassword);
            DB.tbl_users.Add(UsersDTO.ConvertFromDTO(user));
            DB.SaveChanges();
            return Ok(UsersDTO.ConvertToDTO(DB.tbl_users.FirstOrDefault(u => u.UserEmail == user.userEmail)));
        }

        [Authorize(Roles = RolesNames.SYSTEM + ", " + RolesNames.DEVELOPER + ", " + RolesNames.MANAGER + ", " + RolesNames.STORE)]
        [Route("CreateWorker/{storeId}")]
        [HttpPost]
        // PUT: api/Users/5
        public IHttpActionResult CreateWorker(int storeId ,[FromBody]UsersDTO newUser)
        {
            try
            {
                if (DB.tbl_users.FirstOrDefault(u => u.UserEmail == newUser.userEmail && u.RoleId == u.RoleId) != null)
                    return BadRequest("This Email already exists");
                var pass = PasswordsManagement.CreateNewPassword(newUser);
                if (pass != "")
                    newUser.userPassword = pass;
                else
                    throw new Exception("Password wasn't creating successfully");
                DB.tbl_users.Add(UsersDTO.ConvertFromDTO(newUser));
                DB.SaveChanges();

                var userId = DB.tbl_users.FirstOrDefault(u => u.UserEmail == newUser.userEmail && u.UserPassword == newUser.userPassword).Id;
                DB.tbl_workerToStore.Add(new tbl_workerToStore() { StoreId = storeId, UserId = userId, Tip = 0 });
                DB.SaveChanges();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
            return Ok();
        }


        // DELETE: api/Users/5
        [HttpDelete]
        [Authorize(Roles = RolesNames.SYSTEM + ", " + RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [Route("Delete/{deleteUserId}/{userId}")]
        public IHttpActionResult Delete(int deleteUserId, int userId)
        {
            try
            {
                
                tbl_users thisUser = DB.tbl_users.FirstOrDefault(u => u.Id == deleteUserId);
                var storeId = thisUser.tbl_workerToStore.First().StoreId;
                //before deleting user, it adds the user to history table.
                //have to be in sql trigger when we will have time...
                DB.tbl_usersHistory.Add(new tbl_usersHistory()
                {
                    ModifyDate = DateTime.Now,
                    modifyType = 1,
                    RoleId = thisUser.RoleId,
                    ModifyUser = userId,
                    UserEmail = thisUser.UserEmail,
                    Tbl_usersId = deleteUserId,
                    UserName = thisUser.UserName,
                    UserPassword = thisUser.UserPassword,
                    UserPhone = thisUser.UserPhone
                });
                if (DB.tbl_workerToStore.Where(w => w.UserId == deleteUserId)!=null)
                {
                    foreach (var w in DB.tbl_workerToStore.Where(w => w.UserId == deleteUserId))
                    {
                        DB.tbl_workerToStore.Remove(w);
                    }
                }
                if (DB.tbl_purchaseToWorker.Where(w => w.UserId == deleteUserId) != null)
                {
                    foreach (var w in DB.tbl_purchaseToWorker.Where(w => w.UserId == deleteUserId))
                    {
                        DB.tbl_purchaseToWorker.Remove(w);
                    }
                }
                DB.tbl_users.Remove(thisUser);
                DB.SaveChanges();
                return Get((int)storeId);
            }
            catch
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, "Oops! something went wrong..."));
            }
        }
    }
}
