import { Component, OnInit } from '@angular/core';
import { StoresMenu } from 'src/app/classes/stores-menu';
import { StoreService } from 'src/app/services/store.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PurchasesProduct } from 'src/app/classes/purchases-product';
import { PurchasesDetailsAddition } from 'src/app/classes/purchases-details-addition';
import { PurchaseDetailsTaste } from 'src/app/classes/purchase-details-taste';
import { CartService } from 'src/app/services/cart.service';
import { ServerService } from 'src/app/services/server.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  thisProduct: StoresMenu;
  additions: Array<PurchasesDetailsAddition>;
  taste: PurchaseDetailsTaste;


  constructor(private storeServ: StoreService,
             private cartServ: CartService,
             private router: Router, 
             private activatedRoute: ActivatedRoute,
             private serverServ:ServerService) {
    
    //get the product id from the routing parameters
    let prodId;
    activatedRoute.params.subscribe(params => prodId = params["id"]);
    //select the chosen product by the id 
    storeServ.initStore(this.storeServ.storeId);
    this.storeServ.storeSubject.subscribe(data => {
      this.thisProduct = storeServ.Menu.find(m => m.id == prodId);
    
      //to dispaly the match icons in the header.
      this.serverServ.Component.next(2);
    
    })

  }

  ngOnInit() {
    //get new addition from the child component- additions by event emitter through the service
    //and put its values in new variable of type 'PurchasesDetailsAddition'.
    this.additions = new Array<PurchasesDetailsAddition>();
    let newAddition: PurchasesDetailsAddition;

    this.cartServ.additionsCahnged.subscribe(data => {
      newAddition =
      new PurchasesDetailsAddition(
        data.id,
        data.product,
        data.addition
      );
      if (this.additions.find(a => a.addition == newAddition.addition) != undefined) {
        this.additions.splice(this.additions.indexOf(newAddition));
      }
      else {
        this.additions.push(newAddition);
      }
    });


    let newTaste: PurchaseDetailsTaste;


    this.cartServ.tasteCahnged.subscribe(data => {
      newTaste =
      new PurchaseDetailsTaste(
        data.id,
        data.product,
        data.taste
      );
      this.taste = newTaste;
    });
  }

  //return the logo of the store to the div that display the logo.
  storeLogo() {
    return this.storeServ.Store.img;
  }
  //return the name of the store to display.
  storeName() {
    return this.storeServ.Store.storeName;
  }

  addToCart(quantity: string) {

    let purchasesProduct = new PurchasesProduct(0, 0, this.thisProduct.id, parseInt(quantity), this.thisProduct.productPrice, false)
    this.cartServ.addToCart(purchasesProduct, this.additions, this.taste);
    this.router.navigate(["/categoriesList/"]);
  }

  buy() {
    let purchasesProduct = new PurchasesProduct(0, 0, this.thisProduct.id, 1, this.thisProduct.productPrice, false)
    this.cartServ.addToCart(purchasesProduct, this.additions, this.taste);
    this.router.navigate(["order"]);
  }

}

