//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FreeQueueServer
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_purchasesTastesHistory
    {
        public int Id { get; set; }
        public int Tbl_PurchasesTastesId { get; set; }
        public int Product { get; set; }
        public int Taste { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<int> ModifyUser { get; set; }
        public Nullable<int> modifyType { get; set; }
    
        public virtual tbl_modifyTypes tbl_modifyTypes { get; set; }
        public virtual tbl_modifyTypes tbl_modifyTypes1 { get; set; }
    }
}
