﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreeQueueServer.Models
{
    public class PurchasesDetailsAdditionDTO
    {
        public int id { get; set; }
        public int product { get; set; }
        public int? addition { get; set; }
        public static PurchasesDetailsAdditionDTO ConvertToDTO(tbl_purchasesAdditions purchasesAddition)
        {
            if(purchasesAddition == null)
            {
                return null;
            }
            return new PurchasesDetailsAdditionDTO()
            {
                id = purchasesAddition.Id,
                product = (purchasesAddition.Product != 0) ? purchasesAddition.tbl_purchasesProducts.Id : 0,
                addition = purchasesAddition.Addition
            };
        }

        public static List<PurchasesDetailsAdditionDTO> ConvertToDTO(List<tbl_purchasesAdditions> purchasesAdditions)
        {
            return purchasesAdditions.Select(p => ConvertToDTO(p)).ToList();
        }
    }
}