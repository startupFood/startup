﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreeQueueServer.Models
{
    public class WorkerToStoreDTO
    {
        public int id { get; set; }
        public int? userId { get; set; }
        public int? storeId { get; set; }
        public double? tip { get; set; }

        public static WorkerToStoreDTO ConvertToDTO(tbl_workerToStore workerToStore)
        {
            return new WorkerToStoreDTO
            {
                id = workerToStore.Id,
                storeId = workerToStore.StoreId,
                userId = workerToStore.UserId,
                tip = workerToStore.Tip
            };
        }


        public static List<WorkerToStoreDTO> ConvertToDTO(List<tbl_workerToStore> workersToStore)
        {
            return workersToStore.Select(w =>ConvertToDTO(w)).ToList();
        }


        public static tbl_workerToStore ConvertFromDTO(WorkerToStoreDTO workerToStore)
        {
            return new tbl_workerToStore
            {
                Id = workerToStore.id,
                StoreId = workerToStore.storeId,
                UserId = workerToStore.userId,
                Tip = workerToStore.tip
            };
        }


        public static List<tbl_workerToStore> ConvertFromDTO(List<WorkerToStoreDTO> workersToStore)
        {
            return workersToStore.Select(w => ConvertFromDTO(w)).ToList();
        }
    }
}