﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreeQueueServer.Models
{
    public class StoreCategoryDTO
    {
        public int Id { get; set; }
        public string Category { get; set; }

        public static StoreCategoryDTO ConvertToDTO(tbl_storesCategories storeCategory)
        {
            return new StoreCategoryDTO()
            {
                Id = storeCategory.Id,
                Category = storeCategory.Category
            };
        }
        public static List<StoreCategoryDTO> ConvertToDTO(List<tbl_storesCategories> storesCategories)
        {
            return storesCategories.Select(sc => ConvertToDTO(sc)).ToList();
        }
    }

}