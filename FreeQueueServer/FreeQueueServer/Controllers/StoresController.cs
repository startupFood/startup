﻿using FreeQueueServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Globalization;
using System.IO;
using FreeQueueServer.Helpers;
using System.Web.Http.Description;
using System.Web;

namespace FreeQueueServer.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/Store")]
    public class StoresController : ApiController
    {
        DB_freeQueueEntities1 DB = new DB_freeQueueEntities1();

        /// <summary>
        /// convert store object from DTO object to SQL object
        /// </summary>
        /// <param name="store"></param>
        /// <returns>tbl_stores</returns>
        public static tbl_stores ConvertFromDto(StoresDTO store)
        {
            return new tbl_stores()
            {
                Id = store.id,
                StoreName = store.storeName,
                StoreAddress = store.storeAddress,
                Phone = store.phone,
                About = store.about,
                KashrutCertification = store.kashrutCertifiction,
                Img = store.img,
                StoreCategory = store.storeCategory,
                ReservedSeats = store.reservedSeats,
                Club = store.club,
                Tip = store.tip,
                StoreLoad = store.storeLoad
            };
        }

        public static tbl_responses ConvertFromDto(ResponsesDTO response)
        {
            return new tbl_responses()
            {
                Id = response.id,
                UserName = response.userName,
                Email = response.email,
                Content = response.content,
                Header = response.header,
                ResponseDate = response.responseDate.ToString(),
                StroeId = response.storeId
            };
        }

        //public double rad(double x)
        //{
        //    return x * Math.PI / 180;
        //}

        //public double getDistance(double p1, double p2)
        //{
        //        var R = 6378137; // Earth’s mean radius in meter
        //        var dLat = rad(p2.lat() - p1.lat());
        //        var dLong = rad(p2.lng() - p1.lng());
        //        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        //          Math.cos(rad(p1.lat())) * Math.cos(rad(p2.lat())) *
        //          Math.sin(dLong / 2) * Math.sin(dLong / 2);
        //        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        //        var d = R * c;
        //        return d; // returns the distance in meter
        //}

        //public void getTheClosestStoresTrying()
        //{
        //    var getDistance = function(p1, p2) {

        //    };
        //}

        // GET: api/Stores
        /// <summary>
        /// return the stores list
        /// </summary>
        /// <returns>IHttpActionResult</returns>
        [Route("GetStores")]
        public IHttpActionResult Get()
        {
            return Ok(StoresDTO.ConvertToDTO(DB.tbl_stores.ToList()));
        }

        [HttpGet]
        [Route("GetStoresCategories")]
        public IHttpActionResult GetStoresCategories()
        {
            return Ok(StoreCategoryDTO.ConvertToDTO(DB.tbl_storesCategories.ToList()));
        }

        [HttpGet]
        [Route("GetStoresHechshers")]
        public IHttpActionResult GetStoresHechshers()
        {
            return Ok(HechsherDTO.ConvertToDTO(DB.tbl_hechshers.ToList()));
        }

        // GET: api/Stores/5
        /// <summary>
        /// get Id and retun the match store
        /// </summary>
        /// <param name="id"></param>
        /// <returns>StoresDTO</returns>
        [AllowAnonymous]
        [Route("GetStoreDetails/{id}")]
        public IHttpActionResult Get(int id)
        {
            LocalDB.Store = StoresDTO.ConvertToDTO(DB.tbl_stores.FirstOrDefault(s => s.Id == id));
            return Ok(LocalDB.Store);
        }


        [AllowAnonymous]
        [Route("UpdateStoreLoad/{load}")]
        public IHttpActionResult Get(bool load)
        {
            DB.tbl_stores.FirstOrDefault(s => s.Id == LocalDB.Store.id).StoreLoad = load;
            DB.SaveChanges();
            LocalDB.Store.storeLoad = load;
            return Ok(LocalDB.Store.storeLoad);
        }

        // POST: api/Stores
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Stores/5
        /// <summary>
        /// update exists store
        /// </summary>
        /// <param name="id"></param>
        /// <param name="store"></param>
        /// <returns></returns>
        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [Route("UpdateStoreDetails/{id}")]
        public IHttpActionResult Put(int id, [FromBody]StoresDTO store)
        {
            DB.Entry(ConvertFromDto(store)).State = System.Data.Entity.EntityState.Modified;
            DB.SaveChanges();
            return Ok(StoresDTO.ConvertToDTO(DB.tbl_stores.ToList()));
        }


        [HttpPut]
        public IHttpActionResult PutImage(int id, [FromBody]FileStream image)
        {
            //string directoryPath = Path.GetDirectoryName("./assets/images/store" + id+"/storeImage");
            //if (Directory.Exists(directoryPath) == false)                                              // <---
            //{                                                                                    // <---
            //    Directory.CreateDirectory(directoryPath);                                              // <---
            //}   
            //try
            //{
            //    var file = image;

            //    var ext = new FileInfo(file.Name).Extension;
            //    Stream fullPath = Path.Combine(StorageRoot, Path.GetFileName(Guid.NewGuid() + ext));

            //    file.CopyTo(fullPath);

            //    string iName = "storeImage.jpg";   // <---
            //    string filepath =File.  directoryPath+"/storeImage.jpg";    // <---
            //    File.Create(filepath,, appPath + iName); // <---
            //    picProduct.Image = new Bitmap(opFile.OpenFile());
            //}
            //catch (Exception exp)
            //{
            //    MessageBox.Show("Unable to open file " + exp.Message);
            //}
            return Ok();

        }


        // DELETE: api/Stores/5
        /// <summary>
        /// delete store by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>IHttpActionResult</returns>
        [Route("DeleteStore/{id}")]
        [Authorize(Roles = RolesNames.SYSTEM + ", " + RolesNames.DEVELOPER)]
        public IHttpActionResult Delete(int id)
        {
            DB.tbl_stores.Remove(DB.tbl_stores.FirstOrDefault(s => s.Id == id));
            DB.SaveChanges();
            return Ok(StoresDTO.ConvertToDTO(DB.tbl_stores.ToList()));
        }

        [Route("GetStoreResponses/{storeId}")]
        public IHttpActionResult GetStoreResponses(int storeId)
        {
            //list.Sort((emp1, emp2) => emp1.FirstName.CompareTo(emp2.FirstName));
            var q = ResponsesDTO.ConvertToDTO(DB.tbl_responses.Where(r => r.StroeId == storeId).ToList());
            q.Sort((r1, r2) => r2.responseDate.CompareTo(r1.responseDate));
            return Ok(q);
        }

        [HttpPost, Route("AddNewResponse/{storeId}")]
        public IHttpActionResult AddNewResponse(int storeId, [FromBody] ResponsesDTO response)
        {
            try
            {
                DB.tbl_responses.Add(ConvertFromDto(response));
                DB.SaveChanges();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return GetStoreResponses(storeId);
        }

        [Route("GetStoresActivityTime/{storeId}")]
        public IHttpActionResult GetStoresActivityTime(int storeId)
        {
            //int dayOfweek = (int)CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(DateTime.Now);
            // var s = DB.tbl_storesActivityTime.FirstOrDefault(sat => sat.Store == storeId && sat.ActivityDay == dayOfweek);
            //return Ok(StoresActivityTimeDTO.ConvertToDTO(DB.tbl_storesActivityTime.FirstOrDefault(sat => sat.Store == storeId && sat.ActivityDay == dayOfWeek)));
            List<StoresActivityTimeDTO> storesActivityTimeList = StoresActivityTimeDTO.ConvertToDTO( DB.tbl_storesActivityTime.Select(sat => sat).Where(sat1 => sat1.Store == storeId).ToList() );
            return Ok(storesActivityTimeList);

        }

        [HttpGet]
        [Route("GetStorePosition/{storeId}")]
        public IHttpActionResult GetStorePosition(int storeId)
        {
            return Ok(StorePositionDTO.ConvertToDTO(DB.tbl_storesPositions.FirstOrDefault(s => s.StoreId == storeId)));
        }
    }
}
