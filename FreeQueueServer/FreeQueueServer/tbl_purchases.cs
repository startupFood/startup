//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FreeQueueServer
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_purchases
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_purchases()
        {
            this.tbl_purchasesProducts = new HashSet<tbl_purchasesProducts>();
            this.tbl_purchaseToWorker = new HashSet<tbl_purchaseToWorker>();
            this.tbl_purchasesProducts1 = new HashSet<tbl_purchasesProducts>();
            this.tbl_purchaseToWorker1 = new HashSet<tbl_purchaseToWorker>();
        }
    
        public int Id { get; set; }
        public int StoreId { get; set; }
        public string PurchaseDate { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CreditCard { get; set; }
        public string CreditDate { get; set; }
        public string CreditDigits { get; set; }
        public string DeliveryAddress { get; set; }
        public Nullable<int> GroupDetails { get; set; }
        public Nullable<int> ReservedSeats { get; set; }
        public Nullable<bool> Club { get; set; }
        public Nullable<double> Tip { get; set; }
        public double PurchaseSum { get; set; }
        public string ReceiptTime { get; set; }
        public Nullable<bool> Favorite { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<bool> Status { get; set; }
    
        public virtual tbl_groups tbl_groups { get; set; }
        public virtual tbl_groups tbl_groups1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_purchasesProducts> tbl_purchasesProducts { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_purchaseToWorker> tbl_purchaseToWorker { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_purchasesProducts> tbl_purchasesProducts1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_purchaseToWorker> tbl_purchaseToWorker1 { get; set; }
        public virtual tbl_stores tbl_stores { get; set; }
        public virtual tbl_stores tbl_stores1 { get; set; }
        public virtual tbl_users tbl_users { get; set; }
        public virtual tbl_users tbl_users1 { get; set; }
    }
}
