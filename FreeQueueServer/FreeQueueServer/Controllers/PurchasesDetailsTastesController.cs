﻿using FreeQueueServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FreeQueueServer.Controllers
{
    [Authorize]
    [RoutePrefix("api/PurchasesDetailsTastes")]
    public class PurchasesDetailsTastesController : ApiController
    {

        DB_freeQueueEntities1 DB = new DB_freeQueueEntities1();

        /// <summary>
        /// convert PurchasesDetailsTaste object to tbl_purchasesTastes object.
        /// </summary>
        /// <param name="purchasesDetailsTaste"></param>
        /// <returns>tbl_purchasesTastes</returns>
        public  tbl_purchasesTastes ConvertPurchasesTasteFromDTO(PurchasesDetailsTasteDTO purchasesDetailsTaste)
        {
            return new tbl_purchasesTastes()
            {
                Id = purchasesDetailsTaste.id,
                Taste = (purchasesDetailsTaste.taste != null) ? DB.tbl_menuTastes.FirstOrDefault(mt => mt.Id == purchasesDetailsTaste.taste).Id : 0,
                Product = purchasesDetailsTaste.product
            };
        }
        // GET: api/PurchasesDetailsTastes
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [Route("GetPurchaseTastes/{purchaseId}")]
        public IHttpActionResult GetPurchaseTastes(int purchaseId)
        {
            return Ok(PurchasesDetailsTasteDTO.ConvertToDTO(DB.tbl_purchasesTastes.Where(pt => pt.tbl_purchasesProducts.PurchaseId == purchaseId).ToList()));
        }
        [AllowAnonymous]
        [Route("GetPurchasesProductsTastes")]
        public IHttpActionResult Post([FromBody]List<PurchasesProductDTO> purchasesProducts)
        {
            //lock (DB)
            //{

            PurchasesProductController ppc = new PurchasesProductController();
                //null יש מצב שאין למוצר טעם לכן צריך לבדוק מ הקורה בהמרה של 
                List<tbl_purchasesProducts> purchasesProductsList = new List<tbl_purchasesProducts>();
                //convert the purchases products from DTO.
                purchasesProducts.ForEach(pp => purchasesProductsList.Add(ppc.ConvertPurchasesProductFromDTO(pp)));

                List<tbl_purchasesTastes> purchasesTastesList = new List<tbl_purchasesTastes>();
                try
                {

                    //select there tastes.
                    purchasesProductsList.ForEach(ppl => purchasesTastesList.Add(DB.tbl_purchasesTastes.FirstOrDefault(tpt => tpt.Product == ppl.Id)));
                }
                catch (NullReferenceException ex)
                {
                    return null;
                }
                purchasesTastesList.RemoveAll(ptl => ptl == null);

                return Ok(PurchasesDetailsTasteDTO.ConvertToDTO(purchasesTastesList));
            //}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="purchaseProductId"></param>
        /// <returns>taste of this purchase product</returns>
        [AllowAnonymous]
        [Route("GetProductsTastesToProduct/{purchaseProductId}")]
        [HttpGet]
        public IHttpActionResult GetProductsTastesToProduct(int purchaseProductId)
        {
            return Ok(PurchasesDetailsTasteDTO.ConvertToDTO(DB.tbl_purchasesTastes.FirstOrDefault(a => a.Product == purchaseProductId)));
        }


        // POST: api/PurchasesDetailsTastes
        public void Post([FromBody]string value)
        {
        }

        [AllowAnonymous]
        [Route("AddPurchasesDetailsTastes")]
        public IHttpActionResult post([FromBody]List<PurchasesDetailsTasteDTO> purchasesDetailsTastes)
        {
            purchasesDetailsTastes.ForEach(pdt => DB.tbl_purchasesTastes.Add(ConvertPurchasesTasteFromDTO(pdt)));
            DB.SaveChanges();
            return Ok(true);
        }

        // DELETE: api/PurchasesDetailsTastes/5
        public void Delete(int id)
        {
        }
    }
}
