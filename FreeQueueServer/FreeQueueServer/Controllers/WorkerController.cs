﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FreeQueueServer.Models;

namespace FreeQueueServer.Controllers
{
        [RoutePrefix("api/Worker")]
    public class WorkerController : ApiController
    {
        DB_freeQueueEntities1 DB = new DB_freeQueueEntities1();

        // GET: api/Worker
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("getWorkers/{storeId}")]
        // GET: api/Worker/5
        public IHttpActionResult Get(int storeId)
        {
            List<UsersDTO> users = new List<UsersDTO>();
            //select the workers from the users table using the worker to store table.
            DB.tbl_workerToStore.Where(wts => wts.StoreId == storeId).ToList().ForEach(wts1 => users.Add(UsersDTO.ConvertToDTO(DB.tbl_users.FirstOrDefault(u => wts1.UserId == u.Id))));
            return Ok(users);
        }

        // POST: api/Worker
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Worker/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Worker/5
        public void Delete(int id)
        {
        }
    }
}
