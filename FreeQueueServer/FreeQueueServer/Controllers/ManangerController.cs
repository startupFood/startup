﻿using FreeQueueServer.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace FreeQueueServer.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/Manager")]
    public class ManangerController : ApiController
    {
        DB_freeQueueEntities1 DB = new DB_freeQueueEntities1();

        //Images management:

        /// <summary>
        /// Gets an image file and an image name and add it to the right directory.
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="postedFile"></param>
        /// <param name="newName"></param>
        /// <returns>IHttpActionResult</returns>
        public bool UploadImage(int storeId, HttpPostedFile postedFile, string newName)
        {
            try
            {
                //Create custom filename
                if (postedFile != null)
                {
                    //+ postedFile.FileName
                    var filePath = HttpContext.Current.Server.MapPath("~/Models/Resources/" + storeId + "/");
                    if (Directory.Exists(filePath) == false)
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    postedFile.SaveAs(filePath + newName);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            throw new Exception("not found image");

        }

        /// <summary>
        /// Gets an image file, add it to the images folder and update the path to the cuurent path
        /// </summary>
        /// <param name="id"></param>
        /// <param name="store"></param>
        /// <returns></returns>
        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [HttpPost]
        [Route("UploadStoreImage/{storeId}")]
        [ResponseType(typeof(string))]
        public IHttpActionResult UploadStoreImage(int storeId)
        {
            var httpRequest = HttpContext.Current.Request;
            //Upload Image
            HttpPostedFile postedFile = httpRequest.Files["Image"];
            var newName = "store.jpg";
            try
            {
                UploadImage(storeId, postedFile, newName);
                var path= HttpContext.Current.Server.MapPath("~/Models/Resources/" + storeId + "/"+newName);
                DB.tbl_stores.FirstOrDefault(s => s.Id == storeId).Img = path;
                DB.SaveChanges();
                return Ok(DB.tbl_stores.FirstOrDefault(s => s.Id == storeId).Img);
            }
            catch
            {
                return BadRequest();
            }
        }

        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [HttpPost]
        [Route("UploadProductImage/{storeId}")]
        [ResponseType(typeof(string))]
        public IHttpActionResult UploadProductImage(int productId)
        {
            var httpRequest = HttpContext.Current.Request;
            //Upload Image
            HttpPostedFile postedFile = httpRequest.Files["Image"];
            var newName = "product"+ productId+ ".jpg";
            var storeId = DB.tbl_storesMenu.FirstOrDefault(p => p.Id == productId).Store;
            try
            {
                UploadImage(storeId, postedFile, newName);
                var path = HttpContext.Current.Server.MapPath("~/Models/Resources/" + storeId + "/" + newName);
                DB.tbl_stores.FirstOrDefault(s => s.Id == storeId).Img = path;
                DB.SaveChanges();
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [HttpPost]
        [Route("UploadAdditionImage/{storeId}")]
        [ResponseType(typeof(string))]
        public IHttpActionResult UploadAdditionImage(int additionId)
        {
            var httpRequest = HttpContext.Current.Request;
            //Upload Image
            HttpPostedFile postedFile = httpRequest.Files["Image"];
            var newName = "addition" + additionId + ".jpg";
            var storeId = DB.tbl_menuAddittions.FirstOrDefault(a => a.Id == additionId).tbl_storesMenu.Store;
            try
            {
                UploadImage(storeId, postedFile, newName);
                var path = HttpContext.Current.Server.MapPath("~/Models/Resources/" + storeId + "/" + newName);
                DB.tbl_stores.FirstOrDefault(s => s.Id == storeId).Img = path;
                DB.SaveChanges();
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        [Authorize(Roles = RolesNames.SYSTEM + ", " + RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [HttpPost]
        [Route("UploadTasteImage/{storeId}")]
        [ResponseType(typeof(string))]
        public IHttpActionResult UploadTasteImage(int tasteId)
        {
            var httpRequest = HttpContext.Current.Request;
            //Upload Image
            HttpPostedFile postedFile = httpRequest.Files["Image"];
            var newName = "taste" + tasteId + ".jpg";
            var storeId = DB.tbl_menuTastes.FirstOrDefault(a => a.Id == tasteId).tbl_storesMenu.Store;
            try
            {
                UploadImage(storeId, postedFile, newName);
                var path = HttpContext.Current.Server.MapPath("~/Models/Resources/" + storeId + "/" + newName);
                DB.tbl_stores.FirstOrDefault(s => s.Id == storeId).Img = path;
                DB.SaveChanges();
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        // GET: api/Mananger
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Mananger/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Mananger
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Mananger/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Mananger/5
        public void Delete(int id)
        {
        }
    }
}
