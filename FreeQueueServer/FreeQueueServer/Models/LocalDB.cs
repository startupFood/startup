﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreeQueueServer.Models
{
    public class LocalDB
    {
     

        public static List<PurchaseDTO> Purchases { get; set; }
        public static List<PurchasesProductDTO> PurchasesProducts { get; set; }
        public static List<PurchasesDetailsAdditionDTO> PurchasesAdditions { get; set; }
        public static List<PurchasesDetailsTasteDTO> PurchasesTastes { get; set; }
        public static List<GroupDTO> Groups { get; set; }
        public static StoresDTO Store { get; set; }

        public static string Result { get; set; }

        public static List<string> SmsMessages = new List<string>()
        {
            "your purchase is made!",
            "bon appetit!"
        };

    }
}