import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerMenuTastesComponent } from './manager-menu-tastes.component';

describe('ManagerMenuTastesComponent', () => {
  let component: ManagerMenuTastesComponent;
  let fixture: ComponentFixture<ManagerMenuTastesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerMenuTastesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerMenuTastesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
