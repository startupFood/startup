﻿using FreeQueueServer.Helpers;
using FreeQueueServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FreeQueueServer.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/Purchase")]
    public class PurchasesController : ApiController
    {

        DB_freeQueueEntities1 DB = new DB_freeQueueEntities1();

        /// <summary>
        /// gets object of PurchaseDTO class and converts it to object of type tbl_purchases
        /// </summary> 
        /// <param name="purchase"></param>
        /// <returns>tbl_purchases</returns>
        public static tbl_purchases ConvertPurchaseFromDto(PurchaseDTO purchase)
        {
            return new tbl_purchases()
            {
                Id = purchase.id,
                CustomerName = purchase.customerName,
                CustomerPhone = purchase.customerPhone,
                DeliveryAddress = purchase.deliveryAddress,
                Club = purchase.club,
                CreditCard = purchase.creditCard,
                CreditDate = purchase.creditDate,
                CreditDigits = purchase.creditDigits,
                GroupDetails =  purchase.groupId == 0 ? null : purchase.groupId,
                PurchaseDate = purchase.purchaseDate,
                PurchaseSum = purchase.purchaseSum,
                ReceiptTime = purchase.receiptTime,
                ReservedSeats = purchase.reservedSeats,
                StoreId = purchase.store,
                Tip = purchase.tip,
                Favorite = purchase.favorite,
                UserId = purchase.userId,
                Status = purchase.status
            };
        }
        // GET: api/Purchases
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Order
        /// <summary>
        /// Gets store id and returns all the orders of this store
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns>IHttpActionResult</returns>
        [Authorize]
        [Route("GetOrdersByStore/{id}")]
        public IHttpActionResult Get([FromBody] int storeId)
        {
            return Ok(PurchaseDTO.ConvertToDTO(DB.tbl_purchases.Where(p => p.StoreId == storeId).ToList()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="purchase"></param>
        /// <returns></returns>
        [Authorize(Roles = RolesNames.DEVELOPER + ", "  + ", " + RolesNames.CUSTOMER)]
        [Route("AddOrder")]
        public IHttpActionResult Post([FromBody]PurchaseDTO purchase)
        {
            try
            {
            tbl_purchases p = DB.tbl_purchases.Add(ConvertPurchaseFromDto(purchase));
            DB.SaveChanges();
            return Ok(p.Id);
            }
            catch
            {
                return BadRequest("cannot save this purchase.");
            }

        }




        /// <summary>
        /// update the status of the purchase that accepted to true(done).
        /// </summary>
        /// <param name="id"></param>
        /// <returns>true</returns>
        [AllowAnonymous]
        [Route("UpdateStatus/{id}")]
        [HttpGet]
        public IHttpActionResult UpdateStatus(int id)
        {
            DB.tbl_purchases.First(p => p.Id == id).Status = true;
            DB.SaveChanges();
            LocalDB.Purchases = PurchaseDTO.ConvertToDTO(DB.tbl_purchases.ToList());
            return Ok(true);
        }

        // PUT: api/Purchases/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Purchases/5
        public void Delete(int id)
        {
        }
    }
}
