import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ServerService } from './server.service';
import { StoresMenu } from '../classes/stores-menu';
import { Observable } from 'rxjs';
import { StoreMenuWithDetails } from '../classes/store-menu-with-details';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {


  
  private productsAndDetails:Array<StoreMenuWithDetails> = new Array<StoreMenuWithDetails>();
  private sortedList:Array<StoreMenuWithDetails> = new Array<StoreMenuWithDetails>();
  constructor(public httpClient:HttpClient,
    public serverServ:ServerService) { }


    get ProductsAndDetails()
    {
      return this.productsAndDetails;
    }

    set ProductsAndDetails(value:Array<StoreMenuWithDetails>)
    {
      this.productsAndDetails = value;
    }

    

    getProducts():Observable<StoresMenu[]>
    {
      return this.httpClient.get<StoresMenu[]>(this.serverServ.URL + "Menu/getProducts" , {headers:this.serverServ.getHeader()});
    }
}
