﻿using FreeQueueServer.Helpers;
using FreeQueueServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FreeQueueServer.Controllers
{
    //functions of the store menu
    [AllowAnonymous]
    [RoutePrefix("api/Menu")]
    public class MenuController : ApiController
    {
        DB_freeQueueEntities1 DB = new DB_freeQueueEntities1();

        /// <summary>
        /// convert product object from DTO object to SQL object
        /// </summary>
        /// <param name="product"></param>
        /// <returns>tbl_storesMenu</returns>
        public static tbl_storesMenu ConvertFromDto(StoresMenuDTO product)
        {
            return new tbl_storesMenu()
            {
                Id = product.id,
                ProductName = product.productName,
                Store = product.store,
                ProductPrice = product.productPrice,
                ProductCategory = product.productCategory,
                QuickProduct = product.quickProduct,
                PreperationTime = product.preperationTime,
                ProductImage = product.productImage,
                ProductStatus = product.productStatus,
                Barcode = product.barcode,
                AdditionsQuantity = product.additionQuantity
            };
        }

        /// <summary>
        /// convert MenuAddition object from DTO object to SQL object
        /// </summary>
        /// <param name="addittion"></param>
        /// <returns>tbl_storesMenu</returns>
        public static tbl_menuAddittions ConvertAdditionFromDto(MenuAdditionsDTO addittion)
        {
            return new tbl_menuAddittions()
            {
                Id = addittion.id,
                Addition = addittion.additionName,
                AddtionPrice = addittion.additionPrice,
                AdditionImage = addittion.additionImage,
                AdditionStatus = addittion.additionStatus,
                Product = addittion.product
            };
        }

        /// <summary>
        /// convert MenuAddition object from DTO object to SQL object
        /// </summary>
        /// <param name="addittion"></param>
        /// <returns>tbl_storesMenu</returns>
        public static tbl_menuTastes ConvertTasteFromDto(MenuTastesDTO taste)
        {
            return new tbl_menuTastes()
            {
                Id = taste.id,
                Taste = taste.tasteName,
                TasteImage = taste.tasteImage,
                TasteStatus = taste.tasteStatus,
                Product = taste.product
            };
        }

        /// <summary>
        /// convert product object from DTO object to SQL object
        /// </summary>
        /// <param name="product"></param>
        /// <returns>tbl_storesMenu</returns>
        public static tbl_storesMenu ConvertProductFromDto(StoresMenuDTO product)
        {
            return new tbl_storesMenu()
            {
                Id = product.id,
                ProductName = product.productName,
                Store = product.store,
                ProductPrice = product.productPrice,
                ProductCategory = product.productCategory,
                QuickProduct = product.quickProduct,
                PreperationTime = product.preperationTime,
                ProductImage = product.productImage,
                ProductStatus = product.productStatus
            };
        }


        // GET: api/Menu
        [HttpGet]
        [Route("getProducts")]
        [AllowAnonymous]
        public IHttpActionResult Get()
        {
            return Ok(StoresMenuDTO.ConvertToDTO(DB.tbl_storesMenu.ToList()));
        }

        // GET: api/Menu/5
        /// <summary>
        /// get store id and return the products of this store
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns>IHttpActionResult</returns>
        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.MANAGER + ", " + RolesNames.STORE + ", " + RolesNames.CUSTOMER)]
        [Route("GetByStore/{storeId}")]
        public IHttpActionResult Get(int storeId)
        {
            var menu = StoresMenuDTO.ConvertToDTO(DB.tbl_storesMenu.Where(m => m.Store == storeId).ToList());
            return Ok(menu);
        }

        [Authorize]
        [Route("GetMenuCategoriesByStore/{storeId}")]
        public IHttpActionResult GetMenuCategories(int storeId)
        {
            //return a list with all the categories that belong to this store.
            return Ok(ProductsCategoryDTO.ConvertToDTO(DB.tbl_productsCategories.Where(c => c.Store == storeId).ToList()));
        }

        [Authorize]
        [Route("GetStoreQuickProducts/{id}")]
        public IHttpActionResult GetQuickItems(int id)
        {
            return Ok(StoresMenuDTO.ConvertToDTO(DB.tbl_storesMenu.Where(m => m.Store == id && m.QuickProduct == true).ToList()));
        }


        /// <summary>
        /// pull out the menu additions list of the store
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns>IHttpActionResult</returns>
        [Authorize]
        [Route("GetAdditionsByStore/{storeId}")]
        public IHttpActionResult GetAdditions(int storeId)
        {
            var q = MenuAdditionsDTO.ConvertToDTO(DB.tbl_menuAddittions.Where(a => a.tbl_storesMenu.Store == storeId).ToList());
            return Ok(q);
        }

        /// <summary>
        /// pull out the menu additions list of the store
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns>IHttpActionResult</returns>
        [Authorize]
        [Route("GetAdditionsByProduct/{productId}")]
        public IHttpActionResult GetAdditionsByProduct(int productId)
        {
            var q = MenuAdditionsDTO.ConvertToDTO(DB.tbl_menuAddittions.Where(a => a.Product == productId).ToList());
            return Ok(q);
        }

        /// <summary>
        /// returns the additions of the storeMenu product that accepted. 
        /// </summary>
        /// <param name="storeMenuId"></param>
        /// <returns>list of MenuAdditionsDTO</returns>
        public List<MenuAdditionsDTO> GetAdditionsByStoreMenu(int storeMenuId)
            {
               var q = MenuAdditionsDTO.ConvertToDTO(DB.tbl_menuAddittions.Where(a => a.Product == storeMenuId).ToList());
            return q;
    }


       



        /// <summary>
        /// pull out the menu additions list of the store
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns>IHttpActionResult</returns>
        [Authorize]
        [Route("GetTastesByProduct/{productId}")]
        public IHttpActionResult GetTastesByProduct(int productId)
        {
            var q = MenuTastesDTO.ConvertToDTO(DB.tbl_menuTastes.Where(t => t.Product == productId).ToList());
            return Ok(q);
        }

        /// <summary>
        /// pull out the menu tastes list of the store
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns>IHttpActionResult</returns>
        [Authorize]
        [Route("GetTastesByStore/{storeId}")]
        public IHttpActionResult GetTastes(int storeId)
        {
            var storeTastes = MenuTastesDTO.ConvertToDTO(DB.tbl_menuTastes.Where(t => t.tbl_storesMenu.Store == storeId).ToList());
            return Ok(storeTastes);
        }

        // POST: api/Menu
        /// <summary>
        /// add product to DB
        /// </summary>
        /// <param name="product"></param>
        /// <returns>IHttpActionResult</returns>
        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [Route("AddProduct")]
        public IHttpActionResult Post([FromBody]StoresMenuDTO product)
        {
            DB.tbl_storesMenu.Add(ConvertFromDto(product));
            DB.SaveChanges();
            return Ok(StoresMenuDTO.ConvertToDTO(DB.tbl_storesMenu.Where(m => m.Store == product.store).ToList()));
        }

        // POST: api/Menu
        /// <summary>
        /// add addition to DB
        /// </summary>
        /// <param name="addition"></param>
        /// <returns>IHttpActionResult</returns>
        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [Route("AddAddition")]
        public IHttpActionResult Post([FromBody]MenuAdditionsDTO addition)
        {
            DB.tbl_menuAddittions.Add(ConvertAdditionFromDto(addition));
            DB.SaveChanges();
            var storeId = DB.tbl_storesMenu.FirstOrDefault(m => m.Id == addition.product).Store;
            return Ok(MenuAdditionsDTO.ConvertToDTO(DB.tbl_menuAddittions.Where(a => a.tbl_storesMenu.Store == storeId).ToList()));
        }

        // POST: api/Menu
        /// <summary>
        /// add addition to DB
        /// </summary>
        /// <param name="taste"></param>
        /// <returns>IHttpActionResult</returns>
        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [Route("AddTaste")]
        public IHttpActionResult Post([FromBody]MenuTastesDTO taste)
        {
            DB.tbl_menuTastes.Add(ConvertTasteFromDto(taste));
            DB.SaveChanges();
            var storeId = DB.tbl_storesMenu.FirstOrDefault(m => m.Id == taste.product).Store;
            return Ok(MenuTastesDTO.ConvertToDTO(DB.tbl_menuTastes.Where(t => t.tbl_storesMenu.Store == storeId).ToList()));
        }

        // PUT: api/Menu/5
        /// <summary>
        /// update product in DB
        /// </summary>
        /// <param name="id"></param>
        /// <param name="product"></param>
        /// <returns></returns>
        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [Route("UpdateProduct/{id}")]
        public IHttpActionResult Put(int id, [FromBody]StoresMenuDTO product)
        {
            DB.Entry(ConvertFromDto(product)).State = System.Data.Entity.EntityState.Modified;
            DB.SaveChanges();
            return Ok(StoresMenuDTO.ConvertToDTO(DB.tbl_storesMenu.Where(m => m.Store == product.store).ToList()));
        }

        // PUT: api/Menu/5
        /// <summary>
        /// update product in DB
        /// </summary>
        /// <param name="id"></param>
        /// <param name="product"></param>
        /// <returns></returns>
        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [Route("UpdateAddition/{id}")]
        public IHttpActionResult PutAddition(int id, [FromBody]MenuAdditionsDTO addition)
        {
            DB.Entry(ConvertAdditionFromDto(addition)).State = System.Data.Entity.EntityState.Modified;
            DB.SaveChanges();
            return Ok(MenuAdditionsDTO.ConvertToDTO(DB.tbl_menuAddittions.Where(a=>a.Product==addition.product).ToList()));
        }

        // PUT: api/Menu/5
        /// <summary>
        /// update product in DB
        /// </summary>
        /// <param name="id"></param>
        /// <param name="product"></param>
        /// <returns></returns>
        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [Route("UpdateTaste/{id}")]
        public IHttpActionResult PutTaste(int id, [FromBody]MenuTastesDTO taste)
        {
            DB.Entry(ConvertTasteFromDto(taste)).State = System.Data.Entity.EntityState.Modified;
            DB.SaveChanges();
            return Ok(MenuTastesDTO.ConvertToDTO(DB.tbl_menuTastes.Where(t => t.Product == taste.product).ToList()));
        }

        // DELETE: api/Menu/5
        /// <summary>
        /// delete product from DB
        /// </summary>
        /// <param name="id"></param>
        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [Route("DeleteProduct/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var store = DB.tbl_storesMenu.FirstOrDefault(p => p.Id == id).Store;
                foreach (var a in DB.tbl_menuAddittions.Where(aa => aa.Product == id).ToList())
                {
                    DB.tbl_menuAddittions.Remove(a);
                }
                foreach (var t in DB.tbl_menuTastes.Where(tt => tt.Product == id).ToList())
                {
                    DB.tbl_menuTastes.Remove(t);
                }

                DB.tbl_storesMenu.Remove(DB.tbl_storesMenu.FirstOrDefault(m => m.Id == id));
                DB.SaveChanges();
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        // DELETE: api/Menu/5
        /// <summary>
        /// delete product from DB
        /// </summary>
        /// <param name="id"></param>
        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [Route("DeleteAddition/{id}")]
        [HttpDelete]
        public IHttpActionResult Addition(int id)
        {
            DB.tbl_menuAddittions.Remove(DB.tbl_menuAddittions.FirstOrDefault(a => a.Id == id));
            DB.SaveChanges();
            return Ok();
        }

        // DELETE: api/Menu/5
        /// <summary>
        /// delete product from DB
        /// </summary>
        /// <param name="id"></param>
        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [Route("DeleteTaste/{id}")]
        public IHttpActionResult DeleteTaste(int id)
        {
            DB.tbl_menuTastes.Remove(DB.tbl_menuTastes.FirstOrDefault(t => t.Id == id));
            DB.SaveChanges();
            return Ok();
        }

        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [Route("ChangeStatus/{status}")]
        public IHttpActionResult ChangeStatus(int status)
        {
            try
            {
                //switch(status.type)
                //{
                //    case "product": DB.tbl_storesMenu.FirstOrDefault(m => m.Id == status.id).ProductStatus = status.status; break;
                //    case "addition": DB.tbl_menuAddittions.FirstOrDefault(a => a.Id == status.id).AdditionStatus = status.status; break;
                //    case "taste": DB.tbl_menuTastes.FirstOrDefault(t => t.Id == status.id).TasteStatus = status.status; break;
                //}
                DB.SaveChanges();
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetMenuCategories")]
        [AllowAnonymous]
        public IHttpActionResult GetAllCategories()
        {
            return Ok(ProductsCategoryDTO.ConvertToDTO(DB.tbl_productsCategories.ToList()));
        }
    }
    //public struct StatusStruct
    //{
    //    public string type;
    //    public int id;
    //    public bool status;
    //}
}