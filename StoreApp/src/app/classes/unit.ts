export class Unit {

    static getNames(obj:Object):string[]
    {
        const result = [];
        for(let property in obj)
        {
            if(obj.hasOwnProperty(property))
            {
                result.push(property);
            }
        }
        return result;
    }
}
