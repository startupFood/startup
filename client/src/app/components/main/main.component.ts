import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'src/app/classes/user';
import { ServerService } from 'src/app/services/server.service';
import { Store } from 'src/app/classes/store';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  userName: string = "";
  login: boolean;
  storePage: boolean = true;
  cart: boolean;
  menu: boolean;
  product: boolean;
  products: boolean;
  storesList: boolean;
  order: boolean = false;
  public Store: Store;
  constructor(private router: Router,
    private userServ: UsersService,
    private serverServ: ServerService,
    public storeServ: StoreService) {

    this.login = true;
    
    var user: User = JSON.parse(localStorage.getItem('user'));
    if (user) {
      this.userName = user.userName;
    }
    else
      this.userName = "Guest";
    this.serverServ.Component.subscribe(data => {
      
      // 0 -login
      //1 - storesList , products
      //2 - store , menu , product , cart
      //3 - menu , cart
      switch (data) {
        case 0:
          user = JSON.parse(localStorage.getItem('user'));
          if (user) {
            this.userName = user.userName;
          }
          else
            this.userName = "Guest";
          this.login = false;
          this.product = this.products = this.menu = this.cart = this.storesList = this.storePage = false;
          break;
        case 1:
          this.storesList = this.products = true;
          this.menu = this.cart = this.product = this.storePage = this.login = false;
          break;
        case 2:
          this.Store = this.storeServ.Store;
          this.menu = this.cart = this.product = true;
          this.storesList = this.products = this.storePage = this.login = false;
          break;
        case 3:
          this.storePage = true;
          this.menu = this.cart = this.storesList = this.products = this.login = this.product = false;
      }
    })


  }
  profile: boolean = false;
  changeProfile() {
    this.profile = !this.profile;
  }

  ngOnInit() {
  }

  signOut() {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    location.reload();
  }


}
