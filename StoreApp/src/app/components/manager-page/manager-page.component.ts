import { Component, OnInit } from '@angular/core';
import { ManagerService } from 'src/app/services/manager.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-manager-page',
  templateUrl: './manager-page.component.html',
  styleUrls: ['./manager-page.component.css']
})
export class ManagerPageComponent implements OnInit {


  workers:boolean = false;
  menu:boolean = false;
  store:boolean = false;
  history:boolean = false;
  constructor(private managerServ:ManagerService,
    private userServ:UsersService) { 
    
this.userServ.loginFlag.next(false);
 
  }

  ngOnInit() {
    debugger;
    /* 
    this.managerServ.getStoreMenu().subscribe(
      data1=>{this.managerServ.Menu=data1}
    );
    this.managerServ.getStoreAdditions().subscribe(
      data2=>{this.managerServ.Additions=data2}
    );
    this.managerServ.getStoreTastes().subscribe(
      data3=>{this.managerServ.Tastes=data3}
    ); */

  }


  change(page:number)
  {
    switch(page)
    {
      case 0:
        this.workers = true;
        this.menu = this.store = this.history = false;
        break;
        case 1:
          this.menu = true;
          this.store = this.workers = this.history = false;
          break;
          case 2:
            this.store = true;
            this.workers = this.menu = this.history = false;
            break;
            case 3:
              this.history = true;
              this.menu = this.store = this.workers = false;
    }
  }

}
