import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { NgForm } from '@angular/forms';
import { PersonDetails } from 'src/app/classes/person-details';
import { OrderService } from 'src/app/services/order.service';



@Component({
  selector: 'app-order-personal-details',
  templateUrl: './order-personal-details.component.html',
  styleUrls: ['./order-personal-details.component.css']
})




export class OrderPersonalDetailsComponent implements OnInit {

  constructor(private cartServ: CartService, private orderServ: OrderService) {

    this.name = this.cartServ.Order.customerName;
    this.phone = this.cartServ.Order.customerPhone;


    this.orderServ.personal.subscribe(
      data => {
        this.savePersonalDetails();
      }
    )
  }

  public name: string;
  public phone: string;

  ngOnInit() {
  }

  @Output("get-personal") getPersonal: EventEmitter<number> = new EventEmitter();
  @Output("expand-div") expandDiv: EventEmitter<Boolean> = new EventEmitter();
  @Output("validation") validation: EventEmitter<Boolean> = new EventEmitter();

  public group: boolean = false;

  expandGroup() {
    if (this.group) {
      this.expandDiv.emit(false);
    }
    else {
      this.expandDiv.emit(true);
    }
    this.group = !this.group;

  }
  savePersonalDetails() {
    this.cartServ.SetPersonalDetails(this.name, this.phone);
    // this.getPersonal.emit(0);
    this.expandDiv.emit(false);
  }

  isValid() {
    if (this.name != "" && this.phone != "")
      this.validation.emit(true);
    else
      this.validation.emit(false);
  }

}

