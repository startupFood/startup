import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { UsersService } from 'src/app/services/users.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
export class EmailErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}


export interface UserDialogData {
  userId:number;
}
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private usersServ:UsersService, public dialogRef: MatDialogRef<ForgotPasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserDialogData) {

      
     }

  ngOnInit() {
  }
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new EmailErrorStateMatcher();

  
  initPassword(emailAddress:string)
  {
    this.usersServ.initPassword(emailAddress).subscribe(
      data=>{data}
    );
  }

  submitted = false;

  onSubmit(emailAddress) {
    this.submitted = true;
    this.usersServ.initPassword(emailAddress).subscribe(
      data=>{data}
    );
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

}
