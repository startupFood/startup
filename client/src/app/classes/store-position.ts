export class StorePosition {
    constructor(
        public id:number,
        public storeId:number,
        public Longitude:number,
        public Latitude:number
    ) {}
}

