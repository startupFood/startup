import { Component, OnInit } from '@angular/core';
import { Store } from 'src/app/classes/store';
import { StoreService } from 'src/app/services/store.service';
import { UsersService } from 'src/app/services/users.service';
import { ManagerService } from 'src/app/services/manager.service';

@Component({
  selector: 'app-store-page',
  templateUrl: './store-page.component.html',
  styleUrls: ['./store-page.component.css']
})
export class StorePageComponent implements OnInit {

  thisStore:Store;
  favoritePurchases:boolean=false;
  constructor(private storeServ:StoreService, private usersServ:UsersService, private managerServ:ManagerService) { 
    //we have to do in the previous components:
    //init the storeId in the service with the id
    //call to the function initStore():
    storeServ.initStore(1);

    this.managerServ.getStoreDetails().subscribe(
      data=> {this.thisStore=data}
    )
  }

  ngOnInit() {
    if(this.usersServ.UserId!=0)
      this.favoritePurchases=true;
  }

}
