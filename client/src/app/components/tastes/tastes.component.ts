import { Component, OnInit, Input } from '@angular/core';
import { MenuTastes } from 'src/app/classes/menu-tastes';
import { StoreService } from 'src/app/services/store.service';
import { CartService } from 'src/app/services/cart.service';
import { PurchaseDetailsTaste } from 'src/app/classes/purchase-details-taste';
import { FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-tastes',
  templateUrl: './tastes.component.html',
  styleUrls: ['./tastes.component.css']
})
export class TastesComponent implements OnInit {

  @Input() productId: number;

  productsTastes: Array<MenuTastes>;

  tastes:boolean=true;
  chosenTaste:number=0;

  constructor(private storeServe: StoreService, private cartServ: CartService) {
  }

  changeStatus()
  {
    this.tastes=!this.tastes;
  }


  ngOnInit() {
    this.productsTastes = new Array<MenuTastes>();
    this.storeServe.getProductTastes(this.productId).subscribe(
      data => {
        this.productsTastes = data.filter(t => t.product == this.productId);
        if(this.productsTastes.length>0)
          this.chosenTaste=this.productsTastes[0].id;
          this.tasteChosen(this.chosenTaste);
        //this.productsTastes.forEach(tst=> this.productsTastes.push(tst));
      }
    );
  }


  afterLoading() {
    
    if(document.getElementsByClassName('tasteDiv').length>0)
    {
      //document.getElementsByClassName('tasteDiv')[0].classList.add("chosenTasteDiv");
      this.tasteChosen(Number.parseInt(document.getElementsByClassName('tasteDiv')[0].id.split('e')[1]));
    }
  }

  tasteChosen(tasteId: number): void {
    this.chosenTaste=tasteId;
    
    //select the taste from the tastes array.
    let productsTaste = this.productsTastes.find(t => t.id == tasteId);

    this.cartServ.tasteChangedSubject.next(new PurchaseDetailsTaste(0, productsTaste.product, productsTaste.id));
    //emit the taste to the product component.
    this.cartServ.tasteCahnged.emit(new PurchaseDetailsTaste(0, productsTaste.product, productsTaste.id));
  }

}
