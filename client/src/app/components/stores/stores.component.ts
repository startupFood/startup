/// <reference types="@types/googlemaps" />
import { Component, OnInit } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { StoreService } from 'src/app/services/store.service';
import { Observable, Observer } from 'rxjs';
import { Store } from 'src/app/classes/store';
import { Hechshers } from 'src/app/classes/hechshers';
import { StoresCategory } from 'src/app/classes/stores-category';
import { StoreAndDistance } from 'src/app/classes/store-and-distance';
import { Router } from '@angular/router';
import { ServerService } from 'src/app/services/server.service';
import { ProductsService } from 'src/app/services/products.service';
import { StoreMenuWithDetails } from 'src/app/classes/store-menu-with-details';
declare var google: any;


interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

interface Location {
  lat: number;
  lng: number;
  viewport?: Object;
  zoom: number;
  address_level_1?: string;
  address_level_2?: string;
  address_country?: string;
  address_zip?: string;
  address_state?: string;
  marker?: Marker;
}

@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.css']
})
export class StoresComponent implements OnInit {

  //user current position lng & lat
  userAddressPoint = { lng: 0, lat: 0 };
  userAddress = "";
  sort: boolean = false;
  sortBy: string = "";
  filter: string = "";
  categoryFilterNum: number;
  kashrutFilterNum: number;

  storesListData: Array<Store>;
  storesList: Array<StoreAndDistance> = new Array<StoreAndDistance>();
  fullStoresList: Array<StoreAndDistance> = new Array<StoreAndDistance>();
  hechshersList: Array<Hechshers>;
  categoriesList: Array<StoresCategory>;

  err = false;
  load = true;

  constructor(private storeServ: StoreService,
    public mapsAPILoader: MapsAPILoader,
    public router: Router,
    private serverServ: ServerService,
    public productsServ: ProductsService) {
    //to dispaly the match icons in the header.
    this.serverServ.Component.next(1);

    sessionStorage.removeItem("productsCart");
    sessionStorage.removeItem("tastesCart");
    sessionStorage.removeItem("additionsCart");
  }

  ngOnInit() {
    //Load mapsAPILoader and init data when the component is on init.
    this.mapsAPILoader.load().then(() => {
      this.afterLoading = true;
      this.initData();
    }
    );
  }

  initData() {
    //get user position
    if (navigator) {
      navigator.geolocation.getCurrentPosition(pos => {
        this.userAddressPoint.lng = +pos.coords.longitude;
        this.userAddressPoint.lat = +pos.coords.latitude;
        this.geocode(new google.maps.LatLng(this.userAddressPoint.lat, this.userAddressPoint.lng)).subscribe(data => { this.userAddress = data[0].formatted_address });
      });
    };

    this.storeServ.getStoresList().subscribe(
      data => {
        this.storesListData = data;
        this.storeServ.Stores = data;
      },
      err => { err },
      () => { this.sortStoresList(this.storesListData) }
    );

    //get catrgories list
    this.storeServ.getStoresCategories().subscribe(
      data => {
        this.categoriesList = data;
        this.load = false;
        this.storeServ.StoresCategories = data;
      },
      err => { this.err = true }
    );

    //get kashruts list
    this.storeServ.getStoresHechshers().subscribe(
      data => {
        this.hechshersList = data;
        this.storeServ.Hechshers = data;
      }
    );

    //for geocoding- getting address from point numbers.
    this.geocoder = new google.maps.Geocoder();
    var x: google.maps.GeocoderResult[];
  }

  afterLoading: boolean = false;


  //geocoding
  geocoder: google.maps.Geocoder;

  //Gets an literal address and converts it to lng & lat point.
  codeAddress(address: string): Observable<google.maps.GeocoderResult[]> {
    return Observable.create((observer: Observer<google.maps.GeocoderResult[]>) => {
      // Invokes geocode method of Google Maps API geocoding.
      this.geocoder.geocode({ address: address }, (
        (results: google.maps.GeocoderResult[], status: google.maps.GeocoderStatus) => {
          if (status === google.maps.GeocoderStatus.OK) {
            observer.next(results);
            observer.complete();
          } else {
            console.log(
              'Geocoding service: geocode was not successful for the following reason: '
              + status
            );
            observer.error(status);
          }
        })
      );
    });
  }


  //Sorts the stores list when the user choose new place
  getAddress(event) {
    this.filter = "";
    this.userAddressPoint.lat = event.geometry.location.lat();
    this.userAddressPoint.lng = event.geometry.location.lng();

    this.storesList.forEach(s => {
      var distance = this.calculateDistance(s.latitude, s.longitude);
      s.distance = distance;
    });


    this.storesList = this.storesList.sort((p1, p2) => {
      if (p1.distance > p2.distance) {
        return 1;
      }
      if (p1.distance < p2.distance) {
        return -1;
      }
      return 0;
    }).splice(0, 16);
    this.sort = false;

    //this.sortStoresList(this.storesListData);
  }

  //This function is called when a store is chosen
  storeCohsen(storeId) {
    this.storeServ.storeId = storeId;
    this.router.navigate(["storePage"]);
  }

  //calculate the distance between the user position and the store position
  calculateDistance(lat, lng): number {
    //variable for the user position
    const p1 = new google.maps.LatLng(
      this.userAddressPoint.lat,
      this.userAddressPoint.lng
    );
    //variable for the store position
    const p2 = new google.maps.LatLng(
      lat,
      lng
    );
    //calculate the distance and return it.
    return (
      Number.parseFloat((google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000
      ).toString()));
  }

  //get the stores list, and create new list of the stores and the distances, sorted by the distance
  sortStoresList(storesData: Store[]) {
    //init the last list
    this.storesList = new Array<StoreAndDistance>();
    //create new list with store and distance
    storesData.forEach(store => {
      //calculate distance of store
      this.storeServ.getStorePosition(store.id).subscribe(
        data => {
          var distance = this.calculateDistance(data.Latitude, data.Longitude);
          this.storesList.push(
            new StoreAndDistance(store, distance, data.Longitude, data.Latitude)
          );
        });
    });
    this.fullStoresList = this.storesList;
    //sort the list in order to the distance from the store
    this.storesList = this.storesList.sort((p1, p2) => {
      if (p1.distance > p2.distance) {
        return 1;
      }
      if (p1.distance < p2.distance) {
        return -1;
      }
      return 0;
    });
  }

  //Gets lat and lng point and converts it to an literal address.
  geocode(latLng: google.maps.LatLng): Observable<google.maps.GeocoderResult[]> {
    return Observable.create((observer: Observer<google.maps.GeocoderResult[]>) => {
      // Invokes geocode method of Google Maps API geocoding.
      this.geocoder.geocode({ location: latLng }, (
        (results: google.maps.GeocoderResult[], status: google.maps.GeocoderStatus) => {
          if (status === google.maps.GeocoderStatus.OK) {
            observer.next(results);
            observer.complete();
          } else {
            console.log('Geocoding service: geocoder failed due to: ' + status);
            observer.error(status);
          }
        })
      );
    });
  }

  sortList() {
    if (this.sortBy = "distance")
      this.sortByDistance();
    else if (this.sortBy = "name")
      this.sortByName();
    else if (this.sortBy = "category")
      this.sortByCategory();
  }

  categoryFilter(filter: number) {
    this.categoryFilterNum = filter;
    this.storesList = this.fullStoresList.filter(s => s.store.storeCategory == filter);
    this.sortList();
    this.filter = "";
  }

  kashrutFilter(filter: number) {
    this.kashrutFilterNum = filter;
    this.storesList = this.fullStoresList.filter(s => s.store.kashrutCertifiction == filter);
    this.sortList();
    this.filter = "";
  }

  changeFilter(filter: string) {
    if (filter == this.filter)
      this.filter = "";
    else
      this.filter = filter;
    this.sort = false;
  }

  changeSort(sort: string) {
    if (this.sortBy == sort)
      this.sortBy = "";
    else
      this.sortBy = sort;
  }

  sortByDistance() {
    this.sortBy = "distance";
    this.storesList = this.storesList.sort((p1, p2) => {
      if (p1.distance > p2.distance) {
        return 1;
      }
      if (p1.distance < p2.distance) {
        return -1;
      }
      return 0;
    }).splice(0, 16);
    this.sort = false;
  }
  sortByName() {
    this.sortBy = "name";
    this.storesList = this.storesList.sort((p1, p2) => {
      if (p1.store.storeName > p2.store.storeName) {
        return 1;
      }
      if (p1.store.storeName < p2.store.storeName) {
        return -1;
      }
      return 0;
    });
    this.sort = false;
  }
  sortByCategory() {
    this.sortBy = "category";
    //this.storesList=this.storesList.sort((a, b) => a.distance < b.distance ? -1 : a.distance > b.distance ? 1 : 0);
    this.sort = false;
  }

  getCategory(category: number): string {

    var categoryStr = this.categoriesList.find(c => c.Id == category)
    return categoryStr ? categoryStr.Category : "";
  }

  getKashrut(kashrut: number): string {
    var hechsher = this.hechshersList.find(h => h.Id == kashrut);
    return hechsher ? hechsher.Hechsher : "";
  }

  getKashrutImg(kashrut: number): string {
    var hechsher = this.hechshersList.find(h => h.Id == kashrut);
    return hechsher ? hechsher.HechsherImg : "";
  }

  kashrutHasImg(kashrut: number): boolean {
    var hechsher = this.hechshersList.find(h => h.Id == kashrut);
    // if (hechsher != null && hechsher.HechsherImg != null && hechsher.HechsherImg != "")
    //   return true;
    return false;
  }

  clearFilters() {
    this.filter = "";
    this.sort = false;
    this.ngOnInit();
  }

  products() {

    this.storeServ.getAllStoreMenuCategories().subscribe(storesMenuCategories => {


      //init the list with product and its details.
      this.productsServ.getProducts().subscribe(
        data => {
          data.forEach(d => {
            let location: number = 0;
            let store: Store = this.storeServ.Stores.find(store => store.id == d.store);
            let hechser: string = this.storeServ.Hechshers.find(h => h.Id ==
              //find the hechser.
              store.kashrutCertifiction
            ).Hechsher;
            this.productsServ.ProductsAndDetails.push(
              new StoreMenuWithDetails(d,
                location,
                hechser,
                store,
                this.storeServ.StoresCategories.find(sc => sc.Id == store.storeCategory).Category,
                storesMenuCategories.find(smc => smc.id == d.productCategory).category
              ));
          });
          this.router.navigate(['/products']);
        })
    }
    );
  }
}
