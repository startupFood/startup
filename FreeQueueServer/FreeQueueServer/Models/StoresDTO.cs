﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreeQueueServer.Models
{
    public class StoresDTO
    {
        public int id { get; set; }
        public string storeName { get; set; }
        public string storeAddress { get; set; }
        public string phone { get; set; }
        public string about { get; set; }
        public int? kashrutCertifiction { get; set; }
        public string img { get; set; }
        public int storeCategory { get; set; }
        public bool? reservedSeats { get; set; }
        public bool? club { get; set; }
        public bool? tip { get; set; }
        public bool storeLoad { get; set; }

        public static StoresDTO ConvertToDTO(tbl_stores store)
        {
            return new StoresDTO()
            {
                id = store.Id,
                storeName = store.StoreName,
                storeAddress = store.StoreAddress,
                phone = store.Phone,
                about = store.Phone,
                kashrutCertifiction = store.KashrutCertification,
                img = store.Img,
                storeCategory = store.StoreCategory,
                reservedSeats = store.ReservedSeats,
                club = store.Club,
                tip = store.Tip,
                storeLoad = store.StoreLoad
            };
        }

        public static List<StoresDTO> ConvertToDTO(List<tbl_stores> stores)
        {
            return stores.Select(s => ConvertToDTO(s)).ToList();
        }
    }
}