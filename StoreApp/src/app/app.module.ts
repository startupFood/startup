import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AgGridModule } from 'ag-grid-angular';
import { MatFormFieldModule} from '@angular/material/form-field';

import { AppComponent } from './app.component';
import { OpenPageComponent } from './components/open-page/open-page.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { RouterModule } from '@angular/router';
import { RoutingModule } from './routing/routing/routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManagerPageComponent } from './components/manager-page/manager-page.component';
import { ManagerStockComponent } from './components/manager-stock/manager-stock.component';
import { ManagerStoreComponent } from './components/manager-store/manager-store.component';
import { ManagerWorkersComponent } from './components/manager-workers/manager-workers.component';
import { CellCustomComponent } from './helpers/cell-custom/cell-custom.component';
import { ManagerEditWorkerComponent } from './components/manager-edit-worker/manager-edit-worker.component';
import { StoreFormComponent } from './components/store-form/store-form.component';
import { StorePageComponent } from './components/store-page/store-page.component';

import {NgModule} from '@angular/core';

//angular material
import {A11yModule} from '@angular/cdk/a11y';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {PortalModule} from '@angular/cdk/portal';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBadgeModule} from '@angular/material/badge';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDialogRef} from '@angular/material/dialog';


import {MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material/dialog';
import {MatDialogConfig} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTreeModule} from '@angular/material/tree';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ManagerMenuComponent } from './components/manager-menu/manager-menu.component';
import { ManagerMenuAdditionsComponent } from './components/manager-menu-additions/manager-menu-additions.component';

//kendo-angular-grid
import { GridModule } from '@progress/kendo-angular-grid';
import { ManagerMenuTastesComponent } from './components/manager-menu-tastes/manager-menu-tastes.component';
import { ManagerEditProductComponent } from './components/manager-edit-product/manager-edit-product.component';
import { ManagerEditAdditionComponent } from './components/manager-edit-addition/manager-edit-addition.component';
import { ManagerEditTasteComponent } from './components/manager-edit-taste/manager-edit-taste.component';

import { AgmCoreModule } from '@agm/core';
import { PurchasesDetailsComponent } from './components/purchases-details/purchases-details.component';
import { TodaysHistoryComponent } from './components/todays-history/todays-history.component';
import { HistorySearchPipe } from './pipes/history-search.pipe';
import { PurchasePopUpComponent } from './components/todays-history/purchase-pop-up/purchase-pop-up.component';
import { MadeGroupPopUpComponent } from './components/open-page/made-group-pop-up/made-group-pop-up.component';
import { SmsPopUpComponent } from './components/open-page/sms-pop-up/sms-pop-up.component';
import { WorkerComponent } from './components/worker/worker.component';
import { LoginWorkerPopUpComponent } from './components/worker/login-worker-pop-up/login-worker-pop-up.component';





@NgModule({
  declarations: [
    AppComponent,
    OpenPageComponent,
    LoginComponent,
    SignupComponent,
    ManagerPageComponent,
    ManagerStockComponent,
    ManagerStoreComponent,
    ManagerWorkersComponent,
    CellCustomComponent,
    ManagerEditWorkerComponent,
    StoreFormComponent,
    StorePageComponent,
    ManagerMenuComponent,
    ManagerMenuAdditionsComponent,
    ManagerMenuTastesComponent,
    ManagerEditProductComponent,
    ManagerEditAdditionComponent,
    ManagerEditTasteComponent,
    PurchasesDetailsComponent,
    TodaysHistoryComponent,
    HistorySearchPipe,
    PurchasePopUpComponent,
    MadeGroupPopUpComponent,
    SmsPopUpComponent,
    WorkerComponent,
    LoginWorkerPopUpComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    RoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AgGridModule.withComponents([]),
    MatSliderModule,
    MatFormFieldModule,
    MatInputModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAj13gY0dRy3LKgxfbOkCPaqq_twe8eR3k',
      libraries: ["places", "geometry"]
      
      //our free key.
     // apiKey:'AIzaSyALujvBFSM3i5kobYS-X8FGXsVfcurUyV4'
    }),

    //angular material
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    A11yModule,
    DragDropModule,
    PortalModule,
    ScrollingModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    BrowserAnimationsModule,
    GridModule

  ],
  providers: [
// { provide: MatDialogRef, useValue: {} }
{provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
     ],
  entryComponents: [CellCustomComponent , PurchasePopUpComponent ,MadeGroupPopUpComponent , SmsPopUpComponent , LoginWorkerPopUpComponent ],
  bootstrap: [AppComponent],
  exports:[
   TodaysHistoryComponent, OpenPageComponent ,WorkerComponent
  ]
},)
export class AppModule { }
