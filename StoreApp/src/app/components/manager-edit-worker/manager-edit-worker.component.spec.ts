import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerEditWorkerComponent } from './manager-edit-worker.component';

describe('ManagerEditWorkerComponent', () => {
  let component: ManagerEditWorkerComponent;
  let fixture: ComponentFixture<ManagerEditWorkerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerEditWorkerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerEditWorkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
