export class Store {
    constructor(public id:number,
        public storeName:string,
        public storeAddress,
        public phone:string,
        public about:string,
        public kashrutCertifiction:number,
        public img:string,
        public storeCategory:number,
        public reservedSeats:boolean,
        public club:boolean,
        public tip:boolean,
        public storeLoad:boolean){}
}
