import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { AdditionsService } from 'src/app/services/grid-data.service';
import { StoresMenu } from 'src/app/classes/stores-menu';
import { ManagerService } from 'src/app/services/manager.service';
import { MatDialog } from '@angular/material/dialog';
import { ManagerEditAdditionComponent } from '../manager-edit-addition/manager-edit-addition.component';

@Component({
  providers: [AdditionsService],
  selector: 'app-manager-menu-additions',
  templateUrl: './manager-menu-additions.component.html',
  styleUrls: ['./manager-menu-additions.component.css']
})
export class ManagerMenuAdditionsComponent implements OnInit {

 
  OnInit(){}
    @Input() public product: StoresMenu;

    public view: Observable<GridDataResult>;
    public skip = 0;

    constructor(private service: AdditionsService, private managerServ:ManagerService, public dialog: MatDialog) { }

    public ngOnInit(): void {
        this.view = this.service;

        debugger;
        /*load products for the given category*/
        this.service.queryForProduct({ProductId: this.product.id}, { skip: this.skip, take: 5 });
    }
    private editedRowIndex: number;
  
    private editHandler({sender, rowIndex, dataItem}) {
  
      let rowData = dataItem;
      var temp:string=rowData.id;
      const dialogRef = this.dialog.open( ManagerEditAdditionComponent, { disableClose: true,
        width: '500px',
        data: {addition:rowData, productId:dataItem.product}
      });
  
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      })
  
  }
  
    private removeHandler({dataItem}) {
      debugger;
      if(confirm("Are you sure you wanna delete "+dataItem.additionName+" ?")) {
        this.managerServ.deleteAddition(dataItem.id).subscribe(
          data=>{this.ngOnInit();},
          err=>{alert(err.error)}
        )
      }
    }
  
    private createHandler()
    {
      const dialogRef = this.dialog.open(ManagerEditAdditionComponent, { disableClose: true,
        width: '500px',
        data: {addition:null, productId:this.product.id}
      });
  
      dialogRef.afterClosed().subscribe(
        data=>{ this.ngOnInit();}
      )
    }
  
    private closeEditor(grid, rowIndex = this.editedRowIndex) {
      debugger;
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        //this.formGroup = undefined;
    }
}
