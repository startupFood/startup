import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { PurchasesProduct } from 'src/app/classes/purchases-product';
import { StoreService } from 'src/app/services/store.service';
import { StoresMenu } from 'src/app/classes/stores-menu';
import { Router } from '@angular/router';
import { PurchaseDetailsTaste } from 'src/app/classes/purchase-details-taste';
import { MenuTastes } from 'src/app/classes/menu-tastes';
import { ServerService } from 'src/app/services/server.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  myCart: Array<PurchasesProduct>;
  //additions:Array<PurchasesDetailsAddition>= new Array<PurchasesDetailsAddition>();
  tastes: Array<MenuTastes>;
  cartTastes: Array<PurchaseDetailsTaste>;

  products: Array<StoresMenu>;

  constructor(public cartServ: CartService,
    public storeServ: StoreService,
    public router: Router,
    public serverServ: ServerService) {
    //to dispaly the match icons in the header.
    this.serverServ.Component.next(2);

  }

  ngOnInit() {
    this.myCart = this.cartServ.getCart();
    this.products = this.storeServ.Menu;
    this.tastes = this.storeServ.Tastes;
    this.cartTastes = this.cartServ.getCartDetailsTastes();
  }

  getProduct(productId: number): StoresMenu {
    return this.products.find(p => p.id == productId);
  }

  haveAdditions(id: number): boolean {

    if (this.cartServ.getCartDetailsAdditions()[0] != undefined && this.cartServ.getCartDetailsAdditions().find(a => a.product == id) != null)
      return true;
    return false;
  }

  productDetails(productId: number): void {
    this.router.navigate(["product/" + productId]);
  }

  getTaste(productId: number): string {
    // return this.tastes.find(t=>  {
    //   if( this.cartTastes.find(tt=>tt.product==productId) != undefined)
    //   {
    //     t.id==this.cartTastes.find(tt=>tt.product==productId).taste
    //   }
    // }).tasteName;

    if (this.cartTastes.find(tt => tt.product == productId) != undefined) {
      return this.tastes.find(t => t.id == this.cartTastes.find(tt => tt.product == productId).taste).tasteName;
    }
    else {
      return "";
    }
  }

  editQuantity(prodId: number, count: number) {
    let prodCount = this.myCart.find(c => c.id == prodId).productCount + count;
    if (prodCount > 0) {
      this.myCart.find(c => c.id == prodId).productCount = prodCount;
      this.cartServ.setAllCart(this.myCart);
    }
    else if (prodCount == 0) {
      this.myCart.splice(this.myCart.indexOf(this.myCart.find(c => c.id == prodId)), 1);
      this.cartServ.removeFromCart(this.myCart.find(c => c.id == prodId));
    }
  }

  removeFromCart(prodId: number) {
    this.myCart.splice(this.myCart.indexOf(this.myCart.find(c => c.id == prodId)), 1);
    this.cartServ.removeFromCart(this.myCart.find(c => c.id == prodId));
  }

  backToProduct(prodId: number) {
    this.router.navigate(["product/" + prodId]);
  }

  buy() {
    this.router.navigate(["order"]);
  }


}
