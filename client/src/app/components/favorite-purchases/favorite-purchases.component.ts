import { Component, OnInit } from '@angular/core';
import { Purchase } from 'src/app/classes/purchase';
import { StoreService } from 'src/app/services/store.service';
import { StoresMenu } from 'src/app/classes/stores-menu';
import { CartService } from 'src/app/services/cart.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-favorite-purchases',
  templateUrl: './favorite-purchases.component.html',
  styleUrls: ['./favorite-purchases.component.css']
})
export class FavoritePurchasesComponent implements OnInit {

  purchasesList:Array<Purchase>=null;
  constructor(public storeServ:StoreService, public cartServ:CartService, public router:Router) {
    storeServ.getFavoritePurchases().subscribe(
      data=>{this.purchasesList=data}
    );
  }

  ngOnInit() {
  }

  getProducts(id:number):Array<StoresMenu>
  {
    return this.storeServ.getFavoriteProducts(id);
  }

  purchaseChosen(purchaseId:number)
  {
    this.cartServ.getPurchaseProducts(purchaseId).subscribe(
      data=>{this.cartServ.setAllCart(data)},
      err=>{err},
      ()=>{
          this.cartServ.getPurchaseAdditions(purchaseId).subscribe(
            data=>{this.cartServ.setAllCartDetailsAditions(data)},
            err=>{err},
            ()=>{
                this.cartServ.getPurchaseTastes(purchaseId).subscribe(
                  data=>{this.cartServ.setAllCartDetailsTastes(data)},
                  err=>{},
                  ()=>
                  {this.router.navigate(["cart"]);}
                );
            }
          );
      }
    );
    this.cartServ.setSum(this.purchasesList.find(p=>p.id==purchaseId).purchaseSum);
  }

  
}
