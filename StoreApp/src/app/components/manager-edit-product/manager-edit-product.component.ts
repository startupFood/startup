import { Component, OnInit, Inject } from '@angular/core';
import { StoresMenu } from 'src/app/classes/stores-menu';
import { ActivatedRoute, Router } from '@angular/router';
import { ManagerService } from 'src/app/services/manager.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgModel } from '@angular/forms';

export interface ProductDialogData {
  product:StoresMenu;
}

@Component({
  selector: 'app-manager-edit-product',
  templateUrl: './manager-edit-product.component.html',
  styleUrls: ['./manager-edit-product.component.css']
})
export class ManagerEditProductComponent implements OnInit {


  model:StoresMenu = new StoresMenu(0, 0, "", 1, 0, false, false, 0, "a", 0, "a");
  productId=0;

  constructor(private router:Router, private activatedRoute:ActivatedRoute, private managerServ:ManagerService, public dialogRef: MatDialogRef<ManagerEditProductComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ProductDialogData) {
    //this.userId=this.activatedRoute.snapshot.params.user;

  }

  ngOnInit() {    
    debugger;
    if(this.data.product!=null)
    {
      this.model=this.data.product;
      this.productId=this.model.id;
    }
  }
  submitted = false;

  onSubmit() {
    this.submitted = true;
    debugger;
    if(this.productId==0)
    {
      debugger;
      this.model.store=this.managerServ.StoreId;
      this.managerServ.createProduct(this.model).subscribe();
    }
    else
    {
      this.managerServ.updateProduct(this.model).subscribe(
        data=>{},
        err=>{alert(err)}
      );
    }
  }

  cancelClick(): void {
    this.dialogRef.close();
  }


}
