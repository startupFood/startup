﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreeQueueServer.Models
{
    public class PurchasesDetailsTasteDTO
    {
        public int id { get; set; }
        public int product { get; set; }
        public int? taste { get; set; }



        public static PurchasesDetailsTasteDTO ConvertToDTO(tbl_purchasesTastes purchasesTaste)
        {
            
            if(purchasesTaste == null)
            {
                return null;
            }
            return new PurchasesDetailsTasteDTO() {
                id = purchasesTaste.Id,
                product = (purchasesTaste.Product != 0) ? purchasesTaste.tbl_purchasesProducts.Id : 0 ,
                taste = purchasesTaste.Taste
            };
        }

        public static List<PurchasesDetailsTasteDTO> ConvertToDTO(List<tbl_purchasesTastes> purchasesTastes)
        {
            return purchasesTastes.Select(p => ConvertToDTO(p)).ToList();
        }



    }
}