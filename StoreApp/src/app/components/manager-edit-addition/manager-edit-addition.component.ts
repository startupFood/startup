import { Component, OnInit, Inject } from '@angular/core';
import { MenuAdditions } from 'src/app/classes/menu-additions';
import { Router, ActivatedRoute } from '@angular/router';
import { ManagerService } from 'src/app/services/manager.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

  export interface AdditionDialogData {
    addition:MenuAdditions;
    productId:number;
  }

@Component({
  selector: 'app-manager-edit-addition',
  templateUrl: './manager-edit-addition.component.html',
  styleUrls: ['./manager-edit-addition.component.css']
})
export class ManagerEditAdditionComponent implements OnInit {

  model:MenuAdditions = new MenuAdditions(0, 0, "", 0, true, "a");
  additionId=0;

  constructor(private router:Router, private activatedRoute:ActivatedRoute, private managerServ:ManagerService, public dialogRef: MatDialogRef<ManagerEditAdditionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AdditionDialogData) {
    //this.userId=this.activatedRoute.snapshot.params.user;

  }

  ngOnInit() {    
    debugger;
    if(this.data.addition!=null)
    {
      this.model=this.data.addition;
      this.additionId=this.model.id;
    }
    else
      this.model.product=this.data.productId;
  }
  submitted = false;

  onSubmit() {
    this.submitted = true;
    debugger;
    if(this.additionId==0)
    {
      debugger;
      this.managerServ.createAddition(this.model).subscribe(
        data=>{}
      );
    }
    else
    {
      this.managerServ.updateAddition(this.model).subscribe(
        data=>{},
        err=>{alert(err)}
      );
    }
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

}
