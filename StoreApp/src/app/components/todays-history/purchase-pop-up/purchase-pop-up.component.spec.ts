import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasePopUpComponent } from './purchase-pop-up.component';

describe('PurchasePopUpComponent', () => {
  let component: PurchasePopUpComponent;
  let fixture: ComponentFixture<PurchasePopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasePopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasePopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
