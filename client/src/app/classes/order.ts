export class Order {
    constructor(
       public customerName:string,
       public customerPhone: string,
       public creditCard:string,
       public creditDate: string,
       public creditDigits:string,
       public deliveryAddress: string,
       public reservedSeats:number,
       //public tip: number,
       public receiptTime:string
    ){}
}
