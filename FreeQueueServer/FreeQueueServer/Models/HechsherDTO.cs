﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreeQueueServer.Models
{
    public class HechsherDTO
    {
        public int Id { get; set; }
        public string Hechsher { get; set; }
        public string HechsherImg { get; set; }
        public static HechsherDTO ConvertToDTO(tbl_hechshers hechsher)
        {
            return new HechsherDTO()
            {
                Id = hechsher.Id,
                HechsherImg = hechsher.HechsherImg,
                Hechsher = hechsher.Hechsher
            };
        }
        public static List<HechsherDTO> ConvertToDTO(List<tbl_hechshers> hechshers)
        {
            return hechshers.Select(h => ConvertToDTO(h)).ToList();
        }
    }
}