import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/classes/user';
import { LoginWorkerPopUpComponent } from './login-worker-pop-up/login-worker-pop-up.component';
import { MatDialog } from '@angular/material';
import { WorkerService } from 'src/app/services/worker.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-worker',
  templateUrl: './worker.component.html',
  styleUrls: ['./worker.component.css']
})
export class WorkerComponent implements OnInit {

  
  public workers:Array<User> = new Array<User>();

  constructor(private dialog:MatDialog ,
     private workerServ:WorkerService,
     public router:Router) { 
    // this.workers = this.workerServ.WorkersOnDuty;
    debugger;
    this.workers = JSON.parse( localStorage.getItem('workers') );
  }



  openLoginDialog():void {
    debugger;
      const dialogRef = this.dialog.open(LoginWorkerPopUpComponent , {
        width:'20vw'
        // data : {
        // }
      });
    
      dialogRef.afterClosed().subscribe(res => {
        debugger;
        switch(res)
        {
          case 3:
            this.router.navigate(['/manager']);
    break;
    case 4:
        this.router.navigate(["/openPage"]); }
    
        this.workers = JSON.parse( localStorage.getItem('workers') );
      });
    }

 

  ngOnInit() {
  }

}
