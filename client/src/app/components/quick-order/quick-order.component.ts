import { Component, OnInit } from '@angular/core';
import { StoresMenu } from 'src/app/classes/stores-menu';
import { Router } from '@angular/router';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-quick-order',
  templateUrl: './quick-order.component.html',
  styleUrls: ['./quick-order.component.css']
})
export class QuickOrderComponent implements OnInit {

  productsList:Array<StoresMenu>;
  constructor(private router:Router, public storeServ:StoreService) {
    storeServ.getStoreQuickProducts().subscribe(
      data=> { this.productsList=data;}
    )
   }

  order(id:number){
    this.router.navigate(["/order", id]);
}

  navigateToProduct(productId:number):void
  {
    this.router.navigate(["/product",productId]);
  }

  ngOnInit() {
  }


  
}
