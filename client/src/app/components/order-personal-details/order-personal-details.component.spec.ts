import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderPersonalDetailsComponent } from './order-personal-details.component';

describe('OrderPersonalDetailsComponent', () => {
  let component: OrderPersonalDetailsComponent;
  let fixture: ComponentFixture<OrderPersonalDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderPersonalDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderPersonalDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
