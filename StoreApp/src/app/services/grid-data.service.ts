import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { toODataString } from '@progress/kendo-data-query';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ServerService } from './server.service';
import { ManagerService } from './manager.service';
import { StoresMenu } from '../classes/stores-menu';

@Injectable({
  providedIn: 'root'
})
export abstract class GridDataService extends BehaviorSubject<GridDataResult> {

  public loading: boolean;


    constructor(
        private http: HttpClient,
        protected tableName: string,
        private serverServ:ServerService,
        public managerServ:ManagerService
    ) {
        super(null);
    }
    public query(state: any, url:string): void {
        this.fetch(this.tableName, state, url)
            .subscribe(x => super.next(x));
    }

    protected fetch(tableName: string, state: any, url:string): Observable<GridDataResult> {
        const queryStr = `${toODataString(state)}&$count=true`;
        this.loading = true;
        //return this.http.get<StoresMenu[]>(this.serverServ.URL+"Menu/GetByStore/"+this.storeId, {headers : this.serverServ.getHeader()});

        return this.http
            .get<StoresMenu[]>(this.serverServ.URL+url, {headers : this.serverServ.getHeader()})
            .pipe(
                map(response => ( <GridDataResult>{
                    data: response,
                    total: parseInt(response.length.toString(), 10)
                })),
                tap(() => this.loading = false)
            );
    }
}

//products
@Injectable()
export class AdditionsService extends GridDataService {
    constructor(http: HttpClient,
      serverServ:ServerService,
      managerServ:ManagerService) { super(http, 'Products', serverServ, managerServ); }

    public queryForProduct({ ProductId }: { ProductId: number }, state?: any): void {
      this.query(Object.assign({}, state, ProductId ,{
            filter: {
                filters: [{
                    field: 'ProductId', operator: 'eq', value: ProductId
                }],
                logic: 'and'
            }
        }),  "Menu/GetAdditionsByProduct/"+ProductId);
    }
    

    public queryForTastes({ ProductId }: { ProductId: number }, state?: any): void {
        this.query(Object.assign({}, state, ProductId ,{
              filter: {
                  filters: [{
                      field: 'ProductId', operator: 'eq', value: ProductId
                  }],
                  logic: 'and'
              }
          }),  "Menu/GetTastesByProduct/"+ProductId);
      }

    public queryForAdditiontName(AdditionName: string, state?: any): void {
        this.query(Object.assign({}, state, {
            filter: {
                filters: [{
                    field: 'AdditiontName', operator: 'contains', value: AdditionName
                }],
                logic: 'and'
            }
        }), "Menu/GetAdditionsByProduct/");
    }

}

//categories
@Injectable()
export class ProductsService extends GridDataService {
    constructor(http: HttpClient,
      serverServ:ServerService,
      managerServ:ManagerService) { super(http, 'Products', serverServ, managerServ); }

    queryAll(st?: any): Observable<GridDataResult> {
        const state = Object.assign({}, st);
        delete state.skip;
        delete state.take;

        return this.fetch(this.tableName, state, "Menu/GetByStore/"+this.managerServ.StoreId);
    }
}


//create, edit and delete

const CREATE_ACTION = 'create';
const UPDATE_ACTION = 'update';
const REMOVE_ACTION = 'destroy';

@Injectable()
export class EditService extends BehaviorSubject<any[]> {
    constructor(private http: HttpClient) {
        super([]);
    }

    private data: any[] = [];

    public read() {
        debugger;
        if (this.data.length) {
            return super.next(this.data);
        }

        this.fetch()
            .pipe(
                tap(data => {
                    this.data = data;
                })
            )
            .subscribe(data => {
                super.next(data);
            });
    }

    public save(data: any, isNew?: boolean) {
        debugger;
        const action = isNew ? CREATE_ACTION : UPDATE_ACTION;

        this.reset();

        this.fetch(action, data)
            .subscribe(() => this.read(), () => this.read());
    }

    public remove(data: any) {
        debugger;
        this.reset();

        this.fetch(REMOVE_ACTION, data)
            .subscribe(() => this.read(), () => this.read());
    }

    public resetItem(dataItem: any) {
        debugger;
        if (!dataItem) { return; }

        // find orignal data item
        const originalDataItem = this.data.find(item => item.ProductID === dataItem.ProductID);

        // revert changes
        Object.assign(originalDataItem, dataItem);

        super.next(this.data);
    }

    private reset() {
        debugger;
        this.data = [];
    }

    private fetch(action: string = '', data?: any): Observable<any[]> {
        debugger;
        return this.http
            .jsonp(`https://demos.telerik.com/kendo-ui/service/Products/${action}?${this.serializeModels(data)}`, 'callback')
            .pipe(map(res => <any[]>res));
    }

    private serializeModels(data?: any): string {
        debugger;
        return data ? `&models=${JSON.stringify([data])}` : '';
    }
}