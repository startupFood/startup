--create database DB_freeQueue

use DB_freeQueue





create table tbl_stores(
Id int primary key identity,
StoreName nvarchar(50) not null,
StoreAddress nvarchar(200) not null,
Phone nvarchar(20),
About nvarchar(300),
KashrutCertification nvarchar(30) not null,
Img nvarchar(50),
StoreCategory nvarchar(20) not null,
ReservedSeats bit,
Club bit,
Tip bit,
StoreLoad bit not null,
Bank int,
Brunch int,
Account nvarchar(100)
)


go
create table tbl_storesActivityTime(
Id int primary key identity,
Store int foreign key references tbl_stores(Id) not null,
ActivityDay int not null,
StartTime nvarchar(5) not null,
EndTime nvarchar(5) not null
)

go
create table tbl_productsCategories(
Id int primary key identity,
Store int foreign key references tbl_stores(Id) not null,
Category nvarchar(30) not null
)

create table tbl_storesMenu(
Id int primary key identity,
Barcode nvarchar(128),
Store int foreign key references tbl_stores(Id) not null,
ProductName nvarchar(100) not null,
ProductCategory int foreign key references tbl_productsCategories(Id) not null,
ProductPrice float not null,
ProductStatus bit not null,
QuickProduct bit,
ProductImage nvarchar(100) not null,
PreperationTime int not null,
Delivery bit,
AdditionsQuantity int not null
)

create table tbl_menuAddittions(
Id int primary key identity,
Product int foreign key references tbl_storesMenu(Id) not null,
Addition nvarchar(30) not null,
AddtionPrice float not null,
AdditionStatus bit not null,
AdditionImage nvarchar(100) not null
)

create table tbl_menuTastes(
Id int primary key identity,
Product int foreign key references tbl_storesMenu(Id) not null,
Taste nvarchar(30) not null,
TasteStatus bit not null,
TasteImage nvarchar(100) not null
)


go
create table tbl_groups(
	Id int primary key identity,
	StoreId int foreign key references tbl_stores(Id),
	GroupName nvarchar(30),
	GroupPassword nvarchar(100),
	OpeningDate nvarchar(20),
)


create table tbl_modifyTypes(
	Id int primary key identity,
	ModifyType nvarchar(30)
)

create table tbl_purchasesHistory(
	Id int primary key identity,
	Tbl_PurchaseId int not null,
	StoreId int not null,
	PurchaseDate nvarchar(20) not null,
	CustomerName nvarchar(100) not null,
	CustomerPhone nvarchar(20),
	CreditCard nvarchar(100) not null,
	CreditDate nvarchar(5) not null,
	CreditDigits nvarchar(3) not null,
	DeliveryAddress nvarchar(50),
	GroupDetails int,
	ReservedSeats int,
	Club bit,
	Tip float,
	PurchaseSum float not null,
	ReceiptTime nvarchar(5) not null,
	favorite bit,
	ModifyDate datetime,
	ModifyUser int,
	modifyType int foreign key references tbl_modifyTypes(Id)
)

create table tbl_roles(
	Id int primary key identity,
	RoleType nvarchar(20)
)

create table tbl_users(
	Id int primary key identity,
	UserEmail nvarchar(400) not null,
	UserPassword nvarchar(500) not null,
	UserName nvarchar(100) not null,
	UserPhone nvarchar(50),
	RoleId int foreign key references tbl_roles(Id)
)

create table tbl_purchases(
	Id int primary key identity,
	StoreId int foreign key references tbl_stores(Id) not null,
	PurchaseDate nvarchar(20) not null,
	CustomerName nvarchar(100) not null,
	CustomerPhone nvarchar(20),
	CreditCard nvarchar(100) not null,
	CreditDate nvarchar(5) not null,
	CreditDigits nvarchar(3) not null,
	DeliveryAddress nvarchar(50),
	GroupDetails int foreign key references tbl_groups(Id),
	ReservedSeats int,
	Club bit,
	Tip float,
	PurchaseSum float not null,
	ReceiptTime nvarchar(5) not null,
	favorite bit,
	userId int  foreign key references tbl_users(id)
)

create table tbl_purchasesProducts(
	Id int primary key identity,
	PurchaseId int foreign key references tbl_purchases(Id) not null,
	Product int foreign key references tbl_storesMenu(Id) not null,
	ProductCount int not null,
	Price float not null
)

create table tbl_purchasesProductsHistory(
	Id int primary key identity,
	Tbl_PurchaseProductId int not null,
	PurchaseId int not null,
	Product int not null,
	ProductCount int not null,
	Price float not null,
	ModifyDate datetime,
	ModifyUser int,
	modifyType int foreign key references tbl_modifyTypes(Id)
)

create table tbl_purchasesAdditions(
	Id int primary key identity,
	Product int foreign key references tbl_purchasesProducts(Id) not null,
	Addition int foreign key references tbl_menuAddittions(Id) not null
)

create table tbl_purchasesAdditionsHistory(
	Id int primary key identity,
	Tbl_PurchasesAdditionId int not null,
	Product int not null,
	Addition int not null,
	ModifyDate datetime,
	ModifyUser int,
	modifyType int foreign key references tbl_modifyTypes(Id)
)

create table tbl_purchasesTastes(
	Id int primary key identity,
	Product int foreign key references tbl_purchasesProducts(Id) not null,
	Taste int foreign key references tbl_menuTastes(Id) not null
)

create table tbl_purchasesTastesHistory(
	Id int primary key identity,
	Tbl_PurchasesTastesId int not null, 
	Product int not null,
	Taste int not null,
	ModifyDate datetime,
	ModifyUser int,
	modifyType int foreign key references tbl_modifyTypes(Id)
)



create table tbl_usersHistory(
	Id int primary key identity,
	Tbl_usersId int not null,
	UserEmail nvarchar(400) not null,
	UserPassword nvarchar(500) not null,
	UserName nvarchar(100) not null,
	UserPhone nvarchar(50),
	RoleId int,
	ModifyDate datetime,
	ModifyUser int,
	modifyType int foreign key references tbl_modifyTypes(Id)
)

create table tbl_responses(
	Id int primary key identity,
	StroeId int foreign key references tbl_stores(Id) not null,
	UserName nvarchar(100) not null,
	ResponseDate nvarchar(20) not null,
	Header nvarchar(100) not null,
	Content nvarchar(500) not null,
	Email nvarchar(400) not null
)

create table tbl_responsesHistory(
	Id int primary key identity,
	Tbl_responsesId int not null, 
	StroeId int not null,
	UserName nvarchar(100) not null,
	ResponseDate nvarchar(20) not null,
	Header nvarchar(100) not null,
	Content nvarchar(500) not null,
	Email nvarchar(400) not null,
	ModifyDate datetime,
	ModifyUser int,
	modifyType int foreign key references tbl_modifyTypes(Id)
)

create table tbl_workerToStore(
	Id int primary key identity,
	UserId int foreign key references tbl_users(Id),
	StoreId int foreign key references tbl_stores(Id)
)

create table tbl_purchaseToWorker(
	Id int primary key identity,
	UserId int foreign key references tbl_users(Id),
	PurchaseId int foreign key references tbl_purchases(Id)
)
