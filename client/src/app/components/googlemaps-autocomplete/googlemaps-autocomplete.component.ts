import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
//import {} from "googlemaps"
declare var google: any;

@Component({
  selector: 'app-googlemaps-autocomplete',
  templateUrl: './googlemaps-autocomplete.component.html',
  styleUrls: ['./googlemaps-autocomplete.component.css']
})
export class GooglemapsAutocompleteComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    //Inits the input field with the current user address.
    this.autocompleteInput=this.currentAddress;
      this.getPlaceAutocomplete();
  }

  //Gets by input the current user address
  @Input() currentAddress: string;
  @Input() adressType: string;
  //Emits the new address when user change the input.
  @Output() setAddress: EventEmitter<any> = new EventEmitter();
  @ViewChild('addresstext', null) addresstext: any;

  autocompleteInput: string;
  queryWait: boolean;
  Init() {
  }

  ngAfterViewInit() {
  }

  //Auto complete for the address input.
  private getPlaceAutocomplete() {
      const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
          {
              //componentRestrictions: { country: 'IL' },
              types: [this.adressType]  // 'establishment' / 'address' / 'geocode'
          });
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
          const place = autocomplete.getPlace();
          this.invokeEvent(place);
      });
  }

  //Emits the new address
  invokeEvent(place: Object) {
      this.setAddress.emit(place);
  }

}
