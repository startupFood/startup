/// <reference types="@types/googlemaps" />
import { Component, OnInit, NgZone, AfterContentInit, Input } from '@angular/core';
import { StoreService } from 'src/app/services/store.service';
import { Store } from 'src/app/classes/store';
import { Router } from '@angular/router';
import { StoreAndDistance } from 'src/app/classes/store-and-distance';
import { Observable, Observer } from 'rxjs';
import { MapsAPILoader, GoogleMapsAPIWrapper } from '@agm/core'
declare var google: any;

//interfaces for getting an address from the lat ad lng point
interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

interface Location {
  lat: number;
  lng: number;
  viewport?: Object;
  zoom: number;
  address_level_1?: string;
  address_level_2?: string;
  address_country?: string;
  address_zip?: string;
  address_state?: string;
  marker?: Marker;
}

@Component({
  selector: 'app-stores-list',
  templateUrl: './stores-list.component.html',
  styleUrls: ['./stores-list.component.css']
})

export class StoresListComponent implements OnInit {

  //list of store + distance objects
  storesList: Array<StoreAndDistance> = new Array<StoreAndDistance>();
  storesListData: Array<Store>;

  errorMessage: string = "";

  //user current position lng & lat
  userAddressPoint = { lng: 0, lat: 0 };
  userAddress = "";

  afterLoading: boolean = false;

  constructor(private storeServ: StoreService,
    private router: Router,
    public zone: NgZone,
    public mapsAPILoader: MapsAPILoader) {

  }

  initData() {
    this.afterLoading = true;
    //get user position
    if (navigator) {
      navigator.geolocation.getCurrentPosition(pos => {
        this.userAddressPoint.lng = +pos.coords.longitude;
        this.userAddressPoint.lat = +pos.coords.latitude;
        this.geocode(new google.maps.LatLng(this.userAddressPoint.lat, this.userAddressPoint.lng)).subscribe(data => {this.userAddress = data[0].formatted_address });
      });
    };
    //create a list of all the stores in the list
    this.storeServ.getStoresList().subscribe(
      data => { this.storesListData = data },
      err => { err },
      () => { this.sortStoresList(this.storesListData) }
    );

    //for geocoding- getting address from point numbers.
         this.geocoder = new google.maps.Geocoder();
        var x:google.maps.GeocoderResult[]; 
  }


  ngOnInit() {
    //Load mapsAPILoader and init data when the component is on init.
    this.mapsAPILoader.load().then(() => {
      this.initData();
    }
    );

  }

  //calculate the distance between the user position and the store position
  calculateDistance(lat, lng): number {
    //variable for the user position
    const p1 = new google.maps.LatLng(
      this.userAddressPoint.lat,
      this.userAddressPoint.lng
    );
    //variable for the store position
    const p2 = new google.maps.LatLng(
      lat,
      lng
    );
    //calculate the distance and return it.
    return (
      Number.parseFloat((google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000
      ).toString()));
  }

  //get the stores list, and create new list of the stores and the distances, sorting by the distance
  sortStoresList(storesData: Store[]) {
    //init the last list
    /* this.storesList = new Array<StoreAndDistance>();
    //create new list with store and distance
    storesData.forEach(store => {
      //calculate distance of store
      var distance = this.calculateDistance(store.latitude, store.longitude);
      this.storesList.push(
        new StoreAndDistance(store, distance)
      );
    });
    //sort the list in order to the distance from the store
    this.storesList.sort((a, b) => a.distance < b.distance ? -1 : a.distance > b.distance ? 1 : 0).slice(0,10); */
  }

  //geocoding
  geocoder: google.maps.Geocoder;

  //Gets lat and lng point and converts it to an literal address.
  geocode(latLng: google.maps.LatLng): Observable<google.maps.GeocoderResult[]> {
    return Observable.create((observer: Observer<google.maps.GeocoderResult[]>) => {
      // Invokes geocode method of Google Maps API geocoding.
      this.geocoder.geocode({ location: latLng }, (
        (results: google.maps.GeocoderResult[], status: google.maps.GeocoderStatus) => {
          if (status === google.maps.GeocoderStatus.OK) {
            observer.next(results);
            observer.complete();
          } else {
            console.log('Geocoding service: geocoder failed due to: ' + status);
            observer.error(status);
          }
        })
      );
    });
  }

  //Gets an literal address and converts it to lng & lat point.
  codeAddress(address: string): Observable<google.maps.GeocoderResult[]> {
    return Observable.create((observer: Observer<google.maps.GeocoderResult[]>) => {
      // Invokes geocode method of Google Maps API geocoding.
      this.geocoder.geocode({ address: address }, (
        (results: google.maps.GeocoderResult[], status: google.maps.GeocoderStatus) => {
          if (status === google.maps.GeocoderStatus.OK) {
            observer.next(results);
            observer.complete();
          } else {
            console.log(
              'Geocoding service: geocode was not successful for the following reason: '
              + status
            );
            observer.error(status);
          }
        })
      );
    });
  }

  //Sorts the stores list when the user choose new place
  getAddress(event) {
    this.userAddressPoint.lat = event.geometry.location.lat();
    this.userAddressPoint.lng = event.geometry.location.lng();
    this.sortStoresList(this.storesListData);
  }

  //This function is called when a store is chosen
  storeCohsen(storeId) {
    this.storeServ.storeId = storeId;
    this.router.navigate(["storePage"]);
  }



}
