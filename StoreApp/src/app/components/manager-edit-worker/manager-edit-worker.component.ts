import { Component, OnInit, Inject } from '@angular/core';
import { User } from 'src/app/classes/user';
import { Router, ActivatedRoute } from '@angular/router';
import { ManagerService } from 'src/app/services/manager.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface UserDialogData {
  userId:number;
}

@Component({
  selector: 'app-manager-edit-worker',
  templateUrl: './manager-edit-worker.component.html',
  styleUrls: ['./manager-edit-worker.component.css']
})
export class ManagerEditWorkerComponent implements OnInit {


  

  model:User = new User(0, "", "", "", "", 4);
  userId=0;

  constructor(private router:Router, private activatedRoute:ActivatedRoute, private managerServ:ManagerService, public dialogRef: MatDialogRef<ManagerEditWorkerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserDialogData) {
    //this.userId=this.activatedRoute.snapshot.params.user;

  }

  ngOnInit() {    
    this.userId=this.data.userId;
    var user:User=null;
    this.managerServ.getStoreWorkers().subscribe(
      data=>{
        debugger;
        user= data.find(d=>d.id==this.userId);
        if(user)
          this.model=user;},
      err=>{return 'something went wrong!'}
    );
    debugger;



    //var temp:number;
/*     this.activatedRoute.params.subscribe(params=>temp=params["user"]);
    if(temp)
    alert(temp); */
      //this.model=temp;
      //this.userId=this.activatedRoute.snapshot.params.user;

  }
  submitted = false;

  onSubmit() {
    this.submitted = true;
    if(this.userId==0)
    {
      this.managerServ.createUser(this.model, 4).subscribe(
        data=>{window.location.reload();}
      );
    }
    else
    {
      this.managerServ.updateUser(this.model).subscribe(
        data=>{location.reload();},
        err=>{alert(err)}
      );
    }
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

}
