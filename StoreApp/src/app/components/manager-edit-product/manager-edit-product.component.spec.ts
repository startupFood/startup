import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerEditProductComponent } from './manager-edit-product.component';

describe('ManagerEditProductComponent', () => {
  let component: ManagerEditProductComponent;
  let fixture: ComponentFixture<ManagerEditProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerEditProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerEditProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
