import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RoutingModule } from './routing/routing.module';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { StoreMenuComponent} from './components/store-menu/store-menu.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { OrderComponent } from './components/order/order.component';
import { ProductComponent } from './components/product/product.component';
import { ProductsListComponent } from './components/products-list/products-list.component';
import { QuickOrderComponent } from './components/quick-order/quick-order.component';
import { StorePageComponent } from './components/store-page/store-page.component';
import { CartComponent } from './components/cart/cart.component';
import { TastesComponent } from './components/tastes/tastes.component';
import { AdditionsComponent } from './components/additions/additions.component';
import { CategoriesListComponent } from './components/categories-list/categories-list.component';
import { GroupComponent } from './components/group/group.component';
import { CartAdditionsComponent }  from './components/cart-additions/cart-additions.component';
import { FavoritePurchasesComponent } from './components/favorite-purchases/favorite-purchases.component';
import { ResponsesComponent } from './components/responses/responses.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { StoresListComponent } from './components/stores-list/stores-list.component';
import { AddResponseFormComponent } from './components/add-response-form/add-response-form.component';
import { OrderPersonalDetailsComponent } from './components/order-personal-details/order-personal-details.component';
import { MainOrderComponent } from './components/main-order/main-order.component';
import { OrderDetailsComponent } from './components/order-details/order-details.component';
import { MainComponent } from './components/main/main.component';
import { OrderSummaryComponent } from './components/order-summary/order-summary.component';
import { ForgotPasswordComponent } from './components/log-in/forgot-password/forgot-password.component';
import { GoogleMapsComponent } from './components/google-maps/google-maps.component';
import {GooglemapsAutocompleteComponent} from './components/googlemaps-autocomplete/googlemaps-autocomplete.component'
import { PaymentPaypalComponent } from './components/payment-paypal/payment-paypal.component';
import { StoresComponent } from './components/stores/stores.component';
import { UserAccountComponent } from './components/user-account/user-account.component';

//angular material
import {A11yModule} from '@angular/cdk/a11y';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {PortalModule} from '@angular/cdk/portal';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBadgeModule} from '@angular/material/badge';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material/dialog';
import {MatDialogRef} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTreeModule} from '@angular/material/tree';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgxPayPalModule } from 'ngx-paypal';
import { ProductsComponent } from './components/products/products.component';
import { ProductSearchPipe } from './pipes/product-search.pipe';
import { SuccessOrderPopUpComponent } from './components/payment-paypal/success-order-pop-up/success-order-pop-up.component';

@NgModule({
  declarations: [
    AppComponent,
    StoreMenuComponent,
    NotFoundComponent,
    OrderComponent,
    ProductComponent,
    ProductsListComponent,
    QuickOrderComponent,
    StorePageComponent,
    CartComponent,
    AdditionsComponent,
    TastesComponent,
    CategoriesListComponent,
    GroupComponent,
    CartAdditionsComponent,
    FavoritePurchasesComponent,
    ResponsesComponent,
    LogInComponent,
    SignUpComponent,
    StoresListComponent,
    AddResponseFormComponent,
    OrderPersonalDetailsComponent,
    MainOrderComponent,
    OrderDetailsComponent,
    OrderSummaryComponent,
    MainComponent,
    ForgotPasswordComponent,
    GoogleMapsComponent,
    GooglemapsAutocompleteComponent,
    PaymentPaypalComponent,
    StoresComponent,
    UserAccountComponent,
    ProductsComponent,
    ProductSearchPipe,
    SuccessOrderPopUpComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    RoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyAj13gY0dRy3LKgxfbOkCPaqq_twe8eR3k',
      libraries: ["places", "geometry"]
    }),
    NgxPayPalModule,
    
    //angular material
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    // MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    A11yModule,
    DragDropModule,
    PortalModule,
    ScrollingModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    BrowserAnimationsModule

  ], 
  entryComponents:[
    ForgotPasswordComponent ,
     SuccessOrderPopUpComponent
  ],
  providers:[
  {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}],
 
  bootstrap: [AppComponent],
//   exports:[
// PaymentPaypalComponent ,LogInComponent  ]
})
export class AppModule { }
