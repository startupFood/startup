import { Component, OnInit , Inject} from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { StoreService } from 'src/app/services/store.service';
import { PurchasesProduct } from 'src/app/classes/purchases-product';
import { StoresMenu } from 'src/app/classes/stores-menu';
import { forEach } from '@angular/router/src/utils/collection';
import { Router } from '@angular/router';
import { MenuAdditions } from 'src/app/classes/menu-additions';
import { ManagerService } from 'src/app/services/manager.service';
import { MenuTastes } from 'src/app/classes/menu-tastes';
import { Purchase } from 'src/app/classes/purchase';
import { PurchasesToGroup } from 'src/app/classes/purchases-to-group';
@Component({
  selector: 'app-purchase-pop-up',
  templateUrl: './purchase-pop-up.component.html',
  styleUrls: ['./purchase-pop-up.component.css']
})
export class PurchasePopUpComponent implements OnInit {

  public purchaseId:number;
  public purchase:Purchase;
  public purchasesGroup:PurchasesToGroup;
  // list of purchase Products.
  purchasesProducts:Array<PurchasesProduct> = new Array<PurchasesProduct>();
  // list of products.
  products:Array<StoresMenu> =  new Array<StoresMenu>();


  constructor(public storeServ:StoreService,
    public managerServ:ManagerService,
    public dialogRef: MatDialogRef<PurchasePopUpComponent>,
    private router:Router,
    //:DialogData
    @Inject(MAT_DIALOG_DATA) public data) {
      //  this.purchaseId = data.purchaseId;
      this.purchasesGroup = data;
       debugger;

       this.purchasesProducts = new Array<PurchasesProduct>();
      //  single purchase.
       if(this.purchasesGroup.groupId == 0 )
       {
          this.setPurchasesProducts(this.purchasesGroup.purchases[0].id);
       }
       //group.
       else
       {
          this.purchasesGroup.purchases.forEach(p => 
            this.setPurchasesProducts(p.id));
       }
      //  this.purchase = this.storeServ.PurchasesHistory.find( p => p.id == this.purchaseId);
      

       //select the additions to the server.
       this.managerServ.getStoreAdditions().subscribe(
         data => this.storeServ.Additions = data
       );

      //  select the tastes to the server.
       this.managerServ.getStoreTastes().subscribe(
         data => this.storeServ.Tastes = data
       );

    }
  ngOnInit() {
  }

  //set the purchasesProducts of the purchase.
  setPurchasesProducts(purchaseId:number)
  {
 
    this.storeServ.getPurchasesProductToPurchase(purchaseId).subscribe(
      data =>
      {
        // this.purchasesProducts = data;
        data.forEach( d => this.purchasesProducts.push(d));
        let id:Array<number> = new Array<number>();
        //select the id of the storesmenu products.
        this.purchasesProducts.forEach(p=>id.push( p.product ));
        //select the storesMenu products.
       //  this.storeServ.getStoreMenuProducts(id).subscribe(
       //    data =>
       //    {
       //      this.products = data;
       //    }
       //  );
       id.forEach(i => this.products.push( this.storeServ.StoresMenu.find(sm => sm.id == i) ) );
      }
    );
  }

  getName()
  {
    if(this.purchasesGroup.groupId == 0)
    {
      return this.purchasesGroup.purchases[0].customerName;
    }
    else{
      return this.storeServ.GroupesHistory.find(gh => gh.id == this.purchasesGroup.groupId).groupName;
    }
  }

  getProductName(productId:number)
  {
    if(this.products.length)
    return this.products.find(p => p.id == productId).productName;
  }

  getAdditions(productId:number , event:MouseEvent)
{
// select the additions of this product
if(this.products.length)
{
  //create additions td.
  if(document.getElementById('additions'+ productId) == undefined)
  {
this.storeServ.getDetailsAdditionsToPurchaseProduct(productId).subscribe(
  data => 
  {
    var td = document.createElement('td');
    td.setAttribute('id' , 'additions'+productId);
    // list of menu additions.
    let additions:Array<MenuAdditions> = new Array<MenuAdditions>();
    // select the match menu additions.
    data.forEach(d => additions.push(this.storeServ.Additions.find(a => d.addition == a.id)));
    // create additions elements.
    for(let i= 0; i < additions.length ; i ++)
    {
      var addDiv = document.createElement('div');
      addDiv.setAttribute('style' , 'display:inline-block;')
      var addImg = new Image();
      addImg.src = additions[i].additionImage;
      addImg.setAttribute('style' , 'width:50px;height:50px;border-radius:100px;');
      addImg.title = additions[i].additionName;
      addDiv.appendChild(addImg);

      var addSpan = document.createElement('span');
      addSpan.innerHTML = additions[i].additionName;
      addSpan.setAttribute('style' , 'font-size:12;');

      addDiv.appendChild(document.createElement('br'));
      addDiv.appendChild(addSpan);
      td.appendChild(addDiv);
    }
     (event.srcElement as HTMLTableRowElement).parentElement.appendChild(td);
     if(additions.length)
     {
     var th = document.createElement('th');
     th.innerHTML= 'Additions';
     document.getElementById('headers').appendChild(th);
      }

  }

);
  }

  //create taste td.
  if(document.getElementById('taste'+ productId) == undefined)
  {
    this.storeServ.getDetailsTastesToPurchaseProduct(productId).subscribe(
      data => {
      let taste:MenuTastes =  this.storeServ.Tastes.find(t => t.id == data.taste);

        var td = document.createElement('td');
        td.setAttribute('id', 'taste' + productId);
      var tasteDiv = document.createElement('div');
      tasteDiv.setAttribute('style' , 'display:inline-block;')
      var tasteImg = new Image();
      tasteImg.src = taste.tasteImage;
      tasteImg.setAttribute('style' , 'width:50px;height:50px;border-radius:100px;');
      tasteImg.title = taste.tasteName;
      tasteDiv.appendChild(tasteImg);
    
      var tasteSpan = document.createElement('span');
      tasteSpan.innerHTML = taste.tasteName;
      tasteSpan.setAttribute('style' , 'font-size:12;');
    
      tasteDiv.appendChild(document.createElement('br'));
      tasteDiv.appendChild(tasteSpan);
      td.appendChild(tasteDiv);
      (event.srcElement as HTMLTableRowElement).parentElement.appendChild(td);
    
      if(taste)
      {
      var th = document.createElement('th');
      th.innerHTML= 'Tastes';
      document.getElementById('headers').appendChild(th);
      }
      }
    );
    
  }
}
}

  
 
  closePopup() {
    this.router.navigate(["/historyPage"]);
  }

  
  onNoClick()
  {
    try{
          this.dialogRef.close();
    }
    catch
    {
      this.closePopup();
    }

  }

}
