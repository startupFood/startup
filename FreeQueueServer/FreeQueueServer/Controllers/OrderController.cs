﻿using FreeQueueServer.Helpers;
using FreeQueueServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace FreeQueueServer.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/Order")]
    public class OrderController : ApiController
    {

        DB_freeQueueEntities1 DB = new DB_freeQueueEntities1();

        //nothing
        /// <summary>
        /// gets object of PurchaseDTO class and converts it to object of type tbl_purchases
        /// </summary> 
        /// <param name="purchase"></param>
        /// <returns>tbl_purchases</returns>
        public static tbl_purchases ConvertPurchaseFromDto(PurchaseDTO purchase)
        {
            return new tbl_purchases()
            {
                Id = purchase.id,
                CustomerName = purchase.customerName,
                CustomerPhone = purchase.customerPhone,
                DeliveryAddress = purchase.deliveryAddress,
                Club = purchase.club,
                CreditCard = purchase.creditCard,
                CreditDate = purchase.creditDate,
                CreditDigits = purchase.creditDigits,
                GroupDetails = purchase.groupId,
                PurchaseDate = purchase.purchaseDate,
                PurchaseSum = purchase.purchaseSum,
                ReceiptTime = purchase.receiptTime,
                ReservedSeats = purchase.reservedSeats,
                StoreId = purchase.store,
                Tip = purchase.tip,
                Favorite = purchase.favorite,
                UserId = purchase.userId
            };
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="purchasesProducts"></param>
        ///// <returns>List tbl_PurchasesProducts</returns>
        //public tbl_purchasesProducts ConvertPurchasesProductFromDTO(PurchasesProductDTO purchasesProduct)
        //{
        //   return new tbl_purchasesProducts()
        //    {
        //        Id = purchasesProduct.id,
        //        PurchaseId = (purchasesProduct.purchase != null)? DB.tbl_purchases.FirstOrDefault(tp=> tp.Id == purchasesProduct.purchase).Id : 0,
        //        Product = purchasesProduct.product,
        //        Price = (purchasesProduct.price != null) ? DB.tbl_storesMenu.FirstOrDefault(m=> m.Id == purchasesProduct.product).ProductPrice : 0,
        //        ProductCount = purchasesProduct.productCount
        //    };
        //}

        ///// <summary>
        ///// convert PurchasesDetailsAddition object to tbl_purchasesAdditions object.
        ///// </summary>
        ///// <param name="purchasesDetailsAddition"></param>
        ///// <returns>tbl_purchasesAdditions</returns>
        //public tbl_purchasesAdditions ConvertPurchasesDetailsAdditionFromDTO(PurchasesDetailsAdditionDTO purchasesDetailsAddition)
        //{
        //    return new tbl_purchasesAdditions()
        //    {
        //        Id = purchasesDetailsAddition.id,
        //        Addition = (purchasesDetailsAddition.addition != null) ? DB.tbl_menuAddittions.FirstOrDefault(ma => ma.Id == purchasesDetailsAddition.addition).Id : 0,
        //        Product = purchasesDetailsAddition.product                
        //    };
        //}

        ///// <summary>
        ///// convert PurchasesDetailsTaste object to tbl_purchasesTastes object.
        ///// </summary>
        ///// <param name="purchasesDetailsTaste"></param>
        ///// <returns>tbl_purchasesTastes</returns>
        //public tbl_purchasesTastes ConvertPurchasesTasteFromDTO(PurchasesDetailsTasteDTO purchasesDetailsTaste)
        //{
        //    return new tbl_purchasesTastes()
        //    {
        //        Id = purchasesDetailsTaste.id,
        //        Taste = (purchasesDetailsTaste.taste != null) ? DB.tbl_menuTastes.FirstOrDefault(mt => mt.Id == purchasesDetailsTaste.taste).Id : 0,
        //        Product = purchasesDetailsTaste.product
        //    };
        //}

        // GET: api/Order
        /// <summary>
        /// gets store id and returns all the orders of this store
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns>IHttpActionResult</returns>
        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.MANAGER)]
        [Route("GetOrdersByStore/{id}")]
        public IHttpActionResult Get([FromBody] int storeId)
        {
            return Ok(PurchaseDTO.ConvertToDTO(DB.tbl_purchases.Where(p => p.StoreId == storeId).ToList()));
        }


        // GET: api/Order
        /// <summary>
        /// gets store id and returns all the orders of this store in this day.
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns>IHttpActionResult</returns>
        // [Authorize(Roles = RolesNames.SYSTEM + ", " + RolesNames.DEVELOPER + ", " + RolesNames.MANAGER + ", " + RolesNames.STORE)]
        [AllowAnonymous]
        [Route("GetTodaysOrdersByStore/{storeIdentity}")]
        public IHttpActionResult GetTodaysOrders(int storeIdentity)
        {
            //lock (DB)
            //{
                string today = DateTime.Now.ToShortDateString();
                List<PurchaseDTO> purchasesList = PurchaseDTO.ConvertToDTO(DB.tbl_purchases.Where(p => p.StoreId == storeIdentity && (p.PurchaseDate == today) && p.Status == false).ToList());
                return Ok(purchasesList);

            //}
        }
        [AllowAnonymous]
        [Route("GetTodaysHistory/{storeId}")]
        public IHttpActionResult GetTodaysHistory(int storeId)
        {
            string today = DateTime.Now.ToShortDateString();
            List<PurchaseDTO> purchasesList = PurchaseDTO.ConvertToDTO(DB.tbl_purchases.Where(p => p.StoreId == storeId && (p.PurchaseDate == today) && p.Status == true).ToList());
            return Ok(purchasesList);

        }

        // GET: api/Order/5
        //public string GetList([FromBody]int storeId)
        //{
        //    //return 
        //}

        // POST: api/Order
        /// <summary>
        /// gets purchase and add it to DB
        /// </summary>
        /// <param name="purchase"></param>
        //[Route("AddOrder")]
        //public IHttpActionResult Post([FromBody]PurchaseDTO purchase)
        //{
        //    tbl_purchases p =  DB.tbl_purchases.Add(ConvertPurchaseFromDto(purchase));
        //    DB.SaveChanges();
        //    return Ok( p.Id);
        //}

        //[Route("AddPurchasesProduct")]
        //public IHttpActionResult Post([FromBody]List<PurchasesProductDTO> purchasesProducts)
        //{
        //    purchasesProducts.ForEach(p => DB.tbl_purchasesProducts.Add(ConvertPurchasesProductFromDTO(p)));
        //    DB.SaveChanges();
        //    return Ok(true);
        //}

        //[Route("AddPurchasesDetailsAdditions")]
        //public IHttpActionResult Post([FromBody]List<PurchasesDetailsAdditionDTO> purchasesDetailsAdditions)
        //{
        //    purchasesDetailsAdditions.ForEach(pda => DB.tbl_purchasesAdditions.Add(ConvertPurchasesDetailsAdditionFromDTO(pda)));
        //    DB.SaveChanges();
        //    return Ok(true);
        //}

        //[Route("AddPurchasesDetailsTastes")]
        //public IHttpActionResult post([FromBody]List<PurchasesDetailsTasteDTO> purchasesDetailsTastes)
        //{
        //    purchasesDetailsTastes.ForEach(pdt => DB.tbl_purchasesTastes.Add(ConvertPurchasesTasteFromDTO(pdt)));
        //    DB.SaveChanges();
        //    return Ok(true);
        //}

        // PUT: api/Order/5
        /// <summary>
        /// updates purchase in the sql
        /// </summary>
        /// <param name="id"></param>
        /// <param name="purchase"></param>
        [Authorize]
        [Route("UpdateOrder/{id}")]
        public void Put(int id, [FromBody]PurchaseDTO purchase)
        {
            DB.Entry(ConvertPurchaseFromDto(purchase)).State = System.Data.Entity.EntityState.Modified;
            DB.SaveChanges();
        }

        // DELETE: api/Order/5
        /// <summary>
        /// deletes purchase from DB
        /// </summary>
        /// <param name="id"></param>
        [Authorize]
        [Route("DeleteOrder/{id}")]
        public void Delete(int id)
        {
            DB.tbl_purchases.Remove(DB.tbl_purchases.FirstOrDefault(p => p.Id == id));
            DB.SaveChanges();
        }

        /// <summary>
        /// gets group name and check if this group is already exists
        /// </summary>
        /// <param name="group"></param>
        /// <returns>IHttpActionResult</returns>
        [Authorize]
        public IHttpActionResult GroupExists([FromBody]int group)
        {
            if (DB.tbl_purchases.Where(p => p.GroupDetails == group) != null)
                return Ok(true);
            return Ok(false);
        }

        /// <summary>
        /// gets store id and returns if there is load in the store
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns>IHttpActionResult</returns>
        [Authorize]
        public IHttpActionResult Load([FromBody]int storeId)
        {
            return Ok(DB.tbl_stores.FirstOrDefault(s => s.Id == storeId).StoreLoad);
        }


        /// <summary>
        /// gets store id and user id and returns the favorites purchases of this user.
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="userId"></param>
        /// <returns>IHttpActionResult</returns>
        [Authorize(Roles = RolesNames.DEVELOPER + ", "+RolesNames.CUSTOMER)]
        [Route("GetFavoritePurchases/{storeId}/{userId}")]
        public IHttpActionResult GetFavoritePurchases(int storeId, int userId)
        {
            var q = PurchaseDTO.ConvertToDTO(DB.tbl_purchases.Where(p => p.StoreId == storeId && p.UserId == userId && p.Favorite == true).ToList());
            return Ok(q);
        }
        /// <summary>
        /// gets store id and user id and returns the favorites purchases products of this user.
        /// </summary>
        /// <param name="purchaseId"></param>
        /// <returns>IHttpActionResult</returns>
        [Authorize]
        [Route("GetFavoriteProductsByPurchase/{purchaseId}")]
        public IHttpActionResult GetFavoriteProductsByPurchase(int purchaseId)
        {
            return Ok(StoresMenuDTO.ConvertToDTO(DB.tbl_purchasesProducts.Where(p=>p.PurchaseId==purchaseId).Select(pp=>pp.tbl_storesMenu).ToList()));
        }

        //[Route("GetPurchaseProducts/{purchaseId}")]
        //public IHttpActionResult GetPurchaseProducts(int purchaseId)
        //{
        //    return Ok(PurchasesProductDTO.ConvertToDTO(DB.tbl_purchasesProducts.Where(p => p.PurchaseId == purchaseId).ToList()));
        //}

        //[Route("GetPurchaseAdditions/{purchaseId}")]
        //public IHttpActionResult GetPurchaseAdditions(int purchaseId)
        //{
        //    return Ok(PurchasesDetailsAdditionDTO.ConvertToDTO(DB.tbl_purchasesAdditions.Where(pa => pa.tbl_purchasesProducts.PurchaseId == purchaseId).ToList()));
        //}

        //[Route("GetPurchaseTastes/{purchaseId}")]
        //public IHttpActionResult GetPurchaseTastes(int purchaseId)
        //{
        //    return Ok(PurchasesDetailsTasteDTO.ConvertToDTO(DB.tbl_purchasesTastes.Where(pt => pt.tbl_purchasesProducts.PurchaseId == purchaseId).ToList()));
        //}
    }
}