import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ServerService } from './server.service';
import { Observable } from 'rxjs';
import { User } from '../classes/user';
import { StoreService } from './store.service';

@Injectable({
  providedIn: 'root'
})
export class WorkerService {

  private workers:Array<User>;
  private workersOnDuty:Array<User> = new Array<User>();
  
  constructor(private httpClient:HttpClient , private serverServ:ServerService) {

   }

   get Workers()
   {
     return this.workers;
   }


   set Workers(value:Array<User>)
   {
     this.workers = value;
   }

   get WorkersOnDuty()
   {
     return this.workersOnDuty;
   }

   set WorkersOnDuty(value:Array<User>)
   {
     this.workersOnDuty = value;
   }
   
  //  getWorkers():Observable<User[]>
  //  {
  //   //  return this.httpClient.get<User[]>( this.serverServ.URL + "Worker/getWorkers/" + this.storeServ.Store.id, {headers:this.serverServ.getHeader()});
  //  }



}
