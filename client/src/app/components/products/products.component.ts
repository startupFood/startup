import { Component, OnInit } from '@angular/core';
import { StoresMenu } from 'src/app/classes/stores-menu';
import { StoreService } from 'src/app/services/store.service';
import { ProductsService } from 'src/app/services/products.service';
import { Router } from '@angular/router';
import { Store } from 'src/app/classes/store';
import { StoreMenuWithDetails } from 'src/app/classes/store-menu-with-details';
import { Hechshers } from 'src/app/classes/hechshers';
import { StoresCategory } from 'src/app/classes/stores-category';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {


  public products:Array<StoresMenu> = new Array<StoresMenu>();
  public productsAndDetails:Array<StoreMenuWithDetails> = new Array<StoreMenuWithDetails>();
  public sortedList:Array<StoreMenuWithDetails> = new Array<StoreMenuWithDetails>();
  hechshersList: Array<Hechshers>;
  categoriesList: Array<StoresCategory>;


  
  sort: boolean = false;
  sortBy: string = "";
  filter: string = "";
  categoryFilterNum: number;
  kashrutFilterNum: number;
  constructor(public storeServ:StoreService,
    public mapsAPILoader: MapsAPILoader,
    public productsServ:ProductsService,
    public router:Router) { 

      this.categoriesList = this.storeServ.StoresCategories;
      this.hechshersList = this.storeServ.Hechshers;

      this.productsAndDetails = this.productsServ.ProductsAndDetails;
//       this.storeServ.getAllStoreMenuCategories().subscribe(storesMenuCategories => {

      
//       //init the list with product and its details.
//       this.productsServ.getProducts().subscribe(
//         data => {
//           data.forEach(d =>
//             {
//               let location:number = 0;
//               let store:Store = this.storeServ.Stores.find(store => store.id == d.store);
//               let hechser:string = this.storeServ.Hechshers.find(h => h.Id == 
//               //find the hechser.
//               store.kashrutCertifiction
//               ).Hechsher;
//                this.productsAndDetails.push(
//                  new StoreMenuWithDetails(d ,
//                   location ,
//                    hechser ,
//                    store ,
//                    this.storeServ.StoresCategories.find(sc => sc.Id == store.storeCategory).Category,
//                    storesMenuCategories.find(smc => smc.id == d.productCategory).category
               
//                    ));
            
//             });
//           })
            
  this.sortedList = this.productsAndDetails;
//         }
//       )
    }

  ngOnInit() {
  }

  clearFilters()
  {
    this.filter="";
  }
  //after choosing the product - init the store details.
  chooseProduct(productId:number)
  {
    // this.storeServ.storeSubject.subscribe(data => {
      //navigate to the product page
      this.router.navigate(['/product', productId]);
    // });
    this.storeServ.initStore(this.sortedList.find(p => p.product.id == productId).store.id);
  }
  stores()
  {
    this.router.navigate(['stores'])
  }



  sortList() {
    if (this.sortBy = "distance"){}
      // this.sortByDistance();
    else if (this.sortBy = "name")
      this.sortByName();
    else if (this.sortBy = "category")
      this.sortByCategory();
  }

  categoryFilter(filter: number) {
    this.categoryFilterNum = filter;
    this.sortedList = this.productsAndDetails.filter(pad => pad.store.storeCategory == filter);
    // this.storesList = this.fullStoresList.filter(s => s.store.storeCategory == filter);
    this.sortList();
    this.filter = "";
  }

  kashrutFilter(filter: number) {
    this.kashrutFilterNum = filter;
    this.sortedList = this.productsAndDetails.filter(pad => pad.store.kashrutCertifiction == filter);
    // this.storesList = this.fullStoresList.filter(s => s.store.kashrutCertifiction == filter);
    this.sortList();
    this.filter = "";
  }

  changeFilter(filter: string) {
    if (filter == this.filter)
      this.filter = "";
    else
      this.filter = filter;
    this.sort = false;
  }

  sortByName() {
    this.sortBy = "name";
    this.sortedList = this.sortedList.sort((p1, p2) => {
      if (p1.store.storeName > p2.store.storeName) {
        return 1;
      }
      if (p1.store.storeName < p2.store.storeName) {
        return -1;
      }
      return 0;
    });
    this.sort = false;
  }
  //sort by product category.
  sortByCategory() {
    this.sortBy = "category";
    this.sortedList = this.sortedList.sort((a,b) => a.productCategory.charCodeAt(0) -b.productCategory.charCodeAt(0))
    //this.storesList=this.storesList.sort((a, b) => a.distance < b.distance ? -1 : a.distance > b.distance ? 1 : 0);
    this.sort = false;
  }

}
