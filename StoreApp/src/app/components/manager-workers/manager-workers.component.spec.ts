import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerWorkersComponent } from './manager-workers.component';

describe('ManagerWorkersComponent', () => {
  let component: ManagerWorkersComponent;
  let fixture: ComponentFixture<ManagerWorkersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerWorkersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerWorkersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
