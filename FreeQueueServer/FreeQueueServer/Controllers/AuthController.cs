﻿using FreeQueueServer.Helpers;
using FreeQueueServer.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
//using System.IdentityModel.Tokens.jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Security.Claims;

namespace FreeQueueServer.Controllers
{
    [RoutePrefix("api/Auth")]
    [AllowAnonymous]
    public class AuthController : ApiController
    {
        DB_freeQueueEntities1 DB = new DB_freeQueueEntities1();


        // GET: api/Auth
        [Route("getUsers")]
        [Authorize(Roles = RolesNames.DEVELOPER + ", " + RolesNames.SYSTEM)]
        public IHttpActionResult Get()
        {
            return Ok(UsersDTO.ConvertToDTO(DB.tbl_users.ToList()));
        }

        // GET: api/Auth/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Auth
        [Route("login")]
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult LogIn([FromBody] UsersDTO user)
        {
            try
            {
                UsersDTO tempUser = PasswordsManagement.VerifyPassword(user);
                if (tempUser != null)
                {
                    var token = GenerateTokenForUser(tempUser);
                    return Ok(new { token, role = user.roleId, user = tempUser });
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex));
            }

        }


        [Route("loginAsGuest")]
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult LogInAsGuest()
        {
            var guest = new UsersDTO() { id = 0, roleId = int.Parse(RolesNames.GUEST) };
            var token = GenerateTokenForUser(new UsersDTO());
            return Ok(new { token, role = int.Parse(RolesNames.GUEST), user = guest });
        }

        public string GenerateTokenForUser(UsersDTO user)
        {
            var signingKey = "GQDstc21ewfffffffffffFiwDffVvVBrk";
            var now = DateTime.UtcNow;
            var key = new SymmetricSecurityKey(Encoding.Default.GetBytes(signingKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var claimsIdentity = new ClaimsIdentity(new List<Claim>()
            {
                new Claim(ClaimTypes.Name, user.id.ToString()),
                new Claim(ClaimTypes.Role, user.roleId.ToString())
            }, "custom");
            var securityTokenDescriptor = new SecurityTokenDescriptor()
            {
                Issuer = "self",
                Subject = claimsIdentity,
                SigningCredentials = creds,
                Expires = now.AddHours(1)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var plainToken = tokenHandler.CreateToken(securityTokenDescriptor);
            var signedAndEncodedToken = tokenHandler.WriteToken(plainToken);
            return signedAndEncodedToken;
        }

        [Route("initPassword")]
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult InitPassword([FromBody] UsersDTO user)
        {
            try
            {
                var pass = PasswordsManagement.CreateNewPassword(user);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok();
        }


        // DELETE: api/Auth/5
        public void Delete(int id)
        {

        }
    }
}
