import { Pipe, PipeTransform } from '@angular/core';
import { StoresMenu } from '../classes/stores-menu';

@Pipe({
  name: 'productSearch'
})
export class productSearchPipe implements PipeTransform {

  transform(value: StoresMenu[], args?: any): any {
    debugger;
    var res = []; 
    for(var i=0; i<value.length ; i++)
    {
      // search by product price or by customer phone or by receiptTime.
   
        if(args == "" ||value[i].productPrice == args || value[i].purchases[i].customerPhone.indexOf(args)!= -1 
        || value[i].purchases[i].receiptTime.indexOf(args)!= -1  )
       res.push(value[i]);
      }
      //search by groupName.
      else {

        if(args == "" || value[i].groupName.indexOf(args) !=-1 || value[i].purchases[0].receiptTime.indexOf(args)!= -1)
        {
          res.push(value[i]);
        }
      //   if(args == "" ||value[i].customerName.indexOf(args)!= -1 || value[i].customerPhone.indexOf(args)!= -1 
      //   || value[i].receiptTime.indexOf(args)!= -1  )
      //  res.push(value[i]);
      }
    }
    return res;
  }

}
