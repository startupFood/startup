import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { GridDataResult, PageChangeEvent, RowClassArgs } from '@progress/kendo-angular-grid';
import { AdditionsService } from 'src/app/services/grid-data.service';
import { StoresMenu } from 'src/app/classes/stores-menu';
import { MatDialog } from '@angular/material/dialog';
import { ManagerService } from 'src/app/services/manager.service';
import { ManagerEditTasteComponent } from '../manager-edit-taste/manager-edit-taste.component';

@Component({
  selector: 'app-manager-menu-tastes',
  templateUrl: './manager-menu-tastes.component.html',
  styleUrls: ['./manager-menu-tastes.component.css']
})
export class ManagerMenuTastesComponent implements OnInit {

  OnInit(){}
    @Input() public product: StoresMenu;

    public view: Observable<GridDataResult>;
    public skip = 0;

    constructor(private service: AdditionsService, public dialog: MatDialog, private managerServ:ManagerService) { }

    public ngOnInit(): void {
        this.view = this.service;
        /*load products for the given category*/
        this.service.queryForTastes({ProductId: this.product.id}, { skip: this.skip, take: 5 });
        debugger;
    }

    public pageChange({ skip, take }: PageChangeEvent): void {
        this.skip = skip;
        this.service.queryForTastes({ProductId: this.product.id}, { skip, take });
    }

  
    private editedRowIndex: number;
  
    private editHandler({sender, rowIndex, dataItem}) {
  
      let rowData = dataItem;
      const dialogRef = this.dialog.open( ManagerEditTasteComponent, { disableClose: true,
        width: '500px',
        data: {taste:rowData, productId:dataItem.product}
      });
  
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      })
  
  }
  
    private removeHandler({dataItem}) {
      if(confirm("Are you sure you wanna delete "+dataItem.tasteName+" ?")) {
        this.managerServ.deleteTaste(dataItem.id).subscribe(
          data=>{this.ngOnInit();},
          err=>{alert(err.error)}
        )
      }
    }
  
    private createHandler()
    {
      const dialogRef = this.dialog.open(ManagerEditTasteComponent, { disableClose: true,
        width: '500px',
        data: {taste:null, productId:this.product.id}
      });
  
      dialogRef.afterClosed().subscribe(
        data=>{ this.ngOnInit();}
      )
    }

    a(view):boolean
    {debugger; return true;}
  
    private closeEditor(grid, rowIndex = this.editedRowIndex) {
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        //this.formGroup = undefined;
    }
}
