export class Purchase {

    constructor(
        public id:number,
        public store:number,
        public purchaseDate:string,
        public customerName:string,
        public customerPhone:string,
        public creditCard:string,
        public creditDate:string,
        public creditDigits:string,
        public deliveryAddress:string,
        public groupId:number,
        public reservedSeats:number,
        public club:boolean,
        public tip:number,
        public purchaseSum:number,
        public receiptTime:string,
        public favorite:boolean,
        public userId:number,
        public status:boolean){}
}
