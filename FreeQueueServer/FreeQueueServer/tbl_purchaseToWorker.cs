//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FreeQueueServer
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_purchaseToWorker
    {
        public int Id { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<int> PurchaseId { get; set; }
    
        public virtual tbl_purchases tbl_purchases { get; set; }
        public virtual tbl_purchases tbl_purchases1 { get; set; }
        public virtual tbl_users tbl_users { get; set; }
        public virtual tbl_users tbl_users1 { get; set; }
    }
}
