export class Purchase {

    constructor(
        public id:number,
        public store:number,
        public purchaseDate:string,
        public customerName:string,
        public customerPhone:string,
        public creditCard:string,
        public creditDate:string,
        public creditDigits:string,
        public deliveryAddress:string,
        public groupId:number,
        public reservedSeats:number,
        public club:boolean,
        public tip:number,
        public purchaseSum:number,
        public receiptTime:string,
        public favorite:boolean,
        public userId:number,
        public status:boolean){}

/*         static getProperties(obj: any): string[] {
            const result = [];
            for (let property in obj) {
              if (obj.hasOwnProperty(property) && !property.startsWith('_')) {
                result.push(property);
              }
            }
            return result;
          } */



 /*          static getNames(obj:Object):string[]
          {
               
              const result = [];
              for(let property in obj)
              {
                  if(obj.hasOwnProperty(property))
                  {
                      result.push(property);
                  }
              }
              return result;
          } */
}
