import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { WorkerService } from 'src/app/services/worker.service';
import { User } from 'src/app/classes/user';

@Component({
  selector: 'app-login-worker-pop-up',
  templateUrl: './login-worker-pop-up.component.html',
  styleUrls: ['./login-worker-pop-up.component.css']
})
export class LoginWorkerPopUpComponent implements OnInit {

  error:boolean = false;
  constructor(public dialogRef: MatDialogRef<LoginWorkerPopUpComponent>,
    private router:Router,
    //:DialogData
     private workerServ:WorkerService) { }
    //  @Inject(MAT_DIALOG_DATA) public data ,
  ngOnInit() {
  }

login(password:string , name:string)
{

  let worker = this.workerServ.Workers.find(w => w.userPassword == password && w.userName == name);
  if(worker)
  {
    let localWorkers:Array<User> = JSON.parse( localStorage.getItem('workers') );
    // if(localWorkers)
    // {
      //add the worker to the saved workers.
      localWorkers.push(worker);
      localStorage.setItem('workers' , JSON.stringify(localWorkers));
    this.workerServ.WorkersOnDuty.push(worker);
  }
  else{
this.error = true;
  }
this.onNoClick(worker.roleId);
}
  
  closePopup() {
    this.router.navigate(["/openPage"]); 
   }

  
  onNoClick(roleId:number)
  {
    try{
          this.dialogRef.close(roleId);
    }
    catch
    {
      this.closePopup();
    }

  }
}
