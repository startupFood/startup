﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreeQueueServer.Models
{
    public class PurchasesProductDTO
    {
        public int id { get; set; }
        public int? purchase { get; set; }
        public int product { get; set; }
        public int productCount { get; set; }
        public double price { get; set; }
        public bool? status { get; set; }

        public static PurchasesProductDTO ConvertToDTO(tbl_purchasesProducts purchasesProduct)
        {
            return new PurchasesProductDTO()
            {
                id = purchasesProduct.Id,
                purchase = purchasesProduct.tbl_purchases.Id,
                product = purchasesProduct.Product,
                productCount = purchasesProduct.ProductCount,
                price = purchasesProduct.Price,
                status = purchasesProduct.Status
            };
        }

        public static List<PurchasesProductDTO> ConvertToDTO(List<tbl_purchasesProducts> purchasesProducts)
        {
            return purchasesProducts.Select(p => ConvertToDTO(p)).ToList();
        }
    }
}