﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Sql;
using System.ComponentModel.DataAnnotations;

namespace FreeQueueServer.Models
{
    public class GroupDTO
    {
      
        public int id { get; set; }
        public int? storeId { get; set; }
        public string groupName { get; set; }
        public string groupPassword { get; set; }
        public string openingDate { get; set; }

        /// <summary>
        /// get sql entity and convert it to simple object whithout references
        /// </summary>
        /// <param name="group"></param>
        /// <returns>GroupDTO</returns>
        public static GroupDTO ConvertToDTO(tbl_groups group)
        {
            return new GroupDTO() {
                id = group.Id,
                storeId = group.StoreId,
                groupName = group.GroupName,
                groupPassword = group.GroupPassword,
                openingDate = group.OpeningDate
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="groups"></param>
        /// <returns></returns>
        public static List<GroupDTO> ConvertToDTO(List<tbl_groups> groups)
        {
            return groups.Select(g => ConvertToDTO(g)).ToList();

        }
    }
}