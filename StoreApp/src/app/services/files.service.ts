import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ServerService } from './server.service';
import { StoreService } from './store.service';
import { UsersService } from './users.service';
import { ManagerService } from './manager.service';

@Injectable({
  providedIn: 'root'
})
export class FilesService {

  constructor(private httpClient: HttpClient, private serverServ:ServerService, private managerServ:ManagerService) { }

/* 
  uploadImage(componentId, image) {
    debugger;
    const formData: FormData = new FormData();
    formData.append('Image', image, image.name);
    formData.append('ComponentId', componentId);
    return this.http.post(this.serverServ.URL+ '/api/manager/UploadImage', formData, {headers : this.serverServ.getHeader()});
  } */

  postStoreImage(image: File): Observable<string> {
    debugger;
    const formData: FormData = new FormData();
    formData.append('Image', image, image.name);
    return this.httpClient.post<string>(this.serverServ.URL + 'Manager/UploadStoreImage/'+this.managerServ.StoreId, formData, {headers : this.serverServ.getHeader()});
  }

  postProductImage(image: File, productId:number): Observable<string> {
    debugger;
    const formData: FormData = new FormData();
    formData.append('Image', image, image.name);
    return this.httpClient.post<string>(this.serverServ.URL + 'Manager/UploadProductImage/'+productId, formData, {headers : this.serverServ.getHeader()});
  }
  
  postAdditionImage(image: File): Observable<string> {
    debugger;
    const formData: FormData = new FormData();
    formData.append('Image', image, image.name);
    return this.httpClient.post<string>(this.serverServ.URL + 'Manager/UploadProductImage/'+this.managerServ.StoreId, formData, {headers : this.serverServ.getHeader()});
  }


}
