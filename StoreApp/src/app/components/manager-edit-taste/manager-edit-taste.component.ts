import { Component, OnInit, Inject } from '@angular/core';
import { MenuTastes } from 'src/app/classes/menu-tastes';
import { Router, ActivatedRoute } from '@angular/router';
import { ManagerService } from 'src/app/services/manager.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface TasteDialogData {
  taste:MenuTastes;
  productId:number;
}

@Component({
  selector: 'app-manager-edit-taste',
  templateUrl: './manager-edit-taste.component.html',
  styleUrls: ['./manager-edit-taste.component.css']
})
export class ManagerEditTasteComponent implements OnInit {

  model:MenuTastes = new MenuTastes(0, 0, "", true, "a");
  tasteId=0;

  constructor(private router:Router, private activatedRoute:ActivatedRoute, private managerServ:ManagerService, public dialogRef: MatDialogRef<ManagerEditTasteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TasteDialogData) {
    //this.userId=this.activatedRoute.snapshot.params.user;

  }

  ngOnInit() {    
    debugger;
    if(this.data.taste!=null)
    {
      this.model=this.data.taste;
      this.tasteId=this.model.id;
    }
    else
      this.model.product=this.data.productId;
  }
  submitted = false;

  onSubmit() {
    this.submitted = true;
    debugger;
    if(this.tasteId==0)
    {
      debugger;
      this.managerServ.createTaste(this.model).subscribe(
        data=>{}
      );
    }
    else
    {
      this.managerServ.updateTaste(this.model).subscribe(
        data=>{},
        err=>{alert(err)}
      );
    }
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

}
