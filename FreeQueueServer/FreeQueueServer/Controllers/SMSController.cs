﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FreeQueueServer.Models;
using FreeQueueServer.Helpers;

namespace FreeQueueServer.Controllers
{
    [RoutePrefix("api/Sms")]
    public class SMSController : ApiController
    {


        //// GET: api/SMS
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        [HttpGet]
        [Route("getMessages")]
        [AllowAnonymous]
        // GET: api/SMS/5
        public IHttpActionResult Get()
        {
            return Ok(LocalDB.SmsMessages);
        }
        [HttpGet]
        [Route("sendSms/{message}/{phone}")]
        [AllowAnonymous]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetAsync(string message , string phone)
        {
            try
            {
                await SMSSender.SendUsingAPIAsync(message, phone);
                return Ok(true);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST: api/SMS
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/SMS/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/SMS/5
        public void Delete(int id)
        {
        }
    }
}
