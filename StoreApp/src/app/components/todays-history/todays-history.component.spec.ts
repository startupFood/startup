import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodaysHistoryComponent } from './todays-history.component';

describe('TodaysHistoryComponent', () => {
  let component: TodaysHistoryComponent;
  let fixture: ComponentFixture<TodaysHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodaysHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodaysHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
