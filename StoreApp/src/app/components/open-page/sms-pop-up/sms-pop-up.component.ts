import { Component, OnInit ,Inject } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
  import { SmsService } from 'src/app/services/sms.service';

@Component({
  selector: 'app-sms-pop-up',
  templateUrl: './sms-pop-up.component.html',
  styleUrls: ['./sms-pop-up.component.css']
})
export class SmsPopUpComponent implements OnInit {

//the received phone.
  phone:string;
  //the name of the receives.
  name:string;


  selectedValue = "message";
  sent:boolean = false;
  error:boolean = false;

  //list of messages from the server.
  public messages:Array<string>;
  constructor(public dialogRef: MatDialogRef<SmsPopUpComponent>,
    private router:Router,
    //:DialogData
    @Inject(MAT_DIALOG_DATA) public data , 
    private SmsServ:SmsService) { 
debugger;
      this.phone = data.phone;
      this.name = data.name;

    }

  ngOnInit() {

    // debugger;
    // this.messages = this.SmsServ.Messages;

       this.SmsServ.getSmsMessages().subscribe(
      data => this.messages = data
    )
  }


  sendSms(message:string)
  {
    this.SmsServ.sendSms(message , this.phone).subscribe(
      success => {
        this.sent = true;
      },
      err => {
this.error = true;
      }
    )
  }

  
  closePopup() {
    this.router.navigate(["/openPage"]);
  }

  
  onNoClick(result:boolean)
  {
    try{
          this.dialogRef.close(result);
    }
    catch
    {
      this.closePopup();
    }

  }
}
