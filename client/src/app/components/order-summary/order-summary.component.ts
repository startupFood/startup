import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { Router } from '@angular/router';
import { OrderService } from 'src/app/services/order.service';
import { MatDialog } from '@angular/material/dialog';
import { SuccessOrderPopUpComponent } from '../payment-paypal/success-order-pop-up/success-order-pop-up.component';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.css']
})
export class OrderSummaryComponent implements OnInit {

  @Output("get-summary") getSummary: EventEmitter<Number> = new EventEmitter();

  constructor(private cartServ: CartService,
    private router: Router,
    private orderServ: OrderService) {
    this.orderServ.buy.subscribe(data => {
      this.save();
    })
  }

  getSum() {
    return this.cartServ.getSum();
  }



  preComponent() {
    this.getSummary.emit(0);
  }


  save() {
    this.cartServ.addOrder();

    this.cartServ.buySubject.subscribe(data => {
      this.router.navigate(["/storesList"]);
    })
  }
  ngOnInit() {
  }


}
