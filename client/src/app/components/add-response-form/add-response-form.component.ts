import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { StoreService } from 'src/app/services/store.service';
import { Response } from 'src/app/classes/response'
import { User } from 'src/app/classes/user';

@Component({
  selector: 'app-add-response-form',
  templateUrl: './add-response-form.component.html',
  styleUrls: ['./add-response-form.component.css']
})
export class AddResponseFormComponent implements OnInit {

  newResponse:Response;
  @Output() response:EventEmitter<Response>= new EventEmitter<Response>();

  constructor(public storeServ:StoreService) { }

  classError:string="";
  ngOnInit() {
    if(this.storeServ.getUser()!=null)
    {
      var user:User = this.storeServ.getUser();
      this.newResponse=new Response(0, 0, user.userName, "", "", "", user.userEmail);
    }
    else
      this.newResponse=new Response(0, 0, "", "", "", "", "");
  }

  onSubmit()
  {
    var tempResponse:Response = new Response(0, this.storeServ.storeId, this.newResponse.userName,
      (new Date).toLocaleDateString(), this.newResponse.header, this.newResponse.content, this.newResponse.email);
    this.newResponse=new Response(0, 0, "", "", "", "", "");
    this.response.emit(tempResponse);
  }

}
