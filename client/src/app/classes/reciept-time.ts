export class RecieptTime {

    public Day:string;
    public Hour:string;
    public Minutes:string;

    public getRecieptTime()
    {
        return this.Hour + ":" + this.Minutes;
    }
}
