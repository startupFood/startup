//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FreeQueueServer
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_purchasesProducts
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_purchasesProducts()
        {
            this.tbl_purchasesAdditions = new HashSet<tbl_purchasesAdditions>();
            this.tbl_purchasesAdditions1 = new HashSet<tbl_purchasesAdditions>();
            this.tbl_purchasesTastes = new HashSet<tbl_purchasesTastes>();
            this.tbl_purchasesTastes1 = new HashSet<tbl_purchasesTastes>();
        }
    
        public int Id { get; set; }
        public int PurchaseId { get; set; }
        public int Product { get; set; }
        public int ProductCount { get; set; }
        public double Price { get; set; }
        public Nullable<bool> Status { get; set; }
    
        public virtual tbl_purchases tbl_purchases { get; set; }
        public virtual tbl_purchases tbl_purchases1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_purchasesAdditions> tbl_purchasesAdditions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_purchasesAdditions> tbl_purchasesAdditions1 { get; set; }
        public virtual tbl_storesMenu tbl_storesMenu { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_purchasesTastes> tbl_purchasesTastes { get; set; }
        public virtual tbl_storesMenu tbl_storesMenu1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_purchasesTastes> tbl_purchasesTastes1 { get; set; }
    }
}
