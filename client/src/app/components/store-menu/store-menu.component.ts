//what is this component?????????????????????????????????????

import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/services/store.service';
import { ProductCategory } from 'src/app/classes/product-category';
import { Router } from '@angular/router';

@Component({
  selector: 'app-store-menu',
  templateUrl: './store-menu.component.html',
  styleUrls: ['./store-menu.component.css']
})
export class StoreMenuComponent implements OnInit {

  //temp variable until we'll create the router
  storeId:number=1;
  categories:Array<ProductCategory>;
  
  constructor(private storeServ:StoreService, private router:Router) { 
    storeServ.getStoreMenuCategories().subscribe(
      data=>{this.categories=data},
      err=>{console.log(err)}
    );
  }

  getAdditions(name:number):void
  {
    this.storeServ.Additions.find(a=>a.product==name);
  }

  ngOnInit() {
    
  }

  categoryChosen(c:ProductCategory)
  {
    this.router.navigate(["/productsList/",c.id ]);
  }


  //this function find my location
  //it's not real angular script
  //so we have to change it b"h
  geoFindMe() {

    const status = document.querySelector('#status');
    const mapLink = document.querySelector('#map-link');
  
    mapLink.textContent = '';
  
    function success(position) {
      const latitude  = position.coords.latitude;
      const longitude = position.coords.longitude;
  
      status.textContent = '';
      mapLink.textContent = `Latitude: ${latitude} °, Longitude: ${longitude} °`;
    }
  
    function error() {
      status.textContent = 'Unable to retrieve your location';
    }
  
    if (!navigator.geolocation) {
      status.textContent = 'Geolocation is not supported by your browser';
    } else {
      status.textContent = 'Locating…';
      navigator.geolocation.getCurrentPosition(success, error);
    }
  
  }

}
