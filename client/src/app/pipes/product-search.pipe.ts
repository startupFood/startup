import { Pipe, PipeTransform } from '@angular/core';
import { StoreMenuWithDetails } from '../classes/store-menu-with-details';

@Pipe({
  name: 'productSearch'
})
export class ProductSearchPipe implements PipeTransform {

  transform(value: StoreMenuWithDetails[], args?: any): any {
    var res = []; 
    debugger;

    for(var i=0; i<value.length ; i++)
    {
      //name , price , storeCategory , store name , location , kashrut ,product category
    if(args == "" ||
     value[i].product.productName.toLowerCase().indexOf(args.toLowerCase())!= -1 ||
    //  value[i].product.productName.indexOf(args.toUpperCase())!= -1 ||
     value[i].productCategory.indexOf(args) != -1 ||
     value[i].product.productPrice == args || 
     value[i].storeCategory.indexOf(args)!= -1  ||
     value[i].store.storeName.indexOf(args) != -1 ||
     value[i].location == args ||
     value[i].kashrut.indexOf(args) != -1
     )
    {
      res.push(value[i]);
    }
    }
    return res;
  }

}
