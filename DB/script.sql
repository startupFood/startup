USE [master]
GO
/****** Object:  Database [DB_freeQueue]    Script Date: 12/02/2020 22:25:29 ******/
CREATE DATABASE [DB_freeQueue]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DB_freeQueue', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\DB_freeQueue.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DB_freeQueue_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\DB_freeQueue_log.ldf' , SIZE = 1088KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DB_freeQueue] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DB_freeQueue].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DB_freeQueue] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DB_freeQueue] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DB_freeQueue] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DB_freeQueue] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DB_freeQueue] SET ARITHABORT OFF 
GO
ALTER DATABASE [DB_freeQueue] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [DB_freeQueue] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DB_freeQueue] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DB_freeQueue] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DB_freeQueue] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DB_freeQueue] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DB_freeQueue] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DB_freeQueue] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DB_freeQueue] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DB_freeQueue] SET  ENABLE_BROKER 
GO
ALTER DATABASE [DB_freeQueue] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DB_freeQueue] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DB_freeQueue] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DB_freeQueue] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DB_freeQueue] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DB_freeQueue] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DB_freeQueue] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DB_freeQueue] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DB_freeQueue] SET  MULTI_USER 
GO
ALTER DATABASE [DB_freeQueue] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DB_freeQueue] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DB_freeQueue] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DB_freeQueue] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [DB_freeQueue] SET DELAYED_DURABILITY = DISABLED 
GO
USE [DB_freeQueue]
GO
/****** Object:  Table [dbo].[tbl_groups]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_groups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NULL,
	[GroupName] [nvarchar](30) NULL,
	[GroupPassword] [nvarchar](100) NULL,
	[OpeningDate] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_hechshers]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_hechshers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Hechsher] [nvarchar](50) NULL,
	[HechsherImg] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_menuAddittions]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_menuAddittions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Product] [int] NOT NULL,
	[Addition] [nvarchar](30) NOT NULL,
	[AddtionPrice] [float] NOT NULL,
	[AdditionStatus] [bit] NOT NULL,
	[AdditionImage] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_menuTastes]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_menuTastes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Product] [int] NOT NULL,
	[Taste] [nvarchar](30) NOT NULL,
	[TasteStatus] [bit] NOT NULL,
	[TasteImage] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_modifyTypes]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_modifyTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ModifyType] [nvarchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_productsCategories]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_productsCategories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Store] [int] NOT NULL,
	[Category] [nvarchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchases]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchases](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[PurchaseDate] [nvarchar](20) NOT NULL,
	[CustomerName] [nvarchar](100) NOT NULL,
	[CustomerPhone] [nvarchar](20) NULL,
	[CreditCard] [nvarchar](100) NOT NULL,
	[CreditDate] [nvarchar](5) NOT NULL,
	[CreditDigits] [nvarchar](3) NOT NULL,
	[DeliveryAddress] [nvarchar](50) NULL,
	[GroupDetails] [int] NULL,
	[ReservedSeats] [int] NULL,
	[Club] [bit] NULL,
	[Tip] [float] NULL,
	[PurchaseSum] [float] NOT NULL,
	[ReceiptTime] [nvarchar](5) NOT NULL,
	[Favorite] [bit] NULL,
	[UserId] [int] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchasesAdditions]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchasesAdditions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Product] [int] NOT NULL,
	[Addition] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchasesAdditionsHistory]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchasesAdditionsHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tbl_PurchasesAdditionId] [int] NOT NULL,
	[Product] [int] NOT NULL,
	[Addition] [int] NOT NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[modifyType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchasesHistory]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchasesHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tbl_PurchaseId] [int] NOT NULL,
	[StoreId] [int] NOT NULL,
	[PurchaseDate] [nvarchar](20) NOT NULL,
	[CustomerName] [nvarchar](100) NOT NULL,
	[CustomerPhone] [nvarchar](20) NULL,
	[CreditCard] [nvarchar](100) NOT NULL,
	[CreditDate] [nvarchar](5) NOT NULL,
	[CreditDigits] [nvarchar](3) NOT NULL,
	[DeliveryAddress] [nvarchar](50) NULL,
	[GroupDetails] [int] NULL,
	[ReservedSeats] [int] NULL,
	[Club] [bit] NULL,
	[Tip] [float] NULL,
	[PurchaseSum] [float] NOT NULL,
	[ReceiptTime] [nvarchar](5) NOT NULL,
	[favorite] [bit] NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[modifyType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchasesProducts]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchasesProducts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PurchaseId] [int] NOT NULL,
	[Product] [int] NOT NULL,
	[ProductCount] [int] NOT NULL,
	[Price] [float] NOT NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchasesProductsHistory]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchasesProductsHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tbl_PurchaseProductId] [int] NOT NULL,
	[PurchaseId] [int] NOT NULL,
	[Product] [int] NOT NULL,
	[ProductCount] [int] NOT NULL,
	[Price] [float] NOT NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[modifyType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchasesTastes]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchasesTastes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Product] [int] NOT NULL,
	[Taste] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchasesTastesHistory]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchasesTastesHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tbl_PurchasesTastesId] [int] NOT NULL,
	[Product] [int] NOT NULL,
	[Taste] [int] NOT NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[modifyType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchaseToWorker]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchaseToWorker](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[PurchaseId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_responses]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_responses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StroeId] [int] NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[ResponseDate] [nvarchar](20) NOT NULL,
	[Header] [nvarchar](100) NOT NULL,
	[Content] [nvarchar](500) NOT NULL,
	[Email] [nvarchar](400) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_responsesHistory]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_responsesHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tbl_responsesId] [int] NOT NULL,
	[StroeId] [int] NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[ResponseDate] [nvarchar](20) NOT NULL,
	[Header] [nvarchar](100) NOT NULL,
	[Content] [nvarchar](500) NOT NULL,
	[Email] [nvarchar](400) NOT NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[modifyType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_roles]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleType] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_stores]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_stores](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StoreName] [nvarchar](100) NOT NULL,
	[StoreAddress] [nvarchar](200) NOT NULL,
	[Phone] [nvarchar](20) NULL,
	[About] [nvarchar](500) NULL,
	[KashrutCertification] [int] NULL,
	[Img] [nvarchar](200) NULL,
	[StoreCategory] [int] NOT NULL,
	[ReservedSeats] [bit] NULL,
	[Club] [bit] NULL,
	[Tip] [bit] NULL,
	[StoreLoad] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_storesActivityTime]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_storesActivityTime](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Store] [int] NOT NULL,
	[ActivityDay] [int] NOT NULL,
	[StartTime] [nvarchar](5) NOT NULL,
	[EndTime] [nvarchar](5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_storesBankDetails]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_storesBankDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NULL,
	[Bunk] [int] NOT NULL,
	[Brunch] [int] NOT NULL,
	[Account] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_storesCategories]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_storesCategories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_storesMenu]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_storesMenu](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Store] [int] NOT NULL,
	[ProductName] [nvarchar](100) NOT NULL,
	[ProductCategory] [int] NOT NULL,
	[ProductPrice] [float] NOT NULL,
	[ProductStatus] [bit] NOT NULL,
	[QuickProduct] [bit] NULL,
	[ProductImage] [nvarchar](100) NOT NULL,
	[PreperationTime] [int] NOT NULL,
	[Delivery] [bit] NULL,
	[AdditionsQuantity] [int] NOT NULL,
	[Barcode] [nvarchar](128) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_storesPositions]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_storesPositions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NULL,
	[Longitude] [float] NOT NULL,
	[Latitude] [float] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_users]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserEmail] [nvarchar](400) NOT NULL,
	[UserPassword] [nvarchar](500) NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[UserPhone] [nvarchar](50) NULL,
	[RoleId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_usersHistory]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_usersHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tbl_usersId] [int] NOT NULL,
	[UserEmail] [nvarchar](400) NOT NULL,
	[UserPassword] [nvarchar](500) NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[UserPhone] [nvarchar](50) NULL,
	[RoleId] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[modifyType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_workerToStore]    Script Date: 12/02/2020 22:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_workerToStore](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[StoreId] [int] NULL,
	[Tip] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[tbl_groups] ON 

INSERT [dbo].[tbl_groups] ([Id], [StoreId], [GroupName], [GroupPassword], [OpeningDate]) VALUES (18, 1, N'Family', N'ff4422', N'2020-02-10 10:00:00')
SET IDENTITY_INSERT [dbo].[tbl_groups] OFF
SET IDENTITY_INSERT [dbo].[tbl_hechshers] ON 

INSERT [dbo].[tbl_hechshers] ([Id], [Hechsher], [HechsherImg]) VALUES (1, N'Badatz', N' ')
INSERT [dbo].[tbl_hechshers] ([Id], [Hechsher], [HechsherImg]) VALUES (2, N'Meadrin jerusalem', NULL)
INSERT [dbo].[tbl_hechshers] ([Id], [Hechsher], [HechsherImg]) VALUES (3, N'Chatam sofer', NULL)
INSERT [dbo].[tbl_hechshers] ([Id], [Hechsher], [HechsherImg]) VALUES (4, N'Landau', NULL)
INSERT [dbo].[tbl_hechshers] ([Id], [Hechsher], [HechsherImg]) VALUES (5, N'Beit Yosef', NULL)
INSERT [dbo].[tbl_hechshers] ([Id], [Hechsher], [HechsherImg]) VALUES (6, N'Machpud', NULL)
INSERT [dbo].[tbl_hechshers] ([Id], [Hechsher], [HechsherImg]) VALUES (7, N'Rubin', NULL)
SET IDENTITY_INSERT [dbo].[tbl_hechshers] OFF
SET IDENTITY_INSERT [dbo].[tbl_menuAddittions] ON 

INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (1, 1, N'cream', 1, 1, N'/assets/imgs/1/cream.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2, 4, N'mashrooms', 3, 1, N'/assets/imgs/1/mashrooms.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (1002, 4, N'butter', 1, 1, N'/assets/imgs/1/butter.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2004, 1004, N'corn', 1, 1, N'/assets/imgs/1/corn.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2005, 1004, N'mashrooms', 1, 1, N'/assets/imgs/1/mashrooms.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2008, 1005, N'Eggs salad', 2, 1, N'/assets/imgs/1/eggsSalad.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2014, 1005, N'Spelles Bun', 0, 1, N'/assets/imgs/1/spelledBun.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2015, 1005, N'Israeli Salad', 1, 1, N'/assets/imgs/1/israeliSalad.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2017, 1005, N'Green Salad', 1, 1, N'/assets/imgs/1/greenSalad.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2018, 1005, N'Frittetta', 0, 1, N'/assets/imgs/1/frittetta.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2019, 1006, N'Choclate souce', 0, 1, N'/assets/imgs/1/chocolate.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2021, 1014, N'Ketchup', 0, 1, N'/assets/imgs/1/ketchup.png')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2022, 1011, N'Ketchup', 0, 1, N'/assets/imgs/1/ketchup.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2023, 1012, N'Ketchup', 0, 1, N'/assets/imgs/1/ketchup.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2024, 1014, N'Chili', 0, 1, N'/assets/imgs/1/tzili.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2027, 1011, N'Chili', 0, 1, N'/assets/imgs/1/tzili.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2028, 1012, N'Chili', 0, 1, N'/assets/imgs/1/tzili.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2029, 1021, N'Chili', 0, 1, N'/assets/imgs/1/tzili.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2030, 1021, N'corn', 0, 1, N'/assets/imgs/1/corn.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2031, 1021, N'onine', 0, 1, N'/assets/imgs/1/onine.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2032, 1020, N'mashrooms', 0, 1, N'/assets/imgs/1/mashrooms.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2033, 1021, N'Ketchup', 0, 1, N'/assets/imgs/1/ketchup.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2034, 1021, N'chips', 0, 1, N'/assets/imgs/1/chips.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2035, 1043, N'cream', 0, 1, N'/assets/imgs/1/cream.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2036, 1043, N'Choclate souce', 0, 1, N'/assets/imgs/1/chocolate.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2037, 1043, N'Pecan', 0, 1, N'/assets/imgs/1/pecan.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2038, 1043, N'Colorful pops', 0, 1, N'/assets/imgs/1/colorfulPops.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2039, 1047, N'ColorFul pops', 0, 1, N'/assets/imgs/1/colorfulPops.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2040, 1048, N'ColorFul pops', 0, 1, N'/assets/imgs/1/colorfulPops.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2041, 1049, N'ColorFul pops', 0, 1, N'/assets/imgs/1/colorfulPops.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2042, 1047, N'Pecan', 0, 1, N'/assets/imgs/1/pecan.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2043, 1048, N'Pecan', 0, 1, N'/assets/imgs/1/pecan.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2044, 1049, N'Pecan ', 0, 1, N'/assets/imgs/1/pecan.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2046, 1047, N'Cream', 0, 1, N'/assets/imgs/1/cream.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2047, 1048, N'Cream', 0, 1, N'/assets/imgs/1/cream.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2048, 1049, N'Cream ', 0, 1, N'/assets/imgs/1/cream.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2049, 1062, N'Eggplant', 0, 1, N'/assets/imgs/1/eggplant.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2050, 1063, N'Eggplant', 0, 1, N'/assets/imgs/1/eggplant.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2051, 1064, N'Eggplant', 0, 1, N'/assets/imgs/1/eggplant.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2052, 1065, N'Eggplant', 0, 1, N'/assets/imgs/1/eggplant.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2053, 1062, N'Pickels', 0, 1, N'/assets/imgs/1/Pickels.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2054, 1063, N'Pickels', 0, 1, N'/assets/imgs/1/Pickels.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2055, 1064, N'Pickels', 0, 1, N'/assets/imgs/1/Pickels.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2056, 1065, N'Pickels', 0, 1, N'/asstes/imgs/1/Pickels.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2057, 1075, N'Choclate', 0, 1, N'/assets/imgs/1/chocolate.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2058, 1074, N'Choclate', 0, 1, N'/assets/imgs/1/chocolate.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2059, 1076, N'Chocolate', 0, 1, N'/assets/imgs/1/chocolate.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2060, 1074, N'Lentis', 0, 1, N'/assets/imgs/1/choclateLentils.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2061, 1075, N'Lentis', 0, 1, N'/assets/imgs/1/choclateLentils.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2062, 1076, N'Lentis', 0, 1, N'/assets/imgs/1/choclateLentils.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2063, 1074, N'Cream', 0, 1, N'/assets/imgs/1/cream.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2064, 1075, N'Cream', 0, 1, N'/assets/imgs/1/cream.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2065, 1076, N'Cream', 0, 1, N'/assets/imgs/1/cream.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2066, 1074, N'Pecan', 1, 1, N'/assets/imgs/1/pecan.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2067, 1075, N'Pecan', 1, 1, N'/assets/imgs/1/pecan.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2068, 1076, N'Pecan', 1, 1, N'/assets/imgs/1/pecan.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2069, 1079, N'Cinamon', 0, 1, N'/assets/imgs/1/Cinamon.png')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2070, 1079, N'Cocus', 0, 1, N'/assets/imgs/1/Cocus.jpg')
INSERT [dbo].[tbl_menuAddittions] ([Id], [Product], [Addition], [AddtionPrice], [AdditionStatus], [AdditionImage]) VALUES (2071, 1095, N'Pecan', 0, 1, N'/assets/imgs/1/pecan.jpg')
SET IDENTITY_INSERT [dbo].[tbl_menuAddittions] OFF
SET IDENTITY_INSERT [dbo].[tbl_menuTastes] ON 

INSERT [dbo].[tbl_menuTastes] ([Id], [Product], [Taste], [TasteStatus], [TasteImage]) VALUES (1, 1, N'butter', 1, N'/assets/imgs/1/butter.jpg')
INSERT [dbo].[tbl_menuTastes] ([Id], [Product], [Taste], [TasteStatus], [TasteImage]) VALUES (2, 3, N'chocolate', 1, N'/assets/imgs/1/chocolate.jpg')
INSERT [dbo].[tbl_menuTastes] ([Id], [Product], [Taste], [TasteStatus], [TasteImage]) VALUES (3, 2, N'chocolate', 1, N'/assets/imgs/1/chocolate.jpg')
INSERT [dbo].[tbl_menuTastes] ([Id], [Product], [Taste], [TasteStatus], [TasteImage]) VALUES (4, 1076, N'choclate', 1, N'/assets/imgs/1/chocolate.jpg')
INSERT [dbo].[tbl_menuTastes] ([Id], [Product], [Taste], [TasteStatus], [TasteImage]) VALUES (5, 1076, N'strawberry', 1, N'/assets/imgs/1/strawberry.jpg')
INSERT [dbo].[tbl_menuTastes] ([Id], [Product], [Taste], [TasteStatus], [TasteImage]) VALUES (6, 1076, N'nugat', 1, N'/assets/imgs/1/nugat.jpg')
INSERT [dbo].[tbl_menuTastes] ([Id], [Product], [Taste], [TasteStatus], [TasteImage]) VALUES (7, 1076, N'chalva', 1, N'/assets/imgs/1/chalva.jpg')
INSERT [dbo].[tbl_menuTastes] ([Id], [Product], [Taste], [TasteStatus], [TasteImage]) VALUES (8, 1076, N'vanile', 1, N'/assets/imgs/1/vanile.jpg')
SET IDENTITY_INSERT [dbo].[tbl_menuTastes] OFF
SET IDENTITY_INSERT [dbo].[tbl_productsCategories] ON 

INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (1, 1, N' Hot Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (2, 1, N'Pasteries')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (3, 1, N'Ice cream')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (4, 1, N'Pizza')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (5, 1, N'Breakfast')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (9, 4, N'Burger')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (10, 4, N'Fish')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (11, 6, N'Sushi')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (12, 1, N'Desert')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (13, 4, N'Desert')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (14, 6, N'Desert')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (15, 4, N'Hot Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (16, 6, N'Hot Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (17, 6, N'Speical')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (18, 4, N'Children')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (19, 4, N'Additions')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (20, 6, N'Children')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (21, 4, N'Cold Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (22, 6, N'Cold Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (23, 1, N'Cold Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (24, 11, N'Cold Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (27, 28, N'Cold Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (28, 28, N'Hot Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (34, 24, N'Ice Cream')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (35, 24, N'Desert')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (36, 1, N'Pasta')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (37, 20, N'Cold Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (38, 11, N'Shawarme')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (39, 11, N'Falafel')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (40, 13, N'Falafel')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (41, 13, N'Cold Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (42, 14, N'Falafel')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (43, 14, N'Cold Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (44, 15, N'Falafel')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (45, 15, N'Cold Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (46, 20, N'Humus')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (47, 20, N'Cold Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (48, 20, N'Falafel')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (49, 21, N'Hot Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (50, 21, N'Cold Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (51, 21, N'Hot Salad')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (52, 21, N'Salad')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (53, 22, N'Cold Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (54, 22, N'Hot Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (55, 22, N'Desert')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (56, 23, N'Desert')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (57, 22, N'Ice Cream')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (58, 23, N'Ice Cream')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (59, 23, N'Cold Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (60, 23, N'Hot Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (61, 30, N'Ice Cream')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (62, 30, N'Desert')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (63, 27, N'Pizza')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (64, 27, N'Desert')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (65, 27, N'Cold Drink')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (66, 27, N'Pasta')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (67, 1, N'Shake')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (68, 11, N'Burgers')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (69, 28, N'Shake')
INSERT [dbo].[tbl_productsCategories] ([Id], [Store], [Category]) VALUES (70, 24, N'Shake')
SET IDENTITY_INSERT [dbo].[tbl_productsCategories] OFF
SET IDENTITY_INSERT [dbo].[tbl_purchases] ON 

INSERT [dbo].[tbl_purchases] ([Id], [StoreId], [PurchaseDate], [CustomerName], [CustomerPhone], [CreditCard], [CreditDate], [CreditDigits], [DeliveryAddress], [GroupDetails], [ReservedSeats], [Club], [Tip], [PurchaseSum], [ReceiptTime], [Favorite], [UserId], [Status]) VALUES (103, 1, N'10/02/2020', N'Mr. Choen', N'0548473226', N'00000000', N'00/00', N'000', N'', NULL, 0, 1, 0, 25, N'13:30', 1, 1, 0)
INSERT [dbo].[tbl_purchases] ([Id], [StoreId], [PurchaseDate], [CustomerName], [CustomerPhone], [CreditCard], [CreditDate], [CreditDigits], [DeliveryAddress], [GroupDetails], [ReservedSeats], [Club], [Tip], [PurchaseSum], [ReceiptTime], [Favorite], [UserId], [Status]) VALUES (104, 1, N'10/02/2020', N'Mrs. Levi', N'0583250606', N'00000000', N'00/00', N'000', NULL, NULL, 2, 1, 0, 25, N'13:00', 1, 1, 1)
INSERT [dbo].[tbl_purchases] ([Id], [StoreId], [PurchaseDate], [CustomerName], [CustomerPhone], [CreditCard], [CreditDate], [CreditDigits], [DeliveryAddress], [GroupDetails], [ReservedSeats], [Club], [Tip], [PurchaseSum], [ReceiptTime], [Favorite], [UserId], [Status]) VALUES (105, 1, N'10/02/2020', N'Lubow Family', N'025863207', N'00000000', N'00/00', N'000', NULL, 18, 10, 1, 0, 100, N'20:00', 1, 1, 1)
SET IDENTITY_INSERT [dbo].[tbl_purchases] OFF
SET IDENTITY_INSERT [dbo].[tbl_purchasesAdditions] ON 

INSERT [dbo].[tbl_purchasesAdditions] ([Id], [Product], [Addition]) VALUES (2027, 2076, 1002)
INSERT [dbo].[tbl_purchasesAdditions] ([Id], [Product], [Addition]) VALUES (2028, 2077, 1002)
SET IDENTITY_INSERT [dbo].[tbl_purchasesAdditions] OFF
SET IDENTITY_INSERT [dbo].[tbl_purchasesProducts] ON 

INSERT [dbo].[tbl_purchasesProducts] ([Id], [PurchaseId], [Product], [ProductCount], [Price], [Status]) VALUES (2076, 103, 4, 1, 25, 0)
INSERT [dbo].[tbl_purchasesProducts] ([Id], [PurchaseId], [Product], [ProductCount], [Price], [Status]) VALUES (2077, 104, 4, 1, 25, 0)
INSERT [dbo].[tbl_purchasesProducts] ([Id], [PurchaseId], [Product], [ProductCount], [Price], [Status]) VALUES (2078, 105, 1, 10, 100, 0)
SET IDENTITY_INSERT [dbo].[tbl_purchasesProducts] OFF
SET IDENTITY_INSERT [dbo].[tbl_purchasesTastes] ON 

INSERT [dbo].[tbl_purchasesTastes] ([Id], [Product], [Taste]) VALUES (43, 2078, 1)
SET IDENTITY_INSERT [dbo].[tbl_purchasesTastes] OFF
SET IDENTITY_INSERT [dbo].[tbl_roles] ON 

INSERT [dbo].[tbl_roles] ([Id], [RoleType]) VALUES (1, N'system')
INSERT [dbo].[tbl_roles] ([Id], [RoleType]) VALUES (2, N'developer')
INSERT [dbo].[tbl_roles] ([Id], [RoleType]) VALUES (3, N'customer')
INSERT [dbo].[tbl_roles] ([Id], [RoleType]) VALUES (4, N'store')
SET IDENTITY_INSERT [dbo].[tbl_roles] OFF
SET IDENTITY_INSERT [dbo].[tbl_stores] ON 

INSERT [dbo].[tbl_stores] ([Id], [StoreName], [StoreAddress], [Phone], [About], [KashrutCertification], [Img], [StoreCategory], [ReservedSeats], [Club], [Tip], [StoreLoad]) VALUES (1, N'Tostim', N'Jerusalem , the coffee 2 ', N'02-3345654', N'', 1, N'/assets/imgs/1/dairyStore.png', 1, 1, 0, 0, 0)
INSERT [dbo].[tbl_stores] ([Id], [StoreName], [StoreAddress], [Phone], [About], [KashrutCertification], [Img], [StoreCategory], [ReservedSeats], [Club], [Tip], [StoreLoad]) VALUES (4, N'Auto - Shnitzel', N'Jerusalem, Jaffa', N'02-5544345', N'', 2, N'/assets/imgs/1/ShnitzelStore.png', 4, 1, 1, 1, 0)
INSERT [dbo].[tbl_stores] ([Id], [StoreName], [StoreAddress], [Phone], [About], [KashrutCertification], [Img], [StoreCategory], [ReservedSeats], [Club], [Tip], [StoreLoad]) VALUES (6, N'Oshi-Sushi', N'Tel-Aviv', N'03-6767565', N'', 1, N'/assets/imgs/1/logo-sushi.png', 3, 1, 1, 1, 1)
INSERT [dbo].[tbl_stores] ([Id], [StoreName], [StoreAddress], [Phone], [About], [KashrutCertification], [Img], [StoreCategory], [ReservedSeats], [Club], [Tip], [StoreLoad]) VALUES (11, N'Burger King', N'Modiin', N'08-9999999', NULL, 6, N'/assets/imgs/1/BurgerKing_store.jpg', 4, 1, 0, 1, 0)
INSERT [dbo].[tbl_stores] ([Id], [StoreName], [StoreAddress], [Phone], [About], [KashrutCertification], [Img], [StoreCategory], [ReservedSeats], [Club], [Tip], [StoreLoad]) VALUES (13, N'Falafel Gina', N'Modiin', N'08-9999991', NULL, 5, N'/assets/imgs/1/falafelGina.png', 5, 1, 1, 1, 0)
INSERT [dbo].[tbl_stores] ([Id], [StoreName], [StoreAddress], [Phone], [About], [KashrutCertification], [Img], [StoreCategory], [ReservedSeats], [Club], [Tip], [StoreLoad]) VALUES (14, N'Falafel Adar', N'Rishon Letzion', N'08-9999988', NULL, 5, N'/assets/imgs/1/falafelAdar.jpg', 5, 1, 1, 1, 0)
INSERT [dbo].[tbl_stores] ([Id], [StoreName], [StoreAddress], [Phone], [About], [KashrutCertification], [Img], [StoreCategory], [ReservedSeats], [Club], [Tip], [StoreLoad]) VALUES (15, N'Falafel Baribua', N'Lud', N'08-9999981', NULL, 4, N'/assets/imgs/1/falafelBaribua.png', 5, 1, 1, 1, 1)
INSERT [dbo].[tbl_stores] ([Id], [StoreName], [StoreAddress], [Phone], [About], [KashrutCertification], [Img], [StoreCategory], [ReservedSeats], [Club], [Tip], [StoreLoad]) VALUES (20, N'Kaspi', N'Modiin', N'08-9988999', NULL, 5, N'/assets/imgs/1/kaspiHumus.png', 7, 1, 1, 1, 0)
INSERT [dbo].[tbl_stores] ([Id], [StoreName], [StoreAddress], [Phone], [About], [KashrutCertification], [Img], [StoreCategory], [ReservedSeats], [Club], [Tip], [StoreLoad]) VALUES (21, N'Chef Salad', N'Tel Aviv', N'03-3344554', NULL, 3, N'/assets/imgs/1/chefSalad.jpg', 8, 1, 1, 1, 0)
INSERT [dbo].[tbl_stores] ([Id], [StoreName], [StoreAddress], [Phone], [About], [KashrutCertification], [Img], [StoreCategory], [ReservedSeats], [Club], [Tip], [StoreLoad]) VALUES (22, N'Anita', N'Haifa', N'08-9997779', NULL, 4, N'/assets/imgs/1/anitaICream.jpg', 6, 1, 1, 1, 1)
INSERT [dbo].[tbl_stores] ([Id], [StoreName], [StoreAddress], [Phone], [About], [KashrutCertification], [Img], [StoreCategory], [ReservedSeats], [Club], [Tip], [StoreLoad]) VALUES (23, N'Bianco Nero', N'Modiin', N'08-9997788', NULL, 5, N'/assets/imgs/1/biancoNeroICream.png', 6, 1, 1, 1, 0)
INSERT [dbo].[tbl_stores] ([Id], [StoreName], [StoreAddress], [Phone], [About], [KashrutCertification], [Img], [StoreCategory], [ReservedSeats], [Club], [Tip], [StoreLoad]) VALUES (24, N'BFresh', N'Safed', N'08-9987878', NULL, 6, N'/assets/imgs/1/BFreshShake.png', 9, 1, 1, 1, 0)
INSERT [dbo].[tbl_stores] ([Id], [StoreName], [StoreAddress], [Phone], [About], [KashrutCertification], [Img], [StoreCategory], [ReservedSeats], [Club], [Tip], [StoreLoad]) VALUES (27, N'Pizza Center', N'Beer Sheva', N'08-9955555', NULL, 7, N'/assets/imgs/1/pizzaCenter.png', 1, 1, 1, 1, 1)
INSERT [dbo].[tbl_stores] ([Id], [StoreName], [StoreAddress], [Phone], [About], [KashrutCertification], [Img], [StoreCategory], [ReservedSeats], [Club], [Tip], [StoreLoad]) VALUES (28, N'Re Bar', N'Ashdod', N'08-9994444', NULL, 3, N'/assets/imgs/1/reBarShake.jpg', 9, 1, 1, 1, 1)
INSERT [dbo].[tbl_stores] ([Id], [StoreName], [StoreAddress], [Phone], [About], [KashrutCertification], [Img], [StoreCategory], [ReservedSeats], [Club], [Tip], [StoreLoad]) VALUES (30, N'Vaniglida', N'Jerualem', N'02-6565555', NULL, 2, N'/assets/imgs/1/vaniglida.png', 6, 1, 1, 1, 0)
SET IDENTITY_INSERT [dbo].[tbl_stores] OFF
SET IDENTITY_INSERT [dbo].[tbl_storesActivityTime] ON 

INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (4, 1, 0, N'10:00', N'22:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (5, 1, 1, N'17:05', N'22:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (6, 1, 2, N'10:00', N'23:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (7, 1, 3, N'09:45', N'22:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (8, 1, 4, N'10:00', N'22:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (9, 1, 5, N'10:00', N'13:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1009, 4, 0, N'08:00', N'21:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1010, 4, 1, N'08:00', N'21:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1011, 4, 2, N'08:00', N'21:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1012, 4, 3, N'08:00', N'21:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1013, 4, 4, N'08:00', N'21:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1014, 4, 5, N'08:00', N'13:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1015, 4, 6, N'21:00', N'23:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1016, 6, 0, N'08:00', N'21:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1017, 6, 1, N'08:00', N'21:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1018, 6, 2, N'08:00', N'21:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1019, 6, 3, N'08:00', N'21:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1020, 6, 4, N'08:00', N'21:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1021, 6, 5, N'08:00', N'13:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1022, 6, 6, N'21:00', N'23:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1023, 11, 0, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1024, 11, 1, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1025, 11, 2, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1026, 11, 3, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1027, 11, 4, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1028, 11, 5, N'10:00', N'14:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1029, 11, 6, N'20:00', N'23:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1030, 13, 0, N'10:00', N'22:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1031, 13, 1, N'10:00', N'22:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1032, 13, 2, N'10:00', N'22:20')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1033, 13, 3, N'10:00', N'22:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1034, 13, 4, N'10:00', N'22:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1035, 13, 5, N'10:00', N'13:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1036, 13, 6, N'20:00', N'23:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1037, 14, 0, N'09:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1038, 14, 1, N'09:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1039, 14, 2, N'09:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1040, 14, 3, N'09:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1041, 14, 4, N'09:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1042, 14, 5, N'09:00', N'13:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1043, 14, 6, N'20:00', N'23:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1044, 15, 0, N'09:20', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1045, 15, 1, N'09:10', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1046, 15, 2, N'09:20', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1047, 15, 3, N'09:39', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1048, 15, 4, N'13:00', N'22:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1049, 15, 5, N'09:00', N'13:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1050, 15, 6, N'20:00', N'22:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1051, 20, 0, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1052, 20, 1, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1053, 20, 2, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1054, 20, 3, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1055, 20, 4, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1056, 20, 5, N'10:00', N'13:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1057, 20, 6, N'20:00', N'23:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1058, 21, 0, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1059, 21, 1, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1060, 21, 2, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1061, 21, 3, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1062, 21, 4, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1063, 21, 5, N'10:00', N'13:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1064, 21, 6, N'21:00', N'23:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1065, 22, 0, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1066, 22, 1, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1067, 22, 2, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1068, 22, 3, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1069, 22, 4, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1070, 22, 5, N'10:00', N'13:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1071, 22, 6, N'20:00', N'22:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1072, 23, 0, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1073, 23, 1, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1074, 23, 2, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1076, 23, 3, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1077, 23, 4, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1078, 23, 5, N'10:00', N'13:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1079, 23, 6, N'20:00', N'23:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1080, 24, 0, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1081, 24, 1, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1082, 24, 2, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1083, 24, 3, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1084, 24, 4, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1085, 24, 5, N'10:00', N'13:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1086, 24, 6, N'20:00', N'23:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1087, 27, 0, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1088, 27, 1, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1089, 27, 2, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1090, 27, 3, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1091, 27, 4, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1092, 27, 5, N'10:00', N'13:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1093, 27, 6, N'20:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1094, 28, 0, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1095, 28, 1, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1096, 28, 2, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1097, 28, 3, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1098, 28, 4, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1099, 28, 5, N'10:00', N'13:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1100, 28, 6, N'20:00', N'22:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1101, 30, 0, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1102, 30, 1, N'10:00', N'22:00')
GO
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1103, 30, 2, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1104, 30, 3, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1105, 30, 4, N'10:00', N'22:00')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1106, 30, 5, N'10:00', N'13:30')
INSERT [dbo].[tbl_storesActivityTime] ([Id], [Store], [ActivityDay], [StartTime], [EndTime]) VALUES (1107, 30, 6, N'20:00', N'24:00')
SET IDENTITY_INSERT [dbo].[tbl_storesActivityTime] OFF
SET IDENTITY_INSERT [dbo].[tbl_storesCategories] ON 

INSERT [dbo].[tbl_storesCategories] ([Id], [Category]) VALUES (1, N'Pizza')
INSERT [dbo].[tbl_storesCategories] ([Id], [Category]) VALUES (2, N'Dairy')
INSERT [dbo].[tbl_storesCategories] ([Id], [Category]) VALUES (3, N'Sushi')
INSERT [dbo].[tbl_storesCategories] ([Id], [Category]) VALUES (4, N'Meaty')
INSERT [dbo].[tbl_storesCategories] ([Id], [Category]) VALUES (5, N'Falafel')
INSERT [dbo].[tbl_storesCategories] ([Id], [Category]) VALUES (6, N'Ice Cream')
INSERT [dbo].[tbl_storesCategories] ([Id], [Category]) VALUES (7, N'Humus')
INSERT [dbo].[tbl_storesCategories] ([Id], [Category]) VALUES (8, N'Salad')
INSERT [dbo].[tbl_storesCategories] ([Id], [Category]) VALUES (9, N'Shakes')
SET IDENTITY_INSERT [dbo].[tbl_storesCategories] OFF
SET IDENTITY_INSERT [dbo].[tbl_storesMenu] ON 

INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1, 1, N'Coffee', 1, 10, 1, 1, N'./assets/imgs/1/coffee.jpg', 3, NULL, 0, NULL)
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (2, 1, N'Croissant', 2, 12, 1, 1, N'./assets/imgs/1/croissant.jpg', 1, 1, 0, NULL)
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (3, 1, N'Cappuccino', 1, 10, 1, 1, N'./assets/imgs/1/cappuccino.jpg', 3, 0, 2, NULL)
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (4, 1, N'Tost', 2, 25, 1, 1, N'./assets/imgs/1/Tost.jpg', 10, 1, 5, N'12221333000')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1004, 1, N'Pizza', 4, 50, 1, 0, N'./assets/imgs/1/pizza.jpg', 15, 1, 2, N'234560000010')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1005, 1, N'breakfast', 5, 39, 1, 0, N'./assets/imgs/1/Breakfast.jpg', 10, 1, 5, N'111222003332')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1006, 1, N'Ice cream', 3, 12, 1, 1, N'./assets/imgs/1/vanile-cookie-icecream.jpg', 1, 0, 1, N'345634562222')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1011, 4, N'Burger', 9, 40, 1, 0, N'./assets/imgs/1/burger.jpg', 15, 1, 3, N'123451234444')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1012, 4, N'Chips', 9, 5, 1, 1, N'./assets/imgs/1/chips.jpg', 1, 1, 0, N'123324456778')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1013, 6, N'Sushi', 11, 35, 1, 1, N'./assets/imgs/1/sushi2.jpg', 10, 1, 2, N'134346675676')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1014, 4, N'Fish&Chips', 10, 40, 1, 1, N'./assets/imgs/1/fish_and_chips.jpg', 15, 1, 2, N'434343434555')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1017, 11, N'Fish&Chips', 10, 40, 1, 1, N'./assets/imgs/1/fish_and_chips.jpg', 15, 1, 2, N'123412333333')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1018, 11, N'Coca Cola', 24, 8, 1, 1, N'./assets/imgs/1/cocacola.png', 1, 1, 0, N'123456675444')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1020, 11, N'Diet Coke', 24, 8, 1, 1, N'./assets/imgs/1/colaDiet.jpg', 1, 1, 0, N'098878777788')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1021, 11, N'Shawarma', 38, 45, 1, 1, N'./assets/imgs/1/shawarma.jpg', 10, 1, 5, N'121212121222')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1023, 11, N'Falafel', 39, 15, 1, 1, N'./assets/imgs/1/falafel.jpg', 8, 1, 5, N'786546578976')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1024, 13, N'Coca Cola', 41, 8, 1, 1, N'./assets/imgs/1/cocacola.png', 1, 1, 0, N'123443442323')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1025, 14, N'Coca Cola', 43, 8, 1, 1, N'./assets/imgs/1/cocacola.png', 1, 1, 0, N'455555555555')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1026, 15, N'Coca Cola', 45, 8, 1, 1, N'./assets/imgs/1/cocacola.png', 1, 1, 0, N'144444444444')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1027, 21, N'Coca Cola', 50, 8, 1, 1, N'./assets/imgs/1/cocacola.png', 1, 1, 0, N'233333333333')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1028, 1, N'Coca Cola', 23, 8, 1, 1, N'./assets/imgs/1/cocacola.png', 1, 1, 0, N'222223333333')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1029, 1, N'Mineral Water', 23, 8, 1, 1, N'./assets/imgs/1/mineralWater.jpg', 1, 1, 0, N'223333333332')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1030, 1, N'Diet Cola', 23, 8, 1, 1, N'./assets/imgs/1/colaDiet.jpg', 1, 1, 0, N'344889898888')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1031, 1, N'Soda', 23, 8, 1, 1, N'./assets/imgs/1/soda.png', 1, 1, 0, N'765687656767')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1032, 4, N'Coca Cola', 21, 8, 1, 1, N'./assets/imgs/1/cocacola.png', 1, 1, 0, N'765435678976')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1033, 4, N'Diet Cola', 21, 8, 1, 1, N'./assets/imgs/1/colaDiet.jpg', 1, 1, 0, N'765467755565')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1034, 4, N'Mineral Water', 21, 8, 1, 1, N'./assets/imgs/1/mineralWater.jpg', 1, 1, 0, N'765456786577')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1035, 4, N'Soda', 21, 8, 1, 1, N'./assets/imgs/1/soda.png', 1, 1, 0, N'675467865656')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1036, 6, N'Coca Cola ', 22, 8, 1, 1, N'./assets/imgs/1/cocacola.png', 1, 1, 0, N'765467655556')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1037, 6, N'Diet Cola', 22, 8, 1, 1, N'./assets/imgs/1/colaDiet.jpg', 1, 1, 0, N'546578786578')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1038, 6, N'Mineral Water', 22, 8, 1, 1, N'./assets/imgs/1/mineralWater.jpg', 1, 1, 0, N'876545767666')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1039, 6, N'Soda', 22, 8, 1, 1, N'./assets/imgs/1/soda.png', 1, 1, 0, N'876767686766')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1041, 1, N'Tea', 1, 10, 1, 1, N'./assets/imgs/1/tea.png', 5, 1, 0, N'876545768765')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1043, 30, N'Vanila Cookie', 61, 20, 1, 1, N'./assets/imgs/1/vanile-cookie-icecream.jpg', 2, 1, 3, N'656576556565')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1047, 23, N'Vanila Cookie', 62, 20, 1, 1, N'./assets/imgs/1/vanile-cookie-icecream.jpg', 2, 1, 3, N'323232444444')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1048, 22, N'Vanila Cookie', 57, 18, 1, 1, N'./assets/imgs/1/vanile-cookie-icecream.jpg', 2, 1, 3, N'766768686766')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1049, 24, N'Vanila Cookie', 34, 15, 1, 1, N'./assets/imgs/1/vanile-cookie-icecream.jpg', 2, 1, 3, N'786876786876')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1051, 1, N'Ananas Banana', 67, 20, 1, 1, N'./assets/imgs/1/ananasBananaShake.jpg', 5, 1, 0, N'787987887888')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1052, 1, N'Berry', 67, 18, 1, 1, N'./assets/imgs/1/BerriesShake.jpg', 5, 1, 0, N'787987979778')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1053, 20, N'Humus', 46, 30, 1, 1, N'./assets/imgs/1/humus&meat.jpg', 10, 1, 0, N'878798787778')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1055, 21, N'Green Salad', 52, 30, 1, 1, N'./assets/imgs/1/greenSalad.jpg', 10, 1, 1, N'897879798788')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1056, 21, N'Israeli Salad', 52, 20, 1, 1, N'./assets/imgs/1/israeliSalad.jpg', 8, 1, 0, N'787866565777')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1062, 14, N'Falafel', 42, 15, 1, 1, N'./assets/imgs/1/FalafelKululu.png', 7, 1, 2, N'788998249809')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1063, 20, N'Falafel', 48, 13, 1, 1, N'./assets/imgs/1/Falafel2.jpg', 6, 1, 2, N'878797874444')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1064, 15, N'Falafel', 44, 15, 1, 1, N'./assets/imgs/1/Falafel3.png', 7, 1, 2, N'787798789777')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1065, 13, N'Falafel', 40, 15, 1, 1, N'./assets/imgs/1/Falafel4.jpg', 7, 1, 2, N'787877878787')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1066, 20, N'Sprite', 47, 8, 1, 1, N'./assets/imgs/1/sprite.jpg', 1, 1, 0, N'787897787899')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1067, 11, N'Speical Burger', 68, 45, 1, 1, N'./assets/imgs/1/Burger2.jpg', 10, 1, 3, N'878793748274')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1071, 11, N'Spring', 24, 8, 1, 1, N'./assets/imgs/1/spring.png', 1, 1, 0, N'090988880000')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1072, 22, N'Mineral Water', 53, 8, 1, 1, N'./assets/imgs/1/mineralWater.jpg', 1, 1, 0, N'932840829482')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1073, 22, N'Espresso', 54, 10, 1, 1, N'./assets/imgs/1/espresso.jpg', 1, 1, 0, N'003003003034')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1074, 22, N'Krep', 55, 20, 1, 1, N'./assets/imgs/1/Krep.jpg', 5, 1, 0, N'398137098789')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1075, 23, N'Banana Krep', 56, 25, 1, 1, N'./assets/imgs/1/bananaKrep.jpg', 10, 1, 2, N'787897284782')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1076, 23, N'Ice Cream', 58, 18, 1, 1, N'./assets/imgs/1/iceCream.jpg', 4, 1, 2, N'747823478237')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1077, 23, N'Tea', 60, 10, 1, 1, N'./assets/imgs/1/tea.png', 5, 1, 0, N'043004004040')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1078, 23, N'Fuzetea', 59, 8, 1, 1, N'./assets/imgs/1/fuzeTea.jpg', 1, 1, 0, N'723748927842')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1079, 21, N'Sachlab', 51, 10, 1, 1, N'./assets/imgs/1/Sachleb.jpg', 5, 1, 1, N'742674627896')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1081, 21, N'Hot Salad', 52, 40, 1, 0, N'./assets/imgs/1/hotSalad.jpg', 20, 1, 0, N'748729874982')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1082, 27, N'Margaritta', 63, 70, 1, 0, N'./assets/imgs/1/margaritta.jpg', 30, 1, 0, N'686786877866')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1083, 27, N'Krep', 64, 20, 1, 0, N'./assets/imgs/1/Krep.jpg', 10, 1, 0, N'654389657583785')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1084, 27, N'Fuzetea', 65, 8, 1, 1, N'./assets/imgs/1/fuzeTea.jpg', 1, 1, 0, N'957686753865')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1085, 27, N'Pasta', 66, 50, 1, 0, N'./assets/imgs/1/Pasta.jpg', 20, 1, 0, N'578637855674')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1086, 28, N'Tea', 28, 10, 1, 1, N'./assets/imgs/1/tea.png', 5, 1, 0, N'786583647566')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1087, 28, N'Soda', 27, 8, 1, 1, N'./assets/imgs/1/soda.png', 1, 1, 0, N'573785375384')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1088, 28, N'Green Shake', 69, 20, 1, 1, N'./assets/imgs/1/greenShake.jpg', 5, 1, 0, N'576573564765')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1089, 28, N'Strawberry', 69, 22, 1, 1, N'./assets/imgs/1/strawberrryShake.jpg', 5, 1, 0, N'567673567436')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1090, 30, N'Sofle', 62, 15, 1, 1, N'./assets/imgs/1/sofle.jpg', 5, 1, 0, N'676860000000')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1091, 30, N'Cheese Cake', 62, 18, 1, 1, N'./assets/imgs/1/cheeseCake.jpg', 5, 1, 0, N'776483649866')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1093, 24, N'Yellow Shake', 70, 20, 1, 1, N'./assets/imgs/1/yellowShake.jpg', 5, 1, 0, N'4573845837557')
INSERT [dbo].[tbl_storesMenu] ([Id], [Store], [ProductName], [ProductCategory], [ProductPrice], [ProductStatus], [QuickProduct], [ProductImage], [PreperationTime], [Delivery], [AdditionsQuantity], [Barcode]) VALUES (1095, 24, N'PanCake', 35, 20, 1, 0, N'./assets/imgs/1/pancake.jpg', 10, 1, 1, N'5766483695736')
SET IDENTITY_INSERT [dbo].[tbl_storesMenu] OFF
SET IDENTITY_INSERT [dbo].[tbl_storesPositions] ON 

INSERT [dbo].[tbl_storesPositions] ([Id], [StoreId], [Longitude], [Latitude]) VALUES (2, 1, 35.20307, 31.77886)
INSERT [dbo].[tbl_storesPositions] ([Id], [StoreId], [Longitude], [Latitude]) VALUES (3, 4, 35.20307, 31.77886)
INSERT [dbo].[tbl_storesPositions] ([Id], [StoreId], [Longitude], [Latitude]) VALUES (4, 6, 34.78805, 32.07358)
INSERT [dbo].[tbl_storesPositions] ([Id], [StoreId], [Longitude], [Latitude]) VALUES (5, 11, 35.00741, 31.89916)
INSERT [dbo].[tbl_storesPositions] ([Id], [StoreId], [Longitude], [Latitude]) VALUES (6, 14, 34.80162, 31.96102)
INSERT [dbo].[tbl_storesPositions] ([Id], [StoreId], [Longitude], [Latitude]) VALUES (9, 13, 35.00741, 31.89916)
INSERT [dbo].[tbl_storesPositions] ([Id], [StoreId], [Longitude], [Latitude]) VALUES (10, 21, 34.78805, 32.07358)
INSERT [dbo].[tbl_storesPositions] ([Id], [StoreId], [Longitude], [Latitude]) VALUES (11, 15, 34.887909, 31.947035)
INSERT [dbo].[tbl_storesPositions] ([Id], [StoreId], [Longitude], [Latitude]) VALUES (12, 22, 34.98876, 32.81728)
INSERT [dbo].[tbl_storesPositions] ([Id], [StoreId], [Longitude], [Latitude]) VALUES (13, 20, 35.00741, 31.89916)
INSERT [dbo].[tbl_storesPositions] ([Id], [StoreId], [Longitude], [Latitude]) VALUES (14, 23, 35.00741, 31.89916)
INSERT [dbo].[tbl_storesPositions] ([Id], [StoreId], [Longitude], [Latitude]) VALUES (15, 28, 34.64321, 31.78777)
INSERT [dbo].[tbl_storesPositions] ([Id], [StoreId], [Longitude], [Latitude]) VALUES (17, 27, 34.79399, 31.24387)
INSERT [dbo].[tbl_storesPositions] ([Id], [StoreId], [Longitude], [Latitude]) VALUES (18, 24, 35.53681, 32.79509)
INSERT [dbo].[tbl_storesPositions] ([Id], [StoreId], [Longitude], [Latitude]) VALUES (20, 30, 35.20305, 31.77887)
SET IDENTITY_INSERT [dbo].[tbl_storesPositions] OFF
SET IDENTITY_INSERT [dbo].[tbl_users] ON 

INSERT [dbo].[tbl_users] ([Id], [UserEmail], [UserPassword], [UserName], [UserPhone], [RoleId]) VALUES (1, N'etti8972@gmail.com', N'1234', N'Etty Kraus', NULL, 2)
INSERT [dbo].[tbl_users] ([Id], [UserEmail], [UserPassword], [UserName], [UserPhone], [RoleId]) VALUES (2, N'feigiestis@gmail.com', N'7656e05467edd809f5a07cd4d7cbc7444bc813c2c47dcaac96f2cc3b0827f18afa5169cd4a4d83fa411281f99a81569019ccd4609745dbf5d22d6d45c6b0194e', N'Feigi Lobow', NULL, 2)
INSERT [dbo].[tbl_users] ([Id], [UserEmail], [UserPassword], [UserName], [UserPhone], [RoleId]) VALUES (3, N'freequeues@gmail.com', N'0000', N'system', NULL, 1)
INSERT [dbo].[tbl_users] ([Id], [UserEmail], [UserPassword], [UserName], [UserPhone], [RoleId]) VALUES (7, N'stam', N'stamdfdf', N'fgfgdf', N'46565', 3)
INSERT [dbo].[tbl_users] ([Id], [UserEmail], [UserPassword], [UserName], [UserPhone], [RoleId]) VALUES (8, N'mor.lubow@gmail.com', N'mor44221', N'Lubow', N'025863207', 3)
INSERT [dbo].[tbl_users] ([Id], [UserEmail], [UserPassword], [UserName], [UserPhone], [RoleId]) VALUES (9, N'estiseg@gmail.com', N'est11221', N'Estis', N'089743102', 3)
INSERT [dbo].[tbl_users] ([Id], [UserEmail], [UserPassword], [UserName], [UserPhone], [RoleId]) VALUES (10, N'estiseg@gmail.com', N'est11111', N'Est', N'0548473226', 3)
INSERT [dbo].[tbl_users] ([Id], [UserEmail], [UserPassword], [UserName], [UserPhone], [RoleId]) VALUES (11, N'estiseg@gmail.com', N'est22222', N'Estiss', N'0548473226', 3)
SET IDENTITY_INSERT [dbo].[tbl_users] OFF
SET IDENTITY_INSERT [dbo].[tbl_workerToStore] ON 

INSERT [dbo].[tbl_workerToStore] ([Id], [UserId], [StoreId], [Tip]) VALUES (1, 7, 1, NULL)
INSERT [dbo].[tbl_workerToStore] ([Id], [UserId], [StoreId], [Tip]) VALUES (2, 8, 1, NULL)
INSERT [dbo].[tbl_workerToStore] ([Id], [UserId], [StoreId], [Tip]) VALUES (3, 9, 4, NULL)
INSERT [dbo].[tbl_workerToStore] ([Id], [UserId], [StoreId], [Tip]) VALUES (4, 10, 14, NULL)
INSERT [dbo].[tbl_workerToStore] ([Id], [UserId], [StoreId], [Tip]) VALUES (5, 11, 11, NULL)
SET IDENTITY_INSERT [dbo].[tbl_workerToStore] OFF
ALTER TABLE [dbo].[tbl_groups]  WITH CHECK ADD FOREIGN KEY([StoreId])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_groups]  WITH CHECK ADD FOREIGN KEY([StoreId])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_menuAddittions]  WITH CHECK ADD FOREIGN KEY([Product])
REFERENCES [dbo].[tbl_storesMenu] ([Id])
GO
ALTER TABLE [dbo].[tbl_menuAddittions]  WITH CHECK ADD FOREIGN KEY([Product])
REFERENCES [dbo].[tbl_storesMenu] ([Id])
GO
ALTER TABLE [dbo].[tbl_menuTastes]  WITH CHECK ADD FOREIGN KEY([Product])
REFERENCES [dbo].[tbl_storesMenu] ([Id])
GO
ALTER TABLE [dbo].[tbl_menuTastes]  WITH CHECK ADD FOREIGN KEY([Product])
REFERENCES [dbo].[tbl_storesMenu] ([Id])
GO
ALTER TABLE [dbo].[tbl_productsCategories]  WITH CHECK ADD FOREIGN KEY([Store])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_productsCategories]  WITH CHECK ADD FOREIGN KEY([Store])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchases]  WITH CHECK ADD FOREIGN KEY([GroupDetails])
REFERENCES [dbo].[tbl_groups] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchases]  WITH CHECK ADD FOREIGN KEY([GroupDetails])
REFERENCES [dbo].[tbl_groups] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchases]  WITH CHECK ADD FOREIGN KEY([StoreId])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchases]  WITH CHECK ADD FOREIGN KEY([StoreId])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchases]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_users] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchases]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_users] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesAdditions]  WITH CHECK ADD FOREIGN KEY([Addition])
REFERENCES [dbo].[tbl_menuAddittions] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesAdditions]  WITH CHECK ADD FOREIGN KEY([Addition])
REFERENCES [dbo].[tbl_menuAddittions] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesAdditions]  WITH CHECK ADD FOREIGN KEY([Product])
REFERENCES [dbo].[tbl_purchasesProducts] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesAdditions]  WITH CHECK ADD FOREIGN KEY([Product])
REFERENCES [dbo].[tbl_purchasesProducts] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesAdditionsHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesAdditionsHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesProducts]  WITH CHECK ADD FOREIGN KEY([Product])
REFERENCES [dbo].[tbl_storesMenu] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesProducts]  WITH CHECK ADD FOREIGN KEY([Product])
REFERENCES [dbo].[tbl_storesMenu] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesProducts]  WITH CHECK ADD FOREIGN KEY([PurchaseId])
REFERENCES [dbo].[tbl_purchases] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesProducts]  WITH CHECK ADD FOREIGN KEY([PurchaseId])
REFERENCES [dbo].[tbl_purchases] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesProductsHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesProductsHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesTastes]  WITH CHECK ADD FOREIGN KEY([Product])
REFERENCES [dbo].[tbl_purchasesProducts] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesTastes]  WITH CHECK ADD FOREIGN KEY([Product])
REFERENCES [dbo].[tbl_purchasesProducts] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesTastes]  WITH CHECK ADD FOREIGN KEY([Taste])
REFERENCES [dbo].[tbl_menuTastes] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesTastes]  WITH CHECK ADD FOREIGN KEY([Taste])
REFERENCES [dbo].[tbl_menuTastes] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesTastesHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesTastesHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchaseToWorker]  WITH CHECK ADD FOREIGN KEY([PurchaseId])
REFERENCES [dbo].[tbl_purchases] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchaseToWorker]  WITH CHECK ADD FOREIGN KEY([PurchaseId])
REFERENCES [dbo].[tbl_purchases] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchaseToWorker]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_users] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchaseToWorker]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_users] ([Id])
GO
ALTER TABLE [dbo].[tbl_responses]  WITH CHECK ADD FOREIGN KEY([StroeId])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_responses]  WITH CHECK ADD FOREIGN KEY([StroeId])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_responsesHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_responsesHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_stores]  WITH CHECK ADD FOREIGN KEY([KashrutCertification])
REFERENCES [dbo].[tbl_hechshers] ([Id])
GO
ALTER TABLE [dbo].[tbl_stores]  WITH CHECK ADD FOREIGN KEY([KashrutCertification])
REFERENCES [dbo].[tbl_hechshers] ([Id])
GO
ALTER TABLE [dbo].[tbl_stores]  WITH CHECK ADD FOREIGN KEY([StoreCategory])
REFERENCES [dbo].[tbl_storesCategories] ([Id])
GO
ALTER TABLE [dbo].[tbl_stores]  WITH CHECK ADD FOREIGN KEY([StoreCategory])
REFERENCES [dbo].[tbl_storesCategories] ([Id])
GO
ALTER TABLE [dbo].[tbl_storesActivityTime]  WITH CHECK ADD FOREIGN KEY([Store])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_storesActivityTime]  WITH CHECK ADD FOREIGN KEY([Store])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_storesBankDetails]  WITH CHECK ADD FOREIGN KEY([StoreId])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_storesBankDetails]  WITH CHECK ADD FOREIGN KEY([StoreId])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_storesMenu]  WITH CHECK ADD FOREIGN KEY([ProductCategory])
REFERENCES [dbo].[tbl_productsCategories] ([Id])
GO
ALTER TABLE [dbo].[tbl_storesMenu]  WITH CHECK ADD FOREIGN KEY([ProductCategory])
REFERENCES [dbo].[tbl_productsCategories] ([Id])
GO
ALTER TABLE [dbo].[tbl_storesMenu]  WITH CHECK ADD FOREIGN KEY([Store])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_storesMenu]  WITH CHECK ADD FOREIGN KEY([Store])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_storesPositions]  WITH CHECK ADD FOREIGN KEY([StoreId])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_storesPositions]  WITH CHECK ADD FOREIGN KEY([StoreId])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_users]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[tbl_roles] ([Id])
GO
ALTER TABLE [dbo].[tbl_users]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[tbl_roles] ([Id])
GO
ALTER TABLE [dbo].[tbl_usersHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_usersHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_workerToStore]  WITH CHECK ADD FOREIGN KEY([StoreId])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_workerToStore]  WITH CHECK ADD FOREIGN KEY([StoreId])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_workerToStore]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_users] ([Id])
GO
ALTER TABLE [dbo].[tbl_workerToStore]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_users] ([Id])
GO
USE [master]
GO
ALTER DATABASE [DB_freeQueue] SET  READ_WRITE 
GO
