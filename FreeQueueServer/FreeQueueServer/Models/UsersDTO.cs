﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreeQueueServer.Models
{
    public class UsersDTO
    {
        public int id { get; set; }
        public string userEmail { get; set; }
        public string userPassword { get; set; }
        public string userName { get; set; }
        public int? roleId { get; set; }
        public string userPhone { get; set; }


        /// <summary>
        /// get sql entity and convert it to simple object whithout references
        /// </summary>
        /// <param name="user"></param>
        /// <returns>UsersDTO</returns>
        public static UsersDTO ConvertToDTO(tbl_users user)
        {
            return new UsersDTO()
            {
                id = user.Id,
                userEmail = user.UserEmail,
                userName = user.UserName,
                userPassword = user.UserPassword,
                userPhone=user.UserPhone,
                roleId = (user.RoleId != null) ? user.RoleId : 0
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="users"></param>
        /// <returns>List<UsersDTO></returns>
        public static List<UsersDTO> ConvertToDTO(List<tbl_users> users)
        {
            return users.Select(u => ConvertToDTO(u)).ToList();
        }

        static DB_freeQueueEntities1 DB = new DB_freeQueueEntities1();

        public static tbl_users ConvertFromDTO(UsersDTO user)
        {
            return new tbl_users
            {
                Id = user.id,
                UserEmail = user.userEmail,
                UserName = user.userName,
                UserPassword = user.userPassword,
                UserPhone = user.userPhone,
                RoleId = user.roleId
            };
        }

    }
}