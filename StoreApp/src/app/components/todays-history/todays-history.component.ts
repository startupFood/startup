import { Component, OnInit } from '@angular/core';
import { Purchase } from 'src/app/classes/purchase';
import { StoreService } from 'src/app/services/store.service';
import { throwMatDialogContentAlreadyAttachedError } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { PurchasePopUpComponent } from './purchase-pop-up/purchase-pop-up.component';
import { PurchasesToGroup } from 'src/app/classes/purchases-to-group';
import { group } from '@angular/animations';


@Component({
  selector: 'app-todays-history',
  templateUrl: './todays-history.component.html',
  styleUrls: ['./todays-history.component.css']
})
export class TodaysHistoryComponent implements OnInit {

  // list of history purchases.
  purchasesHistory:Array<Purchase> = new Array<Purchase>();
  purchasesAndGroupes:Array<PurchasesToGroup> = new Array<PurchasesToGroup>();
  constructor(private storeServ:StoreService ,
     private router:Router,
    public dialog: MatDialog) { 

    // select the purchases history of today.
    this.storeServ.getTodaysHistory(this.storeServ.Store.id).subscribe(
      data=> {
        data.forEach(d => {
          //purchase.
          if(d.groupId == 0)
          {
            let pArray:Array<Purchase> = [d];
            this.purchasesAndGroupes.push(new PurchasesToGroup(0 ,"", pArray , 0));
          }
          //group.
          else{
            if(this.purchasesAndGroupes.find(pag => pag.groupId == d.groupId))
            {
              this.purchasesAndGroupes.find(pag => pag.groupId == d.groupId).purchases.push(d);
            }
            else
            {
              let pArray:Array<Purchase> = [d];
              this.purchasesAndGroupes.push(new PurchasesToGroup(d.groupId,"" , pArray , 0));
            }
          }
        });
        
        this.storeServ.getGroupsByPurchases(data).subscribe(
          groupes => {
            this.storeServ.GroupesHistory = groupes;
            this.purchasesAndGroupes.filter(pag => pag.groupId != 0).forEach(pag1 =>
              {
                pag1.groupName =  groupes.find(g => g.id == pag1.groupId).groupName;
              });
          }
        )
        this.purchasesHistory = data;
        this.storeServ.PurchasesHistory = data;
      }
    );


  }

  ngOnInit() {
  }

  getCustomerName(purchaseOrGroup:PurchasesToGroup)
  {
    if(purchaseOrGroup.groupId == 0)
      {
        return purchaseOrGroup.purchases[0].customerName;
      }
else{
  let groupName = this.storeServ.GroupesHistory.find(g => g.id == purchaseOrGroup.groupId).groupName;
  return groupName;
}
  }

  getCustomerPhone(purchaseOrGroup:PurchasesToGroup)
  {
    if(purchaseOrGroup.groupId == 0)
      {
        return purchaseOrGroup.purchases[0].customerPhone;
      }
else{
  return "";
}
  }

  getReceiptTime(purchaseOrGroup:PurchasesToGroup)
  {
    
        return purchaseOrGroup.purchases[0].receiptTime;
  }

  getSum(purchaseOrGroup:PurchasesToGroup)
  {
    if(purchaseOrGroup.groupId == 0)
      {
        return purchaseOrGroup.purchases[0].purchaseSum;
      }
else{
  return "";
}
  }

  

  back()
  {
    
    this.router.navigate(["/openPage"]);
  }


  openDialog(purchase:PurchasesToGroup): void {
    debugger;
    let purchaseId = purchase.purchases[0].id;
    const dialogRef = this.dialog.open(PurchasePopUpComponent, {
      width: '50vw',
      // data: {purchaseId}
      data :purchase
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

    });
}
}
