import { Component, OnInit, EventEmitter ,Output } from '@angular/core';
import { Group } from 'src/app/classes/group';
import { GroupService } from 'src/app/services/group.service';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {
  
  // @Output("get-group")getGroup:EventEmitter<Number> = new EventEmitter();   
  groups:Array<Group>;
  exists:boolean = false;
  infoMessage = false;
  newDate:Date;


  constructor(private groupServ:GroupService , private storeServ:StoreService) {
    this.groupServ.getStoresGroups().subscribe(
      data=>{ 
       this.groupServ.Groups = data.filter(g=> g.openingDate == (new Date).toLocaleDateString()) ;
      }      
    );
   }

  joinToGroup(groupName:string , groupPassword:string )
  {
    
  // let groupName:string = document.getElementById('name').get
  // let groupPassword:string = document.getElementById('password').getAttribute('value');
    //if the group exists
    if(this.groupServ.Groups.find(g=>(g.groupName == groupName && g.groupPassword == groupPassword)) != undefined)
    {
      sessionStorage.setItem('groupId' , JSON.stringify( this.groupServ.Groups.find(g=> g.groupName == groupName).id  ) );
      //this.getGroup.emit(this.groupServ.getGroups().find(g=> g.groupName == groupName).id);
      //this.getGroup.emit(1);
      this.exists = true;
    }
    else
    {
      this.infoMessage = true;
    }
  }

  groupMessage:string="";
  //
  createNewGroup( groupName:string , groupPassword:string )
  {

  // let groupName:string = document.getElementById('name').getAttribute('value');
  // let groupPassword:string = document.getElementById('password').getAttribute('value');
//add the new group to the groups.

    this.groupServ.addGroup(new Group(0 , this.storeServ.getStoreId() , groupName , groupPassword , (new Date).toLocaleDateString())).subscribe(
      data=> { this.groupServ.Groups = data.filter(g=> g.openingDate == (new Date).toLocaleDateString() );
        //alert("your group " + groupName + " added successfully!");
        sessionStorage.setItem('groupId' , JSON.stringify( this.groupServ.Groups.find(g=> g.groupName == groupName).id  ) );
        //this.getGroup.emit(this.groupServ.getGroups().find(g=> g.groupName == groupName).id);
        //this.getGroup.emit(1);
        this.groupServ.GroupManager = true;
        this.groupMessage="your group " + groupName + " added successfully!";
        this.infoMessage = false;
      });
  }





  ngOnInit() {
  }

}
