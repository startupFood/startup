import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { OpenPageComponent } from 'src/app/components/open-page/open-page.component';
import { LoginComponent } from 'src/app/components/login/login.component';
import { SignupComponent } from 'src/app/components/signup/signup.component';
import { ManagerPageComponent } from 'src/app/components/manager-page/manager-page.component';
import { ManagerStockComponent } from 'src/app/components/manager-stock/manager-stock.component';
import { ManagerWorkersComponent } from 'src/app/components/manager-workers/manager-workers.component';
import { ManagerStoreComponent } from 'src/app/components/manager-store/manager-store.component';
import { ManagerEditWorkerComponent } from 'src/app/components/manager-edit-worker/manager-edit-worker.component';
import { ManagerMenuComponent } from 'src/app/components/manager-menu/manager-menu.component';
import { ManagerEditProductComponent } from 'src/app/components/manager-edit-product/manager-edit-product.component';
import { ManagerEditAdditionComponent } from 'src/app/components/manager-edit-addition/manager-edit-addition.component';
import { ManagerEditTasteComponent } from 'src/app/components/manager-edit-taste/manager-edit-taste.component';
import { TodaysHistoryComponent } from 'src/app/components/todays-history/todays-history.component';



const routes:Routes = [
  {path: "" , component: LoginComponent },
  {path:"openPage" , component: OpenPageComponent },
  {path: "login" , component: LoginComponent },
  {path: "signup" , component: SignupComponent },
  {path: "manager", component: ManagerPageComponent},
  {path: "managerStock", component: ManagerStockComponent},
  {path: "managerWorkers", component: ManagerWorkersComponent},
  {path: "managerStore", component: ManagerStoreComponent},
  {path: "managerMenu", component: ManagerMenuComponent},
  {path: "editWorker" , component: ManagerEditWorkerComponent},
  {path: "editProduct" , component: ManagerEditProductComponent},
  {path: "editAddition" , component: ManagerEditAdditionComponent},
  {path: "editTaste" , component: ManagerEditTasteComponent},
  {path: "historyPage" , component: TodaysHistoryComponent}

];

@NgModule({
  imports: [
    CommonModule , 
    RouterModule.forRoot(routes)
  ],
  declarations: []
})
export class RoutingModule { }

