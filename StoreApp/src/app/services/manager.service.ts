import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ServerService } from './server.service';
import { StoresMenu } from '../classes/stores-menu';
import { MenuAdditions } from '../classes/menu-additions';
import { MenuTastes } from '../classes/menu-tastes';
import { Observable } from 'rxjs';
import { ProductCategory } from '../classes/product-category';
import { User } from '../classes/user';
import { Store } from '../classes/store';
import { UsersService } from './users.service';
import { Purchase } from '../classes/purchase';
import { Status } from '../components/manager-menu/manager-menu.component';
import { store } from '@angular/core/src/render3';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {

  constructor(private httpClient:HttpClient, private serverServ:ServerService, private usersServ:UsersService) { }

  //temp variables!!!!!!!!!!!!!!!!!!
  private storeId:number=1;
  private userId:number=1;


  get StoreId()
  {
    return this.storeId;
  }

  set StoreId(value:number)
  {
    this.storeId = value;
  }

  //manager menu component

  getStoreMenu():Observable<StoresMenu[]>
  {
    return this.httpClient.get<StoresMenu[]>(this.serverServ.URL+"Menu/GetByStore/"+this.storeId, {headers : this.serverServ.getHeader()});
  }
  
  getStoreAdditions():Observable<MenuAdditions[]>
  {
    return this.httpClient.get<MenuAdditions[]>(this.serverServ.URL+"Menu/GetAdditionsByStore/"+this.storeId, {headers : this.serverServ.getHeader()});
  }
  
  getStoreTastes():Observable<MenuTastes[]>
  {
    return this.httpClient.get<MenuTastes[]>(this.serverServ.URL+"Menu/GetTastesByStore/"+this.storeId, {headers : this.serverServ.getHeader()});
  }

  getStoreCategoties():Observable<ProductCategory[]>
  {
    return this.httpClient.get<ProductCategory[]>(this.serverServ.URL+"Menu/GetCategoriesByStore/"+this.storeId, {headers : this.serverServ.getHeader()});
  }

  //manager workers component

  getStoreWorkers():Observable<User[]>
  {
    debugger;
    return this.httpClient.get<User[]>(this.serverServ.URL+"Users/GetWorkersList/"+this.storeId, {headers : this.serverServ.getHeader()});
  }

  deleteUser(userId:number):Observable<User[]>
  {
    return this.httpClient.delete<User[]>(this.serverServ.URL+"Users/Delete/"+userId+"/"+this.userId, {headers : this.serverServ.getHeader()});
  }

  updateUser(user:User):Observable<any>
  {
    return this.httpClient.put<any>(this.serverServ.URL+"Users/UpdateUser/"+user.id, user, {headers : this.serverServ.getHeader()});
  }

  createUser(user:User, roleId:number):Observable<any>
  {
    user.roleId=roleId;
    return this.httpClient.post<any>(this.serverServ.URL+"Users/CreateWorker/"+this.StoreId, user, {headers : this.serverServ.getHeader()});
  }

  getStoreDetails():Observable<Store>
  {
    debugger;
    return this.httpClient.get<Store>(this.serverServ.URL+"Store/GetStoreDetails/"+this.storeId, {headers : this.serverServ.getHeader()});
  }

  updateStoreImage(image:File):Observable<string>
  {
    return this.httpClient.put<string>(this.serverServ.URL+"Store/UpdateStoreImage/"+this.storeId, image, {headers : this.serverServ.getHeader()});
  }

  updateStore(store:Store, image:File):Observable<Store>
  {
    return this.httpClient.put<Store>(this.serverServ.URL+"Store/UpdateStoreDetails/"+this.storeId, store, {headers : this.serverServ.getHeader()});
  }


  getFavoritePurchases():Observable<Purchase[]>
  {
    return this.httpClient.get<Purchase[]>(this.serverServ.URL+"Store/UpdateStoreDetails/"+this.storeId, {headers : this.serverServ.getHeader()});
  }

  //products management
  deleteProduct(productId:number):Observable<User[]>
  {
    return this.httpClient.delete<User[]>(this.serverServ.URL+"Menu/DeleteProduct/"+productId, {headers : this.serverServ.getHeader()});
  }

  updateProduct(product:StoresMenu):Observable<any>
  {
    return this.httpClient.put<any>(this.serverServ.URL+"Menu/UpdateProduct/"+product.id, product, {headers : this.serverServ.getHeader()});
  }

  createProduct(product:StoresMenu):Observable<any>
  {
    return this.httpClient.post<any>(this.serverServ.URL+"Menu/AddProduct", product, {headers : this.serverServ.getHeader()});
  }

  //additions management
  deleteAddition(additionId:number):Observable<User[]>
  {
    debugger;
    return this.httpClient.delete<User[]>(this.serverServ.URL+"Menu/DeleteAddition/"+additionId, {headers : this.serverServ.getHeader()});
  }

  updateAddition(addition:MenuAdditions):Observable<any>
  {
    return this.httpClient.put<any>(this.serverServ.URL+"Menu/UpdateAddition/"+addition.id, addition, {headers : this.serverServ.getHeader()});
  }

  createAddition(addition:MenuAdditions):Observable<any>
  {
    return this.httpClient.post<any>(this.serverServ.URL+"Menu/AddAddition", addition, {headers : this.serverServ.getHeader()});
  }

  //tastes management
  deleteTaste(tasteId:number):Observable<User[]>
  {
    debugger;
    return this.httpClient.delete<User[]>(this.serverServ.URL+"Menu/DeleteTaste/"+tasteId, {headers : this.serverServ.getHeader()});
  }

  updateTaste(taste:MenuTastes):Observable<any>
  {
    return this.httpClient.put<any>(this.serverServ.URL+"Menu/UpdateTaste/"+taste.id, taste, {headers : this.serverServ.getHeader()});
  }

  createTaste(taste:MenuTastes):Observable<any>
  {
    return this.httpClient.post<any>(this.serverServ.URL+"Menu/AddTaste", taste, {headers : this.serverServ.getHeader()});
  }

  changeStatus(status:Status):Observable<any>
  {
    var x=1;
    debugger;
    return this.httpClient.get<any>(this.serverServ.URL+"Menu/ChangeStatus/"+x, {headers : this.serverServ.getHeader()});
  }
}
