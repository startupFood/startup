import { Component, OnInit, Input } from '@angular/core';
import { StoresMenu } from 'src/app/classes/stores-menu';
import { Router } from '@angular/router';
import { StoreService } from 'src/app/services/store.service';
import { ProductCategory } from 'src/app/classes/product-category';
import {MatBadgeModule} from '@angular/material/badge';
import { ServerService } from 'src/app/services/server.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})

export class ProductsListComponent implements OnInit {

  @Input() category:ProductCategory;
  products:Array<StoresMenu>;

  constructor(private storeServ:StoreService, 
    private router:Router,
    private serverServ:ServerService) {
    //get the products from the service by the category.
    storeServ.getStoreMenu().subscribe(
      data=>{this.products=data.filter(d=>d.productCategory==this.category.id)}
      );

           //to dispaly the match icons in the header.
this.serverServ.Component.next(2);
  }

  ngOnInit() {
  }

  productChosen(id:number):void
  {
    //navigate to the product page
    this.router.navigate(['/product', id]);
  }
}