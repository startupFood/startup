import { Injectable } from '@angular/core';
import { Observable, Subject, throwError } from 'rxjs';
import { User } from '../classes/user';
import { HttpClient } from '@angular/common/http';
import { StoreService } from './store.service';
import { LoginUser } from '../classes/login-user';
import { Router } from '@angular/router';
import { ServerService } from './server.service';
// 
@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(public httpClient: HttpClient,
     public router: Router,
      public storeServ: StoreService ,
      public serverServ:ServerService) { }

  private userId: number = 0;
  private user: User = null;
  // private showLogin:boolean = true;

  // get ShowLogin()
  // {
  //   return this.showLogin;
  // }
  // set ShowLogin(value:boolean)
  // {
  //   this.showLogin = value;
  // }
  public showLoginSubject = new Subject<Number>();

  getUser(): User {
    return this.user;
  }

  setUser(user: User) {
    this.user = user;
  }

  getUserId(): number {
    return this.userId;
  }

  setUserId(id: number) {
    this.userId = id;
  }

  getUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>(this.storeServ.URL + "api/Auth/getUsers");
  }

  loginToServer(user: User): Observable<LoginUser> {
    return this.httpClient.post<LoginUser>(this.storeServ.URL + "api/Auth/login", user);
  }
  login(user: User): boolean {
    var complete: boolean = null;
    this.loginToServer(user).subscribe(
      data => {
        localStorage.setItem('token', data.token);
        localStorage.setItem('user', JSON.stringify(data.user));
        this.user = data.user;
        this.userId = data.user.id;
      },
      err => {         this.showLoginSubject.next(1);
 },
      () => {
        this.showLoginSubject.next(0);
        this.serverServ.Component.next(0);
        this.router.navigate(["/stores"])
      }
    );
    return false;
  }

  loginAsGuestToServer() {
    return this.httpClient.get<LoginUser>(this.storeServ.URL + "api/Auth/loginAsGuest");
  }
  loginAsGuest() {
    this.loginAsGuestToServer().subscribe(
      data => { localStorage.setItem('token', data.token); },
      err => { },
      () => {
        debugger;
        this.showLoginSubject.next(0);
        this.serverServ.Component.next(0);
        this.router.navigate(["/stores"])
      }
    );
  }

  signUp() {
    this.showLoginSubject.next(2);
  }

  addUserToDb(user: User): Observable<User> {
    return this.httpClient.post<User>(this.storeServ.URL + "api/Auth/addUser", user);
  }

  addUser(user: User, changeUser: boolean): boolean {
    this.addUserToDb(user).subscribe(
      data => {
        if (changeUser == true) {
          this.setUser(data);
          this.setUserId(data.id);
        }
        return true;
      },
      err => { return false }
    )
    return null;
  }

  initPassword(emailAddress: string): Observable<string> {
    var user = new User(0, emailAddress, "", "", "", 0);
    return this.httpClient.post<string>(this.storeServ.URL + "api/Auth/initPassword", user);
  }


}
