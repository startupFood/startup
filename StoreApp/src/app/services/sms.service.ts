import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { THRESHOLD_DIFF } from '@progress/kendo-angular-popup/dist/es2015/services/scrollable.service';
import { Observable } from 'rxjs';
import { ServerService } from './server.service';

@Injectable({
  providedIn: 'root'
})
export class SmsService {


  private messages:Array<string>;
  constructor(private httpClient:HttpClient , private serverServ:ServerService) { 

    // this.getSmsMessages().subscribe(
    //   data => this.messages = data
    // )
  }

  get Messages()
  {
    return this.messages;
  }
  sendSms(message:string , phone:string):Observable<boolean>
  {
 return this.httpClient.get<boolean>(this.serverServ.URL + "Sms/sendSms/" + message + "/" + phone , {headers:this.serverServ.getHeader()});
  }


  getSmsMessages():Observable<string[]>
  {
    return this.httpClient.get<string[]>( this.serverServ.URL + "Sms/getMessages" , {headers:this.serverServ.getHeader()});
  }
}
