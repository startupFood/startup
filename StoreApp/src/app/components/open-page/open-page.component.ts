import { Component, OnInit, EventEmitter, ElementRef, Renderer } from '@angular/core';
import { StoreService } from 'src/app/services/store.service';
import { Purchase } from 'src/app/classes/purchase';
import { PurchasesProduct } from 'src/app/classes/purchases-product';
import { ManagerService } from 'src/app/services/manager.service';
import { StoresMenu } from 'src/app/classes/stores-menu';
import { MenuTastes } from 'src/app/classes/menu-tastes';
import { MenuAdditions } from 'src/app/classes/menu-additions';
import { PurchasesDetailsAddition } from 'src/app/classes/purchases-details-addition';
import { PurchasesToGroup } from 'src/app/classes/purchases-to-group';
import { GroupsService, GroupIndicatorComponent } from '@progress/kendo-angular-grid';
import { ServerService } from 'src/app/services/server.service';
import { MatDialog } from '@angular/material/dialog';
import { PurchaseDetailsTaste } from 'src/app/classes/purchase-details-taste';
import { JsonPipe } from '@angular/common';
import { stringify } from '@angular/core/src/util';
import { Router } from '@angular/router';
import { group } from '@angular/animations';
import { DebugRenderer2 } from '@angular/core/src/view/services';
import { store } from '@angular/core/src/render3';
import { BrowserDomAdapter } from '@angular/platform-browser/src/browser/browser_adapter';
import { MadeGroupPopUpComponent } from './made-group-pop-up/made-group-pop-up.component';
import { SmsPopUpComponent } from './sms-pop-up/sms-pop-up.component';
import { SmsService } from 'src/app/services/sms.service';
import { User } from 'src/app/classes/user';
import { WorkerService } from 'src/app/services/worker.service';
import { UsersService } from 'src/app/services/users.service';

export class ImageAndSpan{
  constructor(public img:Element ,public span:Element){}
}

@Component({
  selector: 'app-open-page',
  templateUrl: './open-page.component.html',
  styleUrls: ['./open-page.component.css']
})


export class OpenPageComponent implements OnInit {


  lat = 51.678418;
  lng = 7.809007;
  purchasesMode:boolean = false;
  public dataSource:Array<Purchase> = new Array<Purchase>();
  public purchasesProducts:Array<PurchasesProduct> = new Array<PurchasesProduct>();
  public groupedPurchases:Array<PurchasesToGroup> = new Array<PurchasesToGroup>();
  public mergedPurchases:Array<PurchasesToGroup> = new Array<PurchasesToGroup>();
  open:boolean = false;
  load:string = "unLoad";
  // static purchases:boolean = false;

  
  public headers:Array<String> = ['customer name' , 'customer phone' , 'delivery address' , 'reserved seats' , 'club' , 'reciept time' , 'purchase sum'];

  constructor(private storeServ:StoreService ,
              private smsServ:SmsService ,
              private serverServ:ServerService ,
              private managerServ:ManagerService , 
              public dialog:MatDialog ,
              private router:Router,
              private workerServ:WorkerService,
              private userServ:UsersService) {
 
this.userServ.loginFlag.next(false);
 
    // setTimeout(function() { location.reload(); }, 60000);
    this.storeServ.purchaseSubject.subscribe(data => {this.purchasesMode = data;});
  this.purchasesMode = this.storeServ.PurchasesMode;
 
    this.dataSource = this.storeServ.initStore(1);
     

  
/*     debugger;
    //document.getElementById('noPurchases').setAttribute('style' , 'visibility:visible');
    var table = document.getElementsByClassName("table")[0];
    var p = document.createElement('p');
    p.innerHTML = "there is no purchases yet.";
    table.after(p);  */
  

  this.managerServ.getStoreMenu().subscribe(data => 
    {
      this.storeServ.StoresMenu = data;

    });

    this.storeServ.serverSubject.subscribe(data => 
      {
        this.dataSource = this.storeServ.StoresOrders;
        this.groupedPurchases = this.storeServ.GroupedPurchases;
        switch(this.storeServ.Store.storeLoad)
        {
          case true:this.load = "Load";
          break;
          case false:this.load = "unLoad";
          break;
        }
        this.mergeGroupsAndPurchases();
      });
   }

  ngOnInit() {
      
   
  }



  //function that create image and span corrrect to the additions that gets.
  createAdditionElement(addition:MenuAdditions)
  {
    //var td = document.createElement('td');

    var img = document.createElement('img');
    img.setAttribute('src'  , addition.additionImage);
    img.setAttribute('width' ,'100');

    var span = document.createElement('span');
    span.innerHTML = addition.additionName;

    //td.appendChild(img);
    //td.appendChild(span);

    return new ImageAndSpan(img , span);
  }


//   selectPurchase(purchaseId:number )
//   {
    
//     let localStoreService:StoreService = this.storeServ;
//     debugger;
//     if(document.getElementById('purchaseProduct' + purchaseId) != null)
//     {
//       document.getElementById('purchaseProduct' + purchaseId).remove();
//     }
//     else
//     {
//       alert(document.getElementById('purchase' + purchaseId));
//     var purchaseTr = document.getElementById('purchase' + purchaseId);
//     var div = document.createElement('div');
//     div.setAttribute('id' , 'purchaseProduct' + purchaseId);
//     //add the purchase details under the row of the purchase.
//     var table = document.createElement('table');
//     table.setAttribute('class' , 'table table-hover');

//     var trHeaders = document.createElement('tr');
//     var thProductName = document.createElement('th');
//     thProductName.innerHTML = "product";

//     var thTaste = document.createElement('th');
//     thTaste.innerHTML = "Taste";

//     var thAdditions = document.createElement('th');
//     thAdditions.innerHTML = "Additions";

//     var thCount = document.createElement('th');
//     thCount.innerHTML = "count";

//     trHeaders.appendChild(thProductName);
//     trHeaders.appendChild(thTaste);
//     trHeaders.appendChild(thAdditions);
//     trHeaders.appendChild(thCount);

//     table.appendChild(trHeaders);

//     let productsStoreMenu:StoresMenu;
//     let tastesProduct:MenuTastes;
//     let additionsProducts:Array<PurchasesDetailsAddition> = new Array<PurchasesDetailsAddition>();
//     let menuAdditionsProducts:Array<MenuAdditions> = new Array<MenuAdditions>();
//     this.storeServ.StoresOrdersProducts.forEach(pp => {
//       if( pp.purchase == purchaseId )
//       {
//         //find the store menu item that match to the current product.
//         productsStoreMenu =  this.storeServ.StoresMenu.find(sm => sm.id == pp.product);

//         var tr = document.createElement('tr');
//         tr.setAttribute('id' , 'product' + pp.id);

//         var tdPName = document.createElement('td');
//         tdPName.innerHTML = productsStoreMenu.productName;

//         var tdPTaste = document.createElement('td');
//         tdPTaste.setAttribute('width' , 'auto');
//         //<app-tastes [productId]="thisProduct.id" *ngIf="thisProduct"></app-tastes>

//         // var appTaste = document.createElement('app-taste');
//         // tdPTaste.appendChild(appTaste);


// /*         <div class="tasteDiv" id="taste{{t.id}}" *ngFor="let t of productsTastes" >
//   <img width="100" src="{{t.tasteImage}}"/>
//   <br>
//   <label>{{t.tasteName}}</label>
// </div> */

//          //var imgTaste = document.createElement('img');
//          var imgTaste = new Image();
//         debugger;
//         tastesProduct =  this.storeServ.Tastes.find(t => t.product == productsStoreMenu.id);
//         //in case that there is no taste to this product.
//         if(tastesProduct != undefined)
//         {
//           //    width: 70px;
//    // height: 70px;
//     //border-radius: 100px;
//         imgTaste.setAttribute('style' , 'width:50px;height:50px;border-radius:100px;');
//           imgTaste.src = tastesProduct.tasteImage;
//           //imgTaste.setAttribute('src' , tastesProduct.tasteImage);
  
//           var tasteSpan = document.createElement('span');
//           tasteSpan.innerHTML = tastesProduct.tasteName;
//           tasteSpan.setAttribute('style' , 'font-size:12px');
  
//           tdPTaste.appendChild(imgTaste);
//           tdPTaste.appendChild(document.createElement('br'));
//           tdPTaste.appendChild(tasteSpan); 
   
//         }

         
 

//           var tdPAdditions = document.createElement('td');
//           tdPAdditions.setAttribute('width' , 'auto');

//         //  //select the purchase details additions of this product.
//           this.storeServ.StoresOrdersDetailsAdditions.forEach(add => {
//            if(add.product == pp.id)
//            {
//              additionsProducts.push(add);
//            }
//          }); 

//           //select the menu additions of this product by the purchase details additions.
//           this.storeServ.Additions.forEach(add => {
//            if(add.product == productsStoreMenu.id )
//            {
//              additionsProducts.forEach(addp => {
//                if(addp.addition == add.id)
//                {
//                  menuAdditionsProducts.push(add);
//                }
//              });
//            } 
//           });
//           //adding elements for each addition.
//         menuAdditionsProducts.forEach(map => {
//           if(map.product == productsStoreMenu.id)
//           {
//             var addDiv = document.createElement('div');
//             addDiv.setAttribute('style' , 'display:inline-block;')
//             var addImg = new Image();
//             addImg.src = map.additionImage;
//             addImg.setAttribute('style' , 'width:50px;height:50px;border-radius:100px;');
//             addImg.title = map.additionName;
//             addDiv.appendChild(addImg);

//             var addSpan = document.createElement('span');
//             addSpan.innerHTML = map.additionName;
//             addSpan.setAttribute('style' , 'font-size:12;');

//             addDiv.appendChild(document.createElement('br'));
//             addDiv.appendChild(addSpan);

//             tdPAdditions.appendChild(addDiv);

//           }
//         });

 
//         // let imgspan:ImageAndSpan;
//         // menuAdditionsProducts.forEach(maddp => imgspan = this.createAdditionElement(maddp));
//         // tdPAdditions.appendChild(imgspan.img);
//         // tdPAdditions.appendChild(imgspan.span); 

//         var tdPCount = document.createElement('td');
//         tdPCount.innerHTML = "" + pp.productCount;

//         var tdPDone = document.createElement('td');
//         var doneDiv = document.createElement('div');
//         //doneDiv.appendChild(document.createElement('br'));
//         var doneSpan = document.createElement('sapn');
//         doneSpan.innerHTML = "  Done";
//         //doneSpan.setAttribute('style' , 'margin:60px;9px;')
//         doneDiv.appendChild(doneSpan)
//         //doneDiv.innerHTML = "  Done";
//         let opacityDone = 'width:50px;height:50px;background-color:green;opacity:0.5;border-radius:50px;padding:13px 0px;place-content:center;';
//         let nonOpacityDone ='width:50px;height:50px;background-color:green;opacity:1;border-radius:50px;padding:13px 0px;place-content:center;';
//         //if the status shows not done.
//         debugger;
//         if(pp.status == false)
//         {
//           doneDiv.setAttribute('style' , opacityDone);
//         }
//         //if the status shows done.
//         else{
//           doneDiv.setAttribute('style' , nonOpacityDone);
//         }
//         doneDiv.setAttribute('id' , 'productStatus ' + pp.id );
//         doneDiv.onclick = function(event)
//         {
//           // if(event.srcElement.getAttribute('style') == nonOpacityDone)
//           // {
//           //   event.srcElement.setAttribute('style' , opacityDone)
//           // }
//           // else
//            if(event.srcElement.getAttribute('style') == opacityDone)
//             {
//               debugger;
//               event.srcElement.setAttribute('style' , nonOpacityDone);

//               //update the status in the server.
//               localStoreService.updatePurchasesProductsStatus(pp.id , true).subscribe(data => {alert(data)});

//               //find this product in the services and change his status to true;
//               let purchasesProductStringId = event.srcElement.getAttribute('id').split(" ", 2)[1];
//               let purchasesProductId:number = 0;
//               //convert to number.
//               for(let i = 0; i < purchasesProductStringId.length ; i++ )
//               {
//                 purchasesProductId *= 10;
//                 purchasesProductId += purchasesProductStringId.charCodeAt(i) - 48;
//               }
//               //update the status.
//               localStoreService.StoresOrdersProducts.find(sop => sop.id == purchasesProductId).status = true;

//               //change the status of the purchase.
//               let purchaseStatusDiv = document.getElementById('purchaseStatusDiv' + purchaseId);
              
              



//                 //array of the purchasesProducts of this purchase.
//     let ordersProducts:Array<PurchasesProduct> = new Array<PurchasesProduct>();
//     //flag of carried out.
//     let carriedOut:boolean = false;
//     //flag of done.
//     let done:boolean = false;
//     //flag of notDone.
//     let notDone:boolean = false;

//     //select all the products of this purchase 
//     //if there is one that was done and one that not, so the status is in the middle of execute,
//     //if all the products were done the status is done,
//     //if all the products werent done the status is not done.
    
//     //select the purchaseProducts.
//     localStoreService.StoresOrdersProducts.forEach(sop => {
//       if(sop.purchase == purchaseId)
//       {
//         ordersProducts.push(sop);
//       }
//     });

//     //pass on the array of the products of this purchase.
//     for(let op of ordersProducts)
//     {
//          //check the status and sign the flags.
//          switch( op.status )
//          {
//            case true:done = true;
//                      break;
//            case false:notDone = true;
//                      break;
//          }
//          //if the purchase is carried out cause some of the products were done and some not break the loop.
//          if( done && notDone )
//          {
//            carriedOut = true;
//            break;
//          }
//     }
//       if( carriedOut )
//       {
//         purchaseStatusDiv.setAttribute('style' , 'width:40px;height:40px;border-radius:50px;background-color:orange;')
//       }
//       else if( done )
//       {
//         //update the status of the purchase in the service to done and hide this purchase.
//         localStoreService.updatePurchasesStatus(purchaseId).subscribe(data => data);
//         //purchaseStatusDiv.setAttribute('style' , 'width:40px;height:40px;border-radius:50px;background-color:green;')
//       }
//       else
//       {
//         purchaseStatusDiv.setAttribute('style' , 'width:40px;height:40px;border-radius:50px;background-color:red;')
//       }
//               // //if the status was not done - change the status to carried out.
//               // if(purchaseStatusDiv.getAttribute('style') == 'width:40px;height:40px;border-radius:50px;background-color:red;')
//               // {
//               //   purchaseStatusDiv.setAttribute('style' , 'width:40px;height:40px;border-radius:50px;background-color:orange;')
//               // }

//             }
//         }
//         tdPDone.appendChild(doneDiv);
        

//         tr.appendChild(tdPName);
//         tr.appendChild(tdPTaste);
//         tr.appendChild(tdPAdditions);
//         tr.appendChild(tdPCount);
//         tr.appendChild(tdPDone);

//         table.appendChild(tr);
//       }
//     });

//     debugger;
//     div.appendChild(table);
//     purchaseTr.after(div);


//   }
//   }


  openHistory()
  {
    this.router.navigate(["/historyPage"]);
  }

  loadReporting()
  {
    switch(this.load)
    {
      case 'unLoad':
        this.updateLoad(true);
        this.load = "Load";
        break;
      case 'Load' :
        this.updateLoad(false);
        this.load = "unLoad";
        break;
    }
  }

  updateLoad(load:boolean)
  {
    this.storeServ.updateStoreLoad(load).subscribe(
      data => {this.storeServ.Store.storeLoad = data;
      });
  }

  getGroupName(groupId:number)
  {
    return this.storeServ.Groups.find(g => g.id == groupId).groupName;
    }




//merge the purchases and the groupes to one board.
mergeGroupsAndPurchases()
{
  this.mergedPurchases = new Array<PurchasesToGroup>();
 //list of groupes.
  let groupes:Array<PurchasesToGroup> = new Array<PurchasesToGroup>();
  //list of purchases.
  let purchases:Array<Purchase> = this.storeServ.PurchasesWithOutGroups; 

  let currentTime = (new Date().getHours())*60 + new Date().getMinutes() ;

  let preperationTime = 0;
  //convert the reciept time to number.
  let rTimeFromString:number = 0 , i:number , j:number , rTime = 0;
  let timeString:string ;

//calculation the total preperation time to each group.
  this.storeServ.GroupedPurchases.forEach(gp =>
    {
       preperationTime = 0;
      gp.purchases.forEach(p => {
        this.storeServ.StoresOrdersProducts.forEach(sop => {
          if(sop.purchase == p.id)
          {
            //adding the prepertion time of this product to the total time in this group.
            preperationTime += this.storeServ.StoresMenu.find(sm => sm.id == sop.product).preperationTime;
          } 
        });
        //convert the reciept time to number.
         rTimeFromString = 0  , rTime = 0;
         timeString = gp.purchases[0].receiptTime.substring(0 , 2) ;

        //convert the hours to number.

        rTimeFromString = Number.parseInt(timeString);
        //convert the hour to minutes.
        rTime += rTimeFromString * 60;



        rTimeFromString = Number.parseInt( gp.purchases[0].receiptTime.substring(3,5) );

        //adding the minutes.
        rTime += rTimeFromString;

        //adding the total preperation time to the group.
        //the reciept time - the current time - the preperation time.
        gp.preperationTotalTime = rTime - currentTime - preperationTime;
      })
    });
    //sort the groupes according to the total preperation time.
//fill the list of the groupes with the grouped purchases.
    groupes = this.storeServ.GroupedPurchases;
    groupes.sort((a,b) => a.preperationTotalTime -b.preperationTotalTime);

  // if there are purchases.
if(purchases.length)
{
    //push all the purchases to array in the structure of purchasesToGroup.
    purchases.forEach(p => {
      //list with the purchase.
      let ps:Array<Purchase> = [p];
      // push to new object. 
       this.mergedPurchases.push(new PurchasesToGroup( 0 ,"" , ps  , 0))}
    );
     this.mergedPurchases.forEach(mp => {

    // this.mergedPurchases.forEach(mp => {
      preperationTime = i = rTime = rTimeFromString = 0;
       timeString = mp.purchases[0].receiptTime.substring(0 , 2) ;   
      //convert the hours to number.
   

      rTimeFromString = Number.parseInt(mp.purchases[0].receiptTime.substring(0 , 2));
    
      //convert the hour to minutes.
      rTime += rTimeFromString * 60;

//adding the minutes.
      rTime += Number.parseInt(mp.purchases[0].receiptTime.substring(3,5) );


      //calculation of the preperation time of all products in this purchase.
      this.storeServ.StoresOrdersProducts.forEach(sop => 
        {
          if(sop.purchase == mp.purchases[0].id)
          {
          preperationTime += this.storeServ.StoresMenu.find(sm => sm.id == sop.product).preperationTime;
          }
        });
      mp.preperationTotalTime = rTime - currentTime - preperationTime;
    });
    //sort the purchases array.
    this.mergedPurchases.sort((a,b) => a.preperationTotalTime - b.preperationTotalTime);

  
}
//if there is purchases and groupes.
if(this.mergedPurchases.length && groupes.length)
{
  //merge the arraies to the merged purchases.
  
  for( i = 0 , j = 0; i < this.mergedPurchases.length && j < groupes.length ;)
  {
      //if the group before the purchase.
      if( groupes[j].preperationTotalTime <= this.mergedPurchases[i].preperationTotalTime )
      {
        //push the group to the right place in the merged array.
        this.mergedPurchases.splice(i-1 , 0 ,groupes[j++]);
    
      }
      else if(groupes[j].preperationTotalTime > this.mergedPurchases[i].preperationTotalTime )
      {
        i++;
      }
   
  }

	while (j < groupes.length)
	{
	  this.mergedPurchases.push(groupes[j++]);

	}
  // if(groupes[j].preperationTotalTime > this.mergedPurchases[this.mergedPurchases.length -1 ].preperationTotalTime)
  // {
  //   this.mergedPurchases.push(groupes[j]);
  // }
}
//if there is no purchases.  (in case that there is purchases only they are already in the mergedPurchases array ).

else if(groupes.length){
  groupes.forEach(g => this.mergedPurchases.push(g));
}
else if(!this.mergedPurchases.length && !groupes.length){
  this.purchasesMode = true;
}


this.createPurchasesBoard();

}

//create the purchases board of all the todays purchase.
createPurchasesBoard()
{

  //clear the purchases board
  document.getElementById('purchasesBoard').innerHTML = "";

  //create the purchases board - purchasesBoard.

  this.mergedPurchases.forEach(mp => {
    var li = document.createElement('li');
    li.setAttribute('class' , 'list-group-item')

    //if its single purchase.
    if(mp.groupId == 0)
    {

      this.saveInStorage(mp.purchases[0]);

      li.setAttribute('id' , 'purchase' + mp.purchases[0].id);
  
      createPurchasesElement(mp.purchases[0] , li , this.storeServ , this.dialog);
      
    }
    //if it's group.
    else
    {
      li.setAttribute('id' , 'group' + mp.groupId);

                    //list of products of this group.
                    let products:Array<PurchasesProduct> = new Array<PurchasesProduct>();
                    this.storeServ.StoresOrders.filter(so => so.groupId == mp.groupId).forEach(
                      p => {
                         this.storeServ.StoresOrdersProducts.filter(sop => sop.purchase == p.id).forEach(sopp => products.push(sopp));
                      }
                    );
                    //if all the product were done.
                    if(products.filter(p => p.status == false).length == 0)
                    {
                      li.setAttribute('style' , 'opacity:0.5;');
                    }
      var label = document.createElement('label');
      label.innerHTML  = this.storeServ.Groups.find(g => g.id == mp.groupId).groupName;


      label.addEventListener("click" , (event)=> {
        let groupId:number =  Number.parseInt(  (event.srcElement as HTMLLabelElement).parentElement.getAttribute('id').substring(5, ( event.srcElement as HTMLLabelElement).parentElement.getAttribute('id').length) );
        this.expandGroupDetails(groupId);
       event.stopPropagation();
      });
      // label.onclick = function(event)
      // {
      //   debugger;
        
      //   event.srcElement.addEventListener("fullscreenerror" , listener => {event.srcElement.parentElement.onclick(event)});
      //   event.srcElement.parentElement.onclick(event);
      // }
      li.appendChild(label);

      var i = document.createElement('i');
      i.innerHTML = 'done';
      i.setAttribute('id' , 'groupStatusI' + mp.groupId);
      
     
      i.setAttribute('class' , 'material-icons not-made');
      i.setAttribute('style' , 'margin-left:50vw;')

      i.addEventListener("click" ,(event) =>{
              // remove the group if all the purchases done.

              //list of products of this group.
              let products:Array<PurchasesProduct> = new Array<PurchasesProduct>();
localStoreServ.StoresOrders.filter(so => so.groupId == mp.groupId).forEach(
  p => {
     localStoreServ.StoresOrdersProducts.filter(sop => sop.purchase == p.id).forEach(sopp => products.push(sopp));
  }
);
//if all the product were done.
if(products.filter(p => p.status == false).length == 0)
{

  //delete from the service.
      //  localStoreServ.GroupedPurchases.find(gp => gp.groupId == mp.groupId).purchases.
      //  splice( localStoreServ.GroupedPurchases.
      //    find(gp => gp.groupId == mp.groupId).purchases.indexOf(
      //      localStoreServ.GroupedPurchases.
      //   find(gp => gp.groupId == mp.groupId).purchases.find(p => p.id == purchaseId)));
      //  debugger;
  document.getElementById('group'  +mp.groupId).remove();

  //remove all the purchases.
  //and update there statuses.
localStoreServ.StoresOrders.filter(so => so.groupId == mp.groupId).forEach(p => {
  updateAndRemovePurchaseAndProducts(localStoreServ , p.id);
});

}

else{openGroupDialog(mp.groupId , this.dialog , localStoreServ);
 
}

        //  storeServ.GroupedPurchases[purchase.groupId].purchases.forEach(p => {
        //    if( !p.status )
        //   {
        //     groupDone = false;
        //   }});
        //remove from the board.
        // if(!storeServ.GroupedPurchases.find(gp => gp.groupId == purchase.groupId).purchases.length)
        // {
        //   document.getElementById('group'  +purchase.groupId).remove();

        // }
          // if(groupDone)
          // {
          //   document.getElementById('group'  +purchase.groupId).remove();
          // }
          // else
          // {
            // let result:boolean;
            // result = openDialog(purchase.groupId);
            // //if get true - delete the group.
            // if(result)
            // {
            //   document.getElementById('group'  +purchase.groupId).remove();

            // }
          // }
      
      } );
  li.appendChild(i);
  

      mp.purchases.forEach(p => {
        this.saveInStorage(p);
      })

     let localStoreServ = this.storeServ;
     
     li.addEventListener("click" , (event) => {
       let groupId:number;
     if((event.srcElement as HTMLLIElement).localName == 'label')
     {
      groupId =  Number.parseInt( (event.srcElement as HTMLLIElement).parentElement.getAttribute('id').substring(5,  (event.srcElement as HTMLLIElement).parentElement.getAttribute('id').length) );
     }
     else
     {
       groupId =  Number.parseInt( (event.srcElement as HTMLLIElement).getAttribute('id').substring(5,  (event.srcElement as HTMLLIElement).getAttribute('id').length) );
     }
       this.expandGroupDetails(groupId);
      });
//      li.onclick = function(event)
//      {
//        debugger;

//       let groupId:number = 0;
// // select the groupId from the attribute'id' of the element
//       groupId = Number.parseInt( event.srcElement.getAttribute('id').substring(5,  event.srcElement.getAttribute('id').length) );
      
//       let group:PurchasesToGroup = localStoreServ.GroupedPurchases.find(gp => gp.groupId == groupId);
//       expandGroupDetails(group , localStoreServ);

//       }
    }

     document.getElementById('purchasesBoard').appendChild(li);
   });

}
    

//save the details of purchase in the local storage.
saveInStorage(purchase:Purchase)
{
  let productsCount = 0;
  //additions count = products count + num of addition in specific product.
  let additionsCount ;
  //select the purchase details to the local storage.
this.storeServ.StoresOrdersProducts.forEach(sop => {
  additionsCount = 0;
  //product of this purchase.
  if(sop.purchase == purchase.id )
  {
    sessionStorage.setItem('purchaseProducts' + purchase.id + "/" + productsCount , JSON.stringify( sop ) );
    //select th estors menu object (product details).
    sessionStorage.setItem( 'purchaseProductsMenu' + purchase.id + "/" + productsCount , JSON.stringify( this.storeServ.StoresMenu.find(sm => sm.id == sop.product) ) );
    //add the additions.
    this.storeServ.StoresOrdersDetailsAdditions.forEach(soda => {
      if(soda.product == sop.id)
      {
        sessionStorage.setItem('purchaseProductsAdditionsMenu' + purchase.id + "/" + additionsCount++ , JSON.stringify(this.storeServ.Additions.find(a => a.id == soda.addition)));
      }
    });


    // sop.id = purchase product id.
    sessionStorage.setItem('additionsCount' + sop.id , JSON.stringify( additionsCount ) );
    //add the tastes.

    let taste = this.storeServ.StoresOrdersDetailsTastes.find(sodt => sodt.product == sop.id);
    if(taste != undefined)
    {
      sessionStorage.setItem( 'purchaseProductsTastesMenu' + purchase.id + "/" + productsCount , JSON.stringify(this.storeServ.Tastes.find(t => t.id == taste.taste)));
    }
    sessionStorage.setItem('purchaseProductsCount' + purchase.id , JSON.stringify( ++productsCount ));


  }
});

}

//function that expand the group , to purchases.
expandGroupDetails( groupId:number )
{
 
  // let groupId:number = 0;
  // select the groupId from the attribute'id' of the element
        // groupId = Number.parseInt( event.srcElement.getAttribute('id').substring(5,  event.srcElement.getAttribute('id').length) );
    
        let group:PurchasesToGroup = this.storeServ.GroupedPurchases.find(gp => gp.groupId == groupId);
      
  var groupLi = document.getElementById('group' + group.groupId);
  //if the details are showen.
 
  if(document.getElementById('groupUl' + group.groupId) != undefined)
  {
    //hide the purchases list.
    document.getElementById('groupUl' + group.groupId).remove();
  }
  else 
  {
    var ul = document.createElement('ul');
    ul.setAttribute('id' , 'groupUl' + group.groupId);
    group.purchases.forEach(p=> {
      //create li to each purchase.
      var li = document.createElement('li');
      li.setAttribute( 'id' , 'purchase' + p.id );
      li.setAttribute('class' , 'tr' + group.groupId +' list-group-item list-group-item-action')

      createPurchasesElement( p , li ,this.storeServ , this.dialog);
      let localStoreServ = this.storeServ;
      li.onclick = function(event)
      {
    
        let eventId:string =  (event.srcElement as HTMLLIElement).getAttribute('id') ;
        let purchaseId:number = Number.parseInt( eventId.substring(8 , eventId.length) );
        if(document.getElementById('purchaseDetails' + purchaseId) == undefined)
        {
        expandPurchasesDetails( purchaseId , (event.srcElement as HTMLLIElement) , localStoreServ);
        event.stopPropagation();
        }
        else
        {
          document.getElementById('purchaseDetails' + purchaseId).remove();
    
        }
      }
      ul.appendChild(li);
    });
    //adding the list of the purchases after the line of the group name.
    // ul.onclick = function(event)
    // {
    //   alert(event.srcElement.getAttribute('id'));
    // }
    groupLi.after(ul);
  }
}


}

// function expandGroupDetails( groupId:PurchasesToGroup , localStoreServ:StoreService )

function expandGroupDetails( group:PurchasesToGroup , localStoreServ:StoreService )
{
 
  var groupLi = document.getElementById('group' + group.groupId);
  //if the details are showen.
  if(document.getElementById('groupUl' + group.groupId) != undefined)
  {
    //hide the purchases list.
    document.getElementById('groupUl' + group.groupId).remove();
  }
  else 
  {
    var ul = document.createElement('ul');
    ul.setAttribute('id' , 'groupUl' + group.groupId);
    group.purchases.forEach(p=> {
      //create li to each purchase.
      var li = document.createElement('li');
      li.setAttribute( 'id' , 'purchase' + p.id );
      li.setAttribute('class' , 'tr' + group.groupId +' list-group-item list-group-item-action')

      createPurchasesElement( p , li ,localStoreServ , this.dialog);
      li.onclick = function(event)
      {
        let eventId:string =  (event.srcElement as HTMLLIElement).getAttribute('id') ;
        let purchaseId:number = Number.parseInt( eventId.substring(8 , eventId.length) );
        if(document.getElementById('purchaseDetails' + purchaseId) == undefined)
        {
        expandPurchasesDetails( purchaseId , (event.srcElement as HTMLLIElement) , localStoreServ);
        event.stopPropagation();
        }
        else
        {
          document.getElementById('purchaseDetails' + purchaseId).remove();
    
        }
      }
      ul.appendChild(li);
    });
    //adding the list of the purchases after the line of the group name.
    // ul.onclick = function(event)
    // {
    //   alert(event.srcElement.getAttribute('id'));
    // }
    groupLi.after(ul);
  }
}

//function that create purchase element with her main details.
function createPurchasesElement(purchase:Purchase , element:Element , localStoreServ:StoreService , dialog:MatDialog)
{
  
  var purchaseTr= document.createElement('tr');
  purchaseTr.setAttribute('id' , element.getAttribute('id'));

  //if all the products were done.
  if(localStoreServ.StoresOrdersProducts.filter(sop => sop.purchase == purchase.id).filter(product => product.status == false).length == 0)
  {
    purchaseTr.setAttribute('style' , 'opacity:0.5;');
  }
  var  tdCName , tdCPhone , tdAddress , tdReservedSeats , tdClub , tdRecieptTime , tdPurchasesSum;

  tdCName = document.createElement('td');
  tdCName.setAttribute('style' , 'padding:10px;');
  tdCName.setAttribute('id' , element.getAttribute('id'));
  tdCName.innerHTML = purchase.customerName;
     
  tdCPhone = document.createElement('td');
  tdCPhone.setAttribute('style' , 'padding:10px;');
  tdCPhone.setAttribute('id' , element.getAttribute('id'));
  tdCPhone.innerHTML = purchase.customerPhone;

  tdAddress = document.createElement('td');
  tdAddress.setAttribute('style' , 'padding:10px;');
  tdAddress.setAttribute('id' , element.getAttribute('id'));
  tdAddress.innerHTML = purchase.deliveryAddress;

  
  tdReservedSeats = document.createElement('td');
  tdReservedSeats.setAttribute('style' , 'padding:10px;');
  tdReservedSeats.setAttribute('id' , element.getAttribute('id'));
  if(purchase.reservedSeats)
  tdReservedSeats.innerHTML = purchase.reservedSeats;
  else
  tdReservedSeats.innerHTML = " ";

  tdClub = document.createElement('td');
  tdClub.setAttribute('style' , 'padding:5px;');
  tdClub.setAttribute('id' , element.getAttribute('id'));
  if( purchase.club )
  tdClub.innerHTML = "Club";
  else
  tdClub.innerHTML = " ";

  tdRecieptTime = document.createElement('td');
  tdRecieptTime.setAttribute('style' , 'padding:10px;');
  tdRecieptTime.setAttribute('id' , element.getAttribute('id'));
  tdRecieptTime.innerHTML = purchase.receiptTime;

  tdPurchasesSum = document.createElement('td');
  tdPurchasesSum.setAttribute('style' , 'padding:10px;');
  tdPurchasesSum.setAttribute('id' , element.getAttribute('id'));
  tdPurchasesSum.innerHTML = purchase.purchaseSum + "₪";

  var tdSms = document.createElement('span');
  tdSms.innerHTML = "send sms";
  tdSms.setAttribute('style' , 'margin-left:30vw;color:#af0000;');
  tdSms.addEventListener("click" , (event)=> {
    //open the sms pop up dialog.
    openSmsDialog(dialog , new PurchasesToGroup(0 ,"", [purchase] , 0));
  });

  purchaseTr.appendChild( tdCName );
  purchaseTr.appendChild( tdCPhone );
  purchaseTr.appendChild( tdAddress );
  purchaseTr.appendChild( tdReservedSeats );
  purchaseTr.appendChild( tdClub );
  purchaseTr.appendChild( tdRecieptTime );
  purchaseTr.appendChild( tdPurchasesSum );
  
  purchaseTr.appendChild(tdSms);
  //purchase status exist only in single purchase.
  if(purchase.groupId == 0)
  {
    purchaseTr.appendChild(purchaseStatus(purchase.status , purchase.id , localStoreServ) );
  }

  
  purchaseTr.onclick = function(event){
    
    let eventId:string = (event.srcElement as HTMLTableRowElement).getAttribute('id') ;
    let purchaseId:number = Number.parseInt( eventId.substring(8 , eventId.length) );
    if(document.getElementById('purchaseDetails' + purchaseId) == undefined)
    {
    expandPurchasesDetails( purchaseId , (event.srcElement as HTMLTableRowElement).parentElement.parentElement.parentElement , localStoreServ);
    event.stopPropagation();  
    }
    else
    {
      document.getElementById('purchaseDetails' + purchaseId).remove();
      event.stopPropagation();
    }
  } 
  var table = document.createElement('table');
  table.setAttribute('id' , element.getAttribute('id'));
  table.onclick   = function(event)
  {
    let eventId:string = (event.srcElement as HTMLTableElement).getAttribute('id') ;
    let purchaseId:number = Number.parseInt( eventId.substring(8 , eventId.length) );
    if(document.getElementById('purchaseDetails' + purchaseId) == undefined)
    {
    expandPurchasesDetails( purchaseId ,(event.srcElement as HTMLTableElement).parentElement , localStoreServ);
    }
    else
    {
      document.getElementById('purchaseDetails' + purchaseId).remove();
    }
  }

  
  table.appendChild( purchaseTr );
  element.appendChild(table);

}


//this function expand the purchase that clicked , by openning her details.
function expandPurchasesDetails(purchaseId:number , element:Element , localStoreService:StoreService)
{
  while( element.localName != 'li')
  {
    element = element.parentElement;
  }
  var ul = document.createElement('ul');
  ul.setAttribute('id','purchaseDetails' + purchaseId);
  let additionsCount;
  for(let i = 0 , j; i< JSON.parse( sessionStorage.getItem('purchaseProductsCount' + purchaseId) ) ; i++)
  {
    additionsCount = 0;
    var li = document.createElement('li');
    li.setAttribute('class' , 'list-group-item list-group-item-action' );

    let purchase = localStoreService.StoresOrders.find(so => so.id == purchaseId);
    let product:PurchasesProduct =  JSON.parse( sessionStorage.getItem( 'purchaseProducts' + purchaseId + "/" + i ) ) ;
    li.setAttribute('id','purchaseProduct'+product.id);
    if(product.status)
    {
      li.setAttribute('style' , 'opacity:0.5;');
    }
    let productMenu:StoresMenu = JSON.parse( sessionStorage.getItem('purchaseProductsMenu' + purchaseId +'/' + i) );
    let productTaste:MenuTastes = JSON.parse( sessionStorage.getItem('purchaseProductsTastesMenu' + purchaseId + "/" + i) )
    let productAddition:MenuAdditions;
    // li.innerHTML = productMenu.productName + "     ";
   // li.innerHTML += "     " + product.productCount;
    var table = document.createElement('table');
    var tr = document.createElement('tr');
    var tdName = document.createElement('td');
    tdName.setAttribute('style' , 'padding-right: 3vw;width: 10vw;');
    tdName.innerHTML = productMenu.productName;
    tr.appendChild(tdName);
    var tdTaste =document.createElement('td');
    tdTaste.setAttribute('style' , 'padding-right: 3vw;width: 10vw;');
    if(productTaste != null)
    {
      //li.innerHTML += productTaste.tasteImage;
      var tasteDiv = document.createElement('div');
      tasteDiv.setAttribute('style' , 'display:inline-block;')
      var tasteImg = new Image();
      tasteImg.src = productTaste.tasteImage;
      tasteImg.setAttribute('style' , 'width:50px;height:50px;border-radius:100px;');
      tasteImg.title = productTaste.tasteName;
      tasteDiv.appendChild(tasteImg);

      var tasteSpan = document.createElement('span');
      tasteSpan.innerHTML = productTaste.tasteName;
      tasteSpan.setAttribute('style' , 'font-size:12;');

      tasteDiv.appendChild(document.createElement('br'));
      tasteDiv.appendChild(tasteSpan);
      // li.appendChild(tasteDiv);
      tdTaste.appendChild(tasteDiv);
    }
    tr.appendChild(tdTaste);
    additionsCount = JSON.parse( sessionStorage.getItem('additionsCount' +  product.id) );
    var tdAdditions = document.createElement('td');
    tdAdditions.setAttribute('style' , 'padding-right: 3vw;width: 10vw;');
    for( j= 0; j < additionsCount ; j ++)
    {
      productAddition = JSON.parse(sessionStorage.getItem( 'purchaseProductsAdditionsMenu' + purchaseId + "/" + j ));
      var addDiv = document.createElement('div');
      addDiv.setAttribute('style' , 'display:inline-block;')
      var addImg = new Image();
      addImg.src = productAddition.additionImage;
      addImg.setAttribute('style' , 'width:50px;height:50px;border-radius:100px;');
      addImg.title = productAddition.additionName;
      addDiv.appendChild(addImg);

      var addSpan = document.createElement('span');
      addSpan.innerHTML = productAddition.additionName;
      addSpan.setAttribute('style' , 'font-size:12;');

      addDiv.appendChild(document.createElement('br'));
      addDiv.appendChild(addSpan);
      // li.appendChild(addDiv);
      tdAdditions.appendChild(addDiv);
    }

    tr.appendChild(tdAdditions);

var tdStatus = document.createElement('td');
tdStatus.setAttribute('style' , 'padding-right: 3vw;width: 10vw;');
    //status
    var tdPDone = document.createElement('div');
    var doneDiv = document.createElement('div');
    //doneDiv.appendChild(document.createElement('br'));
    // var doneSpan = document.createElement('sapn');
    // doneSpan.innerHTML = "  Done";
    //doneSpan.setAttribute('style' , 'margin:60px;9px;')
    var link = document.createElement('i');
    link.setAttribute('class' , 'material-icons done-link');
    link.innerHTML = 'done';
    doneDiv.appendChild(link);
    //doneDiv.innerHTML = "  Done";
    let opacityDone = 'width:40px;height:40px;color:black;opacity:0.5;border-radius:40px;padding: 1.2vh 0.75vw;place-content:center;display:inline-block;';
    let nonOpacityDone ='width:40px;height:40px;color:green;opacity:1;border-radius:40px;padding: 1.2vh 0.75vw;place-content:center;display:inline-block;';
    //if the status shows not done.
    if(purchase.status == false)
    {
      doneDiv.setAttribute('style' , opacityDone);
    }
    //if the status shows done.
    else{
      doneDiv.setAttribute('style' , nonOpacityDone);
    }
    doneDiv.setAttribute('id' , 'productStatus ' + purchaseId );
      link.onclick = function(event)
    // doneDiv.onclick = function(event)
    {
      // if(event.srcElement.getAttribute('style') == nonOpacityDone)
      // {
      //   event.srcElement.setAttribute('style' , opacityDone)
      // }
      // else
       if( (event.srcElement as HTMLDivElement).parentElement.getAttribute('style') == opacityDone)
        {
          (event.srcElement as HTMLDivElement).parentElement.setAttribute('style' , nonOpacityDone);
let productId =Number.parseInt( 
  (event.srcElement as HTMLDivElement).parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.getAttribute('id').substring(15) );
          //update the status in the server.
          localStoreService.updatePurchasesProductsStatus(productId , true).subscribe(data => {
           

          //find this product in the services and change his status to true;
          // let purchasesProductId =Number.parseInt( (event.srcElement as HTMLDivElement).getAttribute('id').split(" ", 2)[1] );
     
          //update the status.
          localStoreService.StoresOrdersProducts.find(sop => sop.id == productId).status = true;

          //change the status of the purchase.
          let purchaseStatusDiv = document.getElementById('purchaseStatusI' + purchaseId);

            //array of the purchasesProducts of this purchase.
let ordersProducts:Array<PurchasesProduct> = new Array<PurchasesProduct>();
//flag of carried out.
let carriedOut:boolean = false;
//flag of done.
let done:boolean = false;
//flag of notDone.
let notDone:boolean = false;

//select all the products of this purchase 
//if there is one that was done and one that not, so the status is in the middle of execute,
//if all the products were done the status is done,
//if all the products werent done the status is not done.
//select the purchaseProducts.
localStoreService.StoresOrdersProducts.forEach(sop => {
  if(sop.purchase == purchaseId)
  {
    ordersProducts.push(sop);
  }
});

//pass on the array of the products of this purchase.
for(let op of ordersProducts)
{
     //check the status and sign the flags.
     switch( op.status )
     {
       case true:done = true;
                 break;
       case false:notDone = true;
                 break;
     }
     //if the purchase is carried out cause some of the products were done and some not break the loop.
     if( done && notDone )
     {
       carriedOut = true;
       break;
     }
}
  if( carriedOut )
  {
    purchaseStatusDiv.setAttribute('style' , 'width:40px;height:40px;border-radius:50px;color:orange;')
  }
  else if( done )
  {
    localStoreService.StoresOrders.find(so => so.id == purchaseId).status = true;
   document.getElementById('purchase' + purchaseId).setAttribute('style' , 'opacity:0.5;');
   ordersProducts.forEach(op => {
     document.getElementById('purchaseProduct' + op.id).setAttribute('style' , 'opacity:0.5;');
   });
   //if this group refernce to group,
   //opacity the group of this purchase if all the purchase were done.
   let groupId = localStoreService.StoresOrders.find(p => p.id === purchaseId).groupId;
   let groupDone = true;
   if(groupId != 0)
   {
      localStoreService.StoresOrders.forEach(so => {
if(!so.status)
{
groupDone = false;
}
      });
      if(groupDone)
      {
        document.getElementById('group' + groupId).setAttribute('style' , 'opacity:0.5;');
      }
   }
    //purchaseStatusDiv.setAttribute('style' , 'width:40px;height:40px;border-radius:50px;background-color:green;')
  }
  else
  {
    purchaseStatusDiv.setAttribute('style' , 'width:40px;height:40px;border-radius:50px;background-color:red;')
  }
          // //if the status was not done - change the status to carried out.
          // if(purchaseStatusDiv.getAttribute('style') == 'width:40px;height:40px;border-radius:50px;background-color:red;')
          // {
          //   purchaseStatusDiv.setAttribute('style' , 'width:40px;height:40px;border-radius:50px;background-color:orange;')
          // }
 });
        }
     
    }
    tdPDone.appendChild(doneDiv);
    // li.appendChild(tdPDone);

    tdStatus.appendChild(tdPDone);
    tr.appendChild(tdStatus);
    table.appendChild(tr);
    li.appendChild(table);

    ul.appendChild(li);
  }
  element.after(ul);

}

//create the purchase status.

  //function that return element that show the purchase status.
  // purchase status mean - the purchase picked up!.
  function purchaseStatus(status:boolean , purchaseId:number , storeServ:StoreService)
  {

    //array of the purchasesProducts of this purchase.
    let ordersProducts:Array<PurchasesProduct> = new Array<PurchasesProduct>();
    //flag of carried out.
    let carriedOut:boolean = false;
    //flag of done.
    let done:boolean = false;
    //flag of notDone.
    let notDone:boolean = false;

    //select all the products of this purchase 
    //if there is one that was done and one that not, so the status is in the middle of execute,
    //if all the products were done the status is done,
    //if all the products werent done the status is not done.
    
    //select the purchaseProducts.
    // storeServ.StoresOrdersProducts.forEach(sop => {
    //   if(sop.purchase == purchaseId)
    //   {
    //     ordersProducts.push(sop);
    //   }
    // });

    // //pass on the array of the products of this purchase.
    // for(let op of ordersProducts)
    // {
    //      //check the status and sign the flags.
    //      switch( op.status )
    //      {
    //        case true:done = true;
    //                  break;
    //        case false:notDone = true;
    //                  break;
    //      }
    //      //if the purchase is carried out cause some of the products were done and some not break the loop.
    //      if( done && notDone )
    //      {
    //        carriedOut = true;
    //        break;
    //      }
    // }
    
    // var div = document.createElement('div');
    var i = document.createElement('i');
    i.innerHTML = 'done';
    i.setAttribute('id' , 'purchaseStatusI' + purchaseId);
   
    i.setAttribute('class' , 'material-icons not-made');
    i.setAttribute('style' , 'margin-left:40vw;');


    i.addEventListener('click' ,(event)=>
    {
      // remove this purchase from the purchasesBoard.
      // select the purchase id
      let purchaseId:number = Number.parseInt ((event.srcElement as HTMLLinkElement).getAttribute('id').substring(15 , (event.srcElement as HTMLLinkElement).getAttribute('id').length));
      // change the status to true
      let purchase:Purchase = storeServ.StoresOrders.find(p => p.id == purchaseId);
      updateAndRemovePurchaseAndProducts(storeServ , purchase.id);
//       storeServ.updatePurchasesStatus(purchaseId).subscribe(data =>{
//        storeServ.StoresOrders.splice(storeServ.StoresOrders.indexOf(storeServ.StoresOrders.find(p => p.id == purchaseId)));

//       // remove the purchase element.
//       document.getElementById('purchase'+purchaseId).remove();
//       //remove the showen products of this purchase.
//       if(document.getElementById('purchaseDetails' + purchaseId) != undefined)
//       {
//       document.getElementById('purchaseDetails' + purchaseId).remove();
//       }

//       //show the "there is no purchases" message on the board when the board is empty.
//       if(!document.getElementById('purchasesBoard').hasChildNodes())
// {
//   storeServ.PurchasesMode = true;
// }
// });
    });

    //tdStatus.appendChild(div);
    return i;
    //}
  }

function updateAndRemovePurchaseAndProducts(storeServ:StoreService , purchaseId:number)
{
  storeServ.updatePurchasesStatus(purchaseId).subscribe(data =>{
    //delete from the only purchases list.
    if(storeServ.StoresOrders.find(p => p.id == purchaseId).groupId == 0)
    storeServ.PurchasesWithOutGroups.splice(storeServ.PurchasesWithOutGroups.indexOf(storeServ.PurchasesWithOutGroups.find(p => p.id == purchaseId)));
    storeServ.StoresOrders.splice(storeServ.StoresOrders.indexOf(storeServ.StoresOrders.find(p => p.id == purchaseId)));

    // remove the purchase element.
   document.getElementById('purchase'+purchaseId).remove();
   //remove the showen products of this purchase.
   if(document.getElementById('purchaseDetails' + purchaseId) != undefined)
   {
   document.getElementById('purchaseDetails' + purchaseId).remove();
   }
debugger;
   //show the "there is no purchases" message on the board when the board is empty.
   if(!document.getElementById('purchasesBoard').hasChildNodes())
{
storeServ.PurchasesMode = true;
}});
}
  
 function openGroupDialog(groupId:number , dialog:MatDialog , localStoreServ:StoreService):any {
    const dialogRef = dialog.open(MadeGroupPopUpComponent, {
      width: '50vw',
      data: {groupId}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result)
      {
        document.getElementById('group'  +groupId).remove();
          //remove all the purchases.
  //and update there statuses.
localStoreServ.StoresOrders.filter(so => so.groupId == groupId).forEach(p => {
  updateAndRemovePurchaseAndProducts(localStoreServ , p.id);
});
      }
return result;
    });
}


function openSmsDialog(dialog:MatDialog , purchase:PurchasesToGroup):void {
debugger;
  const dialogRef = dialog.open(SmsPopUpComponent , {
    width:'30vw',
    data : {phone: purchase.purchases[0].customerPhone,
      name:purchase.purchases[0].customerName
    }
  });

  dialogRef.afterClosed().subscribe(res => {

  });
}


  