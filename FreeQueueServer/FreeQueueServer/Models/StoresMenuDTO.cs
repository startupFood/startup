﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreeQueueServer.Models
{
    public class StoresMenuDTO
    {
        public int id { get; set; }
        public int store { get; set; }
        public string productName { get; set; }
        public int productCategory { get; set; }
        public double productPrice { get; set; }
        public bool productStatus { get; set; }
        public bool? quickProduct { get; set; }
        public int preperationTime { get; set; }
        public string productImage { get; set; }
        public string barcode { get; set; }
        public int additionQuantity { get; set; }

        public static StoresMenuDTO ConvertToDTO(tbl_storesMenu storeMenu)
        {
            return new StoresMenuDTO()
            {
                id = storeMenu.Id,
                store = (storeMenu.Store != 0) ? storeMenu.tbl_stores.Id : 0,
                productName = storeMenu.ProductName,
productCategory = storeMenu.ProductCategory,
                productPrice = storeMenu.ProductPrice,
                productStatus = storeMenu.ProductStatus,
                quickProduct = storeMenu.QuickProduct,
                preperationTime = storeMenu.PreperationTime,
                productImage = storeMenu.ProductImage,
                barcode = storeMenu.Barcode,
                additionQuantity = storeMenu.AdditionsQuantity
            };
        }

        public static List<StoresMenuDTO> ConvertToDTO(List<tbl_storesMenu> storeMenu)
        {
            return storeMenu.Select(s => ConvertToDTO(s)).ToList();
        }
    }
}