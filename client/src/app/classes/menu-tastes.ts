export class MenuTastes {
    constructor(
        public id:number,
        public product:number,
        public tasteName:string,
        public tasteStatus:boolean,
        public tasteImage:string
    ){}
}
