﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreeQueueServer.Models
{
    public class ResponsesDTO
    {
        public int id { get; set; }
        public int storeId { get; set; }
        public string userName { get; set; }
        public string responseDate { get; set; }
        public string header { get; set; }
        public string content { get; set; }
        public string email { get; set; }
        
        public static ResponsesDTO ConvertToDTO(tbl_responses response)
        {
            return new ResponsesDTO()
            {
                id = response.Id,
                storeId = response.StroeId,
                userName = response.UserName,
                responseDate = response.ResponseDate,
                header = response.Header,
                content=response.Content,
                email = response.Email
            };
        }

        public static List<ResponsesDTO> ConvertToDTO(List<tbl_responses> response)
        {
            return response.Select(r => ConvertToDTO(r)).ToList();
        }
    }
}