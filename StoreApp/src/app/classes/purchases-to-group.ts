import { Group } from "./group";
import { Purchase } from "./purchase";

export class PurchasesToGroup {
    
    constructor(
    public groupId:number,
    public groupName:string,
    public purchases:Array<Purchase>,
    public preperationTotalTime:number){}
}
