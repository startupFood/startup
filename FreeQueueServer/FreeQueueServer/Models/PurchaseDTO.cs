﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreeQueueServer.Models
{
    public class PurchaseDTO
    {
        public int id { get; set; }
        public int store { get; set; }
        public string purchaseDate  { get; set; }
        public string customerName { get; set; }
        public string customerPhone { get; set; }
        public string creditCard { get; set; }
        public string creditDate { get; set; }
        public string creditDigits { get; set; }
        public string deliveryAddress { get; set; }
        public int? groupId { get; set; }
        public int? reservedSeats { get; set; }
        public bool? club { get; set; }
        public double? tip { get; set; }
        public double purchaseSum { get; set; }
        public string receiptTime { get; set; }
        public bool? favorite { get; set; }
        public int? userId { get; set; }

        public bool? status { get; set; }
        /// <summary>
        /// get sql entity and convert it to simple object whithout references
        /// </summary>
        /// <param name="purchase"></param>
        /// <returns></returns>
        public static PurchaseDTO ConvertToDTO(tbl_purchases purchase)
        {
            return new PurchaseDTO()
            {
                id = purchase.Id,
                store = purchase.StoreId,
                purchaseDate = purchase.PurchaseDate,
                customerName = purchase.CustomerName,
                customerPhone = purchase.CustomerPhone,
                creditCard = purchase.CreditCard,
                creditDate = purchase.CreditDate,
                creditDigits = purchase.CreditDigits,
                deliveryAddress = purchase.DeliveryAddress,
                groupId = (purchase.GroupDetails!=null)?purchase.GroupDetails : 0,
                reservedSeats = purchase.ReservedSeats,
                club = purchase.Club,
                tip = purchase.Tip,
                purchaseSum = purchase.PurchaseSum,
                receiptTime = purchase.ReceiptTime,
                favorite=purchase.Favorite,
                userId=purchase.UserId,
                status = purchase.Status
            };
        }

        public static List<PurchaseDTO> ConvertToDTO(List<tbl_purchases> purchases)
        {
            return purchases.Select(p => ConvertToDTO(p)).ToList();
        }
    }
}