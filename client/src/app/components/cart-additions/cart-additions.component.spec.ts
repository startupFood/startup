import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartAdditionsComponent } from './cart-additions.component';

describe('CartAdditionsComponent', () => {
  let component: CartAdditionsComponent;
  let fixture: ComponentFixture<CartAdditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartAdditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartAdditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
