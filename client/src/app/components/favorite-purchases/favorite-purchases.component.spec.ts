import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoritePurchasesComponent } from './favorite-purchases.component';

describe('FavoritePurchasesComponent', () => {
  let component: FavoritePurchasesComponent;
  let fixture: ComponentFixture<FavoritePurchasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoritePurchasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoritePurchasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
