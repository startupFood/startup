import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessOrderPopUpComponent } from './success-order-pop-up.component';

describe('SuccessOrderPopUpComponent', () => {
  let component: SuccessOrderPopUpComponent;
  let fixture: ComponentFixture<SuccessOrderPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessOrderPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessOrderPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
