export class Response {
    constructor(
        public id:number,
        public storeId:number,
        public userName:string,
        public responseDate:string,
        public header:string,
        public content:string,
        public email:string
    ){}
}
