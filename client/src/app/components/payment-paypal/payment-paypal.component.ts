import { Component, OnInit, Input } from '@angular/core';
import { IPayPalConfig, ICreateOrderRequest } from 'ngx-paypal';
import { Promise } from 'q';
import { SuccessOrderPopUpComponent } from './success-order-pop-up/success-order-pop-up.component';
import { MatDialog } from '@angular/material/dialog';
import { CartService } from 'src/app/services/cart.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment-paypal',
  templateUrl: './payment-paypal.component.html',
  styleUrls: ['./payment-paypal.component.css']
})
export class PaymentPaypalComponent implements OnInit {
  public payPalConfig?: IPayPalConfig;
  public showSuccess:boolean=false;

  constructor(    public dialog:MatDialog,
      public cartServ:CartService,
      public router:Router){
  }
  ngOnInit(): void {
    this.initConfig();
  }

  @Input()sum:number;

  private initConfig(): void {
    this.payPalConfig = {
    currency: 'ILS',
    clientId: 'AZPv7HUUGh9wq1aSDGBghHTMDS8oj2aDvwDorfdhVcNZp1RReOVEle3xMx-Totr44Jz7AAoOj0djTwv4',
    createOrderOnClient: (data) => <ICreateOrderRequest>{
      intent: 'CAPTURE',
      purchase_units: [
        {
          amount: {
            currency_code: 'ILS',
            value: this.sum.toString(),
            breakdown: {
              item_total: {
                currency_code: 'ILS',
                value: this.sum.toString()
              }
            }
          },
          items: [
            {
              name: 'Enterprise Subscription',
              quantity: '1',
              category: 'DIGITAL_GOODS',
              unit_amount: {
                currency_code: 'ILS',
                value: this.sum.toString(),
              },
            }
          ]
        }
      ]
    },
    advanced: {
      commit: 'true'
    },
    style: {
      label: 'paypal',
      layout: 'vertical'
    },
    onApprove: (data, actions) => {
      console.log('onApprove - transaction was approved, but not authorized', data, actions);
      actions.order.get().then(details => {
        console.log('onApprove - you can get full order details inside onApprove: ', details);
      });
    },
    onClientAuthorization: (data) => {
      console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
      this.showSuccess = true;
    },
    onCancel: (data, actions) => {
      this.save();
      this.showSuccess = true;
      console.log('OnCancel', data, actions);
    },
    onError: err => {
      this.showSuccess = true;
      console.log('OnError', err);
    },
    onClick: (data, actions) => {
      this.showSuccess = true;
      console.log('onClick', data, actions);
    },
  };
  }

  save()
  {
    this.cartServ.addOrder(  );
    this.cartServ.buySubject.subscribe(data => {
      this.showSuccess = true;
      this.router.navigate(["/storesList"]);
    })
    // this.openDialog();

  }


  openDialog(): void {
    const dialogRef = this.dialog.open(SuccessOrderPopUpComponent, {
      width: '50vw',
      data: true
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.cartServ.buySubject.subscribe(data => {
        this.router.navigate(["/storesList"]);
      })
    });
}

}
