import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/classes/user';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { UsersService } from 'src/app/services/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(public formBuilder:FormBuilder, public usersServ:UsersService, public router:Router) { }

  signUpForm: FormGroup;
  submitted:boolean=false;
  loading = false;

  user:User=new User(0, "", "", "", "", 0);

  validForm:boolean=false;

  errorMessage:string="";

  ngOnInit(){

  }

  /* ngOnInit() {
    this.signUpForm = this.formBuilder.group({
      userEmail: ['', [Validators.required, Validators.email]],
      userName: ['', Validators.required],
      userPhone: ['', Validators.required],
      userPassword: ['',[ Validators.required, Validators.maxLength(20), Validators.minLength(6) ]]
      });

    // get return url from route parameters or default to '/'
    //this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() { 
    return this.signUpForm.controls;
   }

   get password():AbstractControl
   {
     return this.signUpForm.get("userPassword");
   }
 */
  signUp()
  {
    this.submitted = true;

        // stop here if form is invalid
        if (this.signUpForm.invalid || !this.validForm) {
            return;
        }
        var user:User = new User(
          0,
          this.signUpForm.get("userEmail").value,
          this.signUpForm.get("userName").value,
          this.signUpForm.get("userPassword").value,
          this.signUpForm.get("userPhone").value,
          5
        );
    if(this.usersServ.addUser(user, true))
  {
    this.usersServ.showLoginSubject.next(0);
      this.router.navigate(["store"]);
      
  }
    else
      this.errorMessage="sorry, you wasn't added successfully, please try again.";
        
  }

/*   passwordEquals(confirmPassword:string):void
  {
    if(this.signUpForm.get("userPassword").value!="" && this.signUpForm.get("userPassword").value==confirmPassword)
      this.validForm=true;
    else
      this.validForm=false;
  } */

}
