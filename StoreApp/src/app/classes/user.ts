export class User {
    constructor(
        public id:number,
        public userEmail:string,
        public userName:string,
        public userPassword:string,
        public userPhone:string,
        public roleId:number
    ) {}
}
