import { Store } from "./store";

export class StoreAndDistance {
    constructor(
        public store:Store,
        public distance:number,
        public longitude:number,
        public latitude:number){}
}
