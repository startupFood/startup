import { StoresMenu } from "./stores-menu";
import { Hechshers } from "./hechshers";
import { Store } from "./store";

export class StoreMenuWithDetails {
constructor(
    public product:StoresMenu,
    public location:number,
    public kashrut:string,
    public store:Store,
    public storeCategory:string,
    public productCategory:string
){}

}

