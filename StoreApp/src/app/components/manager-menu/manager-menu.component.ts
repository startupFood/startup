import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { GridComponent,  GridDataResult,  DataStateChangeEvent, RowClassArgs} from '@progress/kendo-angular-grid';
import { SortDescriptor } from '@progress/kendo-data-query';
import { Observable } from 'rxjs';
import { GridDataService, ProductsService, AdditionsService } from 'src/app/services/grid-data.service';
import { ManagerService } from 'src/app/services/manager.service';
import { State, process } from '@progress/kendo-data-query';
import { MatDialog } from '@angular/material/dialog';
import { ManagerEditProductComponent } from '../manager-edit-product/manager-edit-product.component';
import { StoresMenu } from 'src/app/classes/stores-menu';


@Component({
  providers: [ProductsService, AdditionsService],
  selector: 'app-manager-menu',
  templateUrl: './manager-menu.component.html',
  styleUrls: ['./manager-menu.component.css']
})
export class ManagerMenuComponent implements OnInit {

  public view: Observable<GridDataResult>;
  public sort: Array<SortDescriptor> = [];
  public pageSize = 10;
  public skip = 0;

  // For Angular 8
    @ViewChild(GridComponent) public grid: GridComponent;

  constructor(private service:ProductsService, private managerServ:ManagerService, public dialog: MatDialog) { }


  public ngOnInit(): void {
      // Bind directly to the service as it is a Subject
      this.view = this.service;
      // Fetch the data with the initial state
      this.loadData();
  }

    public dataStateChange({ skip, take, sort }: DataStateChangeEvent): void {
        // Save the current state of the Grid component
        this.skip = skip;
        this.pageSize = take;
        this.sort = sort;

        // Reload the data with the new state
        this.loadData();

        // Expand the first row initially
        this.grid.expandRow(0);
    }

    private loadData(): void {
        this.service.query({ skip: this.skip, take: this.pageSize, sort: this.sort }, "Menu/GetByStore/"+this.managerServ.StoreId);
    }


  private cancelHandler({sender, rowIndex}) {
      this.closeEditor(sender, rowIndex);
  }

  private saveHandler({sender, rowIndex, formGroup, isNew}) {
     // const product: Product = formGroup.value;

      //this.editService.save(product, isNew);

      sender.closeRow(rowIndex);
  }
    
  private editedRowIndex: number;

  private editHandler({sender, rowIndex, dataItem}) {

    let rowData = dataItem;
    var temp:string=rowData.id;
    const dialogRef = this.dialog.open(ManagerEditProductComponent, { disableClose: true,
      width: '500px',
      data: {product:rowData}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      //location.reload();
    })

}

  private removeHandler({dataItem}) {
    if(confirm("Are you sure you wanna delete "+dataItem.productName+" ?")) {
      this.managerServ.deleteProduct(dataItem.id).subscribe(
        data=>{location.reload();},
        err=>{alert(err.error)}
      )
    }
  }

  private createHandler()
  {
    const dialogRef = this.dialog.open(ManagerEditProductComponent, { disableClose: true,
      width: '500px',
      data: {product:null}
    });

    dialogRef.afterClosed().subscribe();
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
      grid.closeRow(rowIndex);
      this.editedRowIndex = undefined;
      //this.formGroup = undefined;
  }

  changeStatus(product:StoresMenu)
  {
    var status=new Status("product", product.id, !product.productStatus);
    this.managerServ.changeStatus(status).subscribe();
  }
    
}

export class Status
{
  constructor(
  public type:string,
  public id:number,
  public status:boolean){}
}
