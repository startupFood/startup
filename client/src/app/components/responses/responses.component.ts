import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/services/store.service';
import { Response } from 'src/app/classes/response';

@Component({
  selector: 'app-responses',
  templateUrl: './responses.component.html',
  styleUrls: ['./responses.component.css']
})
export class ResponsesComponent implements OnInit {

  responsesList:Response[];
  viewForm:boolean=false;

  constructor(public storeServ:StoreService) {
    storeServ.getStoreResponses().subscribe(
      data=>{this.responsesList=data}
    );
  }

  ngOnInit() {
  }

  addResponse(response:Response)
  {
    this.storeServ.addNewResponse(response).subscribe(
      data=>{this.responsesList=data},
      err=>{err}
    )
  }

}
