USE [master]
GO
/****** Object:  Database [DB_freeQueue]    Script Date: 03/12/2019 12:23:52 ******/
CREATE DATABASE [DB_freeQueue]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DB_freeQueue', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\DB_freeQueue.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DB_freeQueue_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\DB_freeQueue_log.ldf' , SIZE = 1072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DB_freeQueue] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DB_freeQueue].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DB_freeQueue] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DB_freeQueue] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DB_freeQueue] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DB_freeQueue] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DB_freeQueue] SET ARITHABORT OFF 
GO
ALTER DATABASE [DB_freeQueue] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [DB_freeQueue] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DB_freeQueue] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DB_freeQueue] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DB_freeQueue] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DB_freeQueue] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DB_freeQueue] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DB_freeQueue] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DB_freeQueue] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DB_freeQueue] SET  ENABLE_BROKER 
GO
ALTER DATABASE [DB_freeQueue] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DB_freeQueue] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DB_freeQueue] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DB_freeQueue] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DB_freeQueue] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DB_freeQueue] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DB_freeQueue] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DB_freeQueue] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DB_freeQueue] SET  MULTI_USER 
GO
ALTER DATABASE [DB_freeQueue] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DB_freeQueue] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DB_freeQueue] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DB_freeQueue] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [DB_freeQueue] SET DELAYED_DURABILITY = DISABLED 
GO
USE [DB_freeQueue]
GO
/****** Object:  Table [dbo].[tbl_groups]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_groups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NULL,
	[GroupName] [nvarchar](30) NULL,
	[GroupPassword] [nvarchar](100) NULL,
	[OpeningDate] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_menuAddittions]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_menuAddittions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Product] [int] NOT NULL,
	[Addition] [nvarchar](30) NOT NULL,
	[AddtionPrice] [float] NOT NULL,
	[AdditionStatus] [bit] NOT NULL,
	[AdditionImage] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_menuTastes]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_menuTastes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Product] [int] NOT NULL,
	[Taste] [nvarchar](30) NOT NULL,
	[TasteStatus] [bit] NOT NULL,
	[TasteImage] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_modifyTypes]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_modifyTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ModifyType] [nvarchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_productsCategories]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_productsCategories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Store] [int] NOT NULL,
	[Category] [nvarchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchases]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchases](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[PurchaseDate] [nvarchar](20) NOT NULL,
	[CustomerName] [nvarchar](100) NOT NULL,
	[CustomerPhone] [nvarchar](20) NULL,
	[CreditCard] [nvarchar](100) NOT NULL,
	[CreditDate] [nvarchar](5) NOT NULL,
	[CreditDigits] [nvarchar](3) NOT NULL,
	[DeliveryAddress] [nvarchar](50) NULL,
	[GroupDetails] [int] NULL,
	[ReservedSeats] [int] NULL,
	[Club] [bit] NULL,
	[Tip] [float] NULL,
	[PurchaseSum] [float] NOT NULL,
	[ReceiptTime] [nvarchar](5) NOT NULL,
	[Favorite] [bit] NULL,
	[UserId] [int] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchasesAdditions]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchasesAdditions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Product] [int] NOT NULL,
	[Addition] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchasesAdditionsHistory]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchasesAdditionsHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tbl_PurchasesAdditionId] [int] NOT NULL,
	[Product] [int] NOT NULL,
	[Addition] [int] NOT NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[modifyType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchasesHistory]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchasesHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tbl_PurchaseId] [int] NOT NULL,
	[StoreId] [int] NOT NULL,
	[PurchaseDate] [nvarchar](20) NOT NULL,
	[CustomerName] [nvarchar](100) NOT NULL,
	[CustomerPhone] [nvarchar](20) NULL,
	[CreditCard] [nvarchar](100) NOT NULL,
	[CreditDate] [nvarchar](5) NOT NULL,
	[CreditDigits] [nvarchar](3) NOT NULL,
	[DeliveryAddress] [nvarchar](50) NULL,
	[GroupDetails] [int] NULL,
	[ReservedSeats] [int] NULL,
	[Club] [bit] NULL,
	[Tip] [float] NULL,
	[PurchaseSum] [float] NOT NULL,
	[ReceiptTime] [nvarchar](5) NOT NULL,
	[favorite] [bit] NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[modifyType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchasesProducts]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchasesProducts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PurchaseId] [int] NOT NULL,
	[Product] [int] NOT NULL,
	[ProductCount] [int] NOT NULL,
	[Price] [float] NOT NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchasesProductsHistory]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchasesProductsHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tbl_PurchaseProductId] [int] NOT NULL,
	[PurchaseId] [int] NOT NULL,
	[Product] [int] NOT NULL,
	[ProductCount] [int] NOT NULL,
	[Price] [float] NOT NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[modifyType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchasesTastes]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchasesTastes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Product] [int] NOT NULL,
	[Taste] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchasesTastesHistory]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchasesTastesHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tbl_PurchasesTastesId] [int] NOT NULL,
	[Product] [int] NOT NULL,
	[Taste] [int] NOT NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[modifyType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_purchaseToWorker]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchaseToWorker](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[PurchaseId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_responses]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_responses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StroeId] [int] NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[ResponseDate] [nvarchar](20) NOT NULL,
	[Header] [nvarchar](100) NOT NULL,
	[Content] [nvarchar](500) NOT NULL,
	[Email] [nvarchar](400) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_responsesHistory]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_responsesHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tbl_responsesId] [int] NOT NULL,
	[StroeId] [int] NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[ResponseDate] [nvarchar](20) NOT NULL,
	[Header] [nvarchar](100) NOT NULL,
	[Content] [nvarchar](500) NOT NULL,
	[Email] [nvarchar](400) NOT NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[modifyType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_roles]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleType] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_stores]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_stores](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StoreName] [nvarchar](50) NOT NULL,
	[StoreAddress] [nvarchar](200) NOT NULL,
	[Phone] [nvarchar](20) NULL,
	[About] [nvarchar](300) NULL,
	[KashrutCertification] [nvarchar](30) NOT NULL,
	[Img] [nvarchar](50) NULL,
	[StoreCategory] [nvarchar](20) NOT NULL,
	[ReservedSeats] [bit] NULL,
	[Club] [bit] NULL,
	[Tip] [bit] NULL,
	[StoreLoad] [bit] NOT NULL,
	[Bank] [int] NULL,
	[Brunch] [int] NULL,
	[Account] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_storesActivityTime]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_storesActivityTime](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Store] [int] NOT NULL,
	[ActivityDay] [int] NOT NULL,
	[StartTime] [nvarchar](5) NOT NULL,
	[EndTime] [nvarchar](5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_storesMenu]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_storesMenu](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Store] [int] NOT NULL,
	[ProductName] [nvarchar](100) NOT NULL,
	[ProductCategory] [int] NOT NULL,
	[ProductPrice] [float] NOT NULL,
	[ProductStatus] [bit] NOT NULL,
	[QuickProduct] [bit] NULL,
	[ProductImage] [nvarchar](100) NOT NULL,
	[PreperationTime] [int] NOT NULL,
	[Delivery] [bit] NULL,
	[AdditionsQuantity] [int] NOT NULL,
	[Barcode] [nvarchar](128) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_users]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserEmail] [nvarchar](400) NOT NULL,
	[UserPassword] [nvarchar](500) NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[UserPhone] [nvarchar](50) NULL,
	[RoleId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_usersHistory]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_usersHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tbl_usersId] [int] NOT NULL,
	[UserEmail] [nvarchar](400) NOT NULL,
	[UserPassword] [nvarchar](500) NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[UserPhone] [nvarchar](50) NULL,
	[RoleId] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[modifyType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_workerToStore]    Script Date: 03/12/2019 12:23:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_workerToStore](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[StoreId] [int] NULL,
	[Tip] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[tbl_groups]  WITH CHECK ADD FOREIGN KEY([StoreId])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_menuAddittions]  WITH CHECK ADD FOREIGN KEY([Product])
REFERENCES [dbo].[tbl_storesMenu] ([Id])
GO
ALTER TABLE [dbo].[tbl_menuTastes]  WITH CHECK ADD FOREIGN KEY([Product])
REFERENCES [dbo].[tbl_storesMenu] ([Id])
GO
ALTER TABLE [dbo].[tbl_productsCategories]  WITH CHECK ADD FOREIGN KEY([Store])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchases]  WITH CHECK ADD FOREIGN KEY([GroupDetails])
REFERENCES [dbo].[tbl_groups] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchases]  WITH CHECK ADD FOREIGN KEY([StoreId])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchases]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_users] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesAdditions]  WITH CHECK ADD FOREIGN KEY([Addition])
REFERENCES [dbo].[tbl_menuAddittions] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesAdditions]  WITH CHECK ADD FOREIGN KEY([Product])
REFERENCES [dbo].[tbl_purchasesProducts] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesAdditionsHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesProducts]  WITH CHECK ADD FOREIGN KEY([Product])
REFERENCES [dbo].[tbl_storesMenu] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesProducts]  WITH CHECK ADD FOREIGN KEY([PurchaseId])
REFERENCES [dbo].[tbl_purchases] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesProductsHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesTastes]  WITH CHECK ADD FOREIGN KEY([Product])
REFERENCES [dbo].[tbl_purchasesProducts] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesTastes]  WITH CHECK ADD FOREIGN KEY([Taste])
REFERENCES [dbo].[tbl_menuTastes] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchasesTastesHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchaseToWorker]  WITH CHECK ADD FOREIGN KEY([PurchaseId])
REFERENCES [dbo].[tbl_purchases] ([Id])
GO
ALTER TABLE [dbo].[tbl_purchaseToWorker]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_users] ([Id])
GO
ALTER TABLE [dbo].[tbl_responses]  WITH CHECK ADD FOREIGN KEY([StroeId])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_responsesHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_storesActivityTime]  WITH CHECK ADD FOREIGN KEY([Store])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_storesMenu]  WITH CHECK ADD FOREIGN KEY([ProductCategory])
REFERENCES [dbo].[tbl_productsCategories] ([Id])
GO
ALTER TABLE [dbo].[tbl_storesMenu]  WITH CHECK ADD FOREIGN KEY([Store])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_users]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[tbl_roles] ([Id])
GO
ALTER TABLE [dbo].[tbl_usersHistory]  WITH CHECK ADD FOREIGN KEY([modifyType])
REFERENCES [dbo].[tbl_modifyTypes] ([Id])
GO
ALTER TABLE [dbo].[tbl_workerToStore]  WITH CHECK ADD FOREIGN KEY([StoreId])
REFERENCES [dbo].[tbl_stores] ([Id])
GO
ALTER TABLE [dbo].[tbl_workerToStore]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_users] ([Id])
GO
USE [master]
GO
ALTER DATABASE [DB_freeQueue] SET  READ_WRITE 
GO
