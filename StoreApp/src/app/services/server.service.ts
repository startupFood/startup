import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root'
})
export class ServerService {

  constructor() {
    if(this.currentUserToken == undefined)
    this.currentUserToken="";
  }

  URL="http://localhost:4588/api/";

  public currentUserToken:string;

  setCurrentUserToken(value)
  {
    this.currentUserToken=value;
  }

/*   get CurrentUserToken()
  {
    return this.currentUserToken;
  }

  set CurrentUserToken(value)
  {
    debugger;
    this.currentUserToken=value;
  } */

  getHeader():HttpHeaders{
    var headers_object = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));
    return headers_object;
  }

}
