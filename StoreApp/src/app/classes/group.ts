export class Group {
    constructor(
        public id:number,
        public storeId:number,
        public groupName:string,
        public groupPassword:string,
        public openingDate:string
        ){}
}
