﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FreeQueueServer.Controllers
{
    [AllowAnonymous]
    public class ResponsesController : ApiController
    {
        // GET: api/Responses
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Responses/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Responses
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Responses/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Responses/5
        public void Delete(int id)
        {
        }
    }
}
