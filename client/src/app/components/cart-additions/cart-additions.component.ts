import { Component, OnInit, Input } from '@angular/core';
import { PurchasesDetailsAddition } from 'src/app/classes/purchases-details-addition';
import { CartService } from 'src/app/services/cart.service';
import { StoreService } from 'src/app/services/store.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart-additions',
  templateUrl: './cart-additions.component.html',
  styleUrls: ['./cart-additions.component.css']
})
export class CartAdditionsComponent implements OnInit {

  constructor(public cartServ:CartService, public storeServ:StoreService ) { }

  @Input() productId:number;
  productAdditions:Array<PurchasesDetailsAddition>;

  ngOnInit() {
    
    this.productAdditions=this.cartServ.getCartDetailsAdditions().filter(a=>a.product==this.productId);    
  }

  getAdditionName(id:number):string
  {
    return this.storeServ.Additions.find(a=>a.id==id).additionName;
  }


}
