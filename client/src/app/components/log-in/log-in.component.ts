import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'src/app/classes/user';
import { StoreService } from 'src/app/services/store.service';
import { CartService } from 'src/app/services/cart.service';
import { MatDialog } from '@angular/material/dialog';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ASTWithSource } from '@angular/compiler';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  user: User = new User(0, "", "", "", "", 0);
  loginBookmark: boolean = true;
  errorMessage = false;
  constructor(private router: Router,
     private usersServ: UsersService,
      private dialog: MatDialog) { }

  ngOnInit() {
  }

  login() {
    this.usersServ.login(this.user);

    //after checking the logion details.
    this.usersServ.showLoginSubject.subscribe(
    data => {
      switch(data)
      {
        case 0:
//correct.
          this.errorMessage = false;break;
          case 1:
//in correct.
            this.errorMessage = true;
            break;
      }
    }
    );
  }

  signUp() {
    this.usersServ.signUp();
    //this.router.navigate(["signup"]);
  }

  asGuest() {
    this.usersServ.loginAsGuest();
  }

  forgot: boolean = false;

  changeBookmark(bookmark: boolean) {
    this.loginBookmark = bookmark;
    //change the classes of the from/to active.
    //login 
    if (bookmark) {
      document.getElementById("loginDiv").setAttribute("class", 'bookmark-active');
      document.getElementById('signupDiv').setAttribute('class', 'bookmark');
      document.getElementById('registration-wizard').setAttribute('class', 'login-wizard');
    }
    //signup
    else {
      this.signUp();
      document.getElementById('loginDiv').setAttribute('class', 'bookmark');
      document.getElementById('signupDiv').setAttribute('class', 'bookmark-active');
      document.getElementById('registration-wizard').setAttribute('class', 'signup-wizard');
    }
  }

  openDialog(): void {

    this.forgot = true;
    const dialogRef = this.dialog.open(ForgotPasswordComponent, {
      disableClose: true,
      width: '300px',
      data: { userId: 0 }
    });

    dialogRef.afterClosed().subscribe();
  }

  /*     dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        location.reload();
      });
    } */



}
