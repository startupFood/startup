﻿using FreeQueueServer.Helpers;
using FreeQueueServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FreeQueueServer.Controllers
{
    [Authorize]
    [RoutePrefix("api/PurchasesProduct")]
    public class PurchasesProductController : ApiController
    {
         DB_freeQueueEntities1 DB = new DB_freeQueueEntities1();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="purchasesProducts"></param>
        /// <returns>List tbl_PurchasesProducts</returns>
        public  tbl_purchasesProducts ConvertPurchasesProductFromDTO(PurchasesProductDTO purchasesProduct)
        {

            return new tbl_purchasesProducts()
            {
                Id = purchasesProduct.id,
                PurchaseId = (purchasesProduct.purchase != null) ? DB.tbl_purchases.FirstOrDefault(tp => tp.Id == purchasesProduct.purchase).Id : 0,
                Product = purchasesProduct.product,
                Price = purchasesProduct.price,
                ProductCount = purchasesProduct.productCount,
                Status = purchasesProduct.status
            };
        }
        // GET: api/PurchasesProduct
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }


        [AllowAnonymous]
        [Route("GetPurchasesProductsByPurchases")]
        public IHttpActionResult Post([FromBody]List<PurchaseDTO> purchases)
        {
            //lock(DB){
                List<tbl_purchases> purchasesList = new List<tbl_purchases>();
                List<tbl_purchasesProducts> purchasesProductsList = new List<tbl_purchasesProducts>();
                //convert the purchasesDto list to tbl_purchases list.
                purchases.ForEach(p => purchasesList.Add(PurchasesController.ConvertPurchaseFromDto(p)));
                //select the purchase products of the purchases to a list.
                purchasesList.ForEach(p => DB.tbl_purchasesProducts.Where(pp => pp.PurchaseId == p.Id).ToList().ForEach(tp => purchasesProductsList.Add(tp)));
                //return the purchases products' list.
                List<PurchasesProductDTO> pprdct = PurchasesProductDTO.ConvertToDTO(purchasesProductsList);
                return Ok(pprdct);
            //}
            
        }

        [AllowAnonymous]
        [Route("GetPurchaseProducts/{purchaseId}")]
        [HttpGet]
        public IHttpActionResult GetPurchaseProducts(int purchaseId)
        {
            return Ok(PurchasesProductDTO.ConvertToDTO(DB.tbl_purchasesProducts.Where(p => p.PurchaseId == purchaseId).ToList()));
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="productsId"></param>
        ///// <returns>List of StoresMenuDTO</returns>
        //[AllowAnonymous]
        //[Route("GetProductsByPurchasesProducts")]
        //[HttpPost]
        //public IHttpActionResult GetProducts([FromBody]int[] productsId)
        //{
        //    return Ok(productsId.Select(pi => this.getProductToPurchasesProduct(pi)).ToList());
        //}

        ///// <summary>
        ///// select store menu object that contains the details of the product.
        ///// </summary>
        ///// <param name="productId"></param>
        ///// <returns>StoresMenuDTO</returns>
        //public StoresMenuDTO getProductToPurchasesProduct(int productId)
        //{
        //    return StoresMenuDTO.ConvertToDTO(DB.tbl_storesMenu.FirstOrDefault(p => p.Id == productId));
        //}

        [AllowAnonymous]
        [Route("AddPurchasesProduct")]
        public IHttpActionResult Post([FromBody]List<PurchasesProductDTO> purchasesProducts)
        {
            List<tbl_purchasesProducts> newPurchasesProducts = new List<tbl_purchasesProducts>();
            purchasesProducts.ForEach(p => {
               newPurchasesProducts.Add( DB.tbl_purchasesProducts.Add(ConvertPurchasesProductFromDTO(p)));
                });
            DB.SaveChanges();
            
            return Ok(PurchasesProductDTO.ConvertToDTO(newPurchasesProducts));
        }



        [AllowAnonymous]
        [Route("updateStatus/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                DB.tbl_purchasesProducts.First(pp => pp.Id == id).Status = true;
                DB.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        // PUT: api/PurchasesProduct/5
        public void Put(int id, [FromBody]string value)
        {
        }
        // DELETE: api/PurchasesProduct/5
        public void Delete(int id)
        {
        }
    }
}
