import { Component, OnInit } from '@angular/core';
import { Store } from 'src/app/classes/store';
import { StoreService } from 'src/app/services/store.service';
import { UsersService } from 'src/app/services/users.service';
import { ServerService } from 'src/app/services/server.service';
import { StoresCategory } from 'src/app/classes/stores-category';
import { Hechshers } from 'src/app/classes/hechshers';

@Component({
  selector: 'app-store-page',
  templateUrl: './store-page.component.html',
  styleUrls: ['./store-page.component.css']
})
export class StorePageComponent implements OnInit {

  thisStore: Store;
  favoritePurchases: boolean = false;

  categoriesList: Array<StoresCategory>;
  hechshersList:Array<Hechshers>;

  constructor(private storeServ: StoreService,
    private usersServ: UsersService,
    private serverServ: ServerService) {
    //we have to do in the previous components:
    //init the storeId in the service with the id
    //call to the function initStore():
    storeServ.initStore(this.storeServ.storeId);
    this.storeServ.storeSubject.subscribe(data => {
      this.thisStore = data;
    })

    // storeServ.getStoreDetails().subscribe(
    //   data=> {this.thisStore=data;
    //     
    //   alert(this.thisStore.img)}

    // )

    //to dispaly the match icons in the header.
    this.serverServ.Component.next(3);

    this.storeServ.getStoresCategories().subscribe(
      data => { this.categoriesList = data }
    );

    
    this.storeServ.getStoresHechshers().subscribe(
      data => { this.hechshersList = data }
    );
  }

  s: string = "/assets/imgs/1/dairyStore.png";
  ngOnInit() {
    if (this.usersServ.getUserId() != 0)
      this.favoritePurchases = true;
  }

  getCategory(categoryNum: number): string {
    var category = this.categoriesList.find(c => c.Id == categoryNum);
    if (category)
      return category.Category;
    return "";
  }
  getHechsher(hechsherNum: number): string {
    var hechshers = this.hechshersList.find(h => h.Id == hechsherNum);
    if (hechshers)
      return hechshers.Hechsher;
    return "";
  }

}
