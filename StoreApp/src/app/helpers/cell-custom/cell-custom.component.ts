import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ManagerService } from 'src/app/services/manager.service';
import { User } from 'src/app/classes/user';
import { MatDialog } from '@angular/material/dialog';
import { ManagerEditWorkerComponent } from 'src/app/components/manager-edit-worker/manager-edit-worker.component';

@Component({
  selector: 'app-cell-custom',
  templateUrl: './cell-custom.component.html',
  styleUrls: ['./cell-custom.component.css']
})
export class CellCustomComponent implements OnInit {
  data: any;
  params: any;
  constructor(private managerServ:ManagerService, private router: Router, public dialog: MatDialog) { }
  
  agInit(params) {
  this.params = params;
  this.data =  params.value;
  }
  
  ngOnInit() {}

  
  
  
  edit() {
    debugger;
    //let user=new User(rowData.id, rowData.userEmail, rowData.userName, rowData.userPassword, rowData.userPhone, rowData.roleId);
    this.router.navigate(["editWorker/"+{user: ""}]);
  }

  delete() {
    let rowData = this.params;
    if(confirm("Are you sure you wanna delete "+rowData.data.userName+" ?")) {
      this.managerServ.deleteUser(rowData.data.id).subscribe(
        data=>{location.reload();},
        err=>{alert(err.error)}
      )
    }
  }
  
  viewRow() {
  let rowData = this.params;
  console.log(rowData);
  }

  

  openDialog():void {
    let rowData = this.params.data;
    var temp:string=rowData.id;
    const dialogRef = this.dialog.open(ManagerEditWorkerComponent, { disableClose: true,
      width: '500px',
      data: {userId:temp}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      //location.reload();
    })
  }
}
