import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ProductsListComponent } from '../components/products-list/products-list.component';
import { StorePageComponent } from '../components/store-page/store-page.component';
import { ProductComponent } from '../components/product/product.component';
import { QuickOrderComponent } from '../components/quick-order/quick-order.component';
import { NotFoundComponent } from '../components/not-found/not-found.component';
import { OrderComponent } from '../components/order/order.component';
import { AdditionsComponent } from '../components/additions/additions.component';
import { TastesComponent } from '../components/tastes/tastes.component';
import { CartComponent } from '../components/cart/cart.component';
import { CategoriesListComponent } from '../components/categories-list/categories-list.component';
import { FavoritePurchasesComponent } from '../components/favorite-purchases/favorite-purchases.component';
import { LogInComponent } from '../components/log-in/log-in.component';
import { SignUpComponent } from '../components/sign-up/sign-up.component';
import { StoresListComponent } from '../components/stores-list/stores-list.component';
import { MainOrderComponent } from '../components/main-order/main-order.component';
import { OrderDetailsComponent } from '../components/order-details/order-details.component';
import { MainComponent } from '../components/main/main.component';
import { StoreMenuComponent } from '../components/store-menu/store-menu.component';
import { StoresComponent } from '../components/stores/stores.component';
import { UserAccountComponent } from '../components/user-account/user-account.component';
import { ProductsService } from '../services/products.service';
import { ProductsComponent } from '../components/products/products.component';
import { OrderSummaryComponent } from '../components/order-summary/order-summary.component';


const routes:Routes = [
  {path:"" , component: LogInComponent },
  {path:"productsList" , component: ProductsListComponent },
  {path:"categoriesList" , component: CategoriesListComponent },
  {path:"product/:id" , component: ProductComponent },
  {path:"quickOrder" , component: QuickOrderComponent },
  {path:"storePage" , component:StorePageComponent },
  // - /:id?
  //{path:"order" , component:OrderComponent },
  {path:"order" , component:MainOrderComponent},
  //{path:"order" , component:OrderDetailsComponent},
  {path:"additions/:productId" , component:AdditionsComponent },
  {path:"tastes/:productId" , component:TastesComponent },
  {path:"cart" , component:CartComponent },
  {path:"favorite" , component:FavoritePurchasesComponent },
  {path:"logIn" , component:LogInComponent },
  {path:"signUp" , component:SignUpComponent },
  {path:"stores", component:StoresComponent},
  {path:"not-found" , component:NotFoundComponent },
  {path:"main"  ,component:MainComponent },
  {path:"menu" , component:StoreMenuComponent},
  {path:"account" , component:UserAccountComponent},
  {path:"products" , component:ProductsComponent},
  {path:"summary" , component:OrderSummaryComponent},
  {path:"**" , component:StoresComponent }
  //{path:"**" , redirectTo:"/not-found" }
  ]

@NgModule({
  imports: [
    CommonModule , RouterModule.forRoot(routes)
  ],
  declarations: []
})




export class RoutingModule {
 }
