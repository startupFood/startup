import { MenuAdditions } from "./menu-additions";
import { MenuTastes } from "./menu-tastes";

export class DataRowMenu {
    constructor(
        public id:number,
        public store:number,
        public productName:string,
        public productCategory:string,
        public productPrice:number,
        public quickProduct:boolean,
        public preperationTime:number,
        public productImage:string,
        public AdditionQuantity:number,
        public barcode:string,
        public additions:Array<MenuAdditions>,
        public tastes:Array<MenuTastes>
        ){}
}
