import { Component, OnInit } from '@angular/core';
import { Store } from 'src/app/classes/store';
import { FormControl } from '@angular/forms';
import { FilesService } from 'src/app/services/files.service';
import { StoreService } from 'src/app/services/store.service';

class ImageSnippet {
  constructor(public src: string, public file: File) {}
}

@Component({
  selector: 'app-store-form',
  templateUrl: './store-form.component.html',
  styleUrls: ['./store-form.component.css']
})
export class StoreFormComponent implements OnInit {

  constructor(private filesServ:FilesService , private storeServ:StoreService) {
    this.storeServ.getStore().subscribe(data=> {
      this.model = data
    });
  }

  model:Store=new Store(0, "", "", "", "", "","", "", false, false, false, false, 0, 0, "");
  banks:string[]=['Yaav', 'Pagi', 'Poalim'];
  categories:string[]=['Meaty', 'Dairy', 'Asian', 'Ice Cream Parlor', 'pizzeria', 'Bakery', 'Coffeehouse'];
  bankControl = new FormControl();
  image:File=null;

  ngOnInit() {
  }

  yourName: string = '';

  upload(files: FileList) {
    const file = files.item(0);
    this.image=file;
    debugger;
    this.image;
    
    //this.filesServ.uploadImage(1, file).subscribe(
    //  res => {/* Place your success actions here */},
    //  error => {/* Place your error actions here */}
    //);
  }

  fileToUpload: File;
  handleFileInput(files: FileList) {
    debugger;
    this.fileToUpload = files.item(0);
    this.filesServ.postStoreImage(this.fileToUpload).subscribe(
      data=>{alert(data);}
    );
  }


  submit()
  {
    
  }

  //banks:Array<string>={""}

}
