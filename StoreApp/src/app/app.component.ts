import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { StoreService } from './services/store.service';
import { WorkerService } from './services/worker.service';
import { User } from './classes/user';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'NoQueues';
  login:boolean = true;

  
  constructor(private router:Router , private loginServ:UsersService , private storeServ:StoreService){
    this.storeServ.getStore().subscribe(data =>{ this.storeServ.Store = data;    });
    this.loginServ.loginFlag.subscribe(
      data => this.login = data
    );
    }
}
