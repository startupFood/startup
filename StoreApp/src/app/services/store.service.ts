import { Injectable, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StoresMenu } from '../classes/stores-menu';
import { Observable, Subject, ReplaySubject , of} from 'rxjs';
import { PurchasesProduct } from '../classes/purchases-product';
import { PurchasesDetailsAddition } from '../classes/purchases-details-addition';
import { PurchaseDetailsTaste } from '../classes/purchase-details-taste';
import { Purchase } from '../classes/purchase';
import { MenuTastes } from '../classes/menu-tastes';
import { ManagerService } from './manager.service';
import { MenuAdditions } from '../classes/menu-additions';
import { ServerService } from './server.service';
import { Group } from '../classes/group';
import { PurchasesToGroup } from '../classes/purchases-to-group';
import { Store } from '../classes/store';
import { store } from '@angular/core/src/render3';
import { FindValueSubscriber } from 'rxjs/internal/operators/find';
import { ObserveOnSubscriber } from 'rxjs/internal/operators/observeOn';
import { WorkerService } from './worker.service';
import { User } from '../classes/user';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

 
    constructor(private httpClient:HttpClient ,
       private managerServ:ManagerService,
        private serverServ:ServerService,
         private workerServ:WorkerService) { }

  URL = "http://localhost:4588/";

  private store:Store;
  private storesOrders:Array<Purchase> = new Array<Purchase>();
  private storesOrdersProducts:Array<PurchasesProduct> = new Array<PurchasesProduct>();
  private storesOrdersDetailsAdditions:Array<PurchasesDetailsAddition> = new Array<PurchasesDetailsAddition>();
  private storesOrdersDetailsTastes:Array<PurchaseDetailsTaste> = new Array<PurchaseDetailsTaste>();
  private groups:Array<Group> = new Array<Group>();
  private purchasesWithOutGroups:Array<Purchase> = new Array<Purchase>();
  


  private storesMenu:Array<StoresMenu> = new Array<StoresMenu>();
  private tastes:Array<MenuTastes> = new Array<MenuTastes>();
  private additions:Array<MenuAdditions> = new Array<MenuAdditions>();
  private groupedPurchases:Array<PurchasesToGroup> = new Array<PurchasesToGroup>();

  private purchasesMode:boolean = false;
  //subjects.
public purchaseSubject = new Subject<boolean>();
  public serverSubject = new Subject<boolean>();

//list of purchase history.
  private purchasesHistory:Array<Purchase> = Array<Purchase>();
  private groupesHistory:Array<Group> = new Array<Group>();

  public storeId:number = 1;

  getStore():Observable<Store>
  {
    return this.httpClient.get<Store>(this.serverServ.URL + "Store/GetStoreDetails/" + this.storeId , {headers:this.serverServ.getHeader()});
  }
  get Store()
  {
    return this.store;
  }

  set Store(value:Store)
  {
    this.store = value;
  }
  
  get StoresOrders()
  { 
    this.sortPurchases();
    return this.storesOrders;
  }

  set StoresOrders(value:Array<Purchase>)
  {
    this.storesOrders = value;
  }

  get StoresOrdersProducts()
  {
    return this.storesOrdersProducts;
  }

  set StoresOrdersProducts(value:Array<PurchasesProduct>)
  {
    this.storesOrdersProducts = value;
  }


  get StoresOrdersDetailsAdditions()
  {
    return this.storesOrdersDetailsAdditions;
  }

  set StoresOrdersDetailsAdditions(value:Array<PurchasesDetailsAddition>)
  {
    this.storesOrdersDetailsAdditions = value;
  }

  get StoresOrdersDetailsTastes()
  {
    return this.storesOrdersDetailsTastes;
  }

  set StoresOrdersDetailsTastes(value:Array<PurchaseDetailsTaste>)
  {
    this.storesOrdersDetailsTastes = value;
  }

  get StoresMenu()
  {
    return this.storesMenu;
  }

  set StoresMenu(value:Array<StoresMenu>)
  {
    this.storesMenu = value;
  }

  set Tastes(value:Array<MenuTastes>)
  {
    this.tastes=value;
  }

  get Tastes()
  {
    return this.tastes;
  }

  get Additions()
  {
    return this.additions;
  }

  set Additions(value:Array<MenuAdditions>)
  {
    this.additions = value;
  }

  get PurchasesMode()
  {
    return this.purchasesMode;
  }

  set PurchasesMode(value:boolean)
  {
    this.purchasesMode = value;
    this.purchaseSubject.next(value);
  }

  set Groups(value:Array<Group>)
  {
    this.groups = value;
  }

  get Groups()
  {
    return this.groups;
  }

  get GroupedPurchases()
  {
    return this.groupedPurchases;
  }

  get PurchasesWithOutGroups()
  {
    return this.purchasesWithOutGroups;
  }

  get PurchasesHistory()
  {
    return this.purchasesHistory;
  }

  set PurchasesHistory(value:Array<Purchase>)
  {
    this.purchasesHistory = value;
  }

  get GroupesHistory()
  {
    return this.groupesHistory;
  }

  set GroupesHistory(value:Array<Group>)
  {
    this.groupesHistory = value;
  }

initStore(storeId:number)
{
  //get the purchases.
  this.getStoreOrders(storeId).subscribe(
    data => {
      if(data.length != 0)
      {
      this.StoresOrders = data;
      //get there products.
      this.getStoresPurchasesProducts(this.StoresOrders).subscribe(
      PurchasesProductsData => {
        this.StoresOrdersProducts = PurchasesProductsData;
        //get there additions.
         this.getStoresProductsAdditions(this.StoresOrdersProducts).subscribe(
          productsAdditionsData =>{ 
            this.storesOrdersDetailsAdditions = productsAdditionsData;
          //get there tastes. 
        this.getStoresProductsTastes(this.storesOrdersProducts).subscribe(
          productsTastesData => { 
            this.StoresOrdersDetailsTastes = productsTastesData ;
            this.managerServ.getStoreTastes().subscribe(tastesData => 
              this.tastes = tastesData );


            this.managerServ.getStoreAdditions().subscribe(additionsData =>
              {
                this.additions = additionsData;
                this.getGroupsByPurchases(this.storesOrders).subscribe(groupsData => 
                  {
                  this.groups = groupsData;
                  this.getWorkers().subscribe(
                    workers =>
                    this.workerServ.Workers = workers
                  );
                  // this.getStore().subscribe(
                  //   storeData => {
                  //     this.store = storeData;
                      this.serverSubject.next(true);

                  //   }
                  // )
                  });
              } );
              this.purchasesMode = false;
            return this.storesOrders;
             
          }
         ); }
        );
      });

    }
    //in case there are no purchases.
    else{
      this.purchasesMode = false;       
                this.getStore().subscribe(
        storeData => {
          this.store = storeData;
          this.getWorkers().subscribe(
            workers =>
            this.workerServ.Workers = workers
          );
          this.serverSubject.next(true);

        })
    }
  });

  return this.StoresOrders;
}

sortPurchases()
{
  let lGroupedPurchases:Array<PurchasesToGroup> = new Array<PurchasesToGroup>();

  if(this.storesOrders.length)
  {
  //the purchase that dont realated to a group stay in the array.
  this.storesOrders.forEach(so =>
    {

      //if this purchase realated to a group.
      
        if(lGroupedPurchases[so.groupId] == undefined)
        {
        
          lGroupedPurchases[so.groupId] = new PurchasesToGroup(so.groupId ,"" , new Array<Purchase>() , 0);
        }
        //push the purchase to the purchases array in the groupId place in the purchasesToGroup array.
        lGroupedPurchases[so.groupId].purchases.push(so);
        
    }
  );

  //push all the purchases that dont realated to group to new array.
  
    this.purchasesWithOutGroups.splice(0 , this.purchasesWithOutGroups.length);

    if(lGroupedPurchases[0]!= undefined)
    lGroupedPurchases[0].purchases.forEach(gp => this.purchasesWithOutGroups.push(gp));
        //split the purchases that dont realated to group from the groupes array.
    lGroupedPurchases.shift();
   //split the undefined objects int he array.
  lGroupedPurchases = lGroupedPurchases.filter(lgp => lgp);
    // lGroupedPurchases.forEach(lgp => {

    // })
  //save all the groupes in the public service.
  this.groupedPurchases = lGroupedPurchases;
  //this.storesOrders.sort((a,b) => a.groupId - b.groupId);
}
}


  getStoreOrders(storeIdentity:number):Observable<Purchase[]>
  {
    return this.httpClient.get<Purchase[]>(this.serverServ.URL + 'Order/GetTodaysOrdersByStore/' + storeIdentity, {headers : this.serverServ.getHeader()});
  }


  getStoresPurchasesProducts(purchases:Array<Purchase>):Observable<PurchasesProduct[]>
  {
    return this.httpClient.post<PurchasesProduct[]>(this.serverServ.URL + "PurchasesProduct/GetPurchasesProductsByPurchases/" , purchases, {headers : this.serverServ.getHeader()});
  }


  getStoresProductsAdditions(purchasesProducts:Array<PurchasesProduct>):Observable<PurchasesDetailsAddition[]>
  {
    return this.httpClient.post<PurchasesDetailsAddition[]>(this.serverServ.URL + 'PurchasesDetailsAdditions/GetPurchasesProductsAdditions/' , purchasesProducts, {headers : this.serverServ.getHeader()});
  }

  getStoresProductsTastes(purchasesProducts:Array<PurchasesProduct>):Observable<PurchaseDetailsTaste[]>
  {
    return this.httpClient.post<PurchaseDetailsTaste[]>(this.serverServ.URL + 'PurchasesDetailsTastes/GetPurchasesProductsTastes/' , purchasesProducts, {headers : this.serverServ.getHeader()});
  }

  // getStoresPorductsMenuTastes(tastesDetails:Array<PurchaseDetailsTaste>):Observable<MenuTastes[]>
  // {
  //   return this.httpClient.post<MenuTastes[]>(this.serverServ.URL + 'Menu/GetMenuTastesByTastesDetails/' , tastesDetails , {headers : this.serverServ.getHeader()});
  // }

  // getStoresPorductsMenuAdditions(additionsDetails:Array<PurchasesDetailsAddition>):Observable<MenuAdditions[]>
  // {
  //   return this.httpClient.post<MenuAdditions[]>(this.serverServ.URL + 'Menu/GetMenuAdditionsByAdditionsDetails/' , additionsDetails , {headers : this.serverServ.getHeader()});
  // }
  //GetMenuAdditionsByTasteDetails
  // getStoreTastes():Observable<MenuTastes[]>
  // {
  //   return this.httpClient.get<MenuTastes[]>(this.URL+"api/Menu/GetTastesByStore/"+this.managerServ.StoreId, {headers : this.serverServ.getHeader()});
  // }

  updatePurchasesProductsStatus(id:number , status:boolean):Observable<boolean>
  {
    debugger;
    return this.httpClient.get<boolean>(this.serverServ.URL + 'PurchasesProduct/updateStatus/' + id  , {headers : this.serverServ.getHeader()});
  }

  updatePurchasesStatus(id:number)
  {
    return this.httpClient.get<boolean>(this.serverServ.URL + 'Purchase/UpdateStatus/' + id , {headers:this.serverServ.getHeader()});
  }


  getGroupsByPurchases(purchases:Array<Purchase>):Observable<Group[]>
  {
    return this.httpClient.post<Group[]>(this.serverServ.URL + 'Group/GetGroupsByPurchases/' , purchases ,{headers : this.serverServ.getHeader()} );
  }

  updateStoreLoad(load:boolean):Observable<boolean>
  {
    return this.httpClient.get<boolean>( this.serverServ.URL + "Store/UpdateStoreLoad/" + load , {headers : this.serverServ.getHeader()});
  }


  getTodaysHistory(storeId:number):Observable<Purchase[]>
  {
    return this.httpClient.get<Purchase[]>( this.serverServ.URL + "Order/GetTodaysHistory/" + storeId , {headers:this.serverServ.getHeader()});
  }


  getPurchasesProductToPurchase(purchaseId:number):Observable<PurchasesProduct[]>
  {
    return this.httpClient.get<PurchasesProduct[]>(this.serverServ.URL + "PurchasesProduct/GetPurchaseProducts/" + purchaseId , {headers:this.serverServ.getHeader()});
  }

  getDetailsAdditionsToPurchaseProduct(purchaseProductId:number):Observable<PurchasesDetailsAddition[]>
  {
   return this.httpClient.get<PurchasesDetailsAddition[]>(this.serverServ.URL + "PurchasesDetailsAdditions/GetProductsAdditionsToProduct/" + purchaseProductId , {headers:this.serverServ.getHeader()});
  }

  getDetailsTastesToPurchaseProduct(purchaseProductId):Observable<PurchaseDetailsTaste>
  {
   return this.httpClient.get<PurchaseDetailsTaste>(this.serverServ.URL + "PurchasesDetailsTastes/GetProductsTastesToProduct/" + purchaseProductId, {headers:this.serverServ.getHeader()});
  }


  
   getWorkers():Observable<User[]>
   {
     return this.httpClient.get<User[]>( this.serverServ.URL + "Worker/getWorkers/" + this.Store.id, {headers:this.serverServ.getHeader()});
   }


}



