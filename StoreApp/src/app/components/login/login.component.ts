import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'src/app/classes/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user:User= new User(0, "", "", "", "", 0);
  constructor(private router:Router, private usersServ:UsersService) { }

  ngOnInit() {
  }

  login()
  {
    this.usersServ.login(this.user);
  }

  signUp()
  {
    this.router.navigate(["signup"]);
    
  }

  initPassword(emailAddress:string)
  {
    this.usersServ.initPassword(emailAddress).subscribe(
      data=>{alert(data)}
    );
  }

  

}
