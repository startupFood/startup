import { Pipe, PipeTransform } from '@angular/core';
import { PurchasesToGroup } from '../classes/purchases-to-group';

@Pipe({
  name: 'historySearch'
})
export class HistorySearchPipe implements PipeTransform {

  transform(value: PurchasesToGroup[], args?: any): any {
    debugger;
    var res = []; 
    for(var i=0; i<value.length ; i++)
    {
      // search by customer name or by customer phone or by receiptTime.
      if(value[i].groupId == 0)
      {
        if(args == "" ||value[i].purchases[i].customerName.indexOf(args)!= -1 || value[i].purchases[i].customerPhone.indexOf(args)!= -1 
        || value[i].purchases[i].receiptTime.indexOf(args)!= -1  )
       res.push(value[i]);
      }
      //search by groupName.
      else {

        if(args == "" || value[i].groupName.indexOf(args) !=-1 || value[i].purchases[0].receiptTime.indexOf(args)!= -1)
        {
          res.push(value[i]);
        }
      //   if(args == "" ||value[i].customerName.indexOf(args)!= -1 || value[i].customerPhone.indexOf(args)!= -1 
      //   || value[i].receiptTime.indexOf(args)!= -1  )
      //  res.push(value[i]);
      }
    }
    return res;
  }

}
