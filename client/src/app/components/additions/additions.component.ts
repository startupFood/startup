import { Component, OnInit, Input } from '@angular/core';
import { MenuAdditions } from 'src/app/classes/menu-additions';
import { StoreService } from 'src/app/services/store.service';
import { StoresMenu } from 'src/app/classes/stores-menu';
import { PurchasesDetailsAddition } from 'src/app/classes/purchases-details-addition';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-additions',
  templateUrl: './additions.component.html',
  styleUrls: ['./additions.component.css']
})
export class AdditionsComponent implements OnInit {

  productsAdditions: Array<MenuAdditions>;
  @Input() productId: number;

  chosenAdditions: Array<number> = new Array<number>();

  constructor(private storeServ: StoreService, private cartServ: CartService) {
  }

  ngOnInit() {
    //select all the additions of the product
    this.storeServ.getProductAdditions(this.productId).subscribe(
      data => {
        this.productsAdditions = new Array<MenuAdditions>();
        data.forEach(adt => this.productsAdditions.push(adt));
      }
    )
  }

  additionChosen(additionId: number): void {

    if (this.chosenAdditions.find(a => a == additionId))
      this.chosenAdditions.splice(this.chosenAdditions.findIndex(a => a == additionId), 1);
    else
      this.chosenAdditions.push(additionId);

    //select the addition from the productsAdditions.
    let productsAddition = this.productsAdditions.find(a => a.id == additionId);

    //    //select the addition from the productsAdditions.
    //    let productsAddition = this.productsAdditions.find(a=> a.id == additionId);
    this.cartServ.additionsChangedSubject.next(new PurchasesDetailsAddition(0, productsAddition.product, productsAddition.id));
    // 
    //emit the addition to the product component.
    this.cartServ.additionsCahnged.emit(new PurchasesDetailsAddition(0, productsAddition.product, productsAddition.id));

  }

  additionsCount() {

    let additionsCount = this.storeServ.Menu.find(m => m.id == this.productId).additionQuantity;

    if (additionsCount > 1) {
      return additionsCount + " additions";
    }
    return additionsCount + " addition";
  }

  isChosen(additionId): boolean {
    if (this.chosenAdditions.find(a => a == additionId))
      return true;
    return false;
  }


  // additionChosen(additionId:number):void
  // {
  //    //select the addition from the productsAdditions.
  //     let productsAddition = this.productsAdditions.find(a=> a.id == additionId);
  //     this.cartServ.additionsChangedSubject.next(new PurchasesDetailsAddition(0 , productsAddition.product , productsAddition.id));
  // }
}
// && a.additionStatus == true