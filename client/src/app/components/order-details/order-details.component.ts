import { Component, OnInit, ɵisBoundToModule__POST_R3__, Output, EventEmitter } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { StoreService } from 'src/app/services/store.service';
import { RecieptTime } from 'src/app/classes/reciept-time';
import { debugOutputAstAsTypeScript } from '@angular/compiler';
import { GroupService } from 'src/app/services/group.service';
import { Group } from 'src/app/classes/group';
import { OrderService } from 'src/app/services/order.service';


@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})



export class OrderDetailsComponent implements OnInit {

  constructor(private cartServ: CartService,
     private storeServ: StoreService,
     private groupServ: GroupService,
      private orderServ: OrderService) {
    //set the group member to show / hide the options that the user that conect to group can edit.
    if (!this.groupServ.GroupManager) {
      let groupId = JSON.parse(sessionStorage.getItem('groupId'));
      if (groupId != null || groupId != 0) {
        this.group = this.groupServ.Groups.find(g => g.id == groupId);
      }
    }

    this.storeServ.getStoreDetails().subscribe(data => {
      this.storeServ.Store = data
    });

    this.orderServ.order.subscribe(data => {
      this.saveOrderDetails();
    });

    //show the details (if he edit them and click on 'next' / 'pre' button).
    this.reservedSeats = this.cartServ.Order.reservedSeats;
    this.deliveryAddress = this.cartServ.Order.deliveryAddress;
  }

  public Day: number = 0;
  public Days: Array<string> = new Array<string>();
  public maxHourNumber: number = 0;
  public maxMinutesNumber: number = 0;
  public MinHourNumber: number = 0;
  public MinMinutesNumber: number = 0;
  public disableMinutes: boolean = false;
  public preMinutes: number = 0;
  public preHours: number = 0;
  public Delivery: boolean = false;
  public recieptTime: RecieptTime = new RecieptTime();
  public reservedSeats: number;
  public deliveryAddress: string;
  public group: Group;

  //
  public PreperationTime = 0

  //true - tomorrow , false - today.
  public DayFlag:boolean = false;

  @Output("get-order") getOrder: EventEmitter<Number> = new EventEmitter();
  @Output("validation") validation: EventEmitter<Boolean> = new EventEmitter();

//******************************** */
  // hours: Array<number>;
  //****************************** */
  todaysHoursArray: Array<number> = new Array<number>();
  tomorrowsHoursArray: Array<number> = new Array<number>();
  minutes: Array<string> = new Array<string>();
  //array of the minutes in hour.
  minutesArray: Array<string> = ['00', '05', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55'];

  minTime: number;
  maxTime: number;


  ngOnInit() {
    this.storeServ.serverSubject.subscribe({
      next: (data) => {
      //   this.checkDay();
      //        setTimeout(this.checkDay(),
      // //          function() { 
      // //          debugger;
      // //   // this.checkDay();
      // //   location.reload();
      // // },
      //  300000);
 this.initPage();
      }
    });

    this.storeServ.initStoresActivityTime();
  }


  initPage()
  {
    let openTime , closeTime , max , maxHour , maxMinutes;
          this.cartServ.getCart().forEach(c => 
            this.PreperationTime += this.storeServ.Menu.find(m => m.id == c.product).preperationTime
            );
            let preperationHour =Number.parseInt( (this.PreperationTime/ 60).toString() ) ;
            let preperationMinutes = this.PreperationTime % 60;
 let minHour = 0;
    let minMinutes = 0;

    let i ,min;
           //today.
           closeTime =  this.storeServ.StoresActivityTimes.find(sat => sat.store == this.storeServ.Store.id && sat.activityDay == new Date().getDay()).endTime; 

            if(
              //before the preration time is before the close time of today!!.
             ( Number.parseInt( closeTime.substring(0,2) ) *60 + Number.parseInt( closeTime.substring(3,5)) - this.PreperationTime ) > 
              new Date().getHours() *60 + new Date().getMinutes() 
           )
            {
               //stores opening time.
            openTime = this.storeServ.StoresActivityTimes.find(sat => sat.store == this.storeServ.Store.id && sat.activityDay == new Date().getDay()).startTime; 
            // closeTime =  this.storeServ.StoresActivityTimes.find(sat => sat.store == this.storeServ.Store.id && sat.activityDay == new Date().getDay()).endTime; 
          
     max = Number.parseInt( closeTime.substring(0,2)) * 60 + Number.parseInt( closeTime.substring(3,5) ) - this.PreperationTime;
     maxHour = Number.parseInt( (max / 60).toString());
     maxMinutes = max % 60;
              this.DayFlag = false;
              //fill the days array.
              this.Days = new Array<string>();
              this.Days.push("היום");
              this.Days.push("מחר");
              if (this.storeServ.Store.storeLoad) {
                alert("the store is load in the next hour, order your meal to another hour!");
                //  this.MinHourNumber = new Date().getHours() + this.preHours + 1 ;
                minHour++;
              }
          debugger;
//calc the minimun time to order.
               min = new Date().getHours() * 60 + new Date().getMinutes() + this.PreperationTime;
              minHour = Number.parseInt( ( min / 60  ).toString());
              minMinutes = min % 60;
              for( i = 0; i <= maxHour - minHour ; i++)
              {
                this.todaysHoursArray.push(minHour + i );
              }
              if(i == 0)
              {
                this.todaysHoursArray.push(minHour);
              }
    
             
            }
    //tomorrow.
            else{
               //stores opening time.
            openTime = this.storeServ.StoresActivityTimes.find(sat => sat.store == this.storeServ.Store.id && sat.activityDay == new Date().getDay() + 1).startTime; 
            closeTime =  this.storeServ.StoresActivityTimes.find(sat => sat.store == this.storeServ.Store.id && sat.activityDay == new Date().getDay() + 1).endTime; 
          
     max = Number.parseInt( closeTime.substring(0,2)) * 60 + Number.parseInt( closeTime.substring(3,5) ) - this.PreperationTime;
     maxHour = Number.parseInt( (max / 60).toString());
      maxMinutes = max % 60;
              this.DayFlag = true;
              //fill the days array.
              this.Days = new Array<string>();
              this.Days.push("מחר");

              //calc the minimun time to order.
min = Number.parseInt( openTime.substring(0,2) ) * 60 +Number.parseInt( openTime.substring(3,5)) + this.PreperationTime;
minHour = Number.parseInt( ( min / 60).toString()  );
minMinutes = min % 60 ;
for( i = 0; i < maxHour - minHour ; i++)
    {
      this.tomorrowsHoursArray.push(minHour + i );
    }
    
    
            }  
            debugger;
             //minutes.
             let endMinutes =11;
             if(new Date().getHours() == maxHour && !this.DayFlag)
             {
              endMinutes = Number.parseInt( (maxMinutes / 5).toString() );
             }
             if(Number.parseInt( ( minMinutes / 5 ).toString() ) == 0)
             {
               this.minutes.push(this.minutesArray[0]);
             }
             for(i = Number.parseInt( ( minMinutes / 5 ).toString() )+1 ; i <= endMinutes ; i++)
             {  
       this.minutes.push(this.minutesArray[i]);
             }

  }

selectDay(day:string)
{
  
    let preperationHour =Number.parseInt( (this.PreperationTime/ 60).toString() ) ;
    let preperationMinutes = this.PreperationTime % 60;
let openTime , closeTime;

        //stores opening time.
        if(day == "היום")
{
        openTime = this.storeServ.StoresActivityTimes.find(sat => sat.store == this.storeServ.Store.id && sat.activityDay == new Date().getDay()).startTime; 
        closeTime =  this.storeServ.StoresActivityTimes.find(sat => sat.store == this.storeServ.Store.id && sat.activityDay == new Date().getDay()).endTime; 
}
else
{
        openTime = this.storeServ.StoresActivityTimes.find(sat => sat.store == this.storeServ.Store.id && sat.activityDay == new Date().getDay() + 1).startTime; 
        closeTime =  this.storeServ.StoresActivityTimes.find(sat => sat.store == this.storeServ.Store.id && sat.activityDay == new Date().getDay() + 1).endTime; 
      
}
        let minHour = 0;
        let minMinutes = 0;
        
    let max = Number.parseInt( closeTime.substring(0,2)) * 60 + Number.parseInt( closeTime.substring(3,5) ) - this.PreperationTime;
    let maxHour = Number.parseInt( (max / 60).toString());
    let maxMinutes = max % 60;
        let i ,min;
  if(day == "היום")
{
  this.DayFlag = false;
  if (this.storeServ.Store.storeLoad) {
    alert("the store is load in the next hour, order your meal to another hour!");
    //  this.MinHourNumber = new Date().getHours() + this.preHours + 1 ;
    minHour++;
  }
//calc the minimun time to order.
  min = new Date().getHours() * 60 + new Date().getMinutes() + this.PreperationTime;
  minHour = Number.parseInt( ( min / 60  ).toString());
  minMinutes = min % 60;
  for( i = 0; i < maxHour - minHour ; i++)
  {
    this.todaysHoursArray.push(minHour + i );
  }

}
else{
  this.DayFlag = true;
  //calc the minimun time to order.
  min = Number.parseInt( openTime.substring(0,2) ) * 60 +Number.parseInt( openTime.substring(3,5)) + this.PreperationTime;
  minHour = Number.parseInt( ( min / 60).toString()  );
  minMinutes = min % 60 ;
for( i = 0; i < maxHour - minHour ; i++)
{
this.tomorrowsHoursArray.push(minHour + i );
}


}

  //minutes.
  let endMinutes =11;
  this.minutes = new Array<string>();
  if(new Date().getHours() == maxHour)
  {
   endMinutes = Number.parseInt( (maxMinutes / 5).toString() );
  }
  if(Number.parseInt( ( minMinutes / 5 ).toString() ) == 0)
  {
    this.minutes.push(this.minutesArray[0]);
  }
  for(i = Number.parseInt( ( minMinutes / 5 ).toString() ) + 1; i <= endMinutes ; i++)
  {  
this.minutes.push(this.minutesArray[i]);
  }
}


selectHour(hour:string)
{
  
  let preperationHour =Number.parseInt( (this.PreperationTime/ 60).toString() ) ;
  let preperationMinutes = this.PreperationTime % 60;
let i , openTime ;
      //stores opening time.
     let closeTime ;
     if(!this.DayFlag)
     {
     closeTime =  this.storeServ.StoresActivityTimes.find(sat => sat.store == this.storeServ.Store.id && sat.activityDay == new Date().getDay()).endTime; 
     }
     else{
       closeTime = this.storeServ.StoresActivityTimes.find(sat => sat.store == this.storeServ.Store.id && sat.activityDay == new Date().getDay() +1).endTime; 
     }
  let minMinutes;

  let max = Number.parseInt( closeTime.substring(0,2)) * 60 + Number.parseInt( closeTime.substring(3,5) ) - this.PreperationTime;
  let maxHour = Number.parseInt( (max / 60).toString());
  let maxMinutes = max % 60;
  //minutes.
  let endMinutes =12;
  this.minutes = new Array<string>();
  //today.
  if(!this.DayFlag)
  {
   minMinutes =( new Date().getHours() *60 + new Date().getMinutes() + this.PreperationTime ) % 60;
   openTime = this.storeServ.StoresActivityTimes.find(sat => sat.store == this.storeServ.Store.id && sat.activityDay == new Date().getDay() ).startTime;

 
//   if( !this.DayFlag && maxHour == Number.parseInt( hour ))
//   {
//    endMinutes = Number.parseInt( (maxMinutes / 5).toString() );
//   }
//   if(Number.parseInt( ( minMinutes / 5 ).toString() ) == 0)
//   {
//     this.minutes.push(this.minutesArray[0]);
//   }
//   for(i = Number.parseInt( ( minMinutes / 5 ).toString() ) + 1; i <= endMinutes ; i++)
//   {  
// this.minutes.push(this.minutesArray[i]);
//   }
if(  maxHour == Number.parseInt( hour ))
{
 endMinutes = Number.parseInt( (maxMinutes / 5).toString() ) +1;
}
for(i = 0 ; i < endMinutes ; i++)
{
if( Number.parseInt(hour) * 60 + Number.parseInt( this.minutesArray[i] ) > 
new Date().getHours() * 60  + new Date().getMinutes() + this.PreperationTime ) 
{
  this.minutes.push(this.minutesArray[i]);
}

}
 }
//tomorrow.
//////////////////
////
else{

  //the open time of tomorrow!!!!.
 openTime = this.storeServ.StoresActivityTimes.find(sat => sat.store == this.storeServ.Store.id && sat.activityDay == new Date().getDay() +1).startTime;
 
minMinutes = (Number.parseInt(openTime.substring(0,2)) * 60  + Number.parseInt(openTime.substring(3,5)) + this.PreperationTime ) % 60;
 
 
   if(  maxHour == Number.parseInt( hour ))
  {
   endMinutes = Number.parseInt( (maxMinutes / 5).toString() ) +1;
  }
 debugger;
  for(i = 0 ; i < endMinutes ; i++)
  {
  if( Number.parseInt(hour) * 60 + Number.parseInt( this.minutesArray[i] ) > 
 ( Number.parseInt(openTime.substring(0,2)) * 60  + Number.parseInt(openTime.substring(3,5)) + this.PreperationTime) ) 
  {
    this.minutes.push(this.minutesArray[i]);
  }
  
  }}
}



  setDelivery(delivery: boolean) {
    this.Delivery = delivery;
    //if delivery
    if (delivery == true && document.getElementById('delivery').getAttribute('checked') == 'true') {
      document.getElementById('onPlace').setAttribute('checked', 'false');
    }
    else if (delivery == false && document.getElementById('delivery').getAttribute('checked') == 'true') {
      document.getElementById('delivery').setAttribute('checked', 'false');
    }
  }

  setMinutes(minutes: string) {
    this.recieptTime.Minutes = minutes;
    this.isValid();
  }


  //save the details in the service and emit the match number to change the component that display.
  saveOrderDetails() {
    //  let deliveryAddress:string = "";

    // if(this.Delivery)
    // {
    //   deliveryAddress = document.getElementById('deliveryAddress').getAttribute('value');
    // }

    this.cartServ.SetOrderDetails(this.reservedSeats, this.recieptTime.getRecieptTime(), this.deliveryAddress);
    this.getOrder.emit(2);
  }

  //called when click the pre button saved the details and emit the match number to change the component that display.
  preComponent() {
    this.cartServ.SetOrderDetails(this.reservedSeats, this.recieptTime.getRecieptTime(), this.deliveryAddress);
    this.getOrder.emit(3);
  }


  isValid() {
    if (this.recieptTime.Day
      && this.recieptTime.Hour
      && this.recieptTime.Minutes
      && this.recieptTime.Day != ""
      && this.recieptTime.Hour != ""
      && this.recieptTime.Minutes != "")
      this.validation.emit(true);
    else
      this.validation.emit(false);
  }


}
